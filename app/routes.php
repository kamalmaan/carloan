<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::controller('/login', 'LoginController');

Route::get('/logout', function(){

if(Auth::user()->check())
    Auth::user()->logout();

if(Auth::admin()->check())
    Auth::admin()->logout();

    return Redirect::to('login');
});
//Route::get('admin/dashboard','AdminHomeController@getIndex');
Route::group(
    array(
        'prefix'=>'admin'
    ),
    function(){
        Route::controller('search', 'AdminSearchController');
        Route::controller('permission', 'AdminPermissionController');
        Route::controller('role', 'AdminRoleController');
    //    Route::controller('modules', 'AdminRoleController@modules');
        Route::controller('filter', 'AdminFilterController');
        Route::controller('branch', 'AdminBranchController');
        Route::controller('co-applicant', 'AdminCoApplicantController');
        Route::controller('search', 'AdminSearchController');
        Route::controller('permanent', 'AdminPermanentController');
        Route::controller('insurance', 'AdminInsuranceController');
        Route::controller('validator', 'AdminValidatorController');
        Route::controller('underwritingapprovedlead', 'AdminUnderwritingApproved');
        Route::controller('temporarylead', 'AdminTemporaryLeadController');
        Route::controller('dailytask', 'AdminDailyTaskController');
        Route::controller('todayschedule', 'AdminTodayScheduleController');
        Route::controller('report', 'AdminReportController');
        Route::controller('insurancecompany', 'AdminInsuranceCompanyController');
        Route::controller('fi', 'AdminFiController');
        Route::controller('approve', 'AdminApproveController');
        Route::controller('dealer', 'AdminDealerController');
        Route::controller('vehicle', 'AdminVehicleController');
        Route::controller('state', 'AdminStateController');
        Route::controller('bank', 'AdminBankController');
        Route::controller('lead', 'AdminLeadController');
        Route::controller('lead_type', 'AdminLeadTypeController');
        Route::controller('product', 'AdminProductController');
        Route::controller('users', 'AdminUserController');
        Route::controller('/', 'AdminHomeController');




    }
);
Route::group(
    array(
        'prefix'=>'user'
    ),
    function(){
        Route::controller('/', 'UserHomeController');

    }


);

