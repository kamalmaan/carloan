<?php
// My common functions
/*function role_permission($permission)
{
	$role_id=Auth::admin()->get()->is_superuser;

    if($role_id==8){
        return true;
    }
	$data=DB::select("SELECT r.role_id, s.is_superuser AS role, p.id AS p_id, p.description AS permission
		FROM permission p
		INNER JOIN role_permission r ON p.id = r.permission_id
		LEFT JOIN is_superusers s ON s.id =$role_id
		WHERE r.role_id =$role_id");

	foreach($data as $row){
		if($row->permission == $permission){
			return true; 
		} 
	}
	return 0;
		
}*/

function role_permission($module_id,$sub_permission)
{

    $role_id=Auth::admin()->get()->is_superuser;
    $permission=ModuleRoles::where('role_id',$role_id)->where('module_id',$module_id)->first();
    //if($permission)
    //return $role_id.$permission->permissions;

    if($permission->permissions==2){
        //Full access
        return true;
    }elseif($permission->permissions==1){
        //View Only
        $sub_permission_db=$permission->sub_permissions;
        $sub_permissions=explode(",",$sub_permission_db);

        $st=0;
        for($i=0;$i<count($sub_permissions);$i++){
            //if permission is found
            if($sub_permissions[$i]==$sub_permission){
                $st=1;
                break;
            }
        }
       //echo $st;
        if($st==0){ // permission-no
            return false;
        }else if($st==1){//permission-yes

            return true;
        }
    }else if($permission->permissions==0){
        return false;
    }
}

function module_permission($module_id)
{

    $role_id=Auth::admin()->get()->is_superuser;
    $permission=ModuleRoles::where('role_id',$role_id)->where('module_id',$module_id)->first();
    //if($permission)
    //return $role_id.$permission->permissions;

    if($permission->permissions==2){
        //Full access
        return true;
    }elseif($permission->permissions==1){
        //View Only
         return true;

    }else if($permission->permissions==0){
        return false;
    }
}

function users(){

    $role_id=Auth::admin()->get()->is_superuser;
    $branch_id=Auth::admin()->get()->branch_id;
    $id=Auth::admin()->get()->id;

    if($role_id==8||$role_id==7||$role_id==6){
            $total_users = User::groupBy('id')->get();
        }elseif($role_id==5){           
            $total_users = User::where('is_superuser','<',[5])->where('branch_id',$branch_id)->orwhere('id',$id)->groupBy('id')->get();
        }elseif($role_id==4){
            $total_users = User::where('is_superuser','<',[4])->where('branch_id',$branch_id)->orwhere('id',$id)->groupBy('id')->get();
        }elseif($role_id==3||$role_id==2){
            $total_users = User::where('id',$id)->groupBy('id')->get();
        }
        $users=array();
        $forward_to=array();
        $lead_to=array();
        foreach ($total_users as $row) {  
           // $users[]=$row->id;
            
            $forward_to[] = Lead::where('forward_to',$row->id)->select('id')->get();
            $lead_to[] = Lead::where('lead_to',$row->id)->select('id')->get();
        }        
        
        $forward_to1=array();
        $lead_to1=array();
        foreach ($forward_to as $use) { 
            foreach ($use as $usert) {             
               $forward_to1[]=$usert->id;
            }
        }
        foreach ($lead_to as $use1) { 
            foreach ($use1 as $usert1) {              
               $lead_to1[]=$usert1->id;
            }
        }

        $users_lead=array_merge($forward_to1,$lead_to1);
        $lead_unique=array_unique($users_lead);
       
        return $lead_unique;
}
/*function access(){
echo "roleid=".$role_id=Auth::admin()->get()->is_superuser;
echo "userid=".Auth::admin()->get()->id;echo "<br>";
echo "roleid=".Auth::admin()->get()->is_superuser;echo "<br>";
echo "branch_id".Auth::admin()->get()->branch_id;echo "<br>";
echo "parentid=".Auth::admin()->get()->parent_id;echo "<br>";
     $data = Session::all();
                echo "<pre>";
                print_r($data);
}*/
