<?php

class District extends Eloquent{

    protected $table = 'districts';
    public function city(){

        return $this->hasMany('City','district_id','id');

    }
    public  function state(){
        return $this->hasOne('State','id','district_id');
    }

}