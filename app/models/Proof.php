<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 23/5/15
 * Time: 10:38 AM
 */
Class Proof extends Eloquent{
    protected $table = 'proofs';

    public function lead(){

        return $this->hasOne('Lead','id','lead_id');
    }
}