<?php
class City extends Eloquent{

    protected $table = 'cities';

    public function district(){

        return $this->hasOne('District','id','district_id');
    }
    public function dealerbranch(){

        return $this->hasMany('DealerBranch','branch_id','id');

    }
    public function state(){

        return $this->hasOne('State','id','state_id');

    }

}