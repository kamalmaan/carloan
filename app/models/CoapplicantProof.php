<?php
class CoapplicantProof Extends Eloquent{
    protected $table = "coapplicant_proofs";

    public function co_applicant(){

        return $this->hasOne('Coapplicant','id','co_applicant_id');

    }
}