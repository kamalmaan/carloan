<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 17/4/15
 * Time: 2:59 PM
 */
class Lead extends Eloquent{
    protected $table = 'leads';

    public function product(){

        return $this->hasOne('Product','id','product_id');
    }
    public function lead_type(){

        return $this->hasOne('LeadType','id','lead_type_id');
    }
    public function lead_from(){

        return $this->hasOne('User','id','lead_from');
    }
    public function lead_to(){

        return $this->hasOne('User','id','lead_to');
    }
    public function forward_to(){

        return $this->hasOne('User','id','forward_to');
    }
    public function branch(){

        return $this->hasOne('Branch','id','branch_id');
    }
    public function proof(){

        return $this->hasOne('Proof','lead_id','id');
    }
    public function asset(){

        return $this->hasOne('AssetDetail','lead_id','id');
    }
    public function reference(){

        return $this->hasOne('Reference','lead_id','id');
    }
    public function vehicle(){
        return $this->hasOne('Vehicle','id','vehicle_id');
    }
    public function decline(){
        return $this->hasOne('Decline','lead_id','id');
    }
    public function dealer()
    {
        return $this->hasOne('Dealer', 'id', 'dealer_id');
    }

    public function bank(){
        return $this->hasMany('Bank', 'id', 'bank_id');
    }

    public function permanent_office_city(){
        return $this->hasOne('City', 'id', 'permanent_office_city_id');
    }

    /* current */
    public function current_office_city(){
        return $this->hasOne('City', 'id', 'current_office_city_id');
    }

    public function permanent_resi_city(){
        return $this->hasOne('City', 'id', 'pemanent_resi_city_id');
    }

    public function current_resi_city(){
        return $this->hasOne('City', 'id', 'current_resi_city_id');
    }
    public function fi_initiation(){
        return $this->hasOne('FiInitiation','lead_id','id');
    }
    public function temporary(){
        return $this->hasOne('TemporaryLead','lead_id','id');
    }
    public function finance(){
        return $this->hasOne('Finance','lead_id','id');
    }
    public function coapplicant(){
        return $this->hasMany('co_applicants');
    }
}