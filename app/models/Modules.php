<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 29/8/15
 * Time: 5:04 PM
 */

class Modules extends Eloquent{

    protected $table = 'modules';

    public function module_roles(){

        return $this->hasOne('ModuleRoles','module_id','module_id');

    }
}
