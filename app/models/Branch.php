<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 17/4/15
 * Time: 3:07 PM
 */
class Branch extends Eloquent{

    protected $table = 'branches';
    protected $guarded = array('id');
    public function city(){
        return $this->hasOne('City','id','branch');
    }
}