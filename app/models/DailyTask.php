<?php
class DailyTask extends Eloquent{
    protected $table = 'daily_tasks';
    public function assign_to(){
        return $this->hasOne('User','id','assign_to');
    }
    public function assign_by(){
        return $this->hasOne('User','id','assign_by');
    }
}