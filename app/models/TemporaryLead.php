<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 4/6/15
 * Time: 3:29 PM
 */
class TemporaryLead extends Eloquent{
    protected $table = 'temporary_leads';

    public function lead(){
        return $this->hasOne('Lead','id','lead_id');
    }
}