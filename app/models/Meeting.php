<?php
class Meeting extends Eloquent{

    protected $table = 'meetings';

    public function lead(){

        return $this->hasOne('User','id','lead_agent_id');

    }
    public function user(){

        return $this->hasOne('User','id','lead_agent_id');

    }
    public function leads(){

        return $this->hasOne('Lead','id','lead_id');

    }

}