<?php
class Dealer extends Eloquent{
    protected $table = 'dealers';

    public function bm(){
        return $this->hasMany('DealerBranch','dealer_id','id');
    }
}