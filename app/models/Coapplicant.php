<?php
class Coapplicant  Extends Eloquent{
    protected $table = "co_applicants";


    public function proof(){
        return $this->hasOne('CoapplicantProof','co_applicant_id','id');
    }
    public function lead(){

        return $this->hasOne('Lead','id','lead_id');

    }


}