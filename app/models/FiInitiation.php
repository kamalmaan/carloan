<?php
class FiInitiation extends Eloquent{
    protected $table = 'fi_initiations';

    public function lead(){
       return $this->hasOne('Lead','id','lead_id');
    }
    public function bank(){
        return $this->hasOne('Bank','id','bank_id');
    }
    public function branch(){
        return $this->hasOne('BankBranch','id','branch_id');
    }
}