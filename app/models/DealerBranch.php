<?php
class DealerBranch extends Eloquent{
    protected $table = 'dealer_branches';

    public function city(){
        return $this->hasOne('City','id','branch_id');
    }
}