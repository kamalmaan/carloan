<?php
class Bank extends Eloquent{

    protected $table = 'banks';

    public function branch(){
        return $this->hasMany('Branch','id','bank_id');
    }

}