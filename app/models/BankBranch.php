<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 17/4/15
 * Time: 3:07 PM
 */
class BankBranch extends Eloquent{

    protected $table = 'bank_branches';

    public function bank(){
        return $this->hasOne('Bank','id','bank_id');
    }
    public function city(){
        return $this->hasMany('City','id','branch');
    }
}