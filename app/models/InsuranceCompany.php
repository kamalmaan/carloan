<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 23/5/15
 * Time: 2:26 PM
 */
Class InsuranceCompany extends Eloquent{
    protected $table = 'insurance_companies';

    public function branch(){

        return $this->hasMany('InsuranceBranch','insurance_id','id');

    }
}