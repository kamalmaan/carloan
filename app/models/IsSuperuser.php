<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 18/4/15
 * Time: 1:30 PM
 */
class IsSuperuser extends Eloquent{

    protected $table = 'is_superusers';

    public function super(){

        return $this->hasOne('User','id','is_superuser');
    }
}