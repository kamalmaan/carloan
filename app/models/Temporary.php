<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 9/7/15
 * Time: 3:42 PM
 */
class Temporary Extends Eloquent{
   protected $table = "temporary_customers";

    public function lead(){

        return $this->hasOne('Lead','id','lead_id');
    }
    public function fi_initiation(){
        return $this->hasOne('FiInitiation','lead_id','lead_id');
    }
    public function temporary_lead(){
        return $this->hasOne('TemporaryLead','lead_id','lead_id');
    }

}