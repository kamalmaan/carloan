<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 27/5/15
 * Time: 3:42 PM
 */
class Decline extends Eloquent{
    protected $table = 'declines';

    public function lead(){

        return $this->hasOne('Lead','id','lead_id');

    }
    public function user(){
        return $this->hasOne('User','id','decline_by');
    }
    /*public function aa(){
    	return $this->hasMany('User','id','decline_by');
    }
    public function abc(){

    	$role_id=Auth::admin()->get()->is_superuser;
        $id=Auth::admin()->get()->id;

    	return DB::table('declines')
        ->join('users', function($join) use ($role_id)
        {
          $join->on('users.id', '=', 'declines.decline_by')
              ->where('users.branch_id', '=',Auth::admin()->get()->branch_id )
              ->where('users.is_superuser','<',$role_id );
              /*->orwhere('users.id',$id);*/
       /* });
       ->get();
	}*/
}