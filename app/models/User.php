<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    public function superuser(){
        return $this->hasMany('IsSuperuser','id','is_superuser');
    }
    public function createdby(){
        return $this->hasMany('IsSuperuser','id','created_by');
    }

    public function branch(){
        return $this->hasMany('Branch','id','branch_id');
    }
    public function city(){
        return $this->hasOne('City','id','branch');
    }
}
