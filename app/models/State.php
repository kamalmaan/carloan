<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 22/5/15
 * Time: 10:31 AM
 */
Class State extends Eloquent{
    protected $table = 'states';

    public function district(){

        return $this->hasMany('District','state_id','id');

    }
    public function city(){

        return $this->hasMany('City','state_id','id');

    }

}