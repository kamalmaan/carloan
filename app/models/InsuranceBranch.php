<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 23/5/15
 * Time: 2:28 PM
 */
Class InsuranceBranch extends Eloquent{

    protected $table ='insurance_branches';

    public function insurance(){

        return $this->hasOne('InsuranceCompany','id','insurance_id');
    }
}