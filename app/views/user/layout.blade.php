<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="keywords" content="admin template, admin dashboard, inbox templte, calendar template, form validation">
    <meta name="author" content="DazeinCreative">
    <meta name="description" content="ORB - Powerfull and Massive Admin Dashboard Template with tonns of useful features">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SBE</title>
    {{ HTML::style('css/styles.css') }}

    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('favicon.ico') }}" />

    {{ HTML::script('js/vendors/modernizr/modernizr.custom.js') }}

</head>

<body>

<!--Smooth Scroll-->
<div class="smooth-overflow">
    <!--Navigation-->
    <nav class="main-header clearfix" role="navigation"> <a class="navbar-brand" href="{{ URL::to('user') }}"><span class="text-blue">SBE</span></a>

        <!--Search-->


        <!--Navigation Itself-->

        <div class="navbar-content">

            <!--Sidebar Toggler-->
            <a href="#" class="btn btn-default left-toggler"><i class="fa fa-bars"></i></a>
            <!--Right Userbar Toggler-->
            <a href="#" class="btn btn-user right-toggler pull-right"><i class="entypo-vcard"></i> <span class="logged-as hidden-xs">Logged as</span><span class="logged-as-name hidden-xs">Anton Durant</span></a>

        </div>
    </nav>


            <!--/Navigation-->

            <!--MainWrapper-->
            <div class="main-wrap">

                <!--OffCanvas Menu -->
                <aside class="user-menu">

                    <!-- Tabs -->
                    <div class="tabs-offcanvas">

                        <div class="tab-content">

                            <!--User Primary Panel-->
                            <div class="tab-pane active" id="userbar-one">
                                <div class="main-info">
                                    <div class="user-img"><img src="http://placehold.it/150x150" alt="User Picture" /></div>
                                    <h1>{{ Auth::user()->id()  }}<small>Administrator</small></h1>
                                </div>
                                <div class="list-group">  <a href="{{ URL::to('user/profile')}}" class="list-group-item"><i class="fa fa-cog"></i>Settings</a>
                                    <div class="empthy"></div>
                                    <a href="#" data-toggle="modal" class="list-group-item lockme"><i class="fa fa-lock"></i> Lock</a>
                                    <div class="empthy"></div>
                                    <a data-toggle="modal" href="{{ URL::to('logout') }}" class="list-group-item goaway"><i class="fa fa-power-off"></i> Sign Out</a>
                                </div>
                            </div>

                        </div>

                    </div>

                    <!-- /tabs -->

                </aside>
                <!-- /Offcanvas user menu-->

                <!--Main Menu-->
                <div class="responsive-admin-menu">
                    <div class="responsive-menu">ORB
                        <div class="menuicon"><i class="fa fa-angle-down"></i></div>
                    </div>
                    <ul id="menu">
                        <li><a class="active submenu" href="#" title="Dashboard" data-id="dash-sub"><i class="entypo-briefcase"></i><span> Dashboard</span></a></li>
                        <!--        <li><a href="admin-inbox.html" title="Inbox"><i class="entypo-inbox"></i><span> Inbox <span class="badge">32</span></span></a></li>-->
                        <li><a class="submenu" href="#" title="Widgets" data-id="widgets-sub"><i class="fa fa-user"></i><span>Admin</span></a></li>
                        <!--        <li><a href="#" class="submenu" data-id="tables-sub" title="Tables"><i class="fa fa-table"></i><span> Tables</span></a>-->
                        <!--          <!-- Tables Sub-Menu -->
                        <!--          <ul id="tables-sub" class="accordion">-->
                        <!--            <li><a href="admin-tables.html" title="Timeline"><i class="fa fa-table"></i><span> Tables</span></a></li>-->
                        <!--            <li><a href="admin-datatables.html" title="Profile"><i class="entypo-database"></i><span> Datatables</span></a></li>-->
                        <!--          </ul>-->
                        <!--        </li>-->
                        <!--        <li><a class="submenu" href="#" data-id="other-sub" title="Other Contents"><i class="fa fa-th"></i><span> Forms</span></a>-->
                        <!--          <!-- Forms Sub-Menu -->
                        <!--          <ul id="other-sub" class="accordion">-->
                        <!--            <li><a href="admin-forms.html" title="Forms"><i class="fa fa-th"></i><span> Forms</span></a></li>-->
                        <!--            <li><a href="admin-forms-bootstrap.html" title="Bootstrap Forms"><i class="fa fa-table"></i><span> Bootstrap Forms</span></a></li>-->
                        <!--            <li><a href="admin-forms-plugins.html" title="Forms Plugins"><i class="fa fa-puzzle-piece"></i><span> Form Plugins</span></a></li>-->
                        <!--            <li><a href="admin-forms-validation.html" title="Forms Validation"><i class="fa fa-check-circle"></i><span> Form Validation</span></a></li>-->
                        <!--            <li><a href="admin-forms-wizard.html" title="Forms Wizard"><i class="fa fa-magic"></i><span> Form Wizard</span></a></li>-->
                        <!--          </ul>-->
                        <!--        <li> <a class="submenu" href="#" title="Graph &amp; Charts" data-id="graph-sub"><i class="entypo-chart-area"></i><span> Graph &amp; Charts</span></a> -->
                        <!--          <!-- Graph and Charts Menu -->
                        <!--          <ul id="graph-sub" class="accordion">-->
                        <!--            <li><a href="admin-chart-morris.html" title="Video Gallery"><i class="entypo-chart-bar"></i><span> Morris Charts</span></a></li>-->
                        <!--            <li><a href="admin-chart-flot.html" title="Photo Gallery"><i class="entypo-chart-line"></i><span> Flot Charts</span></a></li>-->
                        <!--            <li><a href="admin-chart-others.html" title="Photo Gallery"><i class="entypo-chart-pie"></i><span> Others Charts</span></a></li>-->
                        <!--          </ul>-->
                        <!--        <li> <a class="submenu" href="#" data-id="interface-sub" title="Interface"><i class="entypo-palette"></i><span> Interface</span></a> -->
                        <!--          <!-- Interface Sub-Menu -->
                        <!--          <ul id="interface-sub" class="accordion">-->
                        <!--            <li><a href="admin-typography.html" title="Typography"><i class="entypo-language"></i><span> Typography</span></a></li>-->
                        <!--            <li><a href="admin-nestable.html" title="Nestable"><i class="entypo-list-add"></i><span> Nestable</span></a></li>-->
                        <!--            <li><a href="admin-tree-view.html" title="Tree View"><i class="entypo-flow-tree"></i><span> Tree View</span></a></li>-->
                        <!--            <li><a href="admin-animation.html" title="Animation"><i class="entypo-cd"></i><span> Animation</span></a></li>-->
                        <!--            <li><a href="admin-components.html" title="Components"><i class="entypo-traffic-cone"></i><span> Components</span></a></li>-->
                        <!--            <li><a href="admin-elements.html" title="Elements"><i class="entypo-archive"></i><span> Elements</span></a></li>-->
                        <!--            <li><a href="admin-buttons.html" title="Items List"><i class="entypo-list"></i><span> Buttons</span></a></li>-->
                        <!--            <li><a href="#" data-id="fonts-sub" title="Icon Fonts" class="submenu"><i class="fa fa-heart"></i><span> Icons</span></a>-->
                        <!--              <ul id="fonts-sub">-->
                        <!--                <li><a href="admin-font-awesome.html" title="Awesome Font"><i class="fa fa-flag"></i><span> Awesome</span></a></li>-->
                        <!--                <li><a href="admin-font-glyphicons.html" title="Glyphicons"><i class="entypo-cancel-circled"></i><span> Glyphicons</span></a></li>-->
                        <!--                <li><a href="admin-font-entypo.html" title="Entypo"><i class="entypo-flag"></i><span> Entypo</span></a></li>-->
                        <!--              </ul>-->
                        <!--            </li>-->
                        <!--          </ul>-->
                        <!--        </li>-->
                        <!--        <li> <a class="submenu" href="#" data-id="pages-sub" title="Pages"><i class="entypo-book"></i><span> Pages</span></a> -->
                        <!--          <!-- Pages Sub-Menu -->
                        <!--          <ul id="pages-sub">-->
                        <!--            <li><a href="admin-megamenu.html" title="MegaMenu"><i class="entypo-menu"></i><span> MegaMenu <span class="badge">New!</span></span></a></li>-->
                        <!--            <li><a href="admin-timeline.html" title="Timeline"><i class="entypo-chart-bar"></i><span> Timeline</span></a></li>-->
                        <!--            <li><a href="admin-profile.html" title="Profile"><i class="entypo-user"></i><span> Profile</span></a></li>-->
                        <!--            <li><a href="admin-gallery.html" title="Gallery"><i class="entypo-picture"></i><span> Gallery</span></a></li>-->
                        <!--            <li><a href="admin-user-list.html" title="Items List"><i class="entypo-list"></i><span> Items List</span></a></li>-->
                        <!--            <li><a href="admin-pricelists.html" title="Pricelists"><i class="entypo-tag"></i><span> Pricelists</span></a></li>-->
                        <!--            <li><a href="admin-forum.html" title="Forum"><i class="entypo-comment"></i><span> Forum</span></a></li>-->
                        <!--            <li><a href="admin-chat.html" title="Chat"><i class="entypo-chat"></i><span> Chat</span></a></li>-->
                        <!--            <li><a href="admin-404.html" title="404"><i class="entypo-window"></i><span> 404 Error</span></a></li>-->
                        <!--            <li><a href="admin-404.html" title="500"><i class="entypo-window"></i><span> 500 Error</span></a></li>-->
                        <!--            <li><a href="admin-login.html" title="Login"><i class="entypo-vcard"></i><span> Login Page</span></a></li>-->
                        <!--            <li><a href="admin-clear.html" title="Clear Page"><i class="entypo-monitor"></i><span> Carte blanche</span></a></li>-->
                        <!--            <li><a href="admin-events.html" title="Events Calendar"><i class="entypo-calendar"></i><span> Calendar</span></a></li>-->
                        <!--            <li><a href="admin-search.html" title="Search Results"><i class="entypo-search"></i><span> Search Results</span></a></li>-->
                        <!--            <li><a href="admin-invoice.html" title="Invoice"><i class="entypo-box"></i><span> Invoice</span></a></li>-->
                        <!--          </ul>-->
                        <!--        </li>-->
                        <!--        <li> <a class="submenu" href="#" data-id="maps-sub" title="Maps"><i class="entypo-map"></i><span> Maps</span></a> -->
                        <!--          <!-- Maps Sub-Menu -->
                        <!--          <ul id="maps-sub">-->
                        <!--            <li><a href="admin-maps-google.html" title="Google Maps"><i class="entypo-location"></i><span> Google Maps</span></a></li>-->
                        <!--            <li><a href="admin-maps-vector.html" title="Vector Maps"><i class="entypo-compass"></i><span> Vector Maps</span></a></li>-->
                        <!--          </ul>-->
                        <!--        </li>-->
                        <!--        -->
                        <!--        <!-- Other Contents Menu -->
                        <!--        <li><a href="#" title="Front End"><i class="entypo-cog"></i><span> Front End <span class="badge">Soon</span></span></a></li>-->
                    </ul>
                </div>
                <!--/MainMenu-->

                <div class="content-wrapper">
                    <!--Content Wrapper--><!--Horisontal Dropdown-->
                    <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
                        <div class="cbp-hsinner">
                            <ul class="cbp-hsmenu">
                                <li> <a href="#"><span class="icon-bar"></span></a>
                                    <ul class="cbp-hssubmenu">
                                        <li><a href="#">
                                                <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                                                    <p class="sparkle-name">project income</p>
                                                    <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                                </div>
                                            </a></li>
                                        <li><a href="#">
                                                <div class="sparkle-dropdown"><span class="linechart">5,6,7,9,9,5,3,2,9,4,6,7</span>
                                                    <p class="sparkle-name">site traffic</p>
                                                    <p class="sparkle-amount">122541 <i class="fa fa-chevron-circle-down"></i></p>
                                                </div>
                                            </a></li>
                                        <li><a href="#">
                                                <div class="sparkle-dropdown"><span class="simpleline">9,6,7,9,3,5,7,2,1,8,6,7</span>
                                                    <p class="sparkle-name">Processes</p>
                                                    <p class="sparkle-amount">890 <i class="fa fa-plus-circle"></i></p>
                                                </div>
                                            </a></li>
                                        <li><a href="#">
                                                <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                                                    <p class="sparkle-name">orders</p>
                                                    <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                                </div>
                                            </a></li>
                                        <li><a href="#">
                                                <div class="sparkle-dropdown"><span class="piechart">1,2,3</span>
                                                    <p class="sparkle-name">active/new</p>
                                                    <p class="sparkle-amount">500/200 <i class="fa fa-chevron-circle-up"></i></p>
                                                </div>
                                            </a></li>
                                        <li><a href="#">
                                                <div class="sparkle-dropdown"><span class="stackedbar">3:6,2:8,8:4,5:8,3:6,9:4,8:1,5:7,4:8,9:5,3:5</span>
                                                    <p class="sparkle-name">fault/success</p>
                                                    <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                                </div>
                                            </a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>

                    <!--Breadcrumb-->



                    @yield('content')


                    <div class="row">
                        <div class="aimru">
                            Powered by <a href=" http://www.aimru.com/ " target="blank">Aimru</a> &copy; 2015
                        </div>
                    </div>
                </div>
                <!-- /Widgets Row End Grid-->
            </div>
            <!-- / Content Wrapper -->

    <!--/MainWrapper-->
</div>
<!--/Smooth Scroll-->

<!-- scroll top -->
<div class="scroll-top-wrapper hidden-xs"> <i class="fa fa-angle-up"></i> </div>
<!-- /scroll top -->

<!--Modals-->

<!--Power Widgets Modal-->
<div class="modal" id="delete-widget">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">
                <p>Are you sure to delete this widget?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="trigger-deletewidget-reset">Cancel</button>
                <button type="button" class="btn btn-primary" id="trigger-deletewidget">Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--Sign Out Dialog Modal-->
<div class="modal" id="signout">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">Are You Sure Want To Sign Out?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="yesigo">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--Lock Screen Dialog Modal-->
<div class="modal" id="lockscreen">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">Are You Sure Want To Lock Screen?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="yesilock">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--Scripts-->
<!--GMaps-->
{{ HTML::script('http://maps.google.com/maps/api/js?sensor=false') }}


<!--JQuery-->
{{ HTML::script('js/vendors/jquery/jquery.min.js') }}
{{ HTML::script('js/vendors/jquery/jquery-ui.min.js') }}


<!--GMap-->
{{ HTML::script('js/vendors/gmap/jquery.gmap.js') }}


<!--EasyPieChart-->
{{ HTML::script('js/vendors/easing/jquery.easing.1.3.min.js') }}
{{ HTML::script('js/vendors/easypie/jquery.easypiechart.min.js') }}


<!--Fullscreen-->
{{ HTML::script('js/vendors/fullscreen/screenfull.min.js') }}


<!--NanoScroller-->
{{ HTML::script('js/vendors/nanoscroller/jquery.nanoscroller.min.js') }}


<!--Sparkline-->
{{ HTML::script('js/vendors/sparkline/jquery.sparkline.min.js') }}


<!--Horizontal Dropdown-->
{{ HTML::script('js/vendors/horisontal/cbpHorizontalSlideOutMenu.js') }}
{{ HTML::script('js/vendors/classie/classie.js') }}

<!--Forms-->
{{ HTML::script('js/vendors/forms/jquery.form.min.js') }}
{{ HTML::script('js/vendors/forms/jquery.validate.min.js') }}
{{ HTML::script('js/vendors/forms/jquery.maskedinput.min.js') }}
{{ HTML::script('js/vendors/jquery-steps/jquery.steps.min.js') }}

    <!--PowerWidgets-->
{{ HTML::script('js/vendors/powerwidgets/powerwidgets.min.js') }}


<!--Morris Chart-->
{{ HTML::script('js/vendors/raphael/raphael-min.js') }}
{{ HTML::script('js/vendors/morris/morris.min.js') }}


<!--FlotChart-->
{{ HTML::script('js/vendors/flotchart/jquery.flot.min.js') }}
{{ HTML::script('js/vendors/flotchart/jquery.flot.resize.min.js') }}
{{ HTML::script('js/vendors/flotchart/jquery.flot.axislabels.js') }}
{{ HTML::script('js/vendors/flotchart/jquery.flot-tooltip.js') }}


<!--Chart.js-->
{{ HTML::script('js/vendors/chartjs/chart.min.js') }}


<!--Calendar-->
{{ HTML::script('js/vendors/fullcalendar/fullcalendar.min.js') }}
{{ HTML::script('js/vendors/fullcalendar/gcal.js') }}

<!--Bootstrap-->
{{ HTML::script('js/vendors/bootstrap/bootstrap.min.js') }}


<!--Vector Map-->
{{ HTML::script('js/vendors/vector-map/jquery.vmap.min.js') }}
{{ HTML::script('js/vendors/vector-map/jquery.vmap.sampledata.js') }}
{{ HTML::script('js/vendors/vector-map/jquery.vmap.world.js') }}


<!--ToDo-->
{{ HTML::script('js/vendors/todos/todos.js') }}


<!--SkyCons-->
{{ HTML::script('js/vendors/skycons/skycons.js') }}

<script>
    var icons = new Skycons({"color": "#fff"}),
        list  = [
            "clear-day", "clear-night", "partly-cloudy-day",
            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
            "fog"
        ],
        i;

    for(i = list.length; i--; )
        icons.set(list[i], list[i]);

    icons.play();
</script>

<!--Main App-->
{{ HTML::script('js/scripts.js') }}


<!--/Scripts-->

</body>
</html>

