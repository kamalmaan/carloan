﻿
      @extends('user/layout')
      @section('content')
      <div class="breadcrumb clearfix">
          <ul>
              <li><a href="{{ URL::to('user') }}"><i class="fa fa-home"></i></a></li>
              <li><a href="{{ URL::to('user') }}">Dashboard</a></li>
              <li class="active">Data</li>
          </ul>
      </div>

      <!--Jumbotron-->
      <div class="jumbotron jumbotron6">
        <h1>Welcome To <strong>User</strong></h1>
        <p class="lead">This User panel is developed to get all the updates and to add admins, branch officers, agents and customers related to Shree Balaji Enterprices.
            Only authorized members can access this panel by entering Username and Password provided by the authoruity. </p>
        <p><a href="http://reedoc.com/meshcron/bank/" target="blank" class="btn margin-top btn-danger">Shree Balaji Enterprises</a></p>
      </div>
      <!--/Jumbotron--> 
      
      <!-- Widget Row Start grid -->
      <div class="row" id="powerwidgets">
        <div class="col-md-12 bootstrap-grid"> 
          
          <!-- New widget -->
          
         <!-- <div class="powerwidget powerwidget-as-portlet-white" id="serverstatuz-indexpage2">
            <header>
              <h2>User Record</h2>
            </header>
            <div>

              <div class="inner-spacer">
                <div class="row">
                  <!--Row-->

                  <!--<div class="col-md-12">

                    <div class="row margin-bottom-10px">
                      <ul class="countries-demo" id="choices">
                        <li class="col-md-2 col-sm-4">
                          <h3>UK <span class="label bg-marine"><i class="fa fa-caret-up"></i> 22%</span></h3>
                          <div class="orb-form">
                            <label class="toggle" for="iduk">
                              <input name="uk" checked id="iduk" type="checkbox">
                              Uncheck to hide
                              </input>
                              <i></i></label>
                          </div>
                        </li>
                        <li class="col-md-2 col-sm-4">
                          <h3>Japan <span class="label bg-red"><i class="fa fa-caret-down"></i> 5%</span></h3>
                          <div class="orb-form">
                            <label class="toggle" for="idjapan">
                              <input name="japan" checked id="idjapan" type="checkbox">
                              Uncheck to hide
                              </input>
                              <i></i></label>
                          </div>
                        </li>
                        <li class="col-md-2 col-sm-4">
                          <h3>USA <span class="label bg-marine"><i class="fa fa-caret-up"></i> 16%</span></h3>
                          <div class="orb-form">
                            <label class="toggle" for="idusa">
                              <input name="usa" checked id="idusa" type="checkbox">
                              Uncheck to hide
                              </input>
                              <i></i></label>
                          </div>
                        </li>
                        <li class="col-md-2 col-sm-4">
                          <h3>Russia <span class="label bg-marine"><i class="fa fa-caret-up"></i> 7%</span></h3>
                          <div class="orb-form">
                            <label class="toggle" for="idrussia">
                              <input name="russia" checked id="idrussia" type="checkbox">
                              Uncheck to hide
                              </input>
                              <i></i></label>
                          </div>
                        </li>
                        <li class="col-md-2 col-sm-4">
                          <h3>China <span class="label bg-marine"><i class="fa fa-caret-up"></i> 1%</span></h3>
                          <div class="orb-form">
                            <label class="toggle" for="idchina">
                              <input name="china" checked id="idchina" type="checkbox">
                              Uncheck to hide
                              </input>
                              <i></i></label>
                          </div>
                        </li>
                        <li class="col-md-2 col-sm-4">
                          <h3>Others <span class="label bg-red"><i class="fa fa-caret-down"></i> 13%</span></h3>
                          <div class="orb-form">
                            <label class="toggle" for="idothers">
                              <input name="others" checked id="idothers" type="checkbox">
                              Uncheck to hide
                              </input>
                              <i></i></label>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <!--/Row--

                  </div>
                  <!--/Col-md-12--
                </div>
                <!--/Row--

              </div>
            </div>
          </div>-->
          
          <!-- End .powerwidget --> 
        </div>

      </div>
        <!-- /Inner Row Col-md-6 -->
      @stop