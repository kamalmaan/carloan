<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="keywords" content="admin template, admin dashboard, inbox templte, calendar template, form validation">
    <meta name="author" content="DazeinCreative">
    <meta name="description" content="ORB - Powerfull and Massive Admin Dashboard Template with tonns of useful features">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SBE</title>
    {{ HTML::style('css/styles.css') }}

    {{ HTML::style('css/jquery.datetimepicker.css') }}
    {{ HTML::style('css/jquery.tagsinput.css') }}
    {{ HTML::style('jquerytypeahead/dist/jquery.typeahead.min.css') }}
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
    {{ HTML::style('css/jquery.multiselect.css') }}
    <!-- {{ HTML::style('css/jquery.multiselect.filter.css') }} -->
     

    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('favicon.ico') }}" />
    {{--<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.10.4/css/jquery.dataTables.min.css">--}}


    {{ HTML::script('js/vendors/jquery/jquery.min.js') }}
    {{ HTML::script('js/vendors/jquery/jquery-ui.min.js') }}

    {{ HTML::script('js/vendors/modernizr/modernizr.custom.js') }}
    {{ HTML::script('js/typeahead.bundle.js') }}
    <!--{{ HTML::script('jquerytypeahead/dist/jquery.typeahead.js') }}-->
    {{ HTML::script('typeahead/bloodhound.js') }}
    {{ HTML::script('js/jquery.tagsinput.js') }}

    {{ HTML::script('js/jquery.multiselect.js') }}
    {{ HTML::script('js/jquery.multiselect.filter.js') }}
    <!-- {{ HTML::script('js/jquery.multiselect.filter.min.js') }} -->
    
    <!-- {{ HTML::script('js/jquery.multiselect.min.js') }} -->
    <!--Forms--> 
   <!--  {{ HTML::script('js/vendors/forms/jquery.form.min.js') }}
    {{ HTML::script('js/vendors/forms/jquery.validate.min.js') }}
    {{ HTML::script('js/vendors/forms/jquery.maskedinput.min.js') }}
    {{ HTML::script('js/vendors/jquery-steps/jquery.steps.min.js') }} -->

<style>
    .dataTables_wrapper .dataTables_paginate {
        color: #333;
    }
    .dataTables_wrapper .dataTables_paginate {
        float: right;
        text-align: right;
        padding-top: 0.25em;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button {
        box-sizing: border-box;
        display: inline-block;
        min-width: 1.5em;
        padding: 0.5em 1em;
        margin-left: 2px;
        text-align: center;
        text-decoration: none !important;
        cursor: pointer;
        color: #333 !important;
        border: 1px solid transparent;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
        color: white !important;
        border: 1px solid #111;
        background-color: #858689;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #585858), color-stop(100%, #111));
        background: -webkit-linear-gradient(top, #585858 0%, #111 100%);
        background: -moz-linear-gradient(top, #585858 0%, #111 100%);
        background: -ms-linear-gradient(top, #585858 0%, #111 100%);
        background: -o-linear-gradient(top, #585858 0%, #111 100%);
        background: linear-gradient(to bottom, #585858 0%, #111 100%);
    }
</style>

</head>

<body>

<!--Smooth Scroll-->
<div class="smooth-overflow">
<!--Navigation-->
<nav class="main-header clearfix" role="navigation"> <a class="navbar-brand" href="{{ URL::to('admin') }}"><span class="text-blue">SBE</span></a>

    <!--Search-->



    <div class="site-search" id="remote">
        <form action="#" id="inline-search">
            <i class="fa fa-search"></i>
            <input type="text" placeholder="Search" class="typeahead" name="">
        </form>
    </div>
    <script>
        $(document).ready(function(){


        var bestPictures = new Bloodhound({

            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: "{{ URL::to('admin/search?q=%QUERY') }}",
            remote: {
                //url: '../data/films/queries/%QUERY.json',
                url: "{{ URL::to('admin/search?q=%QUERY') }}",
                wildcard: '%QUERY'
            }
        });


        $('#remote .typeahead').typeahead(null, {

            display: 'value',
            source: bestPictures
        });


        });

    </script>
    <!--Navigation Itself-->

    <div class="navbar-content">

        <!--Sidebar Toggler-->
        <a href="#" class="btn btn-default left-toggler"><i class="fa fa-bars"></i></a>
        <!--Right Userbar Toggler-->
        <a href="#" class="btn btn-user right-toggler pull-right"><i class="entypo-vcard"></i> <span class="logged-as hidden-xs">Logged as</span><span class="logged-as-name hidden-xs">Anton Durant</span></a>



    </div>
</nav>
<!--/Navigation-->

<!--MainWrapper-->
<div class="main-wrap">

    <!--OffCanvas Menu -->
    <aside class="user-menu">

        <!-- Tabs -->
        <div class="tabs-offcanvas">

            <div class="tab-content">

                <!--User Primary Panel-->
                <div class="tab-pane active" id="userbar-one">
                    <div class="main-info">
                        <?php $img = Auth::admin()->get()->profile_pic ?>
                        <div class="user-img"><img src='{{ asset("upload/$img") }}' height="130" width="150" alt="User Picture" /></div>
                        <h1>{{ ucfirst(Auth::admin()->get()->first_name)." ".ucfirst(Auth::admin()->get()->last_name)}} <small>Adminintrator</small></h1>
                    </div>
                    <div class="list-group">  <a href="{{ URL::to('admin/profile')}}" class="list-group-item"><i class="fa fa-cog"></i>Settings</a>
                        <div class="empthy"></div>
                        <a href="#" data-toggle="modal" class="list-group-item lockme"><i class="fa fa-lock"></i> Lock</a>
                        <div class="empthy"></div>
                        <a data-toggle="modal" href="{{ URL::to('logout') }}" class="list-group-item goaway"><i class="fa fa-power-off"></i> Sign Out</a>
                    </div>
                </div>

            </div>

        </div>

        <!-- /tabs -->

    </aside>
    <!-- /Offcanvas user menu-->

    <!--Main Menu-->



    <div class="responsive-admin-menu">
        <div class="responsive-menu">ORB
            <div class="menuicon"><i class="fa fa-angle-down"></i></div>
        </div>
        <ul id="menu">
           @if(!isset($title))
               {{$title = '';}}
            @endif

                <?php
                   $col = array('LEAD_COMPONENTS','LEAD_TYPE','BANK','STATE','VECHICLE','PRODUCT','DEALER');
                   $ins = array('INSURANCECOMPANY','INSURANCE');
               ?>


            <li><a @if($title == 'DASHBOARD') class="active" @endif href="{{ URL::to('admin') }}" title="Dashboard" data-id="dash-sub"><i class="entypo-briefcase"></i><span> Dashboard</span></a></li>
            @if(module_permission('1'))
            <li><a @if($title == 'USER') class="active" @endif href="{{ URL::to('admin/users') }}" title="Users" ><i class="entypo-folder"></i><span>User</span></a></li>
            @endif
            <!--        <li><a href="admin-inbox.html" title="Inbox"><i class="entypo-inbox"></i><span> Inbox <span class="badge">32</span></span></a></li>-->
            <!--<li><a class="submenu" href="{{ URL::to('admin/') }}" title="Admin" data-id="widgets-sub"><i class="fa fa-user"></i><span>Admin</span></a></li>-->
            @if(module_permission('2'))
            <li><a @if($title == 'LEAD') class="active" @endif href="{{ URL::to('admin/lead') }}" title="Lead" data-id="widgets-sub"><i class="entypo-folder"></i><span>Lead</span></a></li>
            @endif
            @if(module_permission('3'))
            <li><a @if($title == 'DECLINE') class="active" @endif href="{{ URL::to('admin/lead/decline') }}" title="Lead" data-id="widgets-sub"><i class="entypo-folder"></i><span>Declined Leads</span></a></li>
            @endif
            @if(module_permission('4'))
            <li><a class="submenu @if(in_array($title,$col))active @endif" href="#" data-id="other-sub" title="Components of Lead"><i class="fa fa-th"></i><span>Components of Lead</span></a>
                <ul id="other-sub" class="accordion">
                    @if(role_permission('4','component_leadtype'))
                    <li><a @if($title== 'LEAD_TYPE') class="active" @endif href="{{ URL::to('admin/lead_type') }}" title="Lead Type"><i class="entypo-folder"></i><span>Lead Type</span></a></li>
                    @endif
                    @if(role_permission('4','component_bank'))
                    <li><a @if($title=='BANK') class="active" @endif href="{{ URL::to('admin/bank') }}" title="Banks" ><i class="entypo-folder"></i><span>Bank</span></a></li>
                    @endif
                    @if(role_permission('4','component_state'))
                    <li><a @if($title=='STATE') class="active" @endif href="{{ URL::to('admin/state') }}" title="Districts" ><i class="entypo-folder"></i><span>State</span></a></li>
                    @endif
                    @if(role_permission('4','component_vehicle'))
                    <li><a @if($title=='VECHICLE') class="active" @endif href="{{ URL::to('admin/vehicle') }}" title="Vehicles" ><i class="entypo-folder"></i><span>Vehicle</span></a></li>
                    @endif
                    @if(role_permission('4','component_product'))
                    <li><a @if($title=='PRODUCT') class="active" @endif href="{{ URL::to('admin/product') }}" title="Products" ><i class="entypo-folder"></i><span>Product</span></a></li>
                    @endif
                    @if(role_permission('4','component_dealer'))
                    <li><a @if($title=='DEALER') class="active" @endif href="{{ URL::to('admin/dealer') }}" title="Dealers" ><i class="entypo-folder"></i><span>Dealer</span></a></li>
                    @endif
                </ul>
            </li>
            @endif
            @if(module_permission('5'))
            <li><a class="submenu  @if(in_array($title,$ins))active @endif" href="#" data-id="insurance" title="Insurance"><i class="fa fa-th"></i><span>Insurance</span></a>
                <ul id="insurance" class="accordion">
                    @if(role_permission('5','insurance_insurancecompany'))
                    <li><a @if($title== 'INSURANCECOMPANY') class="active" @endif href="{{ URL::to('admin/insurancecompany') }}" title="Dealers" ><i class="entypo-folder"></i><span>Insurance Company</span></a></li>
                    @endif
                    @if(role_permission('5','insurance_insurance'))
                    <li><a @if($title== 'INSURANCE') class="active" @endif  href="{{ URL::to('admin/insurance') }}" title="Dealers" ><i class="entypo-folder"></i><span>Insurance</span></a></li>
                    @endif
                </ul>
            </li>
            @endif
            @if(module_permission('6'))
            <li><a @if($title == 'FI') class="active" @endif href="{{ URL::to('admin/fi') }}" title="FI Status" ><i class="entypo-folder"></i><span>FI Status</span></a></li>
           @endif
            @if(module_permission('7'))
            <li><a @if($title == 'UNDERWRITING') class="active" @endif href="{{ URL::to('admin/fi/underwriting') }}" title="Under Writing" ><i class="entypo-folder"></i><span>Under Writing</span></a></li>
            @endif
            @if(module_permission('8'))
            <li><a @if($title == 'UNDERWRITINGAPPROVED') class="active" @endif href="{{ URL::to('admin/underwritingapprovedlead') }}" title="Under Writing Approved" ><i class="entypo-folder"></i><span>Under Writing Approved</span></a></li>
            @endif
            @if(module_permission('9'))
            <li><a @if($title == 'UNDERWRITINGDECLINE') class="active" @endif href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}" title="Temporary Lead" ><i class="entypo-folder"></i><span>Under Approved Decline </span></a></li>
            @endif
            @if(module_permission('10'))
            <li><a @if($title == 'TEMPLEAD') class="active" @endif href="{{ URL::to('admin/temporarylead') }}" title="Temporary Lead" ><i class="entypo-folder"></i><span>Temporary Customer</span></a></li>
            @endif
            @if(module_permission('11'))
            <li><a @if($title == 'TEMPDECLINE') class="active" @endif href="{{ URL::to('admin/temporarylead/temporarydecline') }}" title="Temporary Lead" ><i class="entypo-folder"></i><span>Temporary Decline Customer</span></a></li>
            @endif
            @if(module_permission('12'))
            <li><a @if($title == 'PERMANENT') class="active" @endif href="{{ URL::to('admin/permanent') }}" title="Permanent Lead" ><i class="entypo-folder"></i><span>Permanent Customer</span></a></li>
            @endif
            @if(module_permission('13'))
            <li><a @if($title == 'TODAYSCHEDULE') class="active" @endif href="{{ URL::to('admin/todayschedule') }}" title="Today Schedule" ><i class="entypo-folder"></i><span>Today Schedule</span></a></li>
            @endif
            @if(module_permission('14'))
            <li><a @if($title == 'DAILYTASK') class="active" @endif href="{{ URL::to('admin/dailytask') }}" title="Daily Task" ><i class="entypo-folder"></i><span>Daily Task</span></a></li>
            @endif
            @if(Auth::admin()->get()->is_superuser=='8')
            <li><a @if($title == 'ROLE') class="active" @endif href="{{ URL::to('admin/role/index') }}" title="ROLE" ><i class="entypo-folder"></i><span>Role</span></a></li>
            @endif
            @if(module_permission('15'))
            <li><a @if($title == 'BRANCH') class="active" @endif href="{{ URL::to('admin/branch') }}" title="Branch" ><i class="entypo-folder"></i><span>Branch</span></a> </li>
            @endif
            @if(module_permission('16'))
            <li><a @if($title == 'SEARCH') class="active" @endif href="{{ URL::to('admin/search') }}" title="SEARCH" ><i class="entypo-folder"></i><span>SEARCH</span></a> </li>
            @endif
                    <!--<li> <a class="submenu" href="#" title="Graph &amp; Charts" data-id="graph-sub"><i class="entypo-chart-area"></i><span> Graph &amp; Charts</span></a>
                      <!-- Graph and Charts Menu
                      <ul id="graph-sub" class="accordion">
                        <li><a href="admin-chart-morris.html" title="Video Gallery"><i class="entypo-chart-bar"></i><span> Morris Charts</span></a></li>
                        <li><a href="admin-chart-flot.html" title="Photo Gallery"><i class="entypo-chart-line"></i><span> Flot Charts</span></a></li>
                        <li><a href="admin-chart-others.html" title="Photo Gallery"><i class="entypo-chart-pie"></i><span> Others Charts</span></a></li>
                      </ul>
                    </li>
                    <li> <a class="submenu" href="#" data-id="interface-sub" title="Interface"><i class="entypo-palette"></i><span> Interface</span></a>
                      <!-- Interface Sub-Menu
                      <ul id="interface-sub" class="accordion">
                        <li><a href="admin-typography.html" title="Typography"><i class="entypo-language"></i><span> Typography</span></a></li>
                        <li><a href="admin-nestable.html" title="Nestable"><i class="entypo-list-add"></i><span> Nestable</span></a></li>
                        <li><a href="admin-tree-view.html" title="Tree View"><i class="entypo-flow-tree"></i><span> Tree View</span></a></li>
                        <li><a href="admin-animation.html" title="Animation"><i class="entypo-cd"></i><span> Animation</span></a></li>
                        <li><a href="admin-components.html" title="Components"><i class="entypo-traffic-cone"></i><span> Components</span></a></li>
                        <li><a href="admin-elements.html" title="Elements"><i class="entypo-archive"></i><span> Elements</span></a></li>
                        <li><a href="admin-buttons.html" title="Items List"><i class="entypo-list"></i><span> Buttons</span></a></li>
                        <li><a href="#" data-id="fonts-sub" title="Icon Fonts" class="submenu"><i class="fa fa-heart"></i><span> Icons</span></a>
                          <ul id="fonts-sub">
                            <li><a href="admin-font-awesome.html" title="Awesome Font"><i class="fa fa-flag"></i><span> Awesome</span></a></li>
                            <li><a href="admin-font-glyphicons.html" title="Glyphicons"><i class="entypo-cancel-circled"></i><span> Glyphicons</span></a></li>
                            <li><a href="admin-font-entypo.html" title="Entypo"><i class="entypo-flag"></i><span> Entypo</span></a></li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li> <a class="submenu" href="#" data-id="pages-sub" title="Pages"><i class="entypo-book"></i><span> Pages</span></a>
                      <!-- Pages Sub-Menu
                      <ul id="pages-sub">
                        <li><a href="admin-megamenu.html" title="MegaMenu"><i class="entypo-menu"></i><span> MegaMenu <span class="badge">New!</span></span></a></li>
                        <li><a href="admin-timeline.html" title="Timeline"><i class="entypo-chart-bar"></i><span> Timeline</span></a></li>
                        <li><a href="admin-profile.html" title="Profile"><i class="entypo-user"></i><span> Profile</span></a></li>
                        <li><a href="admin-gallery.html" title="Gallery"><i class="entypo-picture"></i><span> Gallery</span></a></li>
                        <li><a href="admin-user-list.html" title="Items List"><i class="entypo-list"></i><span> Items List</span></a></li>
                        <li><a href="admin-pricelists.html" title="Pricelists"><i class="entypo-tag"></i><span> Pricelists</span></a></li>
                        <li><a href="admin-forum.html" title="Forum"><i class="entypo-comment"></i><span> Forum</span></a></li>
                        <li><a href="admin-chat.html" title="Chat"><i class="entypo-chat"></i><span> Chat</span></a></li>
                        <li><a href="admin-404.html" title="404"><i class="entypo-window"></i><span> 404 Error</span></a></li>
                        <li><a href="admin-404.html" title="500"><i class="entypo-window"></i><span> 500 Error</span></a></li>
                        <li><a href="admin-login.html" title="Login"><i class="entypo-vcard"></i><span> Login Page</span></a></li>
                        <li><a href="admin-clear.html" title="Clear Page"><i class="entypo-monitor"></i><span> Carte blanche</span></a></li>
                        <li><a href="admin-events.html" title="Events Calendar"><i class="entypo-calendar"></i><span> Calendar</span></a></li>
                        <li><a href="admin-search.html" title="Search Results"><i class="entypo-search"></i><span> Search Results</span></a></li>
                        <li><a href="admin-invoice.html" title="Invoice"><i class="entypo-box"></i><span> Invoice</span></a></li>
                      </ul>
                    </li>
                    <li> <a class="submenu" href="#" data-id="maps-sub" title="Maps"><i class="entypo-map"></i><span> Maps</span></a>
                      <!-- Maps Sub-Menu
                      <ul id="maps-sub">
                        <li><a href="admin-maps-google.html" title="Google Maps"><i class="entypo-location"></i><span> Google Maps</span></a></li>
                        <li><a href="admin-maps-vector.html" title="Vector Maps"><i class="entypo-compass"></i><span> Vector Maps</span></a></li>
                      </ul>
                    </li>

                    <!-- Other Contents Menu
                    <li><a href="#" title="Front End"><i class="entypo-cog"></i><span> Front End <span class="badge">Soon</span></span></a></li>-->
        </ul>
    </div>
    <!--/MainMenu-->

    <div class="content-wrapper">
        <!--Content Wrapper--><!--Horisontal Dropdown-->
        <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
            <div class="cbp-hsinner">
                <ul class="cbp-hsmenu">
                    <li> <a href="#"><span class="icon-bar"></span></a>
                        <ul class="cbp-hssubmenu">
                            <li><a href="#">
                                    <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                                        <p class="sparkle-name">project income</p>
                                        <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                    </div>
                                </a></li>
                            <li><a href="#">
                                    <div class="sparkle-dropdown"><span class="linechart">5,6,7,9,9,5,3,2,9,4,6,7</span>
                                        <p class="sparkle-name">site traffic</p>
                                        <p class="sparkle-amount">122541 <i class="fa fa-chevron-circle-down"></i></p>
                                    </div>
                                </a></li>
                            <li><a href="#">
                                    <div class="sparkle-dropdown"><span class="simpleline">9,6,7,9,3,5,7,2,1,8,6,7</span>
                                        <p class="sparkle-name">Processes</p>
                                        <p class="sparkle-amount">890 <i class="fa fa-plus-circle"></i></p>
                                    </div>
                                </a></li>
                            <li><a href="#">
                                    <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                                        <p class="sparkle-name">orders</p>
                                        <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                    </div>
                                </a></li>
                            <li><a href="#">
                                    <div class="sparkle-dropdown"><span class="piechart">1,2,3</span>
                                        <p class="sparkle-name">active/new</p>
                                        <p class="sparkle-amount">500/200 <i class="fa fa-chevron-circle-up"></i></p>
                                    </div>
                                </a></li>
                            <li><a href="#">
                                    <div class="sparkle-dropdown"><span class="stackedbar">3:6,2:8,8:4,5:8,3:6,9:4,8:1,5:7,4:8,9:5,3:5</span>
                                        <p class="sparkle-name">fault/success</p>
                                        <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                    </div>
                                </a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>

        <!--Breadcrumb-->



        @yield('content')


        <div class="row">
            <div class="aimru">
                Powered by <a href=" http://www.aimru.com/ " target="blank">Aimru</a> &copy; 2015
            </div>
        </div>

    </div>

    <!-- /Widgets Row End Grid-->


</div>
<!-- / Content Wrapper -->

<!--/MainWrapper-->

</div>
<!--/Smooth Scroll-->

<!-- scroll top -->
<div class="scroll-top-wrapper hidden-xs"><i class="fa fa-angle-up"></i> </div>
<!-- /scroll top -->

<!--Modals-->

<!--Power Widgets Modal-->
<div class="modal" id="delete-widget">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">
                <p>Are you sure to delete this widget?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="trigger-deletewidget-reset">Cancel</button>
                <button type="button" class="btn btn-primary" id="trigger-deletewidget">Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--Sign Out Dialog Modal-->
<div class="modal" id="signout">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">Are You Sure Want To Sign Out?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="yesigo">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--Lock Screen Dialog Modal-->
<div class="modal" id="lockscreen">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center" id="confirm-delete"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-ok" id="yesilock">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal" id="message">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-envelop"></i> </div>
            <div class="modal-body text-center" id="message_text"></div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-default" id="yesilock">Ok</button>-->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<div class="modal" id="proof_message">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i style="background:#fdc153" class="fa fa-4x fa-exclamation-triangle"></i> </div>
            <div class="modal-body text-center " id="proof-text"></div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--- confirm Modal --->

<div class="modal" id="confirm_message">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i style="background:#fdc153" class="fa fa-4x fa-exclamation-triangle"></i> </div>
            <div class="modal-body text-center " id="confirm_text"></div>
            <div class="modal-footer">

                <button type="button" class="btn btn-info" data-dismiss="modal" id="yesconfirm">OK</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="noconfirm">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->

<!--Scripts-->
<!--GMaps-->
{{ HTML::script('http://maps.google.com/maps/api/js?sensor=false') }}


<!--JQuery-->



<!--GMap-->
{{ HTML::script('js/vendors/gmap/jquery.gmap.js') }}


<!--EasyPieChart-->
{{ HTML::script('js/vendors/easing/jquery.easing.1.3.min.js') }}
{{ HTML::script('js/vendors/easypie/jquery.easypiechart.min.js') }}


<!--Fullscreen-->
{{ HTML::script('js/vendors/fullscreen/screenfull.min.js') }}


<!--NanoScroller-->
{{ HTML::script('js/vendors/nanoscroller/jquery.nanoscroller.min.js') }}


<!--Sparkline-->
{{ HTML::script('js/vendors/sparkline/jquery.sparkline.min.js') }}


<!--Horizontal Dropdown-->
{{ HTML::script('js/vendors/horisontal/cbpHorizontalSlideOutMenu.js') }}
{{ HTML::script('js/vendors/classie/classie.js') }}

<!--Forms-->
{{ HTML::script('js/vendors/forms/jquery.form.min.js') }}
{{ HTML::script('js/vendors/forms/jquery.validate.min.js') }}
{{ HTML::script('js/vendors/forms/jquery.maskedinput.min.js') }}
{{ HTML::script('js/vendors/jquery-steps/jquery.steps.min.js') }}

<!-- Moment --->

{{ HTML::script('js/moment/moment.js') }}


<!--PowerWidgets-->
{{ HTML::script('js/vendors/powerwidgets/powerwidgets.min.js') }}


<!--Morris Chart-->
{{ HTML::script('js/vendors/raphael/raphael-min.js') }}
{{ HTML::script('js/vendors/morris/morris.min.js') }}


<!--FlotChart-->
{{ HTML::script('js/vendors/flotchart/jquery.flot.min.js') }}
{{ HTML::script('js/vendors/flotchart/jquery.flot.resize.min.js') }}
{{ HTML::script('js/vendors/flotchart/jquery.flot.axislabels.js') }}
{{ HTML::script('js/vendors/flotchart/jquery.flot-tooltip.js') }}


<!--Chart.js-->
{{ HTML::script('js/vendors/chartjs/chart.min.js') }}


<!--Calendar-->
//{{ HTML::script('js/vendors/fullcalendar/fullcalendar.min.js') }}
//{{ HTML::script('js/vendors/fullcalendar/gcal.js') }}
{{ HTML::script('js/jquery.datetimepicker.js') }}

<!--Bootstrap-->
{{ HTML::script('js/vendors/bootstrap/bootstrap.min.js') }}

<!--Datatables-->
{{ HTML::script('js/vendors/datatables/jquery.dataTables.min.js') }}
{{ HTML::script('js/vendors/datatables/jquery.dataTables-bootstrap.js') }}
{{ HTML::script('js/vendors/datatables/dataTables.colVis.js') }}
{{ HTML::script('js/vendors/datatables/colvis.extras.js') }}

<!--Vector Map-->
{{ HTML::script('js/vendors/vector-map/jquery.vmap.min.js') }}
{{ HTML::script('js/vendors/vector-map/jquery.vmap.sampledata.js') }}
{{ HTML::script('js/vendors/vector-map/jquery.vmap.world.js') }}


<!--ToDo-->
{{ HTML::script('js/vendors/todos/todos.js') }}


<!--SkyCons-->
{{ HTML::script('js/vendors/skycons/skycons.js') }}

<!--form master/ form valiadtion-->

{{ HTML::script('validation/lib/jquery.mockjax.js') }}
{{ HTML::script('validation/lib/jquery.form.js') }}
{{ HTML::script('validation/dist/jquery.validate.js') }}
{{ HTML::script('validation/dist/additional-methods.js') }}



<script>
    var icons = new Skycons({"color": "#fff"}),
        list  = [
            "clear-day", "clear-night", "partly-cloudy-day",
            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
            "fog"
        ],
        i;

    for(i = list.length; i--; )
        icons.set(list[i], list[i]);

    icons.play();
</script>

<!--Main App-->
{{ HTML::script('js/scripts.js') }}


<script>

    $( document ).ready(function() {


        $(".close-meeting").click(function(){
            var id = $(this).attr("id");
            //alert(id);
            $('#confirm-delete').text("Are you Sure to delete meeeting ?");
            $('#lockscreen').modal('show');
            $('#lockscreen').on('show.bs.modal', function(e) {

            });
            $('#lockscreen').find('.modal-footer .btn-ok').on('click', function(){

                $.ajax({
                    method:"GET",
                    data:{id:id},
                    url:'{{ URL::to("admin/lead/delleadtimeline") }}',
                    success: function(data){

                        location.reload();
                        /*data = JSON.parse(data);

                        var oldLocation = location;
                        console.log(location.state);
                        console.log(oldLocation.href+"#meetings");
                        var numberOfEntries = window.history.length;
                        console.log(numberOfEntries);

                        window.history.replaceState({}, "meetings", "#meetings");*/

                    }


            }
                );
            });

        });

    });

</script>





<!--/Scripts-->

</body>
</html>
