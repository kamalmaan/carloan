@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/lead') }}">Lead</a></li>
        <li class="active">Asset Deatail</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">


    <a @if(empty($id)) href='{{ URL::to("admin/approve/index/$lead->id") }}' @endif @if(!empty($id)) href='{{ URL::to("admin/approve/index/$id->id") }}' @endif >
        <button type="button" class="btn btn-info">Personal Detail</button>
    </a>
    <a @if(empty($id)) href='{{ URL::to("admin/approve/asset/$lead->id") }}' @endif @if(!empty($id)) href='{{ URL::to("admin/approve/asset/$id->id") }}' @endif >
        <button class="btn btn-info">Add Asset Details</button>
    </a>
    <a href='#'>
        <button type="button" class="btn btn-success">Refrence Datail</button>
    </a>
    @if(empty($id))
        @if(!empty($lead->temporary()->first()->lead_id))
            @if(!empty($lead->temporary()->first()->los_number))
                @if($lead->temporary()->first()->decline == 1)

                    <a @if(empty($id)) href='{{ URL::to("admin/approve/finance/$lead->id")}}' @endif @if(!empty($id)) href='{{ URL::to("admin/approve/finance/$id->id")}}' @endif >
                        <button type="button" class="btn btn-info">Financial Data</button>
                    </a>

                @endif
            @endif
        @endif
    @endif
    @if(!empty($id))
        @if(!empty($id->temporary()->first()->lead_id))
            @if(!empty($id->temporary()->first()->los_number))
                @if($id->temporary()->first()->decline == 1)

                    <a @if(empty($id)) href='{{ URL::to("admin/approve/finance/$lead->id")}}' @endif @if(!empty($id)) href='{{ URL::to("admin/approve/finance/$id->id")}}' @endif >
                        <button type="button" class="btn btn-info">Financial Data</button>
                    </a>

                @endif
            @endif
        @endif
    @endif
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


<div class="col-md-12 bootstrap-grid">


<div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">

<div data-widget-editbutton="false" id="registration-form-validation-widget"
     class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
<header role="heading">
    <h2>
        <Lead></Lead>
        <small>You Can Add Your Lead Type</small>
    </h2>
    <div class="powerwidget-ctrls" role="menu"><a class="button-icon powerwidget-delete-btn" href="#"><i
                class="fa fa-times-circle"></i></a> <a class="button-icon powerwidget-fullscreen-btn" href="#"><i
                class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i
                class="fa fa-chevron-circle-up "></i></a></div>
    <span class="powerwidget-loader"></span></header>
<div class="inner-spacer" role="content">
<form class="orb-form" id="registration-form" @if(empty($id)) action="{{ URL::to('admin/approve/reference/'.$lead->id)}}" @endif @if(!empty($id))  action="{{ URL::to('admin/approve/reference/'.$id->id)}}" @endif method="post"
      novalidate="novalidate" enctype="multipart/form-data">
<header></header>




<div id="dynamic">
        @if(empty($references))
            <fieldset>
                <input type="hidden" name="id[0]" id="" placeholder="Reference Name" value="">
                <div class="row">
                    <section class="  form-group col col-4">
                        <strong class="text text-dark-blue">Reference Name</strong>
                        <label class="input">
                            <i class="icon-append fa fa-phone"></i>
                            <input type="text" name="name[0]" id="" placeholder="Reference Name" >
                            <b class="tooltip tooltip-bottom-right">Needed to enter current Reference Name</b>

                        </label>
                    </section>
                    <section class="  form-group  col col-4">
                        <strong class="text text-dark-blue">Reference Relation</strong>
                        <label class="input">
                            <i class="icon-append fa fa-phone"></i>
                            <input type="text" name="relation[0]" id=""
                                   placeholder="Reference Relation" >
                            <b class="tooltip tooltip-bottom-right">Enter the relation with reference</b>

                        </label>
                    </section>
                    <section class="  form-group  col col-4">
                        <strong class="text text-dark-blue">Reference Address(House and Street Number)</strong>
                        <label class="input">
                            <i class="icon-append fa fa-phone"></i>
                            <input type="text" name="address_street[0]" id=""
                                   placeholder="Address(House and Street Number)" >
                            <b class="tooltip tooltip-bottom-right">Enter the Reference Address(House and Street Number)</b>

                        </label>
                    </section>


                </div>
                <div class="row">
                    <section class="  form-group col col-4">
                        <strong class="text text-dark-blue">Reference City</strong>
                        <label class="select">
                            <select name="city_id[0]" class="city" id="city_id_0">
                                <option value="">--Please Select City Name--</option>

                                @foreach($city as $cities) {
                                <option value="{{ $cities->id }}" >{{ $cities->city}}</option>
                                }
                                @endforeach
                            </select>

                        </label>
                    </section>
                    <section class="  form-group col col-4">
                        <strong class="text text-dark-blue">Reference District</strong>
                        <label class="input">
                            <i class="icon-append fa fa-road"></i>
                            <input type="text" name="district_id[0]" readonly id="district_id_0"
                                   placeholder="Reference District" >
                            <b class="tooltip tooltip-bottom-right">Needed to enter Reference District</b>
                        </label>
                    </section>
                    <section class=" form-group col col-4">
                        <strong class="text text-dark-blue">Reference State</strong>
                        <label class="input">
                            <i class="icon-append fa fa-road"></i>
                            <input type="text" name="state_id[0]" readonly id="state_id_0"
                                   placeholder="Reference District" >
                            <b class="tooltip tooltip-bottom-right">Needed to enter Reference State</b>
                        </label>
                    </section>

                </div>
                <div class="row">
                    <section class=" form-group col col-4">
                        <strong class="text text-dark-blue">Reference Phone</strong>
                        <label class="input">
                            <i class="icon-append fa fa-phone"></i>
                            <input type="number" name="phone[0]" id=""
                                   placeholder="Current Office Phone" >
                            <b class="tooltip tooltip-bottom-right">Needed to enter Reference Phone Number</b>

                        </label>
                    </section>
                    <section class=" form-group col col-4">
                        <strong class="text text-dark-blue">Reference Mobile</strong>
                        <label class="input">
                            <i class="icon-append fa fa-phone"></i>
                            <input type="number" name="mobile[0]" id=""
                                   placeholder="Reference Mobile" >
                            <b class="tooltip tooltip-bottom-right">Needed to enter Reference Mobile</b>

                        </label>
                    </section>
                    <section class=" form-group col col-4">
                        <strong class="text text-dark-blue">Reference Alternate Mobile</strong>
                        <label class="input">
                            <i class="icon-append fa fa-phone"></i>
                            <input type="number" name="alternate_mobile[0]" id=""
                                   placeholder="Current Office Reference Mobile Number" >
                            <b class="tooltip tooltip-bottom-right">Needed to enter Reference Mobile Number</b>

                        </label>
                    </section>

                </div>
                <div class="row">
                    <section class=" form-group col col-4">
                        <strong class="text text-dark-blue">Pin Code</strong>
                        <label class="input">
                            <i class="icon-append fa fa-road"></i>
                            <input type="number" name="pincode[0]" id="" placeholder="Current Office Pin Code" >
                            <b class="tooltip tooltip-bottom-right">Needed to enter PinCode Number
                            </b>
                        </label>
                    </section>
                    <section class=" form-group col col-4">
                        <strong class="text text-dark-blue">STD Code</strong>
                        <label class="input">
                            <i class="icon-append fa fa-phone"></i>
                            <input type="number" name="std_code[0]" id="" placeholder="STD Code">
                            <b class="tooltip tooltip-bottom-right">Needed to enter STD Code
                            </b>
                        </label>
                    </section>
                    <section class=" form-group col col-4">
                        <strong class="text text-dark-blue">Reference Email</strong>
                        <label class="input">
                            <i class="icon-append fa fa-phone"></i>
                            <input type="email" name="email[0]" id="email"
                                   placeholder="Current Office Email" >
                            <b class="tooltip tooltip-bottom-right">Needed to enter Reference Email</b>
                        </label>
                    </section>
                </div>



            </fieldset>

        @else











    <?php $refindex = 0 ?>

    @foreach($references as $reference)

    @if($refindex > 0)
    <fieldset ><h3 class="text-info">Refference Person {{ $refindex + 1 }}</h3><br>
            <button  type="button"  class="close close-reference" value="{{ $reference->id }}"><i class="fa fa-times text-dark-red" style="color:red"></i></button>@endif
        <input type="hidden" name="id[{{ $refindex }}]" id="" placeholder="Reference Name" @if(empty($id)) value="{{ $reference->id }}" @endif @if(!empty($id) || !empty($r_id)) value = "{{ $r_id->id }}"@endif>
        <div class="row">
            <section class="  form-group col col-4">
                <strong class="text text-dark-blue">Reference Name</strong>
                <label class="input">
                    <i class="icon-append fa fa-phone"></i>
                    <input type="text" name="name[{{$refindex }}]" value="{{ $reference->name }}" id="" placeholder="Reference Name" >
                    <b class="tooltip tooltip-bottom-right">Needed to enter current Reference Name</b>

                </label>
            </section>
            <section class="  form-group  col col-4">
                <strong class="text text-dark-blue">Reference Relation</strong>
                <label class="input">
                    <i class="icon-append fa fa-phone"></i>
                    <input type="text" name="relation[{{$refindex }}]" id=""
                           placeholder="Reference Relation"  value="{{ $reference->relation }}">
                    <b class="tooltip tooltip-bottom-right">Enter the relation with reference</b>

                </label>
            </section>
            <section class="  form-group  col col-4">
                <strong class="text text-dark-blue">Reference Address(House and Street Number)</strong>
                <label class="input">
                    <i class="icon-append fa fa-phone"></i>
                    <input type="text" name="address_street[{{$refindex }}]" id=""
                           placeholder="Address(House and Street Number)" value="{{ $reference->address_street }}">
                    <b class="tooltip tooltip-bottom-right">Enter the Reference Address(House and Street Number)</b>

                </label>
            </section>


        </div>
        <div class="row">
            <section class="  form-group col col-4">
                <strong class="text text-dark-blue">Reference City</strong>
                <label class="select">
                    <select name="city_id[{{$refindex }}]" class="city" id="city_id_{{ $refindex}}">
                        <option value="">--Please Select City Name--</option>

                        @foreach($city as $cities) {
                        <option value="{{ $cities->id }}" @if($cities->id == $reference->city_id)selected @endif>{{ $cities->city}}</option>
                        }
                        @endforeach
                    </select>

                </label>
            </section>
            <section class="  form-group col col-4">
                <strong class="text text-dark-blue">Reference District</strong>
                <label class="input">
                    <i class="icon-append fa fa-road"></i>
                    <input type="text" name="district_id[{{$refindex }}]" readonly id="district_id_{{ $refindex}}"
                           placeholder="Reference District" value="{{ $reference->district_id }}">
                    <b class="tooltip tooltip-bottom-right">Needed to enter Reference District</b>
                </label>
            </section>
            <section class=" form-group col col-4">
                <strong class="text text-dark-blue">Reference State</strong>
                <label class="input">
                    <i class="icon-append fa fa-road"></i>
                    <input type="text" name="state_id[{{ $refindex }}]" readonly id="state_id_{{ $refindex}}"
                           placeholder="Reference District" value="{{ $reference->state_id }}" >
                    <b class="tooltip tooltip-bottom-right">Needed to enter Reference State</b>
                </label>
            </section>

        </div>
        <div class="row">
            <section class=" form-group col col-4">
                <strong class="text text-dark-blue">Reference Phone</strong>
                <label class="input">
                    <i class="icon-append fa fa-phone"></i>
                    <input type="number" name="phone[{{ $refindex }}]" id=""
                           placeholder="Current Office Phone" value="{{ $reference->phone }}">
                    <b class="tooltip tooltip-bottom-right">Needed to enter Reference Phone Number</b>

                </label>
            </section>
            <section class=" form-group col col-4">
                <strong class="text text-dark-blue">Reference Mobile</strong>
                <label class="input">
                    <i class="icon-append fa fa-phone"></i>
                    <input type="number" name="mobile[{{ $refindex}}]" id=""
                           placeholder="Reference Mobile" value="{{ $reference->mobile }}">
                    <b class="tooltip tooltip-bottom-right">Needed to enter Reference Mobile</b>

                </label>
            </section>
            <section class=" form-group col col-4">
                <strong class="text text-dark-blue">Reference Alternate Mobile</strong>
                <label class="input">
                    <i class="icon-append fa fa-phone"></i>
                    <input type="number" name="alternate_mobile[{{ $refindex}}]" id=""
                           placeholder="Current Office Reference Mobile Number" value="{{ $reference->alternate_mobile }}" >
                    <b class="tooltip tooltip-bottom-right">Needed to enter Reference Mobile Number</b>

                </label>
            </section>

        </div>
        <div class="row">
            <section class=" form-group col col-4">
                <strong class="text text-dark-blue">Pin Code</strong>
                <label class="input">
                    <i class="icon-append fa fa-road"></i>
                    <input type="number" name="pincode[{{ $refindex}}]" id="" placeholder="Current Office Pin Code" value="{{ $reference->pincode }}">
                    <b class="tooltip tooltip-bottom-right">Needed to enter PinCode Number
                    </b>
                </label>
            </section>
            <section class=" form-group col col-4">
                <strong class="text text-dark-blue">STD Code</strong>
                <label class="input">
                    <i class="icon-append fa fa-phone"></i>
                    <input type="number" name="std_code[{{ $refindex}}]" id="" placeholder="STD Code" value="{{ $reference->std_code }}">
                    <b class="tooltip tooltip-bottom-right">Needed to enter STD Code
                    </b>
                </label>
            </section>
            <section class=" form-group col col-4">
                <strong class="text text-dark-blue">Reference Email</strong>
                <label class="input">
                    <i class="icon-append fa fa-phone"></i>
                    <input type="email" name="email[{{ $refindex}}]" id="email"
                           placeholder="Current Office Email" value="{{ $reference->district_email }}">
                    <b class="tooltip tooltip-bottom-right">Needed to enter Reference Email</b>
                </label>
            </section>
        </div>



    </fieldset>
        <?php $refindex++ ?>

    @endforeach


@endif

</div>
<!------------address---------------->

<button class="btn btn-info add" id="">Add More Reference</button>

<footer>
    <button class="btn btn-default" type="submit" >Update Customer</button><br>
</footer>
</form>
</div>
</div>

</div>
<!-- /Inner Row Col-md-12 -->


</div>

<!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->
<script>

$(document).ready(function(){

        var limit = 10;

        var r = {{ json_encode(count($references)) }}
        //alert(r);
        if(r == 1){
        var x= 0;
        }
        else{
            x = r-1;
        }
        //alert(x);
        $(".add").click(function(e){
            e.preventDefault();



            if(x<limit){
                x++;
                //var reference_name = "name_"+x;
                var appended_div = $('#dynamic').append(addhtml(x));
                appended_div.find('input[name="name[' + x + ']"]').rules('add', {
                        required: false,
                        messages: {}
                    });
                appended_div.find('input[name="relation[' + x + ']"]').rules('add', {
                        required: false,
                        messages: {}
                    });
                appended_div.find('input[name="address_street[' + x + ']"]').rules('add', {
                        required: false,
                        messages: {}
                    });
                appended_div.find('input[name="mobile[' + x + ']"]').rules('add', {
                        required: false,
                        messages: {}
                    });
                appended_div.find('input[name="city_id[' + x + ']"]').rules('add', {
                    required: false,
                    messages: {}
                });
                appended_div.find('input[name="district_id[' + x + ']"]').rules('add', {
                    required: false,
                    messages: {}
                });




            }
            else{
                $('#message_text').text("You Can Add Only 10 maximum 10 References");
                $('#message').modal('show');
            }

       });
        function addhtml(x){

            var num = x+1;
            return '<fieldset>' +'<h3 class="text-info">Refference Person '+num+'</h3><br>'+
                '<button  type="button"  value="" class="close close-reference"><i class="fa fa-times text-dark-red" style="color:red"></i></button>'+
                '<input type="hidden" name="id['+x+']" id="" placeholder="Reference Name" value="">'+
                '<div class="row">' +
                        '<section class="form-group col col-4">' +
                            '<strong class="text text-dark-blue">Reference Name</strong>' +
                            '<label class="input"><i class="icon-append fa fa-phone"></i>' +
                                '<input type="text" name="name['+x+']" id="" placeholder="Reference Name" >' +
                                '<b class="tooltip tooltip-bottom-right">Needed to enter current Reference Name</b>' +
                            '</label>' +
                        '</section>' +
                '<section class="  form-group  col col-4">' +
                    '<strong class="text text-dark-blue">Reference Relation</strong>' +
                    '<label class="input"><i class="icon-append fa fa-phone"></i>' +
                    '<input type="text" name="relation['+x+']" id="" placeholder="Reference Relation" >' +
                    '<b class="tooltip tooltip-bottom-right">Enter the relation with reference</b></label>' +
                '</section>' +
                '<section class="  form-group  col col-4">' +
                    '<strong class="text text-dark-blue">Reference Address(House and Street Number)</strong>' +
                    '<label class="input"><i class="icon-append fa fa-phone"></i>' +
                        '<input type="text" name="address_street['+x+']" id="" placeholder="Address(House and Street Number)" >' +
                        '<b class="tooltip tooltip-bottom-right">Enter the Reference Address(House and Street Number)</b>' +
                    '</label></section></div><div class="row">' +
                '<section class="  form-group col col-4">' +
                    '<strong class="text text-dark-blue">Reference City</strong>' +
                    '<label class="select"><select name="city_id['+x+']" class="city" id="city_id_'+x+'">' +
                    '<option value="">--Please Select City Name--</option>' +
                    '@foreach($city as $cities) {<option value="{{ $cities->id }}" >{{ $cities->city}}</option>}@endforeach' +
                    '</select></label>' +
                '</section>' +
                '<section class="  form-group col col-4">' +
                    '<strong class="text text-dark-blue">Reference District</strong>' +
                    '<label class="input"><i class="icon-append fa fa-road"></i>' +
                        '<input type="text" class="district" name="district_id['+x+']" readonly id="district_id_'+x+'" placeholder="Reference District" >' +
                        '<b class="tooltip tooltip-bottom-right">Needed to enter Reference District</b>' +
                    '</label>' +
                '</section>' +
                '<section class=" form-group col col-4">' +
                    '<strong class="text text-dark-blue">Reference State</strong>' +
                    '<label class="input"><i class="icon-append fa fa-road"></i>' +
                        '<input type="text" class="state" name="state_id['+x+']" readonly id="state_id_'+x+'" placeholder="Reference District" >' +
                        '<b class="tooltip tooltip-bottom-right">Needed to enter Reference State</b>' +
                    '</label>' +
                '</section></div><div class="row">' +
                '<section class=" form-group col col-4">' +
                    '<strong class="text text-dark-blue">Reference Phone</strong>' +
                    '<label class="input"><i class="icon-append fa fa-phone"></i>' +
                        '<input type="number" name="phone['+x+']" id=""placeholder="Current Office Phone" >' +
                        '<b class="tooltip tooltip-bottom-right">Needed to enter Reference Phone Number</b>' +
                    '</label>' +
                '</section>' +
                '<section class=" form-group col col-4">' +
                    '<strong class="text text-dark-blue">Reference Mobile</strong>' +
                    '<label class="input"><i class="icon-append fa fa-phone"></i>' +
                        '<input type="number" name="mobile['+x+']" id="" placeholder="Reference Mobile" >' +
                        '<b class="tooltip tooltip-bottom-right">Needed to enter Reference Mobile</b>' +
                    '</label>' +
                '</section>' +
                '<section class=" form-group col col-4">' +
                    '<strong class="text text-dark-blue">Reference Alternate Mobile</strong>' +
                    '<label class="input"><i class="icon-append fa fa-phone"></i>' +
                    '<input type="number" name="alternate_mobile['+x+']" id="" placeholder="Current Office Reference Mobile Number" >' +
                    '<b class="tooltip tooltip-bottom-right">Needed to enter Reference Mobile Number</b>' +
                    '</label>' +
                '</section></div><div class="row">' +
                '<section class=" form-group col col-4">' +
                    '<strong class="text text-dark-blue">Pin Code</strong>' +
                    '<label class="input"><i class="icon-append fa fa-road"></i>' +
                        '<input type="number" name="pincode['+x+']" id="" placeholder="Current Office Pin Code" >' +
                        '<b class="tooltip tooltip-bottom-right">Needed to enter PinCode Number</b>' +
                    '</label>' +
                '</section>' +
                '<section class=" form-group col col-4">' +
                    '<strong class="text text-dark-blue">STD Code</strong>' +
                    '<label class="input"><i class="icon-append fa fa-phone"></i>' +
                        '<input type="number" name="std_code['+x+']" id="" placeholder="STD Code">' +
                        '<b class="tooltip tooltip-bottom-right">Needed to enter STD Code</b>' +
                    '</label>' +
                '</section>' +
                '<section class=" form-group col col-4">' +
                    '<strong class="text text-dark-blue">Reference Email</strong>' +
                    '<label class="input"><i class="icon-append fa fa-phone"></i>' +
                        '<input type="email" name="email['+x+']" id="email" placeholder="Current Office Email" >' +
                        '<b class="tooltip tooltip-bottom-right">Needed to enter Reference Email</b>' +
                    '</label>' +
                '</section>' +
            '</div>' +
        '</fieldset>';

        }

        $('#dynamic').on("click",".close-reference",function(e){
            e.preventDefault();
            var remove_reference_id = $(this).val();
            //alert(remove_reference_id);
            if(remove_reference_id == ""){
                $(this).parent('fieldset').remove();
                x--;
            }
            else{
                $.ajax({
                    method:'get',
                    data:{id:remove_reference_id},
                    dataType:'json',
                    url:"{{ URL::to('admin/approve/removereference') }}",
                    success:function(){

                        $(this).parent('fieldset').remove();
                        x--;
                        location.reload();
                    }
                })
            }

        });

        $('#dynamic').on("change",".city",function(e){
            e.preventDefault();

           var city = $(this).attr('id');

           var city_id = document.getElementById(city).value;
            $.ajax({
                method:"POST",
                data:{city_id:city_id},
                url:"{{ URL::to('admin/approve/district') }}",
                success:function(data){

                    //alert(data.district);
                    var data = JSON.parse(data);
                    var id = city.split("_");
                    //alert('#district_id_'+id[2]);
                    $('#district_id_'+id[2]).val(data.district);
                    $('#state_id_'+id[2]).val(data.state);

                }
            });


        });

        setTimeout(function(){

            $('.city').change(function (){
                var city = $(this).attr('id');
                //alert(city);

                var city_id = document.getElementById(city).value;
                //alert(city_id);
                $.ajax({
                    method:"POST",
                    data:{city_id:city_id},
                    url:"{{ URL::to('admin/approve/district') }}",
                    success:function(data){

                        //alert(data.district);
                        var data = JSON.parse(data);

                        $('#district_id_0').val(data.district);
                        $('#state_id_0').val(data.state);

                    }
                });
            });

        },25);



    jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        }),

        $('#registration-form').validate({
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                $('#proof_div').slideDown(700);
                $('#address_div').slideDown(700);
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-add-body', 'show');
                //var l = Ladda.create($('[type=submit]', form)[0]);
                //l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        console.log('data')

                        if (data.status == 'fail') {
                            //$('#add').modal('hide');
                            $('#message_text').text(data.message);
                            $('#message').modal('show');

                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            //$('input', form).iCheck('update');

                            // toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            //$('#add').modal('hide');

                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                            window.location.replace("{{ URL::to('admin/lead') }}")
                        } else {

                        }

                        //l.stop();
                        //uiLoader('#form-add-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },




            rules: {
                /*'name[0]':{
                    required:true
                },
                'relation[0]':{
                    required:true
                },
                'address_street[0]':{
                    required:true
                },
                'city_id[0]':{
                    required:true
                },
                'district_id[0]':{
                    required:true
                },
                'state_id[0]':{
                    required:true
                },
                'mobile[0]':{
                    required:true
                }*/

            },
            messages: {}
        });
   });
</script>
@stop




