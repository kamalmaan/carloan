@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/bank') }}">Bank</a></li>
        <li class="active">Branch</li>
    </ul>
</div>

<div class="page-header">

    <button  type="button" class="btn btn-info" id="getcity" >Add Branch</button>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


<div class="col-md-12 bootstrap-grid">


    <div class="powerwidget" id="datatable-basic-init" data-widget-editbutton="false">
        <header>
            <h2>Datatable<small>Basic Init</small></h2>
        </header>
        <div class="inner-spacer">

            {{ $data->render() }}
            <script>
                $('#test')
                    .on('preXhr.dt', function (e, settings, data) {
                        // on start of ajax call
                    }).on( 'draw.dt', function () {
                        $(".status").click(function(){
                            var id = $(this).attr("id");
                           //alert(id);
                            $.ajax({
                                method:"GET",
                                data:{id:id},
                                url:"{{ URL::to('admin/bank/bankbranchstatus') }}",
                                success:function(data){
                                    $('#test').dataTable()._fnAjaxUpdate();
                                }
                            });
                        });

                        $('#getcity').click(function(){

                            $.ajax({
                                    method:"GET",
                                    url:"{{ URL::to('admin/bank/bankbranchadd') }}",
                                    success:function(data){
                                        $('#branch').append(data);
                                        $('#add').modal('show');
                                    }
                                },'json'
                            );
                        });

                        $(".bankbranch").click(function(){
                            var id = $(this).attr("id");
                            //alert(id);
                            $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/bank/bankbranchedit') }}",
                                    success:function(data){
                                        $('.city').append(data.city)
                                        $('#edit').modal('show');
                                        $('#id').val(data.record.id);
                                        $('#bank_id').val(data.record.bank_id);
                                        $('#branch').val(data.record.branch);
                                        $('#conatct_person').val(data.record.contact_person);
                                        $('#phone').val(data.record.phone);
                                        $('#email').val(data.record.email);
                                    }
                                },'json'
                            );
                        });


                    });
            </script>
            {{ $data->script() }}

        </div>
    </div>


</div>

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Branch</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="bankbranch_add" action='{{ URL::to("admin/bank/bankbranchsave") }}' method="post" novalidate="novalidate">
                    <fieldset>
                        <input type="hidden" value="{{ $id }}" name="bank_id">
                        <section class="form-group">
                            <label class="select">
                                <select name="branch" class="city" id="branch">
                                    <option value="">--Please Select Category--</option>
                                </select>

                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-bank"></i>
                                <input type="text" placeholder="Contact Person" name="contact_person" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Branch Manager Name</b>

                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="Mobile" name="phone" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Mobile</b>
                                {{ $errors->first('bm_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="Email" name="email" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Email</b>
                                {{ $errors->first('bm_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Branch</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="bankbranch_edit" action='{{ URL::to("admin/bank/bankbranchupdate") }}' method="post" novalidate="novalidate">
                    <input type="hidden" value="" name="id" id="id">
                    <fieldset>
                        <input type="hidden" value="{{ $id }}" name="bank_id">
                        <section class="form-group">
                            <label class="select">
                                <select name="branch" class="city" id="branch">
                                    <option value="">--Please Select Category--</option>
                                </select>

                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-bank"></i>
                                <input type="text" placeholder="Contact Person" name="contact_person" value="" id="conatct_person">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Branch Manager Name</b>

                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="Mobile" name="phone" value="" id="phone">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Mobile</b>
                                {{ $errors->first('bm_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="Email" name="email" value="" id="email">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Email</b>
                                {{ $errors->first('bm_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<script>

    $().ready(function() {
        $('.callout').hide();
        $('#bankbranch_add').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('#add').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#add').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    branch: {
                        required: true
                    },
                    contact_person: {
                        required: true
                    },
                    phone: {
                        required: true
                    },
                    email: {
                        required: true
                        /*remote: {
                            url: "{{ URL::to('admin/bank/testbank')}}",
                            type: 'POST',
                            data:{
                                rules: 'required|email|unique:bank_branches'
                            }
                        }*/

                    }

                },
                messages: {}
            }
        );
        //eidt district



        $('#bankbranch_edit').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('.form-group', form).removeClass('has-error');
                                $('#edit').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {


                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#edit').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    branch: {
                        required: true
                    },
                    contact_person: {
                        required: true
                    },
                    phone: {
                        required: true
                    },
                    email: {
                        required: true
                    }


                },
                messages: {

                }
            }
        );


    });

</script>

</div>

<!-- /Inner Row Col-md-6 -->

@stop
