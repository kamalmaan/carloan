@extends('admin/layout')
@section('content')

    <div class="col-md-12 bootstrap-grid">
    <!--Breadcrumb-->
    <div class="breadcrumb clearfix">
        <ul>
            <li><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li><a href="index.html">Dashboard</a></li>
            <li class="active">Rrofile</li>
        </ul>
    </div>


    <div class="page-header">
        <!-- <button type="button" class="btn btn-info" data-toggle="modal" data-target="#add">Add User</button> -->
        <a href='{{ URL::to("admin/users") }}'><button type="button" class="btn btn-info" >BACK</button> </a>       
        <br>
        @if(Session::has('message'))
            <div class="callout callout-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Well done ! </strong>{{ Session::get('message')}}
            </div>
        @endif
    </div>
</div>

<div class="col-md-12 bootstrap-grid">
    <!-- Widget Row Start grid -->
    <div class="row" id="powerwidgets">
        <div class="col-md-12 bootstrap-grid">
            <div class="powerwidget powerwidget-as-portlet-white" id="serverstatuz-indexpage2">
                <header><h2>User Record</h2></header>
                <div>
                    <div class="inner-spacer">
                        <div class="row">
                            <!--Row-->
                            <div class="col-md-12">
                                <div class="row margin-bottom-10px">
                                    <ul class="countries-demo" id="choices">
                                        @foreach($total_users as $row)
                                            <li class="col-md-2 col-sm-4">
                                                <h3>{{ $row->is_superuser  }} <span
                                                            class="label bg-marine"> {{ $row->count  }} </span></h3>

                                                <div class="orb-form">
                                                    <label for="iduk">
                                                        <a href="{{URL::to('admin/users/user/'.$row->id)}}"> View All </a>
                                                    </label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!--/Row--->
                            </div>
                            <!--/Col-md-12--->
                        </div>
                        <!--/Row-->
                    </div>
                </div>
            </div>
        </div>
        <!-- /End Widget -->
    </div>
    <!-- /Inner Row Col-md-6 -->
    </div>


    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{$data->render()}}
                <script>
                    $('#test')
                            .on('preXhr.dt', function (e, settings, data) {
                                // on start of ajax call
                            }).on( 'draw.dt', function () {
                                $(".status").click(function(){
                                    var id = $(this).attr("id");
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/users/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".user_edit").click(function(){
                                    var id = $(this).attr("id");
                                    $.ajax({
                                                method:"GET",
                                                data:{id:id},
                                                url:"{{ URL::to('admin/users/edit') }}",
                                                success:function(data){
                                                    data = JSON.parse(data)
                                                    $('#edit').modal('show');                                                    
                                                    $('#username').val(data.record.username);
                                                    $('#id').val(data.record.id);
                                                    $('#first_name').val(data.record.first_name);
                                                    $('#last_name').val(data.record.last_name);
                                                    $('#phone').val(data.record.phone);
                                                    $('#user_type').val(data.record.is_superuser);
                                                    if(data.record.is_superuser==6||data.record.is_superuser==7||data.record.is_superuser==8){
                                                        $("#branch").hide();
                                                    }else{
                                                        $("#branch").show();
                                                    }

                                                    $('#branch').val(data.record.branch_id);
                                                    if(data.options==""){
                                                        $('#parent_role').hide();
                                                    }else{
                                                        $('#parent_role').html(data.options);    
                                                    }
                                                    
                                                    //$('#parent_id').val(data.record.parent_id);
                                                    //alert(data.record.parent_id);
                                                    $('#phone1').val(data.record.phone1);
                                                    $(".attachment_1").attr('src','{{ asset("upload/'+data.record.profile_pic+'") }}');
                                                    $(".attachment_2").attr('src','{{ asset("upload/'+data.record.additional_pic+'") }}');
                                                    $("#permanent_address").val(data.record.permanent_address);
                                                    $("#temporary_address").val(data.record.temporary_address);
                                                    $(".attachment_3").attr('src','{{ asset("upload/'+data.record.agreement+'") }}');
                                                    $(".attachment_4").attr('src','{{ asset("upload/'+data.record.id_proof1+'") }}');
                                                    $(".attachment_5").attr('src','{{ asset("upload/'+data.record.id_proof2+'") }}');
                                                    $(".attachment_6").attr('src','{{ asset("upload/'+data.record.id_proof3+'") }}');
                                                    $(".attachment_7").attr('src','{{ asset("upload/'+data.record.id_proof4+'") }}');                                                                                                  
                                                }
                                            },'json'
                                    );

                                
                                });
                            });
                </script>
                {{ $data->script() }}
                </div>
        </div>
    </div>

    {{--
       user registration model
   --}}
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit User</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="edit_user" action='{{ URL::to("admin/users/update") }}' method="post"
                          novalidate="novalidate">
                        <fieldset>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-automobile"></i>
                                    <input type="text" disabled id="username" placeholder="User name" name="username" value="">

                                    <input type="hidden" id="id" name="id"/>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter username</b>
                                    {{ $errors->first('username','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-pied-piper"></i>
                                    <input id="password" type="password" id="password" placeholder="password" name="password"
                                           name="make" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter password</b>
                                    {{ $errors->first('password','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-pied-piper"></i>
                                    <input id="confirm_password" type="password" id="confirm_password" placeholder="confirm password"
                                           name="confirm_password" name="make" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter confirm password</b>
                                    {{ $errors->first('confirm_password','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="select">
                                    <select id="user_type" name="user_type" class="newuser_role">
                                        <option value="">--Please Select User type--</option>
                                        @foreach($total_users as $row)
                                            <option value="{{$row->id}}">{{ $row->is_superuser  }}</option>
                                        @endforeach
                                    </select>
                                    {{ $errors->first('user_type','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>  
                            <section class="form-group">
                                <label class="select">
                                    <select id="branch" name="branch">
                                        <option value="">--Please Select branch--</option>
                                        @foreach($branches as $row)
                                            <option value="{{$row->id}}">{{ $row->address."(".$row->city()->first()->city .")"  }}</option>
                                        @endforeach
                                    </select>
                                    {{ $errors->first('branch','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>                                                                            
                            <section class="form-group">
                                 <label class="select">
                                    <select name="parent_user_type" class="parent_role" id="parent_role">

                                    </select>
                                 </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" id="first_name" placeholder="First name" name="first_name" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter First name</b>
                                    {{ $errors->first('first_name','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="last_name" placeholder="last name" name="last_name" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter last name</b>
                                    {{ $errors->first('last_name','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="phone" placeholder="Phone number" name="phone" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Phone number</b>
                                    {{ $errors->first('phone','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="phone1" placeholder="Alternate Phone number" name="phone1" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Alternate Phone number</b>
                                    {{ $errors->first('phone1','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <div class="row">
                                <section class="form-group col col-6">                                                                                              
                                    <label for="file" class="input input-file">
                                        <div class="button">
                                            <input type="file" id="profile_pic_1" name="profile_pic" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Profile picture">                                    
                                    </label>
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Profile Picture</b>                                    
                                </section>
                                <section class="form-group col col-6">
                                    <img class="img-thumbnail attachment_1"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="additional_pic_2" name="additional_pic" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Additional picture">
                                    </label>  
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Additional Picture</b>
                                        {{ $errors->first('additional_pic','<em class="invalid text-danger" > :message </em>') }}                              
                                </section>
                                <section class="form-group col col-6"> 
                                    <img class="img-thumbnail attachment_2"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <section class="form-group">                                
                                <label class="textarea"> <i class="icon-append fa fa-car"></i>
                                    <textarea name="permanent_address" placeholder="Permanent Address" id="permanent_address" ></textarea>                                    
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Address</b>
                                    {{ $errors->first('permanent_address','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="checkbox">
                                    <input type="checkbox" name="same_permanent" id="same_permanent"><i></i>Same as Permanent Address
                                </label>
                                <label class="textarea"> <i class="icon-append fa fa-car"></i>
                                    <textarea name="temporary_address" placeholder="Temporary Address"  id="temporary_address"></textarea>                                    
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Temporary Address</b>
                                    {{ $errors->first('temporary_address','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="agreement_proof_3" name="agreement" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Agreement">
                                    </label>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('agreement','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6">
                                    <img class="img-thumbnail attachment_3"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">                                    
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="id_proof1_4" name="id_proof1" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Id proof1">                                   
                                    </label>   
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('id_proof1','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6">
                                    <img class="img-thumbnail attachment_4"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="id_proof2_5" name="id_proof2" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Id proof2">                                       
                                    </label>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('id_proof2','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6">
                                    <img class="img-thumbnail attachment_5"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="id_proof3_6" name="id_proof3" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Id proof3">
                                    </label>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('id_proof3','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6">
                                    <img class="img-thumbnail attachment_6"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="id_proof4_7" name="id_proof4" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Id proof4">
                                    </label>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('id_proof4','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6"> 
                                    <img class="img-thumbnail attachment_7"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                               
                            </div>

                            

                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script>
    function readURL(input) {          
           var id = input.id;
            var t = id.split("_");
           // alert(t[2]);
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.attachment_'+t[2]).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }

        $(document).ready(function () {            
            //on change of role of user
            $(".newuser_role").click(function(){

                var id = $(this).attr("id");
                var role_id=$(this).val();
              
                //alert(role_id);
                if(role_id==3||role_id==2||role_id==4){
                    var branch=$("#branch").val();
                    
                    if(branch==""){
                        alert('Select Branch');
                    }else{  
                    var pid= $("#parent_role").val();                                    
                        $.ajax({
                            method:"POST",
                            data:{role_id:role_id,branch_id:branch},
                            url:"{{ URL::to('admin/users/role') }}",
                            success:function(data){                                
                                $(".parent_role").html(data);
                                $("#branch").show();
                            }
                        });

                    }

                } else if(role_id==5) {     
                            
                        $("#branch").show();
                        $(".parent_role").hide(); 
                } else{
                    
                     $("#branch,.parent_role").hide();
                }
            });
            //onchange of branch
             $("#branch").click(function(){

                //var id = $(this).attr("id");
                var branch_id=$("#branch").val();
                var role_id=$(".newuser_role").val();
              
                if(role_id==3||role_id==2||role_id==4){
                    var branch=$("#branch").val();                 
                    if(branch==null){                      
                    }else{                        
                        $.ajax({
                            method:"POST",
                            data:{role_id:role_id,branch_id:branch},
                            url:"{{ URL::to('admin/users/role') }}",
                            success:function(data){                                
                                $(".parent_role").html(data);
                                $("#branch,.parent_role").show();
                            }
                        });

                    }

                } else if(role_id==5) {     
                            
                        $("#branch").show();
                        $(".parent_role").hide(); 
                } else{
                    
                     $("#branch,.parent_role").hide();
                }
            });

            //set temporary same as permanent
            $("#same_permanent").click(function(){
                var permanent_address=$("#permanent_address").val();                
                $("#temporary_address").val(permanent_address);
            });            

            $('#edit_user').validate(
                    {
                        ignore: [],
                        errorElement: 'span',
                        errorClass: 'help-block',
                        highlight: function (element) { // hightlight error inputs
                            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        submitHandler: function (form) {
                            form = $(form);
                            $('[type=submit]', form).attr('disabled', 'disabled');
                            //uiLoader('#form-add-body', 'show');
                            //var l = Ladda.create($('[type=submit]', form)[0]);
                            //l.start();
                            form.ajaxSubmit({
                                dataType: 'json',
                                success: function (data) {
                                    console.log('data');

                                    if (data.status == 'fail') {

                                        $('#edit').modal('hide');
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                        $('[type=submit]', form).removeAttr('disabled');
                                    } else if (data.status == 'success') {

                                        form[0].reset();
                                        $('.form-group', form).removeClass('has-error');
                                        //$('input', form).iCheck('update');

                                        // toastr['success'](data.message);
                                        $('[type=submit]', form).removeAttr('disabled');
                                        $('#edit').modal('hide');
                                        $('#test').dataTable()._fnAjaxUpdate();
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                    } else {

                                    }

                                    //l.stop();
                                    //uiLoader('#form-add-body', 'hide');
                                }
                            });

                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                        },
                        rules: {
                            username: {
                                required: true,
                                minlength: 6
                            },
                            password: {
                                minlength: 6
                            },
                            confirm_password: {
                                minlength: 6,
                                equalTo: "#password"
                            },
                            user_type: {
                                required: true
                            },
                            first_name: {
                                required: true
                            },
                            last_name: {
                                required: true
                            },
                            phone: {
                                required: true
                            }/*,
                            branch: {
                                required: true
                            }*/
                        },
                        messages: {}
                    }
            );

        })



    </script>

@stop
