@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Insurance Company</li>
    </ul>
</div>

<div class="page-header">

    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#insurancecompany">Add Insurance Company</button>
    <br>
    @if(Session::has('message'))
        <div class="callout callout-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong >Well done ! </strong>{{ Session::get('message')}}
        </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{ $data->render() }}
                <script>
                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                        }).on( 'draw.dt', function () {
                                $(".status").click(function(){
                                    var id = $(this).attr("id");
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/insurancecompany/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".company").click(function(){
                                   var id = $(this).attr("id");
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/insurancecompany/edit') }}",
                                        success:function(data){
                                            data = JSON.parse(data)
                                            $('#edit_form').modal('show');
                                            document.getElementById("insurance_company_id").value=data.record.id;
                                            document.getElementById("insurance_company_name").value=data.record.name;
                                            document.getElementById("insurance_company_phone").value=data.record.phone;
                                            document.getElementById("insurance_company_aphone").value=data.record.alternate_phone;
                                            document.getElementById("insurance_company_email").value=data.record.email;
                                            document.getElementById("insurance_company_aemail").value=data.record.alternate_email;
                                        
                                            }
                                        },'json'
                                    );
                                });
                        });
                </script>

                {{ $data->script() }}
            </div>
        </div>

    </div>


    <div class="modal fade" id="edit_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Company</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="insurancecompany_edit" action='{{ URL::to("admin/insurancecompany/update") }}' method="post" novalidate="novalidate">

                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="Company Name" name="name" value="" id="insurance_company_name"/>
                                    <input type="hidden" placeholder="" name="id" value="" id="insurance_company_id"/>                                   
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="Phone" name="phone" value="" id="insurance_company_phone">                                    
                                </label>                                
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="number" placeholder="Alternate Phone Numbers" name="alternate_phone" value="" id="insurance_company_aphone">                                    
                                </label>
                            </section>                     
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="email" placeholder="Email" name="email" value="" id="insurance_company_email">                                    
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="email" placeholder="Alternate Emails" name="alternate_email" value="" id="insurance_company_aemail">                                    
                                </label>
                            </section>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="insurancecompany" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Insurance Company</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="insurancecompany_add" action='{{ URL::to("admin/insurancecompany/save") }}' method="post" novalidate="novalidate">

                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="Insurance Company" name="name" value="">                                    
                                </label>                                
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="number" placeholder="Phone" name="phone" value="">                                    
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="number" placeholder="Alternate Phone Numbers" name="alternate_phone" value="">                                    
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="email" placeholder="Email" name="email" value="">                                    
                                </label>
                            </section>
                             <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="email" placeholder="Alternate Emails" name="alternate_email" value="">                                    
                                </label>
                            </section>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
<script>

    $().ready(function() {

        $('#insurancecompany_add').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('#insurancecompany').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#insurancecompany').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    name: {
                        required: true
                    },
                    phone: {
                        required: true
                    },
                    email: {
                        required: true
                    }

                },
                messages: {                  
                }
            }
        );
        //eidt district



        $('#insurancecompany_edit').validate(
            { 
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                
                                $('#edit_form').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {


                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#edit_form').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                     name: {
                        required: true
                    },
                    phone: {
                        required: true
                    },
                    email: {
                        required: true
                    }

                },
                messages: {

                }
            }
        );


    });

</script>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->


@stop
