
@extends('admin/layout')
@section('content')
<style>
.ui-multiselect-filter input{
    color:#333;
}
</style>
<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Search</li>
    </ul>
</div>

<div class="page-header">
    <div class="orb-form">
    <div class="row">
        <label class="label">Select  Search Mode</label>
            <section class="col col-md-4 col-md-offset-1">                
                <label class="radio-inline" for="radios-inline-0">
                <input name="radios" id="radios-inline-0" value="user" type="radio" class="search_mode">
                Customer </label>
            </section>
            <section class="col col-md-4">
                <label class="radio-inline" for="radios-inline-0">
                <input name="radios" id="radios-inline-0" value="customer"  type="radio" class="search_mode">
                User </label>
            </section>
    </div>
    <form  id='search' method='post'>
               <div class="row">
        <div class="user">

        </div>
        
        <section class=" form-group col col-3 person_dob" id="person_dob">
            <strong class="text text-dark-blue">From</strong>
            <label class="input">
                <i class="icon-append fa fa-calendar"></i>
                <input type="date" class="dob" name="from_date" id="from_date" placeholder="" value="">
                <b class="tooltip tooltip-bottom-right">Enter From date</b>
            </label>
        </section>
        <section class=" form-group col col-3 person_dob" id="person_dob">
            <strong class="text text-dark-blue">To</strong>
            <label class="input">
                <i class="icon-append fa fa-calendar"></i>
                <input type="date" class="dob" name="to_date" id="to_date" placeholder="" value="">
                <b class="tooltip tooltip-bottom-right">Enter to date</b>
            </label>
        </section>

        <div class="customer">
        </div>
    </div>

        <input type='button' name='' value='Search' class='btn btn-sm btn-info'/>
    </form>    
</div>
</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Search<small>Show Hide Columns</small></h2>
            </header>

            <div class="inner-spacer">

                {{ $data->render() }}
 
                <script>
        
                $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                      }).on( 'draw.dt', function () {
                        
                     });
     
                </script>
                {{ $data->script() }}
            </div>
        </div>

    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->




<script>
$('.search_mode').change(function(){
        var mode_id=$(this).attr('id');
        var mode_value=$("#"+mode_id).val();
         $('select').multiselect().each(function() { 
                this.checked = false;                 
            });
         
        $.ajax({
            method:"POST",
            data:{mode:mode_value},
            url:"{{ URL::to('admin/search/options') }}",
            success:function(data){
                     //alert(data);  
                     $(".user").html(data);
                     $(".person_dob").show();  
                        $("#module_name,#loan_amount,#loan_approved,#address_type").multiselect({
                           multiple: false,
                           /*header: "Select an option",
                           noneSelectedText: "Select an Option",
                           selectedList: 0*/
                        });
                        $("select").multiselect({
                        }).multiselectfilter({
                        click: function (event, ui) {                                   
                            }
                        });                
            }
            },'json');   

    });

var ele=new Array; 

function filter(input){
    var id = input.id;
    var my_array={};
   // console.log("id="+id);
    my_array[id]=$("#"+id).multiselect("getChecked").map(function(){
        return this.value;  
    }).get();
    var s=JSON.stringify(my_array);

   if(ele.length==0 ){   
    ele.push(s);
}else{
  
 //  console.log("first list="+ele);
  
   var st=0;
   $.each(ele, function(index, value) { 
  // console.log('value : ' + value+"index="+index); 
    var obj = jQuery.parseJSON(value);
      $.each(obj, function(index1, value1) { 
        // console.log("index="+index1+"id="+id); 
           if(index1==id){
           // console.log('if'+index);
            st=1;
             ele[index]=null;
             ele.splice(index, 1);
            //  console.log("conole ele after list="+ele);
              ele.push(s);
                               
                }        

            });
               
        });
    if(st==0){
        ele.push(s);            
    }
}

}

$(function(){
    $(".person_dob").hide();
    ele.length = 0;
    $("#search").click(function(event){
       // event.preventDefault();
   
        var module="";var lead_type=new Array;var enquiry_type="";var bank="";
        var loan_amount="";var loan_approved=""; var address_type="";var city="";var product=""; 
        var vehicle_model="";var vehicle_make="";var vehicle_category="";
        var from_date=document.getElementById('from_date').value;
        var to_date=document.getElementById('to_date').value;
        console.log(from_date+to_date);
        alert(from_date+to_date);

       $.each(ele, function(index, value) { 
       // console.log(index + ': ' + value); 
        var obj = jQuery.parseJSON(value);
            $.each(obj, function(index, value) { 
            console.log(index ); 
            console.log(value);
            if(index=="module_name"){
                module=JSON.stringify(value);  
            }else if(index=="lead_type"){               
                lead_type=JSON.stringify(value);               
            }else if(index=="enquiry_type"){
                enquiry_type =JSON.stringify(value); 
            }else if(index=="bank"){
                bank=JSON.stringify(value);
            }else if(index=="loan_amount"){
                loan_amount=JSON.stringify(value);
            }else if(index=="loan_approved"){
                loan_approved=JSON.stringify(value);
            }else if(index=="address_type"){
                address_type=JSON.stringify(value);
            }else if(index=="city"){
                city=JSON.stringify(value);
            }else if(index=="product"){
                product=JSON.stringify(value);
            }else if(index=="vehicle_model"){
                vehicle_model=JSON.stringify(value);
            }else if(index=="vehicle_make"){
                vehicle_make=JSON.stringify(value);
            }else if(index=="vehicle_category"){
                vehicle_category=JSON.stringify(value);
            }           
            });
        });                    
                // alert("search=="+module+lead_type +"---"+enquiry_type+"--"+bank+"--"+loan_amount+"--"+address_type+"=="+city);
                // alert("=="+product+"-=="+vehicle_model+"--"+vehicle_make+"--"+vehicle_category);
        $.post("{{ URL::to('admin/search/fetch') }}",{'module':module,'lead_type':lead_type,'enquiry_type':enquiry_type,'bank':bank,
            'loan_amount':loan_amount,'loan_approved':loan_approved,'address_type':address_type,'city':city,'product':product,'vehicle_model':vehicle_model,
            'vehicle_make':vehicle_make,'vehicle_category':vehicle_category,'from_date':from_date,'to_date':to_date},function(data){            
            //console.log(data);
            if(data.message=="success"){                
                $('#test').dataTable()._fnAjaxUpdate();     
            }             
        },'json');
    });
});
</script>

@stop




