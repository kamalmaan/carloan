@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Decline Leads</li>
    </ul>
</div>

<div class="page-header">

    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>
            <a href="{{ URL::to('admin/lead') }}">
                <button type="button" class="btn btn-info btn-xs">Lead</button>
            </a>
            <a href="{{ URL::to('admin/lead/decline') }}">
                <button type="button" class="btn btn-success btn-xs">Declined Lead</button>
            </a>
            <a href="{{ URL::to('admin/fi') }}">
                <button type="button" class="btn btn-info btn-xs">Fi Status</button>
            </a>
            <a href="{{ URL::to('admin/fi/underwriting') }}">
                <button type="button" class="btn btn-info btn-xs">Under Writing</button>
            </a>
            <a href="{{ URL::to('admin/underwritingapprovedlead') }}">
                <button type="button" class="btn btn-info btn-xs">Under Writing Approved</button>
            </a>
            <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
                <button type="button" class="btn btn-info btn-xs">Under Approve Decline</button>
            </a>
            <a href="{{ URL::to('admin/temporarylead') }}">
                <button type="button" class="btn btn-info btn-xs">Temporary Customer</button>
            </a>
            <a href="{{ URL::to('admin/temporarylead/temporarydecline') }}">
                <button type="button" class="btn btn-info btn-xs">Temporary Decline Customer</button>
            </a>
            <a href="{{ URL::to('admin/permanent') }}">
                <button type="button" class="btn btn-info btn-xs">Permanent Customer</button>
            </a>

            <br><br>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Product<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{ $data->render() }}
                <script>
                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                        }).on( 'draw.dt', function () {
                            //pagingType: "simple";
                            $(".recycle").click(function(){
                                var id = $(this).attr("id")
                              //  alert(id);
                                $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/lead/recycle') }}",
                                    dataType: 'json',
                                    success:function(data){
                                        $('#test').dataTable()._fnAjaxUpdate();
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                    }
                                });
                            });
                            $(".delete").click(function(){
                                var id = $(this).attr("id")
                                alert(id);
                                $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/lead/declinedelete') }}",
                                    dataType:'json',
                                    success:function(data){
                                        $('#test').dataTable()._fnAjaxUpdate();
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                    }
                                });
                            });
                        });
                </script>
                {{ $data->script() }}
            </div>
        </div>

    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->


@stop
