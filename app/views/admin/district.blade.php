@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/state') }}">State</a></li>
        <li class="active">District</li>
    </ul>
</div>

<div class="page-header">

    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#add">Add District</button>
    <br>
    @if(Session::has('message'))
        <div class="callout callout-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong >Well done ! </strong>{{ Session::get('message')}}
        </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">


        <div class="powerwidget" id="datatable-basic-init" data-widget-editbutton="false">
            <header>
                <h2>Datatable<small>Basic Init</small></h2>
            </header>
            <div class="inner-spacer">

                {{ $data->render() }}
                <script>
                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                        }).on( 'draw.dt', function () {
                            $(".status").click(function(){
                                var id = $(this).attr("id");
                                //alert(id);
                                $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/state/districtstatus') }}",
                                    success:function(data){
                                    $('#test').dataTable()._fnAjaxUpdate();
                                    }
                                });
                            });
                            $(".district").click(function(){

                                var id = $(this).attr("id");
                                $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/state/districtedit') }}",
                                        success:function(data){
                                            data = JSON.parse(data)
                                            $('#edit').modal('show');
                                            document.getElementById("district_name").value=data.record.district;
                                            document.getElementById("district_id").value=data.record.id;
                                        }
                                    },'json'
                                );
                            });

                        });
                </script>
                {{ $data->script() }}

            </div>
        </div>


    </div>

    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add District</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="district_add" action='{{ URL::to("admin/state/districtsave") }}' method="post" novalidate="novalidate">
                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="District" name="district" value="">
                                    <input type="hidden" placeholder="" name="id" value="{{ $id }}">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter district for district</b>
                                    {{ $errors->first('district','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit District</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="district_edit" action='{{ URL::to("admin/state/districtupdate") }}' method="post" novalidate="novalidate">

                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>


                                    <input type="text" placeholder="" name="district" value="" id="district_name"/>
                                    <input type="hidden" placeholder="" name="id" value="" id="district_id"/>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter district</b>
                                    {{ $errors->first('district','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <script>

        $().ready(function() {
            $('.callout').hide();
            $('#district_add').validate(
                {
                    ignore: [],
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                    submitHandler: function (form) {
                        form = $(form);
                        $('[type=submit]', form).attr('disabled', 'disabled');
                        //uiLoader('#form-add-body', 'show');
                        //var l = Ladda.create($('[type=submit]', form)[0]);
                        //l.start();
                        form.ajaxSubmit({
                            dataType: 'json',
                            success: function (data) {

                                console.log('data')

                                if (data.status == 'fail') {
                                    $('#add').modal('hide');
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');
                                    $('[type=submit]', form).removeAttr('disabled');
                                } else if (data.status == 'success') {

                                    form[0].reset();
                                    $('.form-group', form).removeClass('has-error');
                                    //$('input', form).iCheck('update');

                                    // toastr['success'](data.message);
                                    $('[type=submit]', form).removeAttr('disabled');
                                    $('#add').modal('hide');
                                    $('#test').dataTable()._fnAjaxUpdate();
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');
                                } else {

                                }

                                //l.stop();
                                //uiLoader('#form-add-body', 'hide');
                            }
                        });

                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        district: {
                            required: true
                        }

                    },
                    messages: {}
                }
            );
            //eidt district



            $('#district_edit').validate(
                {
                    ignore: [],
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                    submitHandler: function (form) {
                        form = $(form);
                        $('[type=submit]', form).attr('disabled', 'disabled');
                        //uiLoader('#form-add-body', 'show');
                        //var l = Ladda.create($('[type=submit]', form)[0]);
                        //l.start();
                        form.ajaxSubmit({
                            dataType: 'json',
                            success: function (data) {
                                console.log('data')

                                if (data.status == 'fail') {
                                    $('.form-group', form).removeClass('has-error');
                                    $('#edit').modal('hide');
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');

                                    $('[type=submit]', form).removeAttr('disabled');
                                } else if (data.status == 'success') {


                                    form[0].reset();
                                    $('.form-group', form).removeClass('has-error');
                                    //$('input', form).iCheck('update');

                                    // toastr['success'](data.message);
                                    $('[type=submit]', form).removeAttr('disabled');
                                    $('#edit').modal('hide');
                                    $('#test').dataTable()._fnAjaxUpdate();
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');

                                } else {

                                }

                                //l.stop();
                                //uiLoader('#form-add-body', 'hide');
                            }
                        });

                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        district: {
                            required: true
                        }

                    },
                    messages: {

                    }
                }
            );


        });

    </script>

</div>

<!-- /Inner Row Col-md-6 -->

@stop
