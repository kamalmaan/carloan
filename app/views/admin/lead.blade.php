@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Lead</li>
    </ul>
</div>

<div class="page-header">
   <!-- @if(!role_permission('2','lead_add')) {{ "disabled" }} @endif-->
    @if(!role_permission('2','lead_add'))
    <button  type="button" class="btn btn-info" disabled="">Add Lead</button>
    @elseif(role_permission('2','lead_add'))
    <a href="{{ URL::to('admin/lead/add')}}"><button  type="button" class="btn btn-info" >Add Lead</button></a>
    @endif
    <br><br>

    @if(Session::has('message'))
        <div class="callout callout-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong >Well done ! </strong>{{ Session::get('message')}}
        </div>
    @endif
</div>
                <a href="{{ URL::to('admin/lead') }}">
                    <button type="button" class="btn btn-success btn-xs">Lead</button>
                </a>
                <a href="{{ URL::to('admin/lead/decline') }}">
                    <button type="button" class="btn btn-info btn-xs">Declined Lead</button>
                </a>
                <a href="{{ URL::to('admin/fi') }}">
                    <button type="button" class="btn btn-info btn-xs">Fi Status</button>
                </a>
                <a href="{{ URL::to('admin/fi/underwriting') }}">
                    <button type="button" class="btn btn-info btn-xs">Under Writing</button>
                </a>
                <a href="{{ URL::to('admin/underwritingapprovedlead') }}">
                    <button type="button" class="btn btn-info btn-xs">Under Writing Approved</button>
                </a>
                <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
                    <button type="button" class="btn btn-info btn-xs">Under Approve Decline</button>
                </a>
                <a href="{{ URL::to('admin/temporarylead') }}">
                    <button type="button" class="btn btn-info btn-xs">Temporary Customer</button>
                </a>
                <a href="{{ URL::to('admin/temporarylead/temporarydecline') }}">
                    <button type="button" class="btn btn-info btn-xs">Temporary Decline Customer</button>
                </a>
                <a href="{{ URL::to('admin/permanent') }}">
                    <button type="button" class="btn btn-info btn-xs">Permanent Customer</button>
                </a>

                <br><br>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">


        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Product<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
            {{ $data->render() }}
                <script>
                    $('#test')
                .on('preXhr.dt', function (e, settings, data) {
                // on start of ajax call
                }).on( 'draw.dt', function () {
                // on table rendered


                            $(".lead_to").change(function(){

                                var lead = $(this).attr("id");
                                //alert(lead);
                                var user=document.getElementById(lead).value;
                                if(user == 0){
                                    alert("You have not selected any lead type")
                                    return false;
                                }
                                $.ajax({
                                    method:"POST",
                                    data:{lead_id:lead,user_id:user},
                                    url:"{{ URL::to('admin/lead/leadto') }}"
                                });


                            });
                            $(".forward_lead").change(function(){
                                
                                var lead = $(this).attr("id");
                                //alert('lll'+lead);
                               // var alead=lead.split('_');
                                var user=document.getElementById(lead).value;
                                //alert(lead+'---'+user+'---'+alead[1]);
                                
                                $.ajax({
                                    method:"POST",
                                    data:{lead_id:lead,user_id:user},
                                    url:"{{ URL::to('admin/lead/forwardto') }}"
                                })


                            });
                            $(".decline").click(function(){
                                var lead_id = $(this).attr("id");
                                //alert(lead_id);

                                $('#lead_id').val(lead_id);
                                $("#declines").modal('show');
                            });
                            $('.fi_initiation').click(function(){
                                var id = $(this).attr("id");
                                //alert(id);
                                $.ajax({
                                    method:"GET",
                                    //data:{id:id},
                                    url:"{{ URL::to('admin/fi/bank') }}",
                                    dataType:'json',
                                    success:function(data){
                                        $('#fi').modal('show');
                                        $('#fi_lead_id').val(id);
                                        $('#branch').html(data);
                                    }
                                })

                            });
                });

                </script>
            {{ $data->script() }}
            </div>
        </div>


    </div>



    <!--decline Modal-->

    <div class="modal fade" id="declines" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Decline Lead</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="decline_lead" action='{{ URL::to("admin/lead/decline") }}' method="post" novalidate="novalidate">
                        <fieldset>
                            <input type="hidden" value="" name="lead_id" id="lead_id">
                            <section class="form-group">
                                <strong class="text text-dark-blue">Choose a reason for decline</strong>
                                <div class="inline-group" >
                                    <label class="radio">
                                        <input type="radio" value = "Customer Not Interested" name="reason">
                                        <i></i>Customer Not Interested</label>
                                    <label class="radio">
                                        <input type="radio" value="Lost to competitor" name="reason">
                                        <i></i>Lost to competitor</label>
                                    <label class="radio">
                                        <input type="radio" value="Profile Mismatch" name="reason">
                                        <i></i>Profile Mismatch</label><br>
                                    <label class="input">
                                        {{ $errors->first('guardian','<em class="invalid text-danger" > :message </em>') }}</label>

                                </div>

                            </section>
                            <section class="form-group">
                                <strong class="text text-dark-blue">You may add a comment</strong>
                                <label class="textarea">
                                    <textarea rows="3" type="text" placeholder="Comments" name="comment" value=""></textarea>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Comments</b>
                                </label>
                            </section>

                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <!---Modal For --->

    <div class="modal fade" id="fi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Decline Lead</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="fi_initiation" action='{{ URL::to("admin/fi/sendfi") }}' method="post" novalidate="novalidate">
                        <fieldset>
                            <input type="hidden" value="" name="lead_id" id="fi_lead_id">

                            <section class="form-group">
                                <strong class="text text-dark-blue">List Of Banks</strong>
                                <label class="select">
                                    <select name="branch" class="" id="branch">
                                        <option value="">--Please Select Category--</option>
                                    </select>

                                </label>
                            </section>
                            <strong class="text text-dark-blue dynamic" style="display: none">Branches</strong>
                            <div class="powerwidget cold-grey dynamic" id="" data-widget-editbutton="false" style="display: none">

                                    <table class="table table-striped table-hover margin-0px" id="table">
                                        <thead>
                                        <tr>
                                            <th>Contact Person</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Choose One</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>

                                    </table>

                            </div>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Send File For FI Initiation</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!--decline lead-->
    <script>
        $().ready(function() {

            $("#branch").change(function(){
               //$('#fi').modal('hide');
                var branch_id = $(this).val();
                //alert(branch_id);
                $.ajax({
                    method:"GET",
                    data:{branch:branch_id},
                    dataType:'json',
                    url:"{{ URL::to('admin/fi/bankbranches') }}",
                    success:function(data){

                        console.log(data);
                        var cars = data;
                        var i = 0;
                        var test = "";

                        while (cars[i]) {

                            var link = "{{ URL::to('admin/lead/save') }}"+"/"+cars[i]['id'];

                            test += "<tr><td>"+cars[i]['contact_person']+"</td><td>"+cars[i]['phone']+"</td><td>"+cars[i]['email']+"</td><td><section class='form-group'><label for='radio'><input type='radio' name='bankbranch_id' value='"+cars[i]['id']+"'></label></section></td></tr>";

                            i++;
                        }
                        $('#table tbody').html(test);
                    }
                })
                $('.dynamic').css({display:'block'});
            });

            $('#decline_lead').validate(
                {
                    ignore: [],
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element) { // hightlight error inputs

                        $(element).closest('.inline-group').addClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                    submitHandler: function (form) {
                        form = $(form);
                        $('[type=submit]', form).attr('disabled', 'disabled');
                        //uiLoader('#form-add-body', 'show');
                        //var l = Ladda.create($('[type=submit]', form)[0]);
                        //l.start();
                        form.ajaxSubmit({
                            dataType: 'json',
                            success: function (data) {
                                console.log('data')

                                if (data.status == 'fail') {
                                    $('#add').modal('hide');
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');

                                    $('[type=submit]', form).removeAttr('disabled');
                                } else if (data.status == 'success') {

                                    form[0].reset();
                                    $('.form-group', form).removeClass('has-error');
                                    //$('input', form).iCheck('update');

                                    // toastr['success'](data.message);
                                    $('[type=submit]', form).removeAttr('disabled');
                                    $('#declines').modal('hide');
                                    $('#test').dataTable()._fnAjaxUpdate();
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');
                                } else {

                                }

                                //l.stop();
                                //uiLoader('#form-add-body', 'hide');
                            }
                        });

                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        reason: {
                            required: true

                        }

                    },
                    messages: {}
                }
            );


            //------ FOR FI INITIATION--------////

            $('#fi_initiation').validate(
                {
                    ignore: [],
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element) { // hightlight error inputs

                        $(element).closest('.inline-group').addClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                    submitHandler: function (form) {
                        form = $(form);
                        $('[type=submit]', form).attr('disabled', 'disabled');
                        //uiLoader('#form-add-body', 'show');
                        //var l = Ladda.create($('[type=submit]', form)[0]);
                        //l.start();
                        form.ajaxSubmit({
                            dataType: 'json',
                            success: function (data) {
                                console.log('data')

                                if (data.status == 'fail') {
                                    $('#add').modal('hide');
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');

                                    $('[type=submit]', form).removeAttr('disabled');
                                } else if (data.status == 'success') {

                                    form[0].reset();
                                    $('.form-group', form).removeClass('has-error');
                                    //$('input', form).iCheck('update');

                                    // toastr['success'](data.message);
                                    $('[type=submit]', form).removeAttr('disabled');
                                    $('#fi').modal('hide');
                                    $('#test').dataTable()._fnAjaxUpdate();
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');
                                } else {

                                }

                                //l.stop();
                                //uiLoader('#form-add-body', 'hide');
                            }
                        });

                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        fi_lead_id: {
                            required: true

                        },
                        branch:{
                            required:true
                        },
                        bankbranch_id:{
                            required:true
                        }

                    },
                    messages: {}
                }
            );

        });
    </script>


    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

@stop
