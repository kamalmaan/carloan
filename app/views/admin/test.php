@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="index.html">Dashboard</a></li>
        <li class="active">Rrofile</li>
    </ul>
</div>


<div class="page-header">

    <a href="{{ URL::to('admin/product/add')}}"><button  type="button" class="btn btn-info">Add Product</button></a>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">




    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

@stop
