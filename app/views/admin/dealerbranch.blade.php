@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/dealer') }}">Dealer</a></li>
        <li class="active">Branch</li>
    </ul>
</div>

<div class="page-header">

    <button  type="button" class="btn btn-info" id="getcity" >Add Branch</button>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


<div class="col-md-12 bootstrap-grid">


    <div class="powerwidget" id="datatable-basic-init" data-widget-editbutton="false">
        <header>
            <h2>Datatable<small>Basic Init</small></h2>
        </header>
        <div class="inner-spacer">

            {{ $data->render() }}
            <script>
                $('#test')
                    .on('preXhr.dt', function (e, settings, data) {
                        // on start of ajax call
                    }).on( 'draw.dt', function () {
                        $(".status").click(function(){
                            var id = $(this).attr("id");
                            //alert(id);
                            $.ajax({
                                method:"GET",
                                data:{id:id},
                                url:"{{ URL::to('admin/dealer/branchdealerstatus') }}",
                                success:function(data){
                                    $('#test').dataTable()._fnAjaxUpdate();
                                }
                            });
                        });

                        $('#getcity').click(function(){

                            $.ajax({
                                    method:"GET",
                                    url:"{{ URL::to('admin/dealer/branchdealeradd') }}",
                                    success:function(data){
                                        $('#branch_id').html(data);
                                        $('#add').modal('show');
                                    }
                                },'json'
                            );
                        });

                        $(".dealerbranch").click(function(){
                            var id = $(this).attr("id");
                            //alert(id);
                            $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/dealer/branchdealeredit') }}",
                                    success:function(data){
                                        $('.city').html(data.city)
                                        $('#edit').modal('show');
                                        $('#id').val(data.record.id);
                                        $('#dealer_id').val(data.record.dealer_id);
                                        $('#branch_id').val(data.record.branch_id);
                                        $('#bm_name').val(data.record.bm_name);
                                        $('#bm_phone').val(data.record.bm_phone);
                                        $('#bm_email').val(data.record.bm_email);
                                    }
                                },'json'
                            );
                        });


                    });
            </script>
            {{ $data->script() }}

        </div>
    </div>


</div>

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Branch</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="dealerbranch_add" action='{{ URL::to("admin/dealer/dealerbranchsave") }}' method="post" novalidate="novalidate">
                    <fieldset>
                        <input type="hidden" value="{{ $id }}" name="dealer_id">
                        <section class="form-group">
                            <label class="select">
                                <select name="branch_id" class="city" id="branch_id">
                                    <option value="">--Please Select Category--</option>
                                </select>
                                {{ $errors->first('branch_id','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-bank"></i>
                                <input type="text" placeholder="BM Name" name="bm_name" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Name</b>
                                {{ $errors->first('bm_name','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="bm Mobile" name="bm_phone" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Mobile</b>
                                {{ $errors->first('bm_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="BM Email" name="bm_email" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Email</b>
                                {{ $errors->first('bm_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Branch</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="dealerbranch_edit" action='{{ URL::to("admin/dealer/dealerbranchupdate") }}' method="post" novalidate="novalidate">
                    <input type="hidden" value="" name="id" id="id">
                    <fieldset>
                        <section class="form-group">
                            <label class="select">
                                <select name="branch_id" class="city" id="branch_id">
                                    <option value="">--Please Select Category--</option>
                                </select>
                                {{ $errors->first('branch_id','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-bank"></i>
                                <input type="text" placeholder="BM Name" name="bm_name" id="bm_name" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Name</b>
                                {{ $errors->first('gm_name','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="BM Mobile" name="bm_phone" id="bm_phone" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Mobile</b>
                                {{ $errors->first('bm_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="BM Email" name="bm_email" id="bm_email" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter BM Email</b>
                                {{ $errors->first('bm_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<script>

    $().ready(function() {
        $('.callout').hide();
        $('#dealerbranch_add').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('#add').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#add').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    branch_id: {
                        required: true
                    },
                    bm_name: {
                        required: true
                    },
                    bm_phone: {
                        required: true
                    },
                    bm_email: {
                        required: true
                    }

                },
                messages: {}
            }
        );
        //eidt district



        $('#dealerbranch_edit').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('.form-group', form).removeClass('has-error');
                                $('#edit').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {


                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#edit').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    branch_id: {
                        required: true
                    },
                    bm_name: {
                        required: true
                    },
                    bm_phone: {
                        required: true
                    },
                    bm_email: {
                        required: true
                    }

                },
                messages: {

                }
            }
        );


    });

</script>

</div>

<!-- /Inner Row Col-md-6 -->

@stop
