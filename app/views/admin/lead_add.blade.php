@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/lead') }}">Lead</a></li>
        <li class="active">Add Lead</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">

    <a href="{{ URL::to('admin/lead')}}"><button  type="button" class="btn btn-info">Back</button></a>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">


        <div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">

            <div data-widget-editbutton="false" id="registration-form-validation-widget" class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
                <header role="heading">
                    <h2><Lead></Lead><small>You Can Add Your Lead Type</small></h2>
                    <div class="powerwidget-ctrls" role="menu"> <a class="button-icon powerwidget-delete-btn" href="#"><i class="fa fa-times-circle"></i></a>  <a class="button-icon powerwidget-fullscreen-btn" href="#"><i class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i class="fa fa-chevron-circle-up "></i></a></div><span class="powerwidget-loader"></span></header>
                <div class="inner-spacer" role="content">
                    <form class="orb-form" id="registration-form" action="{{ URL::to('admin/lead/save/0')}}" method="post" novalidate="novalidate">
                        <header></header>
                        <fieldset>
                            <div class="row">
                                <section class="col col-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="lead_first_name" id="lead_first" placeholder="First Name" value="{{ Input::old('lead_first_name') }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter First Name</b>
                                        {{ $errors->first('lead_first_name','<em class="invalid text-danger" > :message </em>') }}
                                        <p class="text-danger" id="first_name" style="display:none">Please Enter Lead First Name</p>
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="lead_middle_name" id="lead_middle" placeholder="Middle Name" value="{{ Input::old('lead_middle_name') }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Middle Name</b>
                                        {{ $errors->first('lead_middle_name','<em class="invalid text-danger" > :message </em>') }}
                                        <p class="text-danger" id="middle_name" style="display:none">Please Enter Lead Middle Name</p>
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="lead_last_name" id="lead_last" placeholder="Last Name" value="{{ Input::old('lead_last_name') }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Last Name</b>
                                        {{ $errors->first('lead_last_name','<em class="invalid text-danger" > :message </em>') }}
                                        <p class="text-danger" id="last_name" style="display:none">Please Enter Lead Last Name</p>
                                    </label>
                                </section>
                            </div>
                            <div class="row">

                                <section class="col col-6">
                                    <h5 class="text text-dark-blue ">Choose The Type Of User</h5>
                                    <div class="inline-group">
                                        <label class="radio">
                                            <input type="radio" value = "person" class="type" name="type" {{ (Input::old('type') == 'person' || Input::old('type') == '')? 'checked' :null}}>
                                            <i></i>Single Person</label>
                                        <label class="radio">
                                            <input type="radio" value="firm" class="type" name="type" {{ (Input::old('type') == 'firm')? 'checked' :null}}>
                                            <i></i>Firm</label>
                                    </div>
                                </section>

                            </div>

                            <div class="row" id="firm"  style="{{($errors->has('company_name') || $errors->has('constitution') || $errors->has('company_category'))? 'display:block':'display:none' }}">
                                <section class="col col-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="company_name" id="" placeholder="Company Name" value="{{ Input::old('company_name') }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Your Company Name</b>
                                        {{ $errors->first('company_name','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="select">
                                        <select name="constitution">
                                            <option value="">--Please Select Constitution Type--</option>
                                            <option value="Partnership" {{ (Input::old('constitution') == 'Partnership')?"selected":null }}>Partnership</option>
                                            <option value="Public Ltd. Co." {{ (Input::old('constitution') == 'Public Ltd. Co')?"selected":null }}>Public Ltd. Co.</option>
                                            <option value="PVT Ltd. Co." {{ (Input::old('constitution') == 'PVT Ltd. Co.')?"selected":null }}>PVT Ltd. Co.</option>
                                            <option value="Salaried" {{ (Input::old('constitution') == 'Salaried')?"selected":null }}>Salaried</option>
                                            <option value="Self Employed" {{ (Input::old('constitution') == 'Self Employed')?"selected":null }}>Self Employed</option>
                                            <option value="Society" {{ (Input::old('constitution') == 'Society')?"selected":null }}>Society</option>
                                            <option value="Sole Propriertors" {{ (Input::old('constitution') == 'Sole Propriertors')?"selected":null }}>Sole Propriertors</option>
                                            <option value="Student" {{ (Input::old('constitution') == 'Student')?"selected":null }}>Student</option>
                                        </select>
                                        {{ $errors->first('constitution','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="select">
                                        <select name="company_category">
                                            <option value="">--Please Select Company Category--</option>
                                            <option value="CAT D" {{ (Input::old('company_category') == 'CAT D')?"selected":null }}>CAT D</option>
                                            <option value="CAT DEN WITH CLINIC" {{ (Input::old('company_category') == 'CAT DEN WITH CLINIC')?"selected":null }}>CAT DEN WITH CLINIC</option>
                                            <option value="CAT DEN WITHOUT CLINIC" {{ (Input::old('company_category') == 'CAT DEN WITHOUT CLINIC')?"selected":null }}>CAT DEN WITHOUT CLINIC</option>
                                            <option value="CAT DOC HOSPITAL" {{ (Input::old('company_category') == 'CAT DOC HOSPITAL')?"selected":null }}>CAT DOC HOSPITAL</option>
                                            <option value="CAT DOC AYURVEDA" {{ (Input::old('company_category') == 'CAT DOC AYURVEDA')?"selected":null }}>CAT DOC AYURVEDA</option>
                                            <option value="CAT DOC CONSULTANT" {{ (Input::old('company_category') == 'CAT DOC CONSULTANT')?"selected":null }}>CAT DOC CONSULTANT</option>
                                            <option value="CAT DOC DIAGNOSTICS" {{ (Input::old('company_category') == 'CAT DOC DIAGNOSTICS')?"selected":null }}>CAT DOC DIAGNOSTICS</option>
                                            <option value="CAT DOC GP WITH CLINIC" {{ (Input::old('company_category') == 'CAT DOC GP WITH CLINIC')?"selected":null }}>CAT DOC GP WITH CLINIC</option>
                                        </select>
                                        {{ $errors->first('company_category','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                            </div>
                            <div id="guardian" style="{{($errors->has('guardian') || $errors->has('guardian_first_name') || $errors->has('guardian_last_name'))? 'display:block':'display:none' }}">
                                <div class="row">

                                    <section class="col col-3">
                                        <div class="inline-group">
                                            <label class="radio">
                                                <input type="radio" value = "S/O" name="guardian" {{ (Input::old('guardian') == 'S/O')? 'checked' :null}}>
                                                <i></i>S/O</label>
                                            <label class="radio">
                                                <input type="radio" value="W/O" name="guardian" {{ (Input::old('guardian') == 'W/O')? 'checked' :null}}>
                                                <i></i>W/O</label>
                                            <label class="radio">
                                                <input type="radio" value="D/O" name="guardian" {{ (Input::old('guardian') == 'D/O')? 'checked' :null}}>
                                                <i></i>D/O</label><br>
                                        </div>
                                    </section>
                                    <section class="col col-6">
                                        <label class="input">
                                                {{ $errors->first('guardian','<em class="invalid text-danger" style="margin-top: 10px;"> :message </em>') }}
                                        </label>
                                    </section>

                                </div>
                                <div class="row" >
                                    <section class="col col-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="guardian_first_name" id="" placeholder="Guardian First Name" value="{{ Input::old('guardian_first_name') }}">
                                            <b class="tooltip tooltip-bottom-right">Needed to enter Your Gaurdian's First Name</b>
                                            {{ $errors->first('guardian_first_name','<em class="invalid text-danger" > :message </em>') }}
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="guardian_middle_name" id="" placeholder="Guardian Middle Name" value="{{ Input::old('guardian_middle_name') }}">
                                            <b class="tooltip tooltip-bottom-right">Needed to enter Your Gaurdian's Middle Name</b>
                                            {{ $errors->first('guardian_middle_name','<em class="invalid text-danger" > :message </em>') }}
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="guardian_last_name" id="" placeholder="Guardian Last Name" value="{{ Input::old('guardian_last_name') }}">
                                            <b class="tooltip tooltip-bottom-right">Needed to enter Your Gaurdian's Last Name</b>
                                            {{ $errors->first('guardian_last_name','<em class="invalid text-danger" > :message </em>') }}
                                        </label>
                                    </section>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        <input type="text" name="phone" id="lead_phone" placeholder="Phone" value="{{ Input::old('phone') }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Your Contact Number</b>
                                        {{ $errors->first('phone','<em class="invalid text-danger" > :message </em>') }}
                                        <p class="text-danger" id="phone" style="display:none">Please Fill in the phone number</p>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="text" name="email_id" id="" placeholder="E-mail" value="{{ Input::old('email_id') }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Your Email Address</b>
                                        {{ $errors->first('email_id','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-6">
                                   <label class="select">
                                      <select name="meeting_medium">
                                         <option value="">--Please Select Expected Meeting Medium--</option>
                                         <option value="Face To Face" {{ (Input::old('meeting_medium') == 'Face To Face')?"selected":null }}>Face To Face</option>
                                         <option value="Email" {{ (Input::old('meeting_medium') == 'Email')?"selected":null }}>Email</option>
                                         <option value="Phone" {{ (Input::old('meeting_medium') == 'Phone')?"selected":null }}>Phone</option>
                                      </select>
                                      {{ $errors->first('meeting_medium','<em class="invalid text-danger" > :message </em>') }}
                                   </label>
                                </section>
                                <section class="col col-6">
                                    <label class="select">
                                        <select name="enquiry_type">
                                            <option value="">--Please Select Enquiry Type--</option>
                                            <option value="Warm" {{ (Input::old('enquiry_type') == 'Warm')?"selected":null }}>Warm</option>
                                            <option value="Hot" {{ (Input::old('enquiry_type') == 'Hot')?"selected":null }}>Hot</option>
                                            <option value="Cold" {{ (Input::old('enquiry_type') == 'Cold')?"selected":null }}>Cold</option>
                                        </select>
                                        {{ $errors->first('enquiry_type','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <fieldset>
                            <section>
                                <label class="select">
                                    <select name="product_id">
                                        <option value="">--Please Select Product Type--</option>
                                        @foreach($products as $product){
                                        <option value="{{ $product->id}}" {{ (Input::old('product_id') == $product->id)?"selected":null }}>{{ $product->product }}</option>
                                        }
                                        @endforeach
                                    </select>
                                    <i></i> {{ $errors->first('product_id','<em class="invalid text-danger" > :message </em>') }}</label>
                            </section>
                            <section>
                                <label class="select">
                                    <select name="lead_type_id">
                                        <option value="">--Please Select Lead Type--</option>
                                        @foreach($lead_types as $lead_type){
                                        <option value="{{ $lead_type->id}}" {{ (Input::old('lead_type_id') == $lead_type->id)?"selected":null }}>{{ $lead_type->lead_type }}</option>
                                        }
                                        @endforeach
                                    </select>
                                    <i></i> {{ $errors->first('lead_type_id','<em class="invalid text-danger" > :message </em>') }}</label>
                            </section>
                            <section>
                                <label class="select">
                                    <select name="lead_from">
                                        <option value="">--Please Select Lead From--</option>
                                        @foreach($lead_froms as $lead_from){
                                        <option value="{{ $lead_from->id}}" {{ (Input::old('lead_from') == $lead_from->id)?"selected":null }}>{{ ucfirst($lead_from->first_name)." ".ucfirst($lead_from->last_name)." - ".$lead_from->superuser()->first()->is_superuser }}</option>
                                        }
                                        @endforeach
                                    </select>
                                    <i></i> {{ $errors->first('lead_from','<em class="invalid text-danger" > :message </em>') }}</label>
                            </section>
                            <!--<section>
                                <label class="select">
                                    <select name="branch_id">
                                        <option value="">--Please Select Branch--</option>
                                        @foreach($branches as $branch){
                                        <option value="{{ $branch->id}}">{{ ucfirst($branch->branch) }}</option>
                                        }
                                        @endforeach
                                    </select>
                                    <i></i>{{ $errors->first('branch_id','<em class="invalid text-danger" > :message </em>') }} </label>
                            </section>-->
                        </fieldset>
                        <footer>
                            <button class="btn btn-default" type="submit" id="save">Save</button>
                            <button class="btn btn-info" id="existingcustomer" type="button">Check If Existing Customer</button>
                        </footer>
                    </form>
                </div>
            </div>

        </div>
    <!-- /Inner Row Col-md-12 -->

    </div>
    <div class="modal fade" style="width:auto" id="existing_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit City</h4>
                </div>
                <div class="modal-body">
                    <div class="powerwidget cold-grey" id="" data-widget-editbutton="false">
                        <header>
                            <h2>Table<small>Striped, Hovered</small></h2>
                        </header>
                        <div class="inner-spacer">
                            <table class="table table-striped table-hover margin-0px" id="table">
                                <thead>
                                <tr>
                                    <th>Lead Name</th>
                                    <th>Guardian</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Meeting Medium</th>
                                    <th>Enquiry Type</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->
<script>
   $(document).ready(function(){

       $(".type").click(function(){
          var type_check =  $(this).val();
           if(type_check == 'person'){
               $('#guardian').slideToggle(500);
              document.getElementById("firm").style.display="none";

           }
           else{
                $("#firm").slideToggle(500);
               document.getElementById("guardian").style.display="none";
           }
       });

       $('#first_name').hide();
       $('#middle_name').hide();
       $('#last_name').hide();
       $('#phone').hide();

           $('#existingcustomer').click(function(){
               if($('#lead_first').val() == '' || $('#lead_last').val() == '' || $('#lead_phone').val() == ''){
                   $('#first_name').show();
                   $('#middle_name').show();
                   $('#last_name').show();
                   $('#phone').show();
                   return false;
               }

               else{
                   $.post("{{ URL::to('admin/lead/existingcustomer') }}",$('#registration-form').serialize(),function(data){

                       console.log(data);
                       if(data == ""){
                            $("#proof-text").html("No Record Matches Your Search !");
                            $("#proof_message").modal("show");
                       }
                       else{
                           var cars = data;
                           var i = 0;
                           var test = "";

                           while (cars[i]) {
                               var link = "{{ URL::to('admin/lead/save') }}"+"/"+cars[i]['id'];

                                   test += "<tr><td>"+cars[i]['lead_first_name']+" "+cars[i]['lead_middle_name']+" "+cars[i]['lead_last_name']+"</td><td>"+cars[i]['guardian']+" of "+cars[i]['guardian_first_name']+" "+cars[i]['guardian_last_name']+"</td><td>"+cars[i]['phone']+"</td><td>"+cars[i]['email_id']+"</td><td>"+cars[i]['meeting_medium']+"</td><td>"+cars[i]['enquiry_type']+"</td><td><form method='post' action='"+link+"'><button type='submit' class='btn'><span class='label bg-black'>My Account</span></button></form></td></tr>";

                               i++;
                           }
                           console.log(test);

                           $("#registration-form [type=text]").val('');
                           $("#registration-form input[type=checkbox]").prop("checked", false);
                           $('#existing_customer').modal('show');
                           $('#table tbody').append(test);
                       }
                   },'json');
               }
           });


   })
</script>
@stop


