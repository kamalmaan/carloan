@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">State</li>
    </ul>
</div>

<div class="page-header">

    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#state">Add State</button>
    <br>
    @if(Session::has('message'))
        <div class="callout callout-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong >Well done ! </strong>{{ Session::get('message')}}
        </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{ $data->render() }}
                <script>
                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                        }).on( 'draw.dt', function () {
                                $(".status").click(function(){
                                    var id = $(this).attr("id");
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/state/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".state").click(function(){
                                   var id = $(this).attr("id");
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/state/edit') }}",
                                        success:function(data){
                                            data = JSON.parse(data)
                                            $('#edit_form').modal('show');
                                            document.getElementById("state_name").value=data.record.state;
                                            document.getElementById("state_id").value=data.record.id;
                                            }
                                        },'json'
                                    );
                                });
                        });
                </script>

                {{ $data->script() }}
            </div>
        </div>

    </div>


    <div class="modal fade" id="edit_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit State</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="state_edit" action='{{ URL::to("admin/state/update") }}' method="post" novalidate="novalidate">

                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>


                                    <input type="text" placeholder="State" name="state" value="" id="state_name"/>
                                    <input type="hidden" placeholder="" name="id" value="" id="state_id"/>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter state</b>
                                    {{ $errors->first('state','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="state" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add State</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="state_add" action='{{ URL::to("admin/state/save") }}' method="post" novalidate="novalidate">

                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="State" name="state" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter state</b>
                                    {{ $errors->first('state','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
<script>

    $().ready(function() {

        $('#state_add').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('#state').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#state').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    state: {
                        required: true,
                        remote: {
                            url: '{{ URL::to("admin/validator/validate") }}',
                            type: 'POST',
                            data:{
                                rules: 'required|unique:states'
                            }
                        }

                    }

                },
                messages: {
                    state:"State Name already exist!"
                }
            }
        );
        //eidt district



        $('#state_edit').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {

                                $('#edit_form').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {


                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#edit_form').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    state: {
                        required: true
                    }

                },
                messages: {

                }
            }
        );


    });

</script>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->


@stop
