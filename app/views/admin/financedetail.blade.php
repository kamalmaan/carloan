
@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/lead') }}">Lead</a></li>
        <li class="active">Asset Deatail</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">

    <a @if(empty($id)) href='{{ URL::to("admin/approve/index/$lead->id")}}' @endif @if(!empty($id))  href='{{ URL::to("admin/approve/index/{$id->id}")}}' @endif>
    <button type="button" class="btn btn-info">Personal Detail</button>
    </a>
    <a @if(empty($id)) href='{{ URL::to("admin/approve/asset/$lead->id") }}' @endif @if(!empty($id)) href='{{ URL::to("admin/approve/asset/{$id->id}") }}' @endif>
    <button class="btn btn-info">Asset Details</button>
    </a>
    <a @if(empty($id)) href='{{ URL::to("admin/approve/reference/$lead->id")}}' @endif @if(!empty($id)) href='{{ URL::to("admin/approve/reference/{$id->id}")}}' @endif>
    <button type="button" class="btn btn-info">Reference Detail</button>
    </a>
    @if(empty($id))
    @if(!empty($lead->temporary()->first()->lead_id))
    @if(!empty($lead->temporary()->first()->los_number))
    @if($lead->temporary()->first()->decline == 1)

    <a href='#'>
        <button type="button" class="btn btn-success">Financial Data</button>
    </a>

    @endif
    @endif
    @endif
    @endif
    @if(!empty($id))
    @if(!empty($id->temporary()->first()->lead_id))
    @if(!empty($id->temporary()->first()->los_number))
    @if($id->temporary()->first()->decline == 1)

    <a href='#'>
        <button type="button" class="btn btn-success">Financial Data</button>
    </a>

    @endif
    @endif
    @endif
    @endif
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">


        <div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">

            <div data-widget-editbutton="false" id="registration-form-validation-widget"
                 class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
                <header role="heading">
                    <h2>
                        <Lead></Lead>
                        <small>You Can Add Your Lead Type</small>
                    </h2>
                    <div class="powerwidget-ctrls" role="menu"><a class="button-icon powerwidget-delete-btn" href="#"><i
                                class="fa fa-times-circle"></i></a> <a class="button-icon powerwidget-fullscreen-btn" href="#"><i
                                class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i
                                class="fa fa-chevron-circle-up "></i></a></div>
                    <span class="powerwidget-loader"></span></header>
                <div class="inner-spacer" role="content">
                    <form class="orb-form" id="finance" @if(empty($id)) action="{{ URL::to('admin/approve/savefinance/'.$lead->id)}}" @endif @if(!empty($id)) action="{{ URL::to('admin/approve/savefinance/'.$id->id)}}" @endif method="post"
                    novalidate="novalidate" enctype="multipart/form-data">
                    <header></header>
                    <fieldset>
                        <div class="row">

                            <input type="hidden" name="id" @if(empty($id)) @if(!empty($lead->finance()->first()->id)) value="{{  $lead->finance()->first()->id }}" @endif @endif @if(!empty($id)) @if(!empty($id->finance()->first()->id)) value="{{  $id->finance()->first()->id }}" @endif @endif >


                            <section class=" form-group col col-6" >
                                <strong class="text text-dark-blue">Advanced EMI </strong>
                                <label class="select">
                                    <select name="advance_emi" id="advance_emi">

                                        <option value="">--Please Select EMI--</option>
                                        <option value="00" @if(!empty($lead->finance()->first()->advance_emi) ) @if($lead->finance()->first()->advance_emi == '00') {{ "selected" }} @endif @endif>{{ '00' }}</option>
                                        <option value="01" @if(!empty($lead->finance()->first()->advance_emi) ) @if($lead->finance()->first()->advance_emi == '01') {{ "selected" }} @endif @endif>{{ '01' }}</option>
                                        <option value="02" @if(!empty($lead->finance()->first()->advance_emi) ) @if($lead->finance()->first()->advance_emi == '02') {{ "selected" }} @endif @endif>{{ '02' }}</option>
                                        <option value="03" @if(!empty($lead->finance()->first()->advance_emi) ) @if($lead->finance()->first()->advance_emi == '03') {{ "selected" }} @endif @endif>{{ '03' }}</option>


                                    </select>
                                </label>
                            </section>

                            <section class="col col-6 form-group">
                                <strong class="text text-dark-blue">Choose Bank</strong>
                                <label class="select">
                                    <select name="bank_id" id="bank">
                                        <option value="">--Please Select Bank--</option>
                                        @foreach($banks as $bank)
                                        <option value="{{ $bank->id }}" @if(!empty($lead->finance()->first()->bank_id) ) @if($bank->id == $lead->finance()->first()->bank_id) {{ "selected" }} @endif @endif>{{ ucfirst($bank->bank) }}</option>

                                        @endforeach
                                    </select>
                                </label>
                            </section>



                        </div>
                        <div class="row">
                            <section class=" form-group col col-6" >
                                <strong class="text text-dark-blue">Installment Tenure (months)</strong>
                                <label class="input">
                                    <i class="icon-append fa fa-times"></i>
                                    <input type="text" name="tenure" id="tenure" placeholder="Installment tenure" @if(empty($lead->finance()->first()->tenure)) value="{{  $lead->tenure }}" @else value="{{ $lead->finance()->first()->tenure }}" @endif>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter how old is the Vehicle</b>

                                </label>
                            </section>

                            <section class=" form-group col col-6" >
                                <strong class="text text-dark-blue">Date Of Dispersement</strong>
                                <label class="input">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" name="disbursement" id="disbursement" placeholder="" class="dob" @if(!empty($lead->finance()->first()->disbursement)) value="{{  date('d/m/Y', strtotime($lead->finance()->first()->disbursement ))}}" @endif>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Date of Dispersement</b>

                                </label>
                            </section>


                        </div>
                        <div class="row">

                            <section class=" form-group col col-6" >
                                <strong class="text text-dark-blue">First Installment Date</strong>
                                <label class="input">

                                    <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" name="first_installment" id="first_installment" placeholder="{{ date('m/d/Y') }}" readonly @if(!empty($lead->finance()->first()->first_installment)) value="{{  date('d/m/Y',strtotime($lead->finance()->first()->first_installment)) }}" @endif>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Installment Date</b>


                                </label>
                            </section>

                            <section class=" form-group col col-6">
                                <strong class="text text-dark-blue">Last Installment Date</strong>
                                <label class="input">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" class="fi_date" name="last_installment" id="last_installment" placeholder="" @if(!empty($lead->finance()->first()->last_installment)) value="{{  date('m/d/Y',strtotime($lead->finance()->first()->last_installment)) }}" @endif>
                                    <b class="tooltip tooltip-bottom-right">Enter Last Installment Date</b>

                                </label>
                            </section>

                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="row">

                            <section class=" form-group col col-6">
                                <strong class="text text-dark-blue">Upload 1</strong>
                                <label for="file" class="input input-file">
                                    <div class="button"><input type="file" name="upload_1" id="upload_1" onchange="readURL(this);" multiple=""
                                            >Browse
                                    </div>
                                    <input type="text" placeholder="Include some file" readonly="">
                                    <img class="img-thumbnail class_upload_1" @if(!empty($lead->finance()->first()->upload_1)) src='{{ asset("upload/{$lead->finance()->first()->upload_1}") }}' @else src="" @endif alt="No preview" height="50" width="100">
                                </label>
                            </section>

                            <section class=" form-group col col-6">
                                <strong class="text text-dark-blue">Upload 2</strong>
                                <label for="file" class="input input-file">
                                    <div class="button"><input type="file" name="upload_2" id="upload_2" onchange="readURL(this);" multiple=""
                                            >Browse
                                    </div>
                                    <input type="text" placeholder="Include some file" readonly="">
                                    <img class="img-thumbnail class_upload_2" @if(!empty($lead->finance()->first()->upload_2)) src='{{ asset("upload/{$lead->finance()->first()->upload_2}") }}' @else src="" @endif alt="No Preview" height="50" width="100">
                                </label>
                            </section>

                        </div>

                        <div class="row">

                            <section class=" form-group col col-6">
                                <strong class="text text-dark-blue">Upload 3</strong>
                                <label for="file" class="input input-file">
                                    <div class="button"><input type="file" name="upload_3" id="upload_3" onchange="readURL(this);" multiple=""
                                            >Browse
                                    </div>
                                    <input type="text" placeholder="Include some file" readonly="">
                                    <img class="img-thumbnail class_upload_3" @if(!empty($lead->finance()->first()->upload_3)) src='{{ asset("upload/{$lead->finance()->first()->upload_3}") }}' @else src="" @endif alt="No preview" height="50" width="100">
                                </label>
                            </section>

                            <section class=" form-group col col-6">
                                <strong class="text text-dark-blue">Upload 4</strong>
                                <label for="file" class="input input-file">
                                    <div class="button"><input type="file" name="upload_4" id="upload_4" onchange="readURL(this);" multiple=""
                                            >Browse
                                    </div>
                                    <input type="text" placeholder="Include some file" readonly="">
                                    <img class="img-thumbnail class_upload_4" @if(!empty($lead->finance()->first()->upload_4)) src='{{ asset("upload/{$lead->finance()->first()->upload_4}") }}' @else src="" @endif alt="No Preview" height="50" width="100">
                                </label>
                            </section>

                        </div>

                    </fieldset>
                    <!-------------- Customer verifications and proof -------------->

                    <footer>
                        <button class="btn btn-default" type="submit" >Update Customer</button>
                    </footer>
                    </form>
                </div>
            </div>

        </div>
        <!-- /Inner Row Col-md-12 -->


    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->
<script>

    function last_install(install){
        //alert("I have been called!");
        var advance_emi = $("#advance_emi").val();
        var tenure = $("#tenure").val().split(" ");

        var cal_tenure = tenure[0]-advance_emi;
        //alert(cal_tenure);

        if(tenure.length == 1){
            tenure[1] = "months";
            alert(tenure[1]);
        }
        var add_tenure = moment(install).add(cal_tenure,tenure[1]).format("DD/MM/YYYY");
        $("#last_installment").val(add_tenure);

    }


    $(document).ready(function () {


        $("#disbursement").blur(function(){
            var disburse = $(this).val();
            var bank_id = $("#bank").val();
            if(bank_id == ""){
                $('#proof-text').text("Please slect Bank first !");
                $('#proof_message').modal('show');
            }
            else{
                $.get("{{ URL::to('admin/approve/bank')}}",{'id':bank_id},function(data){
                    var bank = data;

                    var date = moment(disburse).format('DD');
                    var year = moment(disburse).format('YYYY');

                    if(bank == "HDFC"){
                        if(date <= 20){
                            var add_month = moment(disburse).add(1,'M').format();
                            var month = moment(add_month).format('MM');
                            //var install = month+'/07/'+year;
                            var install = 07+'/'+month+'/'+year;
                            $("#first_installment").val(install);
                            last_install(install);
                        }
                        if(date >= 21){
                            var add_month = moment(disburse).add(3,'M').format();
                            var month = moment(add_month).format('MM');
                            //var install = month+'/05/'+year;
                            var install = 05+'/'+month+'/'+year;
                            $("#first_installment").val(install);
                            last_install(install)
                        }
                    }
                    if(bank == "ICICI"){
                        if(date <= 15){
                            var add_month = moment(disburse).add(1,'M').format();
                            var month = moment(add_month).format("MM");
                            // var install = month+'/01/'+year;
                            var install = 01+'/'+month+'/'+year;
                            $("#first_installment").val(install);

                            last_install(install)

                        }
                        if(date >= 16 && date <= 25){
                            var add_month = moment(disburse).add(1,'M').format();
                            var month = moment(add_month).format("MM");
                            //var install = month+'/10/'+year;
                            var install = 10+'/'+month+'/'+year;
                            $("#first_installment").val(install);

                            last_install(install)
                        }
                        if(date >= 26){
                            var add_month = moment(disburse).add(1,'M').format();
                            var month = moment(add_month).format("MM");
                            // var install = month+'/15/'+year;
                            var install = 15+'/'+month+'/'+year;
                            $("#first_installment").val(install);

                            last_install(install)
                        }
                    }

                });


            }


        });




        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        }),

            $('#finance').validate({
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group

                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                //$('#add').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                //$('#add').modal('hide');

                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                location.reload();
                                //window.location.replace("{{ URL::to('admin/approve/reference/"+data.id+"') }}")
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },

                rules: {
                    bank_id:{
                        required:true
                    },
                    upload_1:{
                        required:false,
                        accept:"image/*"
                    },
                    upload_2:{
                        required:false,
                        accept:"image/*"
                    },
                    upload_3:{
                        required:false,
                        accept:"image/*"
                    },
                    upload_4:{
                        required:false,
                        accept:"image/*"
                    }

                },
                messages: {}
            });
    });




</script>
@stop





