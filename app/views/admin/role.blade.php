@extends('admin/layout')
@section('content')


<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href=""><i class="fa fa-home"></i></a></li>
        <li><a href="">Dashboard</a></li>
        <li class="active">Role & Permissions</li>
    </ul>
</div><!--Breadcrumb-->

<!-- Session message -->
<div class="page-header">
    @if(Session::has('message'))
        <div class="callout callout-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Well done ! </strong>{{ Session::get('message')}}
        </div>
    @endif
</div><!-- Session message -->

<!-- Add permission to roles -->
<div class="row" id="powerwidgets">
    <div class="col-md-12 bootstrap-grid">

    <form class="orb-form" id="registration-form" action="<?php echo  URL::to('admin/role/modules') ?>" method="post" novalidate="novalidate">

    <fieldset>
        <div class="row" id="firm" >
            <section class="col col-2"></section>
            <section class="col col-2">
                <label class="input"> Select Role</label>
            </section>
            <section class="col col-4">
                <label class="select">
                    <select name="role" id="role_id">
                        <option value="0">Select Role</option>
                        @foreach($roles as $role){
                        <option value="{{ $role->id}}" {{ ($role->id == Session::get('role_id') )?"selected":null }} > {{ ucfirst($role->is_superuser) }}</option>
                        }
                        @endforeach
                    </select>
                </label>
            </section>
            <section class="col col-4">
                <label class="select">
                    <input type="submit" value="Enter" class="btn btn-info"/>
                </label>
            </section>

        </div>
        </fieldset>

    </form>

    </div>
</div>
<!-- Add permission to roles -->

<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{ $data->render() }}
                <script>



                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                        }).on( 'draw.dt', function () {

                            $(".permissions").click(function(){

                                var id = $(this).attr("id");

                                module_role=id.split("_");
                                //module_role_id
                                var module_role_id=module_role[1];
                                //access of role for a module
                                var permissions=document.getElementById(id).value;
                                //selected role_id
                                var role_id=document.getElementById("role_id").value;
                                if(role_id==""){
                                    alert('Select role first');
                                }
                                else{
                                   // alert(module_role_id+'---'+permissions+'--'+role_id);
                                    $.ajax({
                                     method:"POST",
                                     data:{module_role_id:module_role_id,role_id:role_id,permissions:permissions},
                                     url:"{{ URL::to('admin/role/updatepermission') }}",
                                      success:function(data){
                                         $('#test').dataTable()._fnAjaxUpdate();
                                      }
                                     });
                                }
                            });
                        //subpermissions
                            $(".sub_permissions").click(function(){

                                var id = $(this).attr("id");

                                module_role=id.split("-");
                                //module_role_id
                                var module_role_id=module_role[0];
                                //access of role for a module
                                var permissions=module_role[1];
                                //selected role_id
                                var role_id=document.getElementById("role_id").value;
                               // alert(module_role_id+'---'+permissions+'--'+role_id);
                                if(role_id==""){
                                    alert('Select role first');
                                }
                                else{
                                    // alert(module_role_id+'---'+permissions+'--'+role_id);
                                    $.ajax({
                                        method:"POST",
                                        data:{module_role_id:module_role_id,role_id:role_id,permissions:permissions},
                                        url:"{{ URL::to('admin/role/updatesubpermission') }}",
                                        success:function(data){
                                           //alert(data);
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                }
                            });
                        });
                </script>
                {{ $data->script() }}
            </div>
        </div>
<script>
   /* $(document).ready(function(){

    $("#role_id").click(function(){

        var id = $(this).val();
        alert(id);
        $.ajax({
            method:"GET",
            data:{id:id},
            url:"{{ URL::to('admin/role/index') }}",
            success:function(data){
//alert(data);
               $('#test').dataTable()._fnAjaxUpdate();
            }
        });
    });
    });*/
</script>
    </div>
@stop
