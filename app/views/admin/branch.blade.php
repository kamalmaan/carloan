@extends('admin/layout')
@section('content')

<style type="text/css">
    .dob {
        z-index: 99999 !important;
    }

    .xdsoft_datetimepicker {
        z-index: 99999 !important;
    }
</style>
<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="index.html">Dashboard</a></li>
        <li class="active">Profile</li>
    </ul>
</div>


<div class="page-header">

    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#add">Add Branch</button>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>

<div class="col-md-12 bootstrap-grid">

    <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
        <header>
            <h2>Bank
                <small>Show Hide Columns</small>
            </h2>
        </header>
        <div class="inner-spacer">
            {{$data->render()}}
            <script>
                $('#test')
                    .on('preXhr.dt', function (e, settings, data) {
                        // on start of ajax call
                    }).on('draw.dt', function () {
                        $(".status").click(function () {
                            var id = $(this).attr("id");
                            $.ajax({
                                method: "GET",
                                data: {id: id},
                                url: "{{ URL::to('admin/users/status') }}",
                                success: function (data) {
                                    $('#test').dataTable()._fnAjaxUpdate();
                                }
                            });
                        });
                        $(".branch_edit").click(function () {
                         var id = $(this).attr("id");
                            $.ajax({
                                    method: "GET",
                                    data: {id: id},
                                    url: "{{ URL::to('admin/branch/edit') }}",
                                    success: function (data) {
                                        data = JSON.parse(data)

                                        $('#edit').modal('show');
                                        $("#address").val(data.record.address);
                                        $('#city_edit').val(data.record.branch);
                                        $('#id').val(data.record.id);
                                    }
                                }, 'json'
                            );
                        });
                        $("#branch_city").click(function () {
                            $.ajax({
                                    method: "GET",
                                    data: {id: id},
                                    url: "{{ URL::to('admin/branch/add') }}",
                                    dataType: 'json',
                                    success: function (data) {
                                        data = JSON.parse(data)
                                        $('#add').modal('show');
                                        $('#city_add').html(data);

                                    }
                                }, 'json'
                            );

                        });

                        $(".delete").click(function () {
                            var id = $(this).attr('id');
                            if (confirm('You really want to delete ?')) {
                                $.ajax({
                                        method: "GET",
                                        data: {id: id},
                                        url: "{{ URL::to('admin/branch/destroy') }}",
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data.status == 'success') {
                                                $('#test').dataTable()._fnAjaxUpdate();
                                            } else {
                                                $('#message_text').text(data.message);
                                                $('#message').modal('show');
                                            }
                                        }
                                    }, 'json'
                                );
                            }
                        })
                    });
            </script>
            {{ $data->script() }}
        </div>
    </div>
</div>



{{--
Branch registration model
--}}
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Branch</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="add_branch" action='{{ URL::to("admin/branch/add") }}'
                      method="post"
                      novalidate="novalidate">

                    <section class="form-group">
                        <label class="select">
                            <select name="branch" id="city" class="">
                                <option value="">Branch City</option>
                                @foreach($city as $row)
                                <option value="{{ $row->id }}">{{$row->city}}</option>
                                @endforeach
                            </select>
                            {{ $errors->first('branch','<em class="invalid text-danger" > :message </em>') }}
                        </label>
                    </section>

                    <section class="form-group">
                        <label class="input"> <i class="icon-append fa fa-car"></i>
                            <input type="text" placeholder="Address" name="address"
                                   value="">
                            <b class="tooltip tooltip-bottom-right">Needed to Address</b>
                            {{ $errors->first('address','<em class="invalid text-danger" > :message </em>') }}
                        </label>
                    </section>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


{{--
Branch Edition registration model
--}}
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Branch</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="edit_branch" action='{{ URL::to("admin/branch/update") }}'
                      method="post"
                      novalidate="novalidate">
                    <input type="hidden" name="id" id="id">
                    <section class="form-group">
                        <label class="select">
                            <select name="branch" id="city_edit" class="">
                                <option value="">Branch</option>
                                @foreach($city as $row)
                                <option value="{{ $row->id }}" >{{$row->city}}</option>
                                @endforeach
                            </select>
                            {{ $errors->first('branch','<em class="invalid text-danger" > :message </em>') }}
                        </label>
                    </section>
                    <section class="form-group">
                        <label class="input"> <i class="icon-append fa fa-car"></i>
                            <input type="text" id="address" placeholder="address" name="address" class=""
                                   value="">
                            <b class="tooltip tooltip-bottom-right">Needed to Address</b>
                            {{ $errors->first('address','<em class="invalid text-danger" > :message </em>') }}
                        </label>
                    </section>



                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {


    /*$('#city').change(function(){
     var city = $(this).val();
     $.ajax({
     method: "GET",
     data: {id: city},
     dataType:'json',
     url: "{{ URL::to('admin/insurance/city') }}",
     success: function (data) {
     if(data == null){
     $("#add_distt").val('');
     } else {
     $("#add_distt").val(data.district);
     }
     }
     });
     });
     */
    /*For addition*/

    /*$('#edit_residential_city').change(function(){
     var city=$(this).val();

     $.ajax({
     methos:"GET",
     data:{id:city},
     dataType:'json',
     url: "{{ URL::to('admin/insurance/city') }}",
     success: function (data) {
     if(data == null){
     $("#residential_distt").val('');
     } else {
     $("#residential_distt").val(data.district);
     }
     }
     })
     })*/


    $('#add_branch').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-add-body', 'show');
                //var l = Ladda.create($('[type=submit]', form)[0]);
                //l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        console.log('data');

                        if (data.status == 'fail') {
                            //$('#add').modal('hide');
                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            //$('input', form).iCheck('update');

                            // toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#add').modal('hide');
                            $('#test').dataTable()._fnAjaxUpdate();
                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                        } else {

                        }

                        //l.stop();
                        //uiLoader('#form-add-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                branch: {
                    required: true
                },
                address:{
                    required: true
                }

            },
            messages: {}
        }
    );




    $('#edit_branch').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-add-body', 'show');
                //var l = Ladda.create($('[type=submit]', form)[0]);
                //l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        console.log('data');

                        if (data.status == 'fail') {
                            //$('#add').modal('hide');
                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            //$('input', form).iCheck('update');

                            // toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#add').modal('hide');
                            $('#edit').modal('hide');
                            $('#test').dataTable()._fnAjaxUpdate();
                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                        } else {

                        }

                        //l.stop();
                        //uiLoader('#form-add-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                branch: {
                    required: true
                },
                address:{
                    required: true
                }


            },
            messages: {}
        }
    );

})


</script>

@stop
