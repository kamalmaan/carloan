@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Co-Applicant</li>
    </ul>
</div>

<div class="page-header">

    <a href='{{ URL::to("admin/co-applicant/add/$lead_id")}}'><button  type="button" class="btn btn-info">Add Co-Applicant</button></a>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


<div class="col-md-12 bootstrap-grid">


    <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
        <header>
            <h2>Product<small>Show Hide Columns</small></h2>
        </header>
        <div class="inner-spacer">
            {{ $data->render() }}
            <script>
                $('#test')
                    .on('preXhr.dt', function (e, settings, data) {
                        // on start of ajax call
                    }).on( 'draw.dt', function () {
                        // on table rendered

                        $(".delete").click(function(){
                            var id = $(this).attr("id");
                            $("#confirm_message").modal("show");
                            $("#confirm_text").text("Confirm Submit !");
                            $("#yesconfirm").click(function(){
                                //alert(lead_id);
                                $.get("{{ URL::to('admin/co-applicant/delete') }}",{'id':id},function(data){
                                    if(data.status == 'success'){
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                        $('#test').dataTable()._fnAjaxUpdate();
                                    }
                                },'json');
                            });
                        });

                    });

            </script>
            {{ $data->script() }}
        </div>
    </div>


</div>

<script>
    $().ready(function() {


        $('#decline_lead').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs

                    $(element).closest('.inline-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('#add').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#declines').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    reason: {
                        required: true

                    }

                },
                messages: {}
            }
        );



    });
</script>


<!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

@stop
