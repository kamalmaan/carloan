@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/insurancecompany') }}">Insurance Company</a></li>
        <li class="active">Insurance Branch</li>
    </ul>
</div>

<div class="page-header">

    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#insurancebranch">Add Insurance Branch</button>
    <br>
    @if(Session::has('message'))
        <div class="callout callout-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong >Well done ! </strong>{{ Session::get('message')}}
        </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{ $data->render() }}
                <script>
                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                        }).on( 'draw.dt', function () {
                                $(".status").click(function(){
                                    var id = $(this).attr("id");                                
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/insurancecompany/branchstatus') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".branch").click(function(){
                                   var id = $(this).attr("id");
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/insurancecompany/branchedit') }}",
                                        success:function(data){
                                            data = JSON.parse(data)
                                            $('#edit_form').modal('show');
                                            document.getElementById("insurance_branch_id").value=data.record.id;                                            
                                            document.getElementById("insurance_branch_branch").value=data.record.branch;
                                            document.getElementById("insurance_branch_name").value=data.record.name;                                            
                                            document.getElementById("insurance_branch_phone").value=data.record.phone;                                                                                
                                            }
                                        },'json'
                                    );
                                });
                        });
                </script>

                {{ $data->script() }}
            </div>
        </div>

    </div>


    <div class="modal fade" id="edit_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Company</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="insurancebranch_edit" action='{{ URL::to("admin/insurancecompany/branchupdate") }}' method="post" novalidate="novalidate">

                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="Insurance Branch" name="branch" value="" id="insurance_branch_branch"/>
                                    <input type="hidden" placeholder="" name="id" value="" id="insurance_branch_id"/>                                   
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="Branch Name" name="name" value="" id="insurance_branch_name">                                    
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="Phone" name="phone" value="" id="insurance_branch_phone">                                    
                                </label>                                
                            </section>                            
                            
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="insurancebranch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Insurance Branch</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="insurancebranch_add" action='{{ URL::to("admin/insurancecompany/branchsave") }}' method="post" novalidate="novalidate">
                        
                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="hidden" placeholder="" name="id" value="{{ $id }}">
                                    <input type="text" placeholder="Insurance Branch" name="branch" value="">                                    
                                </label>                                
                            </section>
                             <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="email" placeholder="Branch Name" name="name" value="">                                    
                                </label>
                            </section> 
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="number" placeholder="Phone" name="phone" value="">                                    
                                </label>
                            </section>                                                                                
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
<script>

    $().ready(function() {

        $('#insurancebranch_add').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('#insurancebranch').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#insurancebranch').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    branch: {
                        required: true
                    },
                    name: {
                        required: true
                    },
                    phone: {
                        required: true
                    }                   
                },
                messages: {                  
                }
            }
        );
        //eidt district



        $('#insurancebranch_edit').validate(
            { 
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                
                                $('#edit_form').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {


                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#edit_form').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    branch: {
                        required: true
                    },
                    name: {
                        required: true
                    },
                    phone: {
                        required: true
                    }                    

                },
                messages: {

                }
            }
        );


    });

</script>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->


@stop
