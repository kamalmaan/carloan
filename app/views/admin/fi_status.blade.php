@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">{{ $breadcrum }}</li>
    </ul>
</div>

<div class="page-header">
    <a href="{{ URL::to('admin/lead') }}">
        <button type="button" class="btn btn-info btn-xs">Lead</button>
    </a>
    <a href="{{ URL::to('admin/lead/decline') }}">
        <button type="button" class="btn btn-info btn-xs">Declined Lead</button>
    </a>



    <?php $segment = Request::segment(3) ?>
     @if($segment == "underwriting")
        <a href="{{ URL::to('admin/fi') }}">
            <button type="button" class="btn btn-info btn-xs">Fi Status</button>
        </a>
        <a href="{{ URL::to('admin/fi/underwriting') }}">
            <button type="button" class="btn btn-success btn-xs">Under Writing</button>
        </a>
        @else
        <a href="{{ URL::to('admin/fi') }}">
            <button type="button" class="btn btn-success btn-xs">Fi Status</button>
        </a>
        <a href="{{ URL::to('admin/fi/underwriting') }}">
            <button type="button" class="btn btn-info btn-xs">Under Writing</button>
        </a>
        @endif

    <a href="{{ URL::to('admin/underwritingapprovedlead') }}">
        <button type="button" class="btn btn-info btn-xs">Under Writing Approved</button>
    </a>
    <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
        <button type="button" class="btn btn-info btn-xs">Under Approve Decline</button>
    </a>
    <a href="{{ URL::to('admin/temporarylead') }}">
        <button type="button" class="btn btn-info btn-xs">Temporary Customer</button>
    </a>
    <a href="{{ URL::to('admin/temporarylead/temporarydecline') }}">
        <button type="button" class="btn btn-info btn-xs">Temporary Decline Customer</button>
    </a>
    <a href="{{ URL::to('admin/permanent') }}">
        <button type="button" class="btn btn-info btn-xs">Permanent Customer</button>
    </a>

    <br>


    <br>
    @if(Session::has('message'))
        <div class="callout callout-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong >Well done ! </strong>{{ Session::get('message')}}
        </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                FI Status
            </header>
            <div class="inner-spacer">
                {{ $data->render() }}
                <script>
                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                        }).on( 'draw.dt', function () {
                                $(".accept").click(function(){
                                    var id = $(this).attr("id")
                                    //alert(id);
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id,status:'accept'},
                                        url:"{{ URL::to('admin/fi/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".reject").click(function(){
                                    var id = $(this).attr("id")
                                    //alert(id);
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id,status:'reject'},
                                        url:"{{ URL::to('admin/fi/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                
                            $(".decline").click(function(){
                                var id = $(this).attr("id")
                                //alert(id);
                                $.ajax({
                                    method:"GET",
                                    data:{id:id,status:'decline'},
                                    url:"{{ URL::to('admin/fi/status') }}",
                                    success:function(data){
                                        $('#test').dataTable()._fnAjaxUpdate();
                                    }
                                });
                            });
                            
                            $('.apply_again').click(function(){
                                var id = $(this).attr("id");
                                //alert(id);
                                $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/fi/bank') }}",
                                    dataType:'json',
                                    success:function(data){
                                        $('#fi').modal('show');
                                        $('#fi_lead_id').val(id);
                                        $('#branch').html(data);
                                    }
                                })

                            });
                            $(".los_number").click(function(){
                                var id = $(this).attr("id")
                                //alert(id);
                                $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/fi/lead') }}",
                                    dataType:'json',
                                    success:function(data){
                                        console.log(data.amount);
                                        //alert(data.id+);
                                        $('#los_number').modal('show');
                                        $('#los_lead_id').val(data.lead_id);
                                        $('#fi_id').val(data.fi_id);
                                        $('#applied_loan_amount').val(data.amount);

                                    }
                                });
                            });
                            $(".send_mail").click(function(){
                                var id = $(this).attr("id")
                                //alert(id);
                                $('#mail').modal('show');
                                $('#lead_id').val(id);
                            });
                            $(".mail_direct").click(function(){
                                var id = $(this).attr("id")
                                //alert(id);
                                $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/fi/mail') }}",
                                    dataType:'json',
                                    success:function(data){
                                        if(data.status == 'success'){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                            $('#message_text').text(data.message);
                                            $('#message').modal('show');
                                        }
                                        else{
                                            alert("fail");
                                        }

                                    }
                                });

                            });
                        });
                </script>
                {{ $data->script() }}
            </div>
        </div>

    </div>

<!--    Apply Again Modal-->


    <div class="modal fade" id="fi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Decline Lead</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="fi_initiation" action='{{ URL::to("admin/fi/sendfi") }}' method="post" novalidate="novalidate">
                        <fieldset>
                            <input type="hidden" value="" name="lead_id" id="fi_lead_id">

                            <section class="form-group">
                                <strong class="text text-dark-blue">List Of Banks</strong>
                                <label class="select">
                                    <select name="branch" class="" id="branch">
                                        <option value="">--Please Select Category--</option>
                                    </select>

                                </label>
                            </section>
                            <strong class="text text-dark-blue dynamic" style="display: none">Branches</strong>
                            <div class="powerwidget cold-grey dynamic" id="" data-widget-editbutton="false" style="display: none">

                                <table class="table table-striped table-hover margin-0px" id="table">
                                    <thead>
                                    <tr>
                                        <th>Contact Person</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Choose One</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>

                                </table>

                            </div>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Send File For FI Initiation</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


<!--     SEND MAIL-->


    <div class="modal fade" id="mail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-dark-blue" id="myModalLabel">Upload Excel Sheet</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="mail_form" action='{{ URL::to("admin/fi/mail") }}' method="post" novalidate="novalidate">
                        <fieldset>
                            <input type="hidden" value="" name="lead_id" id="lead_id">

                            <section class=" form-group col col-5">

                                <label for="file" class="form-group input input-file">
                                    <div class="button"><input type="file" name="file" id="id_passport_7" onchange="readURL(this);" multiple=""
                                            >Upload FI Excel Sheet
                                    </div>
                                    <!--<input type="text" placeholder="Include some file" readonly="">-->

                                </label>



                            </section>
                            <section class="col col-7">
                                <span class="alert alert-info blah_7" style="display:none"></span>
                                <!--<img class="img-thumbnail blah_7" src="" height="50" width="100" style="display:none">-->
                            </section>

                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Mail File For FI Initiation</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

<!--    Nos Number -->


    <div class="modal fade" id="los_number" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Appove With NOS Number</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="add_los_number" action='{{ URL::to("admin/underwritingapprovedlead/losnumber") }}' method="post" novalidate="novalidate">
                        <fieldset>
                            <input type="hidden" value="" name="lead_id" id="los_lead_id">
                            <input type="hidden" value="" name="fi_id" id="fi_id">


                            <section class="form-group">
                                <strong class="text text-dark-blue">Applied Laon Amount</strong>
                                <label class="input">
                                    <i class="icon-append fa fa-money"></i>
                                    <input type="number"value="" name="applied_loan_amount" id="applied_loan_amount" readonly >
                            </section>
                            <section class="form-group">
                                <strong class="text text-dark-blue">Approved Laon Amount</strong>
                                <label class="input">
                                    <i class="icon-append fa fa-money"></i>
                                    <input type="number"value="" name="approve_loan_amount" >
                            </section>
                            <section class="form-group">
                                <strong class="text text-dark-blue">LOS Number</strong>
                                <label class="input">
                                    <i class="icon-append fa fa-money"></i>
                                    <input type="text"value="" name="approve_los_number" >
                            </section>
                            <section class="form-group">
                                <strong class="text text-dark-blue">Special Condition</strong>
                                <label class="input">
                                    <i class="icon-append fa fa-money"></i>
                                    <input type="text"value="" name="special_condition" >
                            </section>

                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Forward with LOS Number</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <script>


    function readURL(input) {
        //console.log(input);
        var id = input.id;
        var t = id.split("_");
        //alert(t[2]);
        if (input.files && input.files[0]) {


            var reader = new FileReader();

            reader.onload = function (e) {

                $('.blah_'+t[2]).css("display","block");
                $('.blah_'+t[2]).append("File Is Uploaded !");
            }

            reader.readAsDataURL(input.files[0]);

        }

    }



        $().ready(function() {

            $("#branch").change(function(){
                //$('#fi').modal('hide');
                var branch_id = $(this).val();
                //alert(branch_id);
                $.ajax({
                    method:"GET",
                    data:{branch:branch_id},
                    dataType:'json',
                    url:"{{ URL::to('admin/fi/bankbranches') }}",
                    success:function(data){

                        console.log(data);
                        var cars = data;
                        var i = 0;
                        var test = "";

                        while (cars[i]) {

                            var link = "{{ URL::to('admin/lead/save') }}"+"/"+cars[i]['id'];

                            test += "<tr><td>"+cars[i]['contact_person']+"</td><td>"+cars[i]['phone']+"</td><td>"+cars[i]['email']+"</td><td><section class='form-group'><label for='radio'><input type='radio' name='bankbranch_id' value='"+cars[i]['id']+"'></label></section></td></tr>";

                            i++;
                        }
                        $('#table tbody').html(test);
                    }
                })
                $('.dynamic').css({display:'block'});
            });


            $('#fi_initiation').validate(
                {
                    ignore: [],
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element) { // hightlight error inputs

                        $(element).closest('.inline-group').addClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                    submitHandler: function (form) {
                        form = $(form);
                        $('[type=submit]', form).attr('disabled', 'disabled');
                        //uiLoader('#form-add-body', 'show');
                        //var l = Ladda.create($('[type=submit]', form)[0]);
                        //l.start();
                        form.ajaxSubmit({
                            dataType: 'json',
                            success: function (data) {
                                console.log('data')

                                if (data.status == 'fail') {
                                    $('#add').modal('hide');
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');

                                    $('[type=submit]', form).removeAttr('disabled');
                                } else if (data.status == 'success') {

                                    form[0].reset();
                                    $('.form-group', form).removeClass('has-error');
                                    //$('input', form).iCheck('update');

                                    // toastr['success'](data.message);
                                    $('[type=submit]', form).removeAttr('disabled');
                                    $('#fi').modal('hide');
                                    $('#test').dataTable()._fnAjaxUpdate();
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');
                                } else {

                                }

                                //l.stop();
                                //uiLoader('#form-add-body', 'hide');
                            }
                        });

                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        fi_lead_id: {
                            required: true

                        },
                        branch:{
                            required:true
                        },
                        bankbranch_id:{
                            required:true
                        }

                    },
                    messages: {}
                }
            );


            $('#mail_form').validate(
                {
                    ignore: [],
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element) { // hightlight error inputs

                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group

                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                    submitHandler: function (form) {
                        form = $(form);
                        $('[type=submit]', form).attr('disabled', 'disabled');
                        //uiLoader('#form-add-body', 'show');
                        //var l = Ladda.create($('[type=submit]', form)[0]);
                        //l.start();
                        form.ajaxSubmit({
                            dataType: 'json',
                            success: function (data) {
                                console.log('data')

                                if (data.status == 'fail') {
                                    $('#add').modal('hide');
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');

                                    $('[type=submit]', form).removeAttr('disabled');
                                } else if (data.status == 'success') {

                                    form[0].reset();
                                    $('.form-group', form).removeClass('has-error');
                                    //$('input', form).iCheck('update');

                                    // toastr['success'](data.message);
                                    $('[type=submit]', form).removeAttr('disabled');
                                    $('#mail').modal('hide');
                                    $('#test').dataTable()._fnAjaxUpdate();
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');
                                } else {

                                }

                                //l.stop();
                                //uiLoader('#form-add-body', 'hide');
                            }
                        });

                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        file: {
                            required: true,
                            extension: "xls"

                        }

                    },
                    messages: {
                        file:"Invalid Excel Format !"
                    }
                }
            );


            $('#add_los_number').validate(
                {
                    ignore: [],
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element) { // hightlight error inputs

                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                    submitHandler: function (form) {
                        form = $(form);
                        $('[type=submit]', form).attr('disabled', 'disabled');
                        //uiLoader('#form-add-body', 'show');
                        //var l = Ladda.create($('[type=submit]', form)[0]);
                        //l.start();
                        form.ajaxSubmit({
                            dataType: 'json',
                            success: function (data) {
                                console.log('data')

                                if (data.status == 'fail') {
                                    $('#add').modal('hide');
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');

                                    $('[type=submit]', form).removeAttr('disabled');
                                } else if (data.status == 'success') {

                                    form[0].reset();
                                    $('.form-group', form).removeClass('has-error');
                                    //$('input', form).iCheck('update');

                                    // toastr['success'](data.message);
                                    $('[type=submit]', form).removeAttr('disabled');
                                    $('#los_number').modal('hide');
                                    $('#test').dataTable()._fnAjaxUpdate();
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');
                                } else {

                                }

                                //l.stop();
                                //uiLoader('#form-add-body', 'hide');
                            }
                        });

                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    rules: {
                        applied_loan_amount: {
                            required: true

                        },
                        approve_loan_amount:{
                            required:true
                        },
                        approve_los_number:{
                            required:true
                        },
                        special_condition:{
                            required:true
                        }

                    },
                    messages: {}
                }
            );


        })
    </script>



</div>
<!-- /Inner Row Col-md-6 -->


@stop