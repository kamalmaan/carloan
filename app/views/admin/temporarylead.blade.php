@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">{{ $breadcrum }}</li>
    </ul>
</div>

<div class="page-header">


    <br>
    @if(Session::has('message'))
        <div class="callout callout-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong >Well done ! </strong>{{ Session::get('message')}}
        </div>
    @endif

</div>
             <a href="{{ URL::to('admin/lead') }}">
                <button type="button" class="btn btn-info btn-xs">Lead</button>
             </a>
            <a href="{{ URL::to('admin/lead/decline') }}">
               <button type="button" class="btn btn-info btn-xs">Declined Lead</button>
            </a>
            <a href="{{ URL::to('admin/fi') }}">
                <button type="button" class="btn btn-info btn-xs">Fi Status</button>
            </a>
            <a href="{{ URL::to('admin/fi/underwriting') }}">
               <button type="button" class="btn btn-info btn-xs">Under Writing</button>
            </a>


      <?php


      $segment = Request::segments();
    //print_r($segment);
     $total_segment=count($segment);
    //echo $total_segment;
      $segment1 = Request::segment($total_segment);
      $segment2 = Request::segment($total_segment-1);
      //echo $segment2;
     //echo $segment1;

      ?>

            @if($segment1 == "underwritingapprovedlead")
              <a href="{{ URL::to('admin/underwritingapprovedlead') }}">
                <button type="button" class="btn btn-success btn-xs">Under Writing Approved</button>
              </a>
             <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
               <button type="button" class="btn btn-info btn-xs">Under Approve Decline</button>
             </a>
            <a href="{{ URL::to('admin/temporarylead') }}">
              <button type="button" class="btn btn-info btn-xs">Temporary Customer</button>
            </a>
            <a href="{{ URL::to('admin/temporarylead/temporarydecline') }}">
               <button type="button" class="btn btn-info btn-xs">Temporary Decline Customer</button>
            </a>

            <a href="{{ URL::to('admin/permanent') }}">
                <button type="button" class="btn btn-info btn-xs">Permanent Customer</button>
            </a>


            @elseif($segment2 == "temporarylead")
                    <a href="{{ URL::to('admin/underwritingapprovedlead') }}">
                        <button type="button" class="btn btn-info btn-xs">Under Writing Approved</button>
                    </a>
                    <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
                        <button type="button" class="btn btn-info btn-xs">Under Approve Decline</button>
                    </a>
                    <a href="{{ URL::to('admin/temporarylead') }}">
                        <button type="button" class="btn btn-info btn-xs">Temporary Customer</button>
                    </a>
                    <a href="{{ URL::to('admin/temporarylead/temporarydecline') }}">
                        <button type="button" class="btn btn-success btn-xs">Temporary Decline Customer</button>
                        </a>
                        <a href="{{ URL::to('admin/permanent') }}">
                            <button type="button" class="btn btn-info btn-xs">Permanent Customer</button>
                        </a>

                        @elseif($segment2 == "underwritingapprovedlead")
                        <a href="{{ URL::to('admin/underwritingapprovedlead') }}">
                            <button type="button" class="btn btn-info btn-xs">Under Writing Approved</button>
                        </a>
                        <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
                            <button type="button" class="btn btn-success btn-xs">Under Approve Decline</button>
                        </a>
                        <a href="{{ URL::to('admin/temporarylead') }}">
                            <button type="button" class="btn btn-info btn-xs">Temporary Customer</button>
                        </a>
                        <a href="{{ URL::to('admin/temporarylead/temporarydecline') }}">
                            <button type="button" class="btn btn-info btn-xs">Temporary Decline Customer</button>
                        </a>
                        <a href="{{ URL::to('admin/permanent') }}">
                            <button type="button" class="btn btn-info btn-xs">Permanent Customer</button>
                        </a>


               @elseif($segment1 == "temporarylead")
                        <a href="{{ URL::to('admin/underwritingapprovedlead') }}">
                            <button type="button" class="btn btn-info btn-xs">Under Writing Approved</button>
                        </a>
                        <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
                            <button type="button" class="btn btn-info btn-xs">Under Approve Decline</button>
                        </a>
                    <a href="{{ URL::to('admin/temporarylead') }}">
                        <button type="button" class="btn btn-success btn-xs">Temporary Customer</button>
                    </a>
                    <a href="{{ URL::to('admin/temporarylead/temporarydecline') }}">
                        <button type="button" class="btn btn-info btn-xs">Temporary Decline Customer</button>
                    </a>
                    <a href="{{ URL::to('admin/permanent') }}">
                        <button type="button" class="btn btn-info btn-xs">Permanent Customer</button>
                    </a>


              @elseif($segment1 == "permanent")
                        <a href="{{ URL::to('admin/underwritingapprovedlead') }}">
                            <button type="button" class="btn btn-info btn-xs">Under Writing Approved</button>
                        </a>
                        <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
                            <button type="button" class="btn btn-info btn-xs">Under Approve Decline</button>
                        </a>
                        <a href="{{ URL::to('admin/temporarylead') }}">
                            <button type="button" class="btn btn-info btn-xs">Temporary Customer</button>
                        </a>
                        <a href="{{ URL::to('admin/temporarylead/temporarydecline') }}">
                            <button type="button" class="btn btn-info btn-xs">Temporary Decline Customer</button>
                        </a>
                        <a href="{{ URL::to('admin/permanent') }}">
                            <button type="button" class="btn btn-success btn-xs">Permanent Customer</button>
                        </a>


        @endif

<?php /*$segment = Request::segment(3) */?><!--
          @if($segment == "temporarydecline")
         <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
            <button type="button" class="btn btn-success btn-xs">Under Approve Decline</button>
         </a>

          @else

         <a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
           <button type="button" class="btn btn-success btn-xs">Under Approve Decline</button>
         </a>
          @endif
--><!--
        <a href="{{ URL::to('admin/permanent') }}">
            <button type="button" class="btn btn-info btn-xs">Permanent Customer</button>
        </a>-->


<!--<a href="{{ URL::to('admin/underwritingapprovedlead') }}">
    <button type="button" class="btn btn-success btn-xs">Under Writing Approved</button>
</a>
<a href="{{ URL::to('admin/underwritingapprovedlead/temporarydecline') }}">
    <button type="button" class="btn btn-info btn-xs">Under Approve Decline</button>
</a>-->



<br><br>


<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{ $data->render() }}
                <script>
                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                        }).on( 'draw.dt', function () {
                                $(".decline_temporary").click(function(){
                                    var id = $(this).attr("id")
                                    //alert(id);
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id,status:'decline'},
                                        url:"{{ URL::to('admin/underwritingapprovedlead/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".decline_temporary_customer").click(function(){
                                    var id = $(this).attr("id")
                                    //alert(id);
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id,status:'decline'},
                                        url:"{{ URL::to('admin/temporarylead/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".recycle_temporary").click(function(){
                                    var id = $(this).attr("id")
                                    //alert(id);
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id,status:'recycle'},
                                        url:"{{ URL::to('admin/underwritingapprovedlead/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                            $(".recycle_temporary_customer").click(function(){
                                var id = $(this).attr("id")
                                //alert(id);
                                $.ajax({
                                    method:"GET",
                                    data:{id:id,status:'recycle'},
                                    url:"{{ URL::to('admin/temporarylead/status') }}",
                                    success:function(data){
                                        $('#test').dataTable()._fnAjaxUpdate();
                                    }
                                });
                            });
                                $(".final").click(function(){
                                    var id = $(this).attr("id")
                                    //alert(id);
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/underwritingapprovedlead/final') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".rc").click(function(){
                                    var id = $(this).attr("id")
                                    var rc = $(this).val();
                                    //alert(rc);
                                        $("#id").val(id);
                                        $("#rc_val").val(rc);
                                        $('#rc').modal('show');

                                });

                        });
                </script>
                {{ $data->script() }}
            </div>
        </div>
        <div class="modal fade" id="rc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add RC</h4>
                    </div>
                    <div class="modal-body">
                        <form class="orb-form cmxform" id="add_rc" action='{{ URL::to("admin/permanent/rc") }}' method="post" novalidate="novalidate">
                            <input type="hidden" placeholder="" name="id" value="" id="id">
                            <fieldset>
                                <section class="form-group">
                                    <label class="input"> <i class="icon-append fa fa-file-text"></i>
                                        <input type="text" placeholder="RC" name="rc" id="rc_val" value="">

                                        <b class="tooltip tooltip-bottom-right">Needed to enter RC</b>

                                    </label>
                                </section>
                            </fieldset>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-info">Save changes</button>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- /End Widget -->
    <script>
        $().ready(function() {

            $('#add_rc').validate(
                {
                    ignore: [],
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function (element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                    submitHandler: function (form) {
                        form = $(form);
                        $('[type=submit]', form).attr('disabled', 'disabled');
                        //uiLoader('#form-add-body', 'show');
                        //var l = Ladda.create($('[type=submit]', form)[0]);
                        //l.start();
                        form.ajaxSubmit({
                            dataType: 'json',
                            success: function (data) {
                                console.log('data')

                                if (data.status == 'fail') {
                                    $('#add').modal('hide');
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');
                                    $('[type=submit]', form).removeAttr('disabled');
                                } else if (data.status == 'success') {

                                    form[0].reset();
                                    $('.form-group', form).removeClass('has-error');
                                    //$('input', form).iCheck('update');

                                    // toastr['success'](data.message);
                                    $('[type=submit]', form).removeAttr('disabled');
                                    $('#add').modal('hide');
                                    $('#test').dataTable()._fnAjaxUpdate();
                                    $('#message_text').text(data.message);
                                    $('#message').modal('show');
                                } else {

                                }

                                //l.stop();
                                //uiLoader('#form-add-body', 'hide');
                            }
                        });

                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    rules: {

                    },
                    messages: {}
                }
            );
        //eidt district
        });
    </script>


</div>
<!-- /Inner Row Col-md-6 -->


@stop