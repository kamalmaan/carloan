@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/lead') }}">Lead</a></li>
        <li class="active">Asset Deatail</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">

    <a @if(empty($id)) href='{{ URL::to("admin/approve/index/$lead->id")}}' @endif @if(!empty($id)) href='{{ URL::to("admin/approve/index/$id->id") }}' @endif>
        <button type="button" class="btn btn-info">Personal Detail</button>
    </a>
    <a href='#'>
        <button class="btn btn-success">Asset Details</button>
    </a>
    <a @if(empty($id)) href='{{ URL::to("admin/approve/reference/$lead->id")}}' @endif @if(!empty($id))  href='{{ URL::to("admin/approve/reference/$id->id")}}' @endif>
        <button type="button" class="btn btn-info">Reference Detail</button>
    </a>
    @if(empty($id))
        @if(!empty($lead->temporary()->first()->lead_id))
            @if(!empty($lead->temporary()->first()->los_number))
                @if($lead->temporary()->first()->decline == 1)

                    <a @if(empty($id)) href='{{ URL::to("admin/approve/finance/$lead->id")}}' @endif @if(!empty($id)) href='{{ URL::to("admin/approve/finance/$id->id")}}' @endif>
                        <button type="button" class="btn btn-info">Financial Data</button>
                    </a>

                @endif
            @endif
        @endif
    @endif
    @if(!empty($id))
        @if(!empty($id->temporary()->first()->lead_id))
            @if(!empty($id->temporary()->first()->los_number))
                @if($id->temporary()->first()->decline == 1)

                    <a @if(empty($id)) href='{{ URL::to("admin/approve/finance/$lead->id")}}' @endif @if(!empty($id)) href='{{ URL::to("admin/approve/finance/$id->id")}}' @endif>
                        <button type="button" class="btn btn-success">Financial Data</button>
                    </a>

                @endif
            @endif
        @endif
    @endif
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


<div class="col-md-12 bootstrap-grid">


<div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">

<div data-widget-editbutton="false" id="registration-form-validation-widget"
     class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
<header role="heading">
    <h2>
        <Lead></Lead>
        <small>You Can Add Your Lead Type</small>
    </h2>
    <div class="powerwidget-ctrls" role="menu"><a class="button-icon powerwidget-delete-btn" href="#"><i
                class="fa fa-times-circle"></i></a> <a class="button-icon powerwidget-fullscreen-btn" href="#"><i
                class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i
                class="fa fa-chevron-circle-up "></i></a></div>
    <span class="powerwidget-loader"></span></header>
<div class="inner-spacer" role="content">
<form class="orb-form" id="registration-form" @if(empty($id)) action="{{ URL::to('admin/approve/asset/'.$lead->id)}}" @endif @if(!empty($id)) action="{{ URL::to('admin/approve/asset/'.$id->id)}}" @endif method="post"
      novalidate="novalidate" enctype="multipart/form-data">
<header></header>
<fieldset>
    <div class="row">

        <section class="col col-6 form-group">
            <strong class="text text-dark-blue">Asset Type for loan</strong>
                <label class="select">
                    <select name="product_id">
                        <option value="">--Please Select Asset Type--</option>
                        @foreach($product as $products) {
                        <option value="{{ $products->id }}" @if($products->id == $lead->product_id) {{ "selected" }} @endif>{{ ucfirst($products->product) }}</option>
                        }
                        @endforeach
                    </select>
                </label>
        </section>

        <section class="  form-group  col col-6" >
            <strong class="text text-dark-blue">Asset Model Variant</strong>
            <label class="select">
                <select name="vehicle_id">
                    <option value="">--Please Select Vehicle Type--</option>
                    @foreach($vehicle as $vehicles) {
                    <option value="{{ $vehicles->id }}"
                    @if($vehicles->id == $lead->vehicle_id) {{ "selected" }} @endif >{{ $vehicles->make ." ".
                    $vehicles->model ." (". $vehicles->variant . "," . $vehicles->category .")" }}</option>
                    }
                    @endforeach
                </select>
            </label>
        </section>
    </div>



</fieldset>
<!------------address---------------->


<fieldset>



    <div class="row">


        <section class=" form-group col col-4" >
            <strong class="text text-dark-blue">Chasis Number</strong>
            <label class="input">
                <i class="icon-append fa fa-user"></i>
                <input type="text" name="chasis_number" id="" placeholder="Chasis Number" @if(!empty($lead->asset()->first()->chasis_number)) value="{{ $lead->asset()->first()->chasis_number}}" @endif>
                <b class="tooltip tooltip-bottom-right">Needed to enter Chasis Number</b>

            </label>
        </section>
        <section class=" form-group col col-4" >
            <strong class="text text-dark-blue">Engine Number</strong>
            <label class="input">
                <i class="icon-append fa fa-user"></i>
                <input type="text" name="engine_number" id="" placeholder="Engine Number" @if(!empty($lead->asset()->first()->engine_number)) value="{{ $lead->asset()->first()->engine_number}}" @endif>
                <b class="tooltip tooltip-bottom-right">Needed to enter Engine Number</b>

            </label>
        </section>
        <section class=" form-group col col-4" >
            <strong class="text text-dark-blue">Vehicle Number</strong>
            <label class="input">
                <i class="icon-append fa fa-user"></i>
                <input type="text" name="vehicle_number" id="" placeholder="Vehicle Number" @if(!empty($lead->asset()->first()->vehicle_number)) value="{{ $lead->asset()->first()->vehicle_number}}" @endif>
                <b class="tooltip tooltip-bottom-right">Needed to enter Vehicle Number</b>

            </label>
        </section>

    </div>
    <div class="row">
        <section class=" form-group col col-4" >
            <strong class="text text-dark-blue">Asset Description</strong>
            <label class="input">

                <textarea name="asset_description" id="" cols="37" placeholder="Asset Description">@if(!empty($lead->asset()->first()->asset_description)) {{ $lead->asset()->first()->asset_description}} @endif</textarea>
                <b class="tooltip tooltip-bottom-right">Needed to enter asset description</b>

            </label>
        </section>
        <section class=" form-group col col-4" >
            <strong class="text text-dark-blue">Age Of Asset</strong>
            <label class="input">
                <i class="icon-append fa fa-user"></i>
                <input type="number" name="asset_age" id="" placeholder="Asset Age" @if(!empty($lead->asset()->first()->asset_age)) value="{{ $lead->asset()->first()->asset_age}}" @endif >
                <b class="tooltip tooltip-bottom-right">Needed to enter how old is the Vehicle</b>

            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Year Of manufacture</strong>
            <label class="input">
                <i class="icon-append fa fa-calendar"></i>
                <input type="date" class="dob" name="manufacture_year" id="" placeholder="" @if(!empty($lead->asset()->first()->manufacture_year)) value="{{ $lead->asset()->first()->manufacture_year}}" @endif>
                <b class="tooltip tooltip-bottom-right">Enter Year of manufacture date of vehicle</b>

            </label>
        </section>

    </div>
    <div class="row">
        <section class="col col-6 form-group">
            <strong class="text text-dark-blue">Insurance Company</strong>
            <label class="select">
                <select name="insurance_id" id="insurance_id">
                    <option value="">--Please Select Insurance Company--</option>
                    @foreach($insurance as $insurances) {
                    <option value="{{ $insurances->id }}" @if(!empty($lead->asset()->first()->insurance_id)) @if($insurances->id == $lead->asset()->first()->insurance_id) {{ "selected" }} @endif @endif >{{ ucfirst($insurances->name) }}</option>
                    }
                    @endforeach
                </select>
            </label>
        </section>
        <section class="col col-6 form-group">
            <strong class="text text-dark-blue">Insurance Branch</strong>
            <label class="select">
                <select name="insurance_branch_id" id="insurance_branch_id">


                </select>
            </label>
        </section>

    </div>




</fieldset>
<!-------------- Customer verifications and proof -------------->

<footer>
    <button class="btn btn-default" type="submit" >Update Customer</button>
</footer>
</form>
</div>
</div>

</div>
<!-- /Inner Row Col-md-12 -->


</div>

<!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->
<script>
    $(document).ready(function () {



        $('#insurance_id').change(function (){

            var id = $(this).val();

            $.ajax({
                method:"POST",
                data:{id:id},
                url:"{{ URL::to('admin/approve/insurance') }}",
                success:function(data){

                    //alert(data.district);
                    var data = JSON.parse(data);
                    $('#insurance_branch_id').html(data);
                }
            });
        });




        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        }),

        $('#registration-form').validate({
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                $('#proof_div').slideDown(700);
                $('#address_div').slideDown(700);
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-add-body', 'show');
                //var l = Ladda.create($('[type=submit]', form)[0]);
                //l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        console.log('data')

                        if (data.status == 'fail') {
                            //$('#add').modal('hide');
                            $('#message_text').text(data.message);
                            $('#message').modal('show');

                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            //$('input', form).iCheck('update');

                            // toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            //$('#add').modal('hide');

                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                            window.location.replace("{{ URL::to('admin/approve/reference/"+data.id+"') }}")
                        } else {

                        }

                        //l.stop();
                        //uiLoader('#form-add-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },

            rules: {
                product_id: { required: false },
                vehicle_id: { required: false },
                /*asset_description: { required: true },*/
                chasis_number: { required: false },
                asset_age: { required: false },
                engine_number: { required: false },
                manufacture_year: { required: false },
                insurance_id: { required: false },
                insurance_branch_id: { required: false },
                vehicle_number: { required: false }

            },
            messages: {}
        });
    });
</script>
@stop





