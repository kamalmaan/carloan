@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Bank</li>
    </ul>
</div>

<div class="page-header">

    <a href="{{ URL::to('admin/bank/add')}}"><button  type="button" class="btn btn-info">Add Bank</button></a>
    <br>
    @if(Session::has('message'))
        <div class="callout callout-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong >Well done ! </strong>{{ Session::get('message')}}
        </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{ $data->render() }}
                <script>
                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {
                            // on start of ajax call
                        }).on( 'draw.dt', function () {
                                $(".btn").click(function(){
                                    var id = $(this).attr("id")
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/bank/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                        });
                </script>
                {{ $data->script() }}
            </div>
        </div>

    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->


@stop
