@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="index.html">Dashboard</a></li>
        <li class="active">Rrofile</li>
    </ul>
</div>


<div class="page-header">

    <a href="{{ URL::to('admin/product/add')}}"><button  type="button" class="btn btn-info">Add Product</button></a>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget powerwidget-as-portlet-white" id="serverstatuz-indexpage2">
            <header>
              <h2>User Record</h2>
            </header>
            <div>

              <div class="inner-spacer">
                <div class="row">
                  <!--Row-->

        <div class="col-md-12">

          <div class="row margin-bottom-10px">
            <ul class="countries-demo" id="choices">
              <li class="col-md-2 col-sm-4">
                <h3>UK <span class="label bg-marine"><i class="fa fa-caret-up"></i> 22%</span></h3>
                <div class="orb-form">
                  <label class="toggle" for="iduk">
                    <input name="uk" checked id="iduk" type="checkbox">
                    Uncheck to hide
                    </input>
                    <i></i></label>
                </div>
              </li>
              <li class="col-md-2 col-sm-4">
                <h3>Japan <span class="label bg-red"><i class="fa fa-caret-down"></i> 5%</span></h3>
                <div class="orb-form">
                  <label class="toggle" for="idjapan">
                    <input name="japan" checked id="idjapan" type="checkbox">
                    Uncheck to hide
                    </input>
                    <i></i></label>
                </div>
              </li>
              <li class="col-md-2 col-sm-4">
                <h3>USA <span class="label bg-marine"><i class="fa fa-caret-up"></i> 16%</span></h3>
                <div class="orb-form">
                  <label class="toggle" for="idusa">
                    <input name="usa" checked id="idusa" type="checkbox">
                    Uncheck to hide
                    </input>
                    <i></i></label>
                </div>
              </li>
              <li class="col-md-2 col-sm-4">
                <h3>Russia <span class="label bg-marine"><i class="fa fa-caret-up"></i> 7%</span></h3>
                <div class="orb-form">
                  <label class="toggle" for="idrussia">
                    <input name="russia" checked id="idrussia" type="checkbox">
                    Uncheck to hide
                    </input>
                    <i></i></label>
                </div>
              </li>
              <li class="col-md-2 col-sm-4">
                <h3>China <span class="label bg-marine"><i class="fa fa-caret-up"></i> 1%</span></h3>
                <div class="orb-form">
                  <label class="toggle" for="idchina">
                    <input name="china" checked id="idchina" type="checkbox">
                    Uncheck to hide
                    </input>
                    <i></i></label>
                </div>
              </li>
              <li class="col-md-2 col-sm-4">
                <h3>Others <span class="label bg-red"><i class="fa fa-caret-down"></i> 13%</span></h3>
                <div class="orb-form">
                  <label class="toggle" for="idothers">
                    <input name="others" checked id="idothers" type="checkbox">
                    Uncheck to hide
                    </input>
                    <i></i></label>
                </div>
              </li>
            </ul>
          </div>
          <!--/Row--->

        </div>
        <!--/Col-md-12--->
      </div>
      <!--/Row-->

    </div>
  </div>
</div>


    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

@stop
