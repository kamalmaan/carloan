@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/lead') }}">Lead</a></li>
        <li class="active">Edit Lead</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">

    <a href="{{ URL::to('admin/lead')}}"><button  type="button" class="btn btn-info">Back</button></a>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">


        <div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">

            <div data-widget-editbutton="false" id="registration-form-validation-widget" class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
                <header role="heading">
                    <h2><Lead></Lead><small>You Can Add Your Lead Type</small></h2>
                    <div class="powerwidget-ctrls" role="menu"> <a class="button-icon powerwidget-delete-btn" href="#"><i class="fa fa-times-circle"></i></a>  <a class="button-icon powerwidget-fullscreen-btn" href="#"><i class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i class="fa fa-chevron-circle-up "></i></a></div><span class="powerwidget-loader"></span></header>
                <div class="inner-spacer" role="content">
                    <form class="orb-form" id="registration-form" action="{{ URL::to('admin/lead/update/'.$leads->id)}}" method="post" novalidate="novalidate">
                        <header></header>
                        <fieldset>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="lead_first_name" id="" placeholder="First Name" value="{{ $leads->lead_first_name }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter First Name</b>
                                        {{ $errors->first('lead_first_name','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="lead_last_name" id="" placeholder="Last Name" value="{{ $leads->lead_last_name }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Last Name</b>
                                        {{ $errors->first('lead_last_name','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                            </div>
                            <div class="row">

                                <section class="col col-6">
                                    <h5 class="text text-dark-blue ">Choose The Type Of User</h5>
                                    <div class="inline-group">
                                        <label class="radio">
                                            <input type="radio" value = "person" class="type" name="type" {{ ($leads->type == 'person' || $lead->type == "" || Input::old('type') == 'person')? 'checked' :null}}>
                                            <i></i>Single Person</label>
                                        <label class="radio">
                                            <input type="radio" value="firm" class="type" name="type" {{ ($leads->type == 'firm' || Input::old('type') == 'firm')? 'checked' :null}}>
                                            <i></i>Firm</label>
                                    </div>
                                </section>

                            </div>

                            <div class="row" id="firm"  style="{{($errors->has('company_name') || $errors->has('constitution') || $errors->has('company_category'))? 'display:block':'display:none' }}">
                                <section class="col col-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="company_name" id="" placeholder="Company Name" value="{{ $leads->company_name }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Your Company Name</b>
                                        {{ $errors->first('company_name','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="select">
                                        <select name="constitution">
                                            <option value="">--Please Select Constitution Type--</option>
                                            <option value="Partnership" {{ ($leads->constitution == 'Partnership')?"selected":null }}>Partnership</option>
                                            <option value="Public Ltd. Co." {{ ($leads->constitution == 'Public Ltd. Co.')?"selected":null }}>Public Ltd. Co.</option>
                                            <option value="PVT Ltd. Co." {{ ($leads->constitution == 'PVT Ltd. Co.')?"selected":null }}>PVT Ltd. Co.</option>
                                            <option value="Salaried" {{ ($leads->constitution == 'Salaried')?"selected":null }}>Salaried</option>
                                            <option value="Self Employed" {{ ($leads->constitution == 'Self Employed')?"selected":null }}>Self Employed</option>
                                            <option value="Society" {{ ($leads->constitution == 'Society')?"selected":null }}>Society</option>
                                            <option value="Sole Propriertors" {{ ($leads->constitution == 'Sole Propriertors')?"selected":null }}>Sole Propriertors</option>
                                            <option value="Student" {{ ($leads->constitution == 'Student')?"selected":null }}>Student</option>
                                        </select>
                                        {{ $errors->first('constitution','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="select">
                                        <select name="company_category">
                                            <option value="">--Please Select Company Category--</option>
                                            <option value="CAT D" {{ ($leads->company_category == 'CAT D')?"selected":null }}>CAT D</option>
                                            <option value="CAT DEN WITH CLINIC" {{ ($leads->company_category == 'CAT DEN WITH CLINIC')?"selected":null }}>CAT DEN WITH CLINIC</option>
                                            <option value="CAT DEN WITHOUT CLINIC" {{ ($leads->company_category == 'CAT DEN WITHOUT CLINIC')?"selected":null }}>CAT DEN WITHOUT CLINIC</option>
                                            <option value="CAT DOC HOSPITAL" {{ ($leads->company_category == 'CAT DOC HOSPITAL')?"selected":null }}>CAT DOC HOSPITAL</option>
                                            <option value="CAT DOC AYURVEDA" {{ ($leads->company_category == 'CAT DOC AYURVEDA')?"selected":null }}>CAT DOC AYURVEDA</option>
                                            <option value="CAT DOC CONSULTANT" {{ ($leads->company_category == 'CAT DOC CONSULTANT')?"selected":null }}>CAT DOC CONSULTANT</option>
                                            <option value="CAT DOC DIAGNOSTICS" {{ ($leads->company_category == 'CAT DOC DIAGNOSTICS')?"selected":null }}>CAT DOC DIAGNOSTICS</option>
                                            <option value="CAT DOC GP WITH CLINIC" {{ ($leads->company_category == 'CAT DOC GP WITH CLINIC')?"selected":null }}>CAT DOC GP WITH CLINIC</option>
                                        </select>
                                        {{ $errors->first('company_category','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                            </div>
                            <div id="guardian" style="{{($errors->has('guardian') || $errors->has('guardian_first_name') || $errors->has('guardian_last_name'))? 'display:block':'display:none' }}">
                                <div class="row">

                                    <section class="col col-3">
                                        <div class="inline-group">
                                            <label class="radio">
                                                <input type="radio" value = "S/O" name="guardian" {{ ($leads->guardian == 'S/O')? 'checked' :null}}>
                                                <i></i>S/O</label>
                                            <label class="radio">
                                                <input type="radio" value="W/O" name="guardian" {{ ($leads->guardian == 'W/O')? 'checked' :null}}>
                                                <i></i>W/O</label>
                                            <label class="radio">
                                                <input type="radio" value="D/O" name="guardian" {{ ($leads->guardian == 'D/O')? 'checked' :null}}>
                                                <i></i>D/O</label><br>
                                        </div>
                                    </section>
                                    <section class="col col-6">
                                        <label class="input">
                                            {{ $errors->first('guardian','<em class="invalid text-danger" style="margin-top: 10px;"> :message </em>') }}
                                        </label>
                                    </section>

                                </div>
                                <div class="row" >
                                    <section class="col col-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="guardian_first_name" id="" placeholder="Guardian First Name" value="{{ $leads->guardian_first_name }}">
                                            <b class="tooltip tooltip-bottom-right">Needed to enter Your Gaurdian's First Name</b>
                                            {{ $errors->first('guardian_first_name','<em class="invalid text-danger" > :message </em>') }}
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="guardian_middle_name" id="" placeholder="Guardian Middle Name" value="{{ $leads->guardian_middle_name }}">
                                            <b class="tooltip tooltip-bottom-right">Needed to enter Your Gaurdian's Middle Name</b>
                                            {{ $errors->first('guardian_middle_name','<em class="invalid text-danger" > :message </em>') }}
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="guardian_last_name" id="" placeholder="Guardian Last Name" value="{{ $leads->guardian_last_name }}">
                                            <b class="tooltip tooltip-bottom-right">Needed to enter Your Gaurdian's Last Name</b>
                                            {{ $errors->first('guardian_last_name','<em class="invalid text-danger" > :message </em>') }}
                                        </label>
                                    </section>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        <input type="text" name="phone" id="" placeholder="Phone" value="{{ $leads->phone }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Your Contact Number</b>
                                        {{ $errors->first('phone','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="text" name="email_id" id="" placeholder="E-mail" value="{{ $leads->email }}">
                                        <b class="tooltip tooltip-bottom-right">Needed to enter Your Email Address</b>
                                        {{ $errors->first('email_id','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-6">
                                    <label class="select">
                                        <select name="meeting_medium">
                                            <option value="">--Please Select Expected Meeting Medium--</option>
                                            <option value="Face To Face" @if($leads->meeting_medium == "Face To Face") { {{"selected"}} } @endif>Face To Face</option>
                                            <option value="Email" @if($leads->meeting_medium == "Email") { {{"selected"}} } @endif>Email</option>
                                            <option value="Phone" @if($leads->meeting_medium == "Phone") { {{"selected"}} } @endif>Phone</option>
                                        </select>
                                        {{ $errors->first('meeting_medium','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="select">
                                        <select name="enquiry_type">
                                            <option value="">--Please Select Enquiry Type--</option>
                                            <option value="Warm" @if($leads->enquiry_type == "Warm") { {{"selected"}} } @endif>Warm</option>
                                            <option value="Hot" @if($leads->enquiry_type == "Hot") { {{"selected"}} } @endif>Hot</option>
                                            <option value="Cold" @if($leads->enquiry_type == "Cold") { {{"selected"}} } @endif>Cold</option>
                                        </select>
                                        {{ $errors->first('enquiry_type','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <fieldset>
                            <section>
                                <label class="select">
                                    <select name="product_id">
                                        <option value="">--Please Select Product Type--</option>
                                        @foreach($products as $product){
                                        <option value="{{ $product->id}}" @if($leads->product_id == $product->id) { {{ "selected" }} } @endif>{{ $product->product }}</option>
                                        }
                                        @endforeach
                                    </select>
                                    <i></i> {{ $errors->first('product_id','<em class="invalid text-danger" > :message </em>') }}</label>
                            </section>
                            <section>
                                <label class="select">
                                    <select name="lead_type_id">
                                        <option value="">--Please Select Lead Type--</option>
                                        @foreach($lead_types as $lead_type){
                                        <option value="{{ $lead_type->id}}" @if($leads->lead_type_id == $lead_type->id) { {{ "selected" }} } @endif>{{ $lead_type->lead_type }}</option>
                                        }
                                        @endforeach
                                    </select>
                                    <i></i> {{ $errors->first('lead_type_id','<em class="invalid text-danger" > :message </em>') }}</label>
                            </section>
                            <section>
                                <label class="select">
                                    <select name="lead_from">
                                        <option value="">--Please Select Lead From--</option>
                                        @foreach($lead_froms as $lead_from){
                                        <option value="{{ $lead_from->id}}" @if($leads->lead_from == $lead_from->id) { {{ "selected" }} } @endif>{{ ucfirst($lead_from->first_name)." ".ucfirst($lead_from->last_name) ." - ".$lead_from->superuser()->first()->is_superuser}}</option>
                                        }
                                        @endforeach
                                    </select>
                                    <i></i> {{ $errors->first('lead_from','<em class="invalid text-danger" > :message </em>') }}</label>
                            </section>
                            <!--<section>
                                <label class="select">
                                    <select name="branch_id">
                                        <option value="">--Please Select Branch--</option>
                                        @foreach($branches as $branch){
                                        <option value="{{ $branch->id}}" @if($leads->branch_id == $branch->id) { {{ "selected" }} } @endif>{{ ucfirst($branch->branch) }}</option>
                                        }
                                        @endforeach
                                    </select>
                                    <i></i> {{ $errors->first('branch_id','<em class="invalid text-danger" > :message </em>') }}</label>
                            </section>-->
                        </fieldset>
                        <footer>
                            <button class="btn btn-default" type="submit">Update</button>
                        </footer>
                    </form>
                </div>
            </div>

        </div>
    <!-- /Inner Row Col-md-12 -->
<script>
    $(document).ready(function(){
        $(".type").click(function(){
            var type_check =  $(this).val();
            if(type_check == 'person'){
                $('#guardian').slideToggle(500);
                document.getElementById("firm").style.display="none";

            }
            else{
                $("#firm").slideToggle(500);
                document.getElementById("guardian").style.display="none";
            }
        });
    });
</script>

    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

@stop



