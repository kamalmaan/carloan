@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Rrofile</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">

    <a href="{{ URL::to('admin/product/add')}}"><button  type="button" class="btn btn-info">Add Product</button></a>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>


<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">


        <div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">

            <div data-widget-editbutton="false" id="registration-form-validation-widget" class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
                <header role="heading">
                    <h2>Profile<small>You Can Update Your Profile Here</small></h2>
                    <div class="powerwidget-ctrls" role="menu"> <a class="button-icon powerwidget-delete-btn" href="#"><i class="fa fa-times-circle"></i></a>  <a class="button-icon powerwidget-fullscreen-btn" href="#"><i class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i class="fa fa-chevron-circle-up "></i></a></div><span class="powerwidget-loader"></span></header>
                <div class="inner-spacer" role="content">
                    <form class="orb-form" enctype="multipart/form-data" id="registration-form" action="{{ URL::to('admin/profile')}}" method="post" novalidate="novalidate">
                        <header>Profile was last updated on {{ date('d, M Y',strtotime($data->updated_at)) }} at {{ date('h:i A',strtotime($data->updated_at)) }}</header>
                        <fieldset>
                            <section>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text" placeholder="Username" name="username" value="{{ $data->username }}" readonly="readonly">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter the website</b>
                                    {{ $errors->first('username','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <section>
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input type="password" id="password" placeholder="Password" name="new_password">
                                    <b class="tooltip tooltip-bottom-right">Don't forget your password</b>
                                    {{ $errors->first('new_password','<em class="invalid text-danger"> :message </em>') }}
                                </label>
                            </section>
                            <section>
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input type="password" placeholder="Confirm password" name="confirm_password">
                                    <b class="tooltip tooltip-bottom-right">Don't forget your password</b>
                                    {{ $errors->first('confirm_password','<em class="invalid text-danger"> :message </em>') }}
                                </label>
                            </section>
                            <section>
                                <label class="input"> <i class="icon-append fa fa-phone"></i>
                                    <input type="tel" placeholder="Phone" name="phone" value="{{ $data->phone }}">
                                    <b class="tooltip tooltip-bottom-right">Your Contact Number Here</b>
                                    {{ $errors->first('phone','<em class="invalid text-danger"> :message </em>') }}
                                </label>
                            </section>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="input">
                                        <input type="text" placeholder="First name" name="first_name" value="{{ $data->first_name }}">
                                        {{ $errors->first('first_name','<em class="invalid text-danger"> :message </em>') }}
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input">
                                        <input type="text" placeholder="Last name" name="last_name" value="{{ $data->last_name }}">
                                        {{ $errors->first('last_name','<em class="invalid text-danger"> :message </em>') }}
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class=" form-group col col-6">
                                    <strong class="text text-dark-blue">Upload Your Image</strong>
                                    <label for="file" class="input input-file">
                                    {{ $errors->first('profile_pic','<em class="invalid text-danger"> :message </em>') }}
                                        <img src='{{ asset("upload/$data->profile_pic") }}' height="50" width="100"class="img-thumbnail">
                                        <span class="button btn-sm"><input type="file" name="profile_pic" multiple="">Browse </span>
                                    </label>
                                </section>

                            </div>

                        </fieldset>
                        <footer>
                            <button class="btn btn-default" type="submit">Submit</button>
                        </footer>
                    </form>
                </div>
            </div>

        </div>
    <!-- /Inner Row Col-md-12 -->


    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

@stop




