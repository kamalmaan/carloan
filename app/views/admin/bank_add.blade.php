@extends('admin/layout')
@section('content')
<script type="text/javascript">
    $(function() {
        $('#firm_tag').tagsInput({width: 'auto'});
        $('#single_tag').tagsInput({width: 'auto'});
    });

</script>
<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/bank') }}">Bank</a></li>
        <li class="active">Add Bank</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">

    <a href="{{ URL::to('admin/bank')}}"><button  type="button" class="btn btn-info">Back</button></a>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">


        <div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">

            <div data-widget-editbutton="false" id="registration-form-validation-widget" class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
                <header role="heading">
                    <h2>Bank<small>You Can Add Bank Here</small></h2>
                    <div class="powerwidget-ctrls" role="menu"> <a class="button-icon powerwidget-delete-btn" href="#"><i class="fa fa-times-circle"></i></a>  <a class="button-icon powerwidget-fullscreen-btn" href="#"><i class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i class="fa fa-chevron-circle-up "></i></a></div><span class="powerwidget-loader"></span></header>
                <div class="inner-spacer" role="content">
                    <form  enctype="multipart/form-data" class="orb-form" id="registration-form" action="{{ URL::to('admin/bank/save')}}" method="post" novalidate="novalidate">
                        <header></header>
                        <fieldset>
                            <section>
                                <label class="input"> <i class="icon-append entypo-sweden"></i>
                                    <input type="text" placeholder="Bank" name="bank" value="">
                                    <b class="tooltip tooltip-bottom-right">Need to enter the Bank</b>
                                    {{ $errors->first('bank','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <!--<section>
                                <label for="file" class="input input-file">
                                    <div class="button"><input type="file" name="bank_form" id="2" onchange="readURL(this);" multiple="">Browse</div>
                                    <input type="text" placeholder="Upload Form" readonly="">
                                    <b class="tooltip tooltip-bottom-right">Need to upload the Bank Form</b>
                                    {{ $errors->first('bank_form','<em class="invalid text-danger"> :message </em>') }}
                                </label>
                            </section>-->

                            <!--<section>
                                <label class="input">For Firm <i class="icon-append entypo-sweden"></i>
                                    <input type="text" id="firm_tag" class="tags" placeholder="Firm Tag" name="firm_tag" value="">
                                    <b class="tooltip tooltip-bottom-right">Need to enter the Firm Tag</b>
                                    {{ $errors->first('firm_tag','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <section>
                                <label class="input">For Single Person <i class="icon-append entypo-sweden"></i>
                                    <input type="text" id="single_tag" class="tags" placeholder="Single Tag" name="single_tag" value="">
                                    <b class="tooltip tooltip-bottom-right">Need to enter the Single Tag</b>
                                    {{ $errors->first('single_tag','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>-->

                        </fieldset>

                        <footer>
                            <button class="btn btn-default" type="submit">Save</button>
                        </footer>
                    </form>
                </div>
            </div>

        </div>
    <!-- /Inner Row Col-md-12 -->


    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

@stop




