@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/lead') }}">Lead</a></li>
        <li class="active">Timeline</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">

    <a href='{{ URL::to("admin/co-applicant/index/$coapplicants->lead_id")}}'><button  type="button" class="btn btn-info">Back</button></a>
    <br>
    @if(Session::has('warning'))
    <div class="callout callout-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Please Check! </strong>{{ Session::get('warning')}}
    </div>
    @endif
    @if(Session::has('message'))

    <script>
        $(function() {
            window.location.href="#myTabContent";
            $('#myTab a[href="#meetings"]').tab('show')

        });
    </script>
    @endif



</div>
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


<div class="col-md-12 bootstrap-grid">


<div class="powerwidget cold-grey" id="profile" data-widget-editbutton="false">
<header>
    <h2>Profile Page<small>Basic View</small></h2>
</header>
<div class="inner-spacer">

<!--Profile-->
<div class="user-profile">
    <div class="main-info">


</div>


    </div>
    <div id="carousel-example-generic" class="carousel slide">

        <!--<div class="carousel-inner">
            <div class="item item1 active"> </div>
            <div class="item item2"></div>
            <div class="item item3"></div>
        </div>-->

    </div>
    <div class="user-profile-info">
        <div class="tabs-white">
            <ul id="myTab" class="nav nav-tabs nav-justified">
                <li class="active"><a href="#timeline" data-toggle="tab">Profile</a></li>


            </ul>




<div id="myTabContent" class="tab-content">
    <div class="tab-pane in active" id="timeline">
        <div class="profile-header">About</div>

        <table class="table">
            <tr>
                <td><strong>Name:</strong></td>
                <td>{{ ucfirst($coapplicants->first_name)." ".ucfirst($coapplicants->last_name) }}</td>
                <td><strong>Guardian Name</strong></td>
                <td>{{ucfirst($coapplicants->guardian_first_name)."".ucfirst($coapplicants->guardian_last_name)}}</td>
            </tr>
            <tr>
                <td><strong>Phone: </strong></td>
                <td>{{($coapplicants->phone)}}</td>
                <td><strong>Email:</strong></td>
                <td>{{($coapplicants->email_id)}}</td>
            </tr>
            <!--<tr>
                <td><strong>DOB:</strong></td>
                <td>{{($coapplicants->dob)}}</td>
                <td><strong>State:</strong></td>
                <td>{{($coapplicants->permanent_resi_state_id)}}</td>
            </tr>-->
            <tr>
                <td><strong>District:</strong></td>
                <td>{{($coapplicants->permanent_resi_district_id)}}</td>
                <!--<td><strong>Address(Home):</strong></td>
                <td>{{($coapplicants->permanent_resi_street)}}
                </td>-->

            </tr>
            <!--<tr>
                <td><strong>Address(office):</strong></td>
                <td>{{($coapplicants->current_resi_street)}}</td>
                <td><strong></strong></td>
                <td></td>
            </tr>-->
        </table>
        <!--<a href="{{ URL::to('admin/co-applicant/edit/')}}"><button class="btn btn-info">Update</button></a>
    </div>


    </table>

</div>
-->
            <!--/discussion modal-->

         <!--   <div class="modal" id="add_discussion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-body">
                            <form class="orb-form cmxform" id="discussion_form" action='' method="post" novalidate="novalidate">
                                <input type="hidden" name="meeting_id" id="meeting_id"/>
                                <section class="form-group">

                                    <label class="textarea"> <i class="icon-append fa fa-comment"></i>
                                        <textarea name="meeting_discussion" class="phone-group" id="comment" required placeholder="Meeting Discussion" rows="3"></textarea>
                                        <b class="tooltip tooltip-right">Discussionn</b> </label>

                                </section>


                                <input type="hidden" name="lead_agent_id" value=''>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary btn-sm" id="add_next_meeting">Add Meeting</button>
                                    <button type="submit" class="btn btn-info btn-sm">Save changes</button>
                                </div>

                            </form>

                            <form class="orb-form" id="next_meeting_form" style="display:none" action="" method="post" novalidate="novalidate">
                                <h4 class="modal-title" id="myModalLabel">Add Meeting</h4>
                                <input type="hidden" name="modal_input" value="yes">
                                Meeting Date
                                <label class="input form-group">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" class="datetimepicker6" name="meeting_date" id="" placeholder="">
                                    <b class="tooltip tooltip-bottom-right">Fixed Meeting Date</b>
                                                                 </label>
                                </br>
                                Meeting Medium
                                <label class="select form-group">
                                    <select name="meeting_medium">
                                        <option value="">--Please Select Expected Meeting Medium--</option>
                                        <option value="Face To Face">Face To Face</option>
                                        <option value="Email">Email</option>
                                        <option value="Phone">Phone</option>
                                    </select>

                                </label>

                                </br>
                                Meeting Topic
                                <label class="input form-group">
                                    <i class="icon-append fa fa-text-height"></i>
                                    <input type="text" name="meeting_topic" id="" placeholder="Meeting Topic">
                                    <b class="tooltip tooltip-bottom-right">Meeting Topic</b>


                                </label>
                                </br>



                                <input type="hidden" name="lead_agent_id" value=''>



                                <footer>
                                    <button class="btn btn-info btn-sm" type="submit">Add Meeting or disscussion</button>
                                </footer>-->
                            </form>

                        </div>

                    </div>
                </div>
            </div>



            <!--Remarks-->
            <div class="modal" id="add_remarks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add Meeting Remarks</h4>
                        </div>
                        <div class="modal-body">
                            <form class="orb-form cmxform" id="remarks_form" action='' method="post" novalidate="novalidate">
                                <input type="hidden" name="remark_id" id="remark_id"/>
                                <section class="form-group">

                                    <label class="textarea"> <i class="icon-append fa fa-comment"></i>
                                        <textarea name="remarks" class="phone-group" id="remarks" required placeholder="Meeting Remarks" rows="3"></textarea>
                                        <b class="tooltip tooltip-right">Remarks</b> </label>

                                </section>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-sm remarks_close" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-info btn-sm">Save changes</button>

                                </div>

                            </form>



                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</div>

<!--/Profile-->
</div>
<!-- /Inner Row Col-md-12 -->


</div>

<!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

<!----------Modal For discussion ------->


<script>


</script>
@stop



