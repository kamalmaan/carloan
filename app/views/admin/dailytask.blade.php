@extends('admin/layout')
@section('content')

<style type="text/css">

</style>


<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a hef="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Daily Task</li>
    </ul>
</div>

<div class="page-header">
    <form id="agent" class="orb-form">

        <div class="row">
            <section class="col col-md-3">
                <label class="label">Select</label>
                <label class="select ">
                    <select name="assigned" id="assigned">
                        <option value="">Choose name</option>
                        @foreach($user as $users)
                        <option value="{{ $users->id }}">{{ $users->first_name." ".$users->last_name}}</option>
                        @endforeach
                    </select>
                    <i></i> </label>

            </section>
            <section class="col col-md-3">
                <label class="label">Select</label>
                <label class="select ">
                    <select name="forward" id="forward">
                        <option value="">Choose name</option>
                        @foreach($user as $users)
                        <option value="{{ $users->id }}">{{ $users->first_name." ".$users->last_name}}</option>
                        @endforeach
                    </select>
                    <i></i> </label>

            </section>
            <section class="col col-md-3" style="margin-top: 30px;">
                <button  type="submit" class="btn btn-info" >Assign</button>
                <button  type="button" class="btn btn-info" onclick="task_print()">Print</button>
            </section>
        </div>

    </form>

</div>
<div class="page-header">

    <button  type="button" class="btn btn-info" data-toggle="modal" data-toggle="modal" data-target="#dailytask" id="task_add">Add New</button>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>


<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

<div class="col-md-12 bootstrap-grid">

    <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
        <header>
            <h2>Daily Task<small>Show Hide Columns</small></h2>
        </header>
        <div class="inner-spacer">
            {{ $data->render() }}
            <script>
                $('#test')
                    .on('preXhr.dt', function (e, settings, data) {
                        // on start of ajax call
                    }).on( 'draw.dt', function () {
                        //task pending/ completion status
                        $(".status").click(function(){
                            var id = $(this).attr("id");
                            $("#confirm_message").modal("show");
                            $("#confirm_text").text("Confirm Submit !");
                            $("#yesconfirm").click(function(){

                                //alert(id);
                                $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/dailytask/status') }}",
                                    success:function(data){
                                        $('#test').dataTable()._fnAjaxUpdate();
                                    }
                                });
                            });
                        });
                        // edit/disable
                        $(".dailytask").click(function(){
                            var id = $(this).attr("id");
                            $.ajax({
                                method:"GET",
                                data:{id:id},
                                url:"{{ URL::to('admin/dailytask/statusdelete') }}",
                                success:function(data){
                                    $('#test').dataTable()._fnAjaxUpdate();
                                }
                            });
                        });
                        //model to edit
                        $(".edittask").click(function(){
                            var id = $(this).attr("id");
                            $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/dailytask/edit') }}",
                                    success:function(data){
                                        data = JSON.parse(data)

                                        $('#edit_form').modal('show');
                                        document.getElementById("id").value=data.record.id;
                                        document.getElementById("client").value=data.record.client;
                                        document.getElementById("phone_id").value=data.record.phone;
                                        document.getElementById("assign_on").value=data.record.assign_on;
                                        document.getElementById("completion_on").value=data.record.completion_on;
                                        document.getElementById("assignto").value=data.record.assign_to;
                                        $("#task").html(data.record.task);
                                    }
                                },'json'
                            );
                        });
                        //change assign to /user on view
                        $('.assign_to').change(function(){
                            var id=$(this).attr('id');
                            //alert('id='+id);
                            var userid=$("#"+id).val();
                            // alert('leadid='+leadid);
                            $.ajax({
                                    method:"POST",
                                    data:{id:id,userid:userid},
                                    url:"{{ URL::to('admin/dailytask/updateassignto') }}",
                                    success:function(data){
                                        /*data = JSON.parse(data)
                                         $('#edit_form').modal('show');
                                         document.getElementById("state_name").value=data.record.state;
                                         document.getElementById("state_id").value=data.record.id;*/
                                    }
                                },'json'
                            );
                        });
                        /*            $('.edittask').click(function(){
                         $.ajax({
                         method:"GET",
                         url:"{{ URL::to('admin/dailytask/assignto') }}",
                         datType:'json',
                         success:function(data){

                         $("#assignto").html(data);
                         $(".to").html(data);
                         //  $('#dailytask').modal('show');
                         }
                         });
                         }); */
                        $('#task_add').click(function(){
                            $.ajax({
                                method:"GET",
                                url:"{{ URL::to('admin/dailytask/assignto') }}",
                                datType:'json',
                                success:function(data){

                                    //   $("#assignto").html(data);
                                    // $(".to").html(data);
                                    //   $('#dailytask').modal('show');
                                }
                            });
                        });
                    });
            </script>

            {{ $data->script() }}
        </div>
    </div>

</div>


<div class="modal fade" id="edit_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Daily Task</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="task_edit" action='{{ URL::to("admin/dailytask/update") }}' method="post" novalidate="novalidate">
                    <fieldset>
                        <input type="hidden"  name="id" id="id" value="">
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-info"></i>
                                <input type="text" placeholder="Client" name="client" id="client" value="">
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-info"></i>
                                <input type="number" name="phone" id="phone_id" value="">
                            </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                <input type="text" name="assign_on" id="assign_on" class="datetimepicker6" placeholder="Assign On">
                            </label>
                        </section>
                        <section>
                            <label class="input">
                                <i class="icon-append fa fa-calendar"></i>
                                <input type="text" class="datetimepicker6" name="completion_on" id="completion_on" placeholder="Completion date">
                                <b class="tooltip tooltip-bottom-right">Fixed Meeting Date</b>
                            </label>
                            <!--  <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                 <input type="text" name="completion_on" id="completion_on" class="datetimepicker6 dob" placeholder="Completion date">
                             </label> -->
                        </section>
                        <section class="form-group">
                            <label class="select">
                                <select id="assignto" name="assign_to">
                                    <option value="">Assign To</option>
<!--//error-->
                                </select>
                                <i></i>
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="textarea"> <i class="icon-append fa fa-info"></i>
                                <textarea placeholder="Task" name="task" id="task" class="phone-group"></textarea>
                            </label>
                        </section>
                    </fieldset>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info" >Save changes</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="dailytask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Daily Task</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="dailytask_add" action='{{ URL::to("admin/dailytask/save") }}' method="post" novalidate="novalidate">

                    <fieldset>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-info"></i>
                                <input type="text" placeholder="Client" name="client" value="">
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-info"></i>
                                <input type="number" placeholder="Phone" name="phone" value="">
                            </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                <input type="text" name="assign_on" class="datetimepicker6" placeholder="Assign On">
                            </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                <input type="text" name="completion_on" class="datetimepicker6 " placeholder="Completion date">
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="select">
                                <select id="assignto" class="to" name="assign_to">
                                   <!--//error-->
                                </select>
                                <i></i>
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="textarea"> <i class="icon-append fa fa-info"></i>
                                <textarea placeholder="Task" name="task" class="phone-group"></textarea>
                            </label>
                        </section>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<script>

function task_print(){
    var assigned = $("#assigned").val();
    if(assigned == ""){
        $('#proof-text').text("Please Select agent First !");
        $('#proof_message').modal('show');
        return false;
    }
    else{
        window.location = "{{ URL::to('admin/dailytask/print') }}/"+assigned;
    }

}


$().ready(function() {

    $("#agent").submit(function(e){
        e.preventDefault();
        var assigned = $("#assigned").val();
        var forward = $("#forward").val();
        if(assigned == "")
        {
            $('#proof-text').text("Please select a agent whose lead you want to forward !");
            $('#proof_message').modal('show');
            return false;
        }
        if(forward == "")
        {
            $('#proof-text').text("Please select a agent whome you want to forward leads !");
            $('#proof_message').modal('show');
            return false;
        }
        if(assigned == forward)
        {
            $('#proof-text').text("Oops you have selected same agent !");
            $('#proof_message').modal('show');
            return false;
        }
        else{
            $("#confirm_message").modal("show");
            $("#confirm_text").text("Confirm Submit !");
            $("#yesconfirm").click(function(){
                $.get("{{ URL::to('admin/dailytask/agent') }}",{'assigned':assigned,'forward':forward},function(data){

                    if(data.status = "success"){

                        $('#test').dataTable()._fnAjaxUpdate();
                        $('#message_text').text(data.message);
                        $('#message').modal('show');
                    }
                    if(data.status == "error"){

                    }
                },"json");
            })
        }
    });



    $('#dailytask_add').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-add-body', 'show');
                //var l = Ladda.create($('[type=submit]', form)[0]);
                //l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        console.log('data')

                        if (data.status == 'fail') {
                            alert('if');
                            $('#dailytask').modal('hide');
                            $('#message_text').text(data.message);
                            $('#message').modal('show');

                            $('[type=submit]', form).removeAttr('disabled');

                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            //$('input', form).iCheck('update');

                            // toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#dailytask').modal('hide');
                            $('#test').dataTable()._fnAjaxUpdate();
                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                        } else {

                        }

                        //l.stop();
                        //uiLoader('#form-add-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                client: {
                    required: true
                },
                assign_on:{
                    required: true
                },
                assign_to:{
                    required: true
                },
                task:{
                    required: true
                }

            },
            messages: {}
        }
    );
    //eidt district



    $('#task_edit').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-add-body', 'show');
                //var l = Ladda.create($('[type=submit]', form)[0]);
                //l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        console.log('data')

                        if (data.status == 'fail') {

                            $('#edit_form').modal('hide');
                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {


                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            //$('input', form).iCheck('update');

                            // toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#edit_form').modal('hide');
                            $('#test').dataTable()._fnAjaxUpdate();
                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                        } else {

                        }

                        //l.stop();
                        //uiLoader('#form-add-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                client: {
                    required: true
                },
                assign_on:{
                    required: true
                },
                assign_to:{
                    required: true
                },
                task:{
                    required: true
                }

            },
            messages: {

            }
        }
    );


});

</script>

<!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->


@stop