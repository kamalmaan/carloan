@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Vehicle</li>
    </ul>
</div>

<div class="page-header">

    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#add">Add Vehicle</button>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

<div class="col-md-12 bootstrap-grid">

    <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
        <header>
            <h2>Bank<small>Show Hide Columns</small></h2>
        </header>
        <div class="inner-spacer">
            {{ $data->render() }}
            <script>
                $('#test')
                    .on('preXhr.dt', function (e, settings, data) {
                        // on start of ajax call




                    }).on( 'draw.dt', function () {
                        $(".status").click(function(){
                            var id = $(this).attr("id");
                            $.ajax({
                                method:"GET",
                                data:{id:id},
                                url:"{{ URL::to('admin/vehicle/status') }}",
                                success:function(data){
                                    $('#test').dataTable()._fnAjaxUpdate();
                                }
                            });
                        });
                        $(".vehicle").click(function(){
                            var id = $(this).attr("id");
                            //alert(id);
                            $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/vehicle/edit') }}",
                                    success:function(data){
                                        data = JSON.parse(data)
                                        $('#edit').modal('show');
                                        $('#model').val(data.record.model);
                                        $('#id').val(data.record.id);
                                        $('#make').val(data.record.make);
                                        $('#variant').val(data.record.variant);
                                        $('#category').val(data.record.category);
                                        $('#segment').val(data.record.segment);
                                        $('#cc').val(data.record.cc);
                                    }
                                },'json'
                            );
                        });
                    });
            </script>

            {{ $data->script() }}
        </div>
    </div>

</div>


<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Vehicle</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="vehicle_add" action='{{ URL::to("admin/vehicle/save") }}' method="post" novalidate="novalidate">

                    <fieldset>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-automobile"></i>
                                <input type="text" placeholder="Vehicle Model" name="model" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter vehicle Model</b>
                                {{ $errors->first('model','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-pied-piper"></i>
                                <input type="text" placeholder="Vehicle Make" name="make" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter vehicle Make</b>
                                {{ $errors->first('make','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-info"></i>
                                <input type="text" placeholder="Vehicle Variant" name="variant" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter vehicle Variant</b>
                                {{ $errors->first('variant','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="select">
                                <select name="category">
                                    <option value="">--Please Select Category--</option>
                                    <option value="Tier-1">Tier-1</option>
                                    <option value="Tier-1">Tier-2</option>
                                    <option value="Tier-1">Tier-3</option>
                                    <option value="Tier-1">Tier-4</option>
                                    <option value="Tier-1">Tier-5</option>
                                    <option value="Tier-1">Tier-6</option>
                                    <option value="Other">Other</option>

                                </select>
                                {{ $errors->first('category','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-qrcode"></i>
                                <input type="text" placeholder="Vehicle Segment" name="segment" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter vehicle Segment</b>
                                {{ $errors->first('segment','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-car"></i>
                                <input type="text" placeholder="Vehicle CC" name="cc" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter vehicle CC</b>
                                {{ $errors->first('cc','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Vehicle</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="vehicle_edit" action='{{ URL::to("admin/vehicle/update") }}' method="post" novalidate="novalidate">

                    <fieldset>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-automobile"></i>
                                <input type="text" placeholder="Vehicle Model" name="model" value="" id="model"/>
                                <input type="hidden" placeholder="" name="id" value="" id="id"/>
                                <b class="tooltip tooltip-bottom-right">Needed to enter district</b>
                                {{ $errors->first('model','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-pied-piper"></i>
                                <input type="text" placeholder="Vehicle Make" name="make" id="make" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter vehicle Make</b>
                                {{ $errors->first('make','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-info"></i>
                                <input type="text" placeholder="Vehicle Variant" name="variant" id="variant" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter vehicle Variant</b>
                                {{ $errors->first('variant','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="select">
                                <select name="category" id="category">
                                    <option value="">--Please Select Category--</option>
                                    <option value="Tier-1">Tier-1</option>
                                    <option value="Tier-1">Tier-2</option>
                                    <option value="Tier-1">Tier-3</option>
                                    <option value="Tier-1">Tier-4</option>
                                    <option value="Tier-1">Tier-5</option>
                                    <option value="Tier-1">Tier-6</option>
                                    <option value="Other">Other</option>

                                </select>
                                {{ $errors->first('category','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-qrcode"></i>
                                <input type="text" placeholder="Vehicle Segment" name="segment" id="segment" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter vehicle Segment</b>
                                {{ $errors->first('segment','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-car"></i>
                                <input type="text" placeholder="Vehicle CC" name="cc" id="cc" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter vehicle CC</b>
                                {{ $errors->first('cc','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>



<script>

    $().ready(function() {

        $('#vehicle_add').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('#add').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#add').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    model: {
                        required: true
                    },
                    make:{
                        required:true
                    },
                    variant:{
                        required:true
                    },
                    category:{
                        required:true
                    },
                    segment:{
                        required:true
                    },
                    cc:{
                        required:true
                    }

                },
                messages: {}
            }
        );
        //eidt district



        $('#vehicle_edit').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {

                                $('#edit').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#edit').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    model: {
                        required: true
                    },
                    make:{
                        required:true
                    },
                    variant:{
                        required:true
                    },
                    category:{
                        required:true
                    },
                    segment:{
                        required:true
                    },
                    cc:{
                        required:true
                    }

                },
                messages: {

                }
            }
        );


    });

</script>

<!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->


@stop
