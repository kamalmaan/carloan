@extends('admin/layout')
@section('content')

    <style type="text/css">
        .dob {
            z-index: 99999 !important;
        }

        .xdsoft_datetimepicker {
            z-index: 99999 !important;
        }
    </style>
    <!--Breadcrumb-->
    <div class="breadcrumb clearfix">
        <ul>
            <li><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li><a href="index.html">Dashboard</a></li>
            <li class="active">Profile</li>
        </ul>
    </div>


    <div class="page-header">

        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#add">Add Insurance</button>
        <br>
        @if(Session::has('message'))
            <div class="callout callout-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Well done ! </strong>{{ Session::get('message')}}
            </div>
        @endif

    </div>

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank
                    <small>Show Hide Columns</small>
                </h2>
            </header>
            <div class="inner-spacer">
                {{$data->render()}}
                <script>
                    $('#test')
                            .on('preXhr.dt', function (e, settings, data) {
                                // on start of ajax call
                            }).on('draw.dt', function () {
                                $(".status").click(function () {
                                    var id = $(this).attr("id");
                                    $.ajax({
                                        method: "GET",
                                        data: {id: id},
                                        url: "{{ URL::to('admin/users/status') }}",
                                        success: function (data) {
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".insurance_edit").click(function () {
                                    var id = $(this).attr("id");
                                    $.ajax({
                                                method: "GET",
                                                data: {id: id},
                                                url: "{{ URL::to('admin/insurance/edit') }}",
                                                success: function (data) {
                                                    data = JSON.parse(data)
                                                    $('#edit').modal('show');
                                                    $('#customer_name').val(data.record.customer_name);
                                                    $('#customer_phone').val(data.record.customer_phone);
                                                    $('#from').val(data.record.from);
                                                    $('#to').val(data.record.to);
                                                    $('#company_vehicle').val(data.record.company_vehicle);
                                                    $('#insurance_amount').val(data.record.insurance_amount);
                                                    $('#residential_street').val(data.record.residential_street);
                                                    $('#edit_residential_city').val(data.record.residential_city);
                                                    $('#residential_distt').val(data.record.residential_distt);
                                                    $('#dob').val(data.record.dob);
                                                    $('#issue_date').val(data.record.issue_date);
                                                    $('#manufacturing_year').val(data.record.manufacturing_year);
                                                    $('#nominee_name').val(data.record.nominee_name);
                                                    $('#nominee_relation').val(data.record.nominee_relation);
                                                    $('#nominee_dob').val(data.record.nominee_dob);
                                                    $('#id').val(data.record.id);
                                                }
                                            }, 'json'
                                    );
                                });

                                $(".delete").click(function () {
                                    var id = $(this).attr('id');
                                    if (confirm('You really want to delete ?')) {
                                        $.ajax({
                                                    method: "GET",
                                                    data: {id: id},
                                                    url: "{{ URL::to('admin/insurance/destroy') }}",
                                                    dataType: 'json',
                                                    success: function (data) {
                                                        if (data.status == 'success') {
                                                            $('#test').dataTable()._fnAjaxUpdate();
                                                        } else {
                                                            $('#message_text').text(data.message);
                                                            $('#message').modal('show');
                                                        }
                                                    }
                                                }, 'json'
                                        );
                                    }
                                })
                            });
                </script>
                {{ $data->script() }}
            </div>
        </div>
    </div>



    {{--
        Insuranace registration model
    --}}
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Insurance</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="add_user" action='{{ URL::to("admin/insurance/add") }}'
                          method="post"
                          novalidate="novalidate">
                        <fieldset>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-automobile"></i>
                                    <input type="text" placeholder="Customer name" name="customer_name" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Customer name</b>
                                    {{ $errors->first('customer_name','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="number" placeholder="Customer Phone Number" name="customer_phone"
                                           value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Customer Phone Number</b>
                                    {{ $errors->first('customer_phone','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="date" placeholder="From" class="dob" name="from" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Fill From</b>
                                    {{ $errors->first('from','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="date" placeholder="To" class="dob" name="to" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Fill To</b>
                                    {{ $errors->first('to','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="Company Vehicle" name="company_vehicle" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Company Vehicle</b>
                                    {{ $errors->first('company_vehicle','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="number" placeholder="Insurance Amount" name="insurance_amount"
                                           value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Insurance Amount</b>
                                    {{ $errors->first('insurance_amount','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="Residential Street" name="residential_street"
                                           value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Residential Street</b>
                                    {{ $errors->first('residential_street','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="select">
                                    <select name="residential_city" id="city" onchange=residential_city() class="add_residential_city">
                                        <option value="">Residential City</option>
                                        @foreach($cities as $row)
                                            <option value="{{ $row->id }}">{{$row->city}}</option>
                                        @endforeach
                                    </select>
                                    {{ $errors->first('residential_city','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="add_distt" placeholder="Residential Distt" name="residential_distt"
                                           value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Residential Distt</b>
                                    {{ $errors->first('residential_distt','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="DOB" class="dob" name="dob" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to DOB</b>
                                    {{ $errors->first('dob','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="Issue Date" class="dob" name="issue_date" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Issue Date</b>
                                    {{ $errors->first('issue_date','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="Manufacturing Year" name="manufacturing_year"
                                           value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Manufacturing Year</b>
                                    {{ $errors->first('manufacturing_year','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="Nominee Name" name="nominee_name" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Nominee Name</b>
                                    {{ $errors->first('nominee_name','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="Nominee Relation" name="nominee_relation" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Nominee Relation</b>
                                    {{ $errors->first('nominee_relation','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="Nominee DOB" class="dob" name="nominee_dob"
                                           value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Nominee DOB</b>
                                    {{ $errors->first('nominee_dob','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            `
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    {{--
       Insuranace Edition registration model
   --}}
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Insurance</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="edit_insurance" action='{{ URL::to("admin/insurance/update") }}'
                          method="post"
                          novalidate="novalidate">
                        <fieldset>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-automobile"></i>
                                    <input type="text" id="customer_name" placeholder="Customer name"
                                           name="customer_name" value="">
                                    <input type="hidden" name="id" id="id" value=""/>
                                    <b class="tooltip tooltip-bottom-right">Needed to Customer name</b>
                                    {{ $errors->first('customer_name','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="number" id="customer_phone" placeholder="Customer Phone Number"
                                           name="customer_phone" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Customer Phone Number</b>
                                    {{ $errors->first('customer_phone','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="date" id="from" placeholder="From" class="dob" name="from" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Fill From</b>
                                    {{ $errors->first('from','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="date" id="to" placeholder="To" class="dob" name="to" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Fill To</b>
                                    {{ $errors->first('to','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="company_vehicle" placeholder="Company Vehicle"
                                           name="company_vehicle" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Company Vehicle</b>
                                    {{ $errors->first('company_vehicle','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="number" id="insurance_amount" placeholder="Insurance Amount"
                                           name="insurance_amount" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Insurance Amount</b>
                                    {{ $errors->first('insurance_amount','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="residential_street" placeholder="Residential Street"
                                           name="residential_street" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Residential Street</b>
                                    {{ $errors->first('residential_street','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="select">
                                    <select name="residential_city" id="edit_residential_city">
                                        <option value="">Residential City</option>
                                        @foreach($cities as $row)
                                            <option value="{{ $row->id }}">{{$row->city}}</option>
                                        @endforeach
                                    </select>
                                    {{ $errors->first('residential_city','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="residential_distt" placeholder="Residential Distt"
                                           name="residential_distt" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Residential Distt</b>
                                    {{ $errors->first('residential_distt','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="dob" placeholder="DOB" class="dob" name="dob" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to DOB</b>
                                    {{ $errors->first('dob','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="issue_date" placeholder="Issue Date" class="dob"
                                           name="issue_date" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Issue Date</b>
                                    {{ $errors->first('issue_date','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="manufacturing_year" placeholder="Manufacturing Year"
                                           name="manufacturing_year" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Manufacturing Year</b>
                                    {{ $errors->first('manufacturing_year','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="nominee_name" placeholder="Nominee Name" name="nominee_name"
                                           value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Nominee Name</b>
                                    {{ $errors->first('nominee_name','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="nominee_relation" placeholder="Nominee Relation"
                                           name="nominee_relation" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Nominee Relation</b>
                                    {{ $errors->first('nominee_relation','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" id="nominee_dob" placeholder="Nominee DOB" class="dob"
                                           name="nominee_dob" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to Nominee DOB</b>
                                    {{ $errors->first('nominee_dob','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {


            $('#city').change(function(){
                var city = $(this).val();
                $.ajax({
                    method: "GET",
                    data: {id: city},
                    dataType:'json',
                    url: "{{ URL::to('admin/insurance/city') }}",
                    success: function (data) {
                        if(data == null){
                            $("#add_distt").val('');
                        } else {
                            $("#add_distt").val(data.district);
                        }
                    }
                });
            });

            /*For addition*/

            $('#edit_residential_city').change(function(){
                var city=$(this).val();

                $.ajax({
                    methos:"GET",
                    data:{id:city},
                    dataType:'json',
                    url: "{{ URL::to('admin/insurance/city') }}",
                    success: function (data) {
                        if(data == null){
                            $("#residential_distt").val('');
                        } else {
                            $("#residential_distt").val(data.district);
                        }
                    }
                })
            })


            $('#add_user').validate(
                    {
                        ignore: [],
                        errorElement: 'span',
                        errorClass: 'help-block',
                        highlight: function (element) { // hightlight error inputs
                            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        submitHandler: function (form) {
                            form = $(form);
                            $('[type=submit]', form).attr('disabled', 'disabled');
                            //uiLoader('#form-add-body', 'show');
                            //var l = Ladda.create($('[type=submit]', form)[0]);
                            //l.start();
                            form.ajaxSubmit({
                                dataType: 'json',
                                success: function (data) {
                                    console.log('data');

                                    if (data.status == 'fail') {
                                        //$('#add').modal('hide');
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                        $('[type=submit]', form).removeAttr('disabled');
                                    } else if (data.status == 'success') {

                                        form[0].reset();
                                        $('.form-group', form).removeClass('has-error');
                                        //$('input', form).iCheck('update');

                                        // toastr['success'](data.message);
                                        $('[type=submit]', form).removeAttr('disabled');
                                        $('#add').modal('hide');
                                        $('#test').dataTable()._fnAjaxUpdate();
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                    } else {

                                    }

                                    //l.stop();
                                    //uiLoader('#form-add-body', 'hide');
                                }
                            });

                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                        },
                        rules: {
                            customer_name: {
                                required: true
                            },
                            customer_phone: {
                                required: true
                            },
                            from: {
                                required: true
                            },
                            to: {
                                required: true
                            },
                            company_vehicle: {
                                required: true
                            },
                            insurance_amount: {
                                required: true
                            },
                            residential_street: {
                                required: true
                            },
                            residential_city: {
                                required: true
                            },
                            residential_distt: {
                                required: true
                            },
                            dob: {
                                required: true
                            },
                            issue_date: {
                                required: true
                            },
                            manufacturing_year: {
                                required: true
                            },
                            nominee_name: {
                                required: true
                            },
                            nominee_relation: {
                                required: true
                            },
                            nominee_dob: {
                                required: true
                            }

                        },
                        messages: {}
                    }
            );



            $('#edit_insurance').validate(
                    {
                        ignore: [],
                        errorElement: 'span',
                        errorClass: 'help-block',
                        highlight: function (element) { // hightlight error inputs
                            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        submitHandler: function (form) {
                            form = $(form);
                            $('[type=submit]', form).attr('disabled', 'disabled');
                            //uiLoader('#form-add-body', 'show');
                            //var l = Ladda.create($('[type=submit]', form)[0]);
                            //l.start();
                            form.ajaxSubmit({
                                dataType: 'json',
                                success: function (data) {
                                    console.log('data');

                                    if (data.status == 'fail') {
                                        //$('#add').modal('hide');
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                        $('[type=submit]', form).removeAttr('disabled');
                                    } else if (data.status == 'success') {

                                        form[0].reset();
                                        $('.form-group', form).removeClass('has-error');
                                        //$('input', form).iCheck('update');

                                        // toastr['success'](data.message);
                                        $('[type=submit]', form).removeAttr('disabled');
                                        $('#add').modal('hide');
                                        $('#edit').modal('hide');
                                        $('#test').dataTable()._fnAjaxUpdate();
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                    } else {

                                    }

                                    //l.stop();
                                    //uiLoader('#form-add-body', 'hide');
                                }
                            });

                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                        },
                        rules: {
                            customer_name: {
                                required: true
                            },
                            customer_phone: {
                                required: true
                            },
                            from: {
                                required: true
                            },
                            to: {
                                required: true
                            },
                            company_vehicle: {
                                required: true
                            },
                            insurance_amount: {
                                required: true
                            },
                            residential_street: {
                                required: true
                            },
                            residential_city: {
                                required: true
                            },
                            residential_distt: {
                                required: true
                            },
                            dob: {
                                required: true
                            },
                            issue_date: {
                                required: true
                            },
                            manufacturing_year: {
                                required: true
                            },
                            nominee_name: {
                                required: true
                            },
                            nominee_relation: {
                                required: true
                            },
                            nominee_dob: {
                                required: true
                            }

                        },
                        messages: {}
                    }
            );

        })


    </script>

@stop
