@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/lead_type') }}">Lead Type</a></li>
        <li class="active">Add Lead Type</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">

    <a href="{{ URL::to('admin/lead_type')}}"><button  type="button" class="btn btn-info">Back</button></a>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">


        <div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">

            <div data-widget-editbutton="false" id="registration-form-validation-widget" class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
                <header role="heading">
                    <h2>Lead Type<small>You Can Add Lead Type</small></h2>
                    <div class="powerwidget-ctrls" role="menu"> <a class="button-icon powerwidget-delete-btn" href="#"><i class="fa fa-times-circle"></i></a>  <a class="button-icon powerwidget-fullscreen-btn" href="#"><i class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i class="fa fa-chevron-circle-up "></i></a></div><span class="powerwidget-loader"></span></header>
                <div class="inner-spacer" role="content">
                    <form class="orb-form" id="registration-form" action="{{ URL::to('admin/lead_type/save')}}" method="post" novalidate="novalidate">
                        <header></header>
                        <fieldset>
                            <section>
                                <label class="input"> <i class="icon-append entypo-sweden"></i>
                                    <input type="text" placeholder="Lead Type" name="lead_type" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter the Lead type</b>
                                    {{ $errors->first('lead_type','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                        </fieldset>

                        <footer>
                            <button class="btn btn-default" type="submit">Save</button>
                        </footer>
                    </form>
                </div>
            </div>

        </div>
    <!-- /Inner Row Col-md-12 -->


    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

@stop




