@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/lead') }}">Lead</a></li>
        <li class="active">Timeline</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">

    <a href="{{ URL::to('admin/lead')}}"><button  type="button" class="btn btn-info">Back</button></a>
    <br>
    @if(Session::has('warning'))
    <div class="callout callout-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Please Check! </strong>{{ Session::get('warning')}}
    </div>
    @endif
    @if(Session::has('message'))

    <script>
        $(function() {
            window.location.href="#myTabContent";
            $('#myTab a[href="#meetings"]').tab('show')

        });
    </script>
    @endif



</div>
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


    <div class="col-md-12 bootstrap-grid">


    <div class="powerwidget cold-grey" id="profile" data-widget-editbutton="false">
    <header>
        <h2>Profile Page<small>Basic View</small></h2>
    </header>
    <div class="inner-spacer">

    <!--Profile-->
    <div class="user-profile">
    <div class="main-info">

        <div class="user-img">
            @if($leads->profile_picture!=null)
            <img src='{{ asset("upload/{$leads->profile_picture}") }}' height="140" width="150" alt="User Picture" />
            @endif
        </div>
        <h1>{{ ucfirst($leads->lead_first_name)." ".ucfirst($leads->lead_last_name) }}</h1>
        Enquiry Type: @if($leads->enquiry_type == 'Warm') <span class="" title="Warm"><i class="fa fa-star"></i>Warm</span> @elseif($leads->enquiry_type == 'Hot')<span class="" title="Hot"><i class="entypo-bell"></i>Hot</span> @else <span class="" title="Cold"><i class="fa fa-warning"></i>Cold</span> @endif |
        Product: @if(!empty($leads->product()->first()->product))  {{ $leads->product()->first()->product }} @endif
    </div>
    <div id="carousel-example-generic" class="carousel slide">

        <div class="carousel-inner">
            <div class="item item1 active"> </div>
            <div class="item item2"></div>
            <div class="item item3"></div>
        </div>

    </div>
    <div class="user-profile-info">
    <div class="tabs-white">
    <ul id="myTab" class="nav nav-tabs nav-justified">
        <li class="active"><a href="#timeline" data-toggle="tab">Profile</a></li>
        <li><a href="#add_meeting" data-toggle="tab">Add Meetings</a></li>
        <li><a href="#meetings" data-toggle="tab">Meetings</a></li>

    </ul>
    <div id="myTabContent" class="tab-content">
    <div class="tab-pane in active" id="timeline">
        <div class="profile-header">About</div>

        <table class="table">
            <tr>
                <td><strong>Name:</strong></td>
                <td>{{ ucfirst($leads->lead_first_name)." ".ucfirst($leads->lead_last_name) }}</td>
                <td><strong>{{ $leads->guardian}} of:</strong></td>
                <td>{{ ucfirst($leads->guardian_first_name) ." ". ucfirst($leads->guardian_last_name) }}</td>
            </tr>
            <tr>
                <td><strong>Phone: </strong></td>
                <td>{{ $leads->phone }}</td>
                <td><strong>Email:</strong></td>
                <td>{{ $leads->email }}</td>
            </tr>
            <tr>
                <td><strong>Product:</strong></td>
                <td>@if(!empty($leads->product()->first()->product)) {{ $leads->product()->first()->product}} @endif</td>
                <td><strong>Enquiry Type</strong></td>
                <td>@if($leads->enquiry_type == 'Warm') <span class="text-success" title="Warm"><i class="fa fa-star"></i>Warm</span> @elseif($leads->enquiry_type == 'Hot')<span class="text-danger" title="Hot"><i class="entypo-bell"></i>Hot</span> @else <span class="text-warning" title="Cold"><i class="fa fa-warning"></i>Cold</span> @endif </td>
            </tr>
            <tr>
                <td><strong>Lead From</strong></td>
                <td>@if(!empty($leads->lead_from()->first()->first_name)){{ ucfirst($leads->lead_from()->first()->first_name)." ".ucfirst($leads->lead_from()->first()->last_name)."(".$leads->lead_from()->first()->superuser()->first()->is_superuser.")" }}@endif</td>
                <td><strong>Lead To/Forward</strong></td>
                <td>@if($leads->forward_to != '' && $leads->lead_to != '') @if($leads->forward_to == '0'){{ "Assign To ".ucfirst($leads->lead_to()->first()->first_name)." ".ucfirst($leads->lead_to()->first()->last_name)."(".$leads->lead_to()->first()->superuser()->first()->is_superuser.")" }}
                    @else {{ "Forward To ".ucfirst($leads->forward_to()->first()->first_name)." ".ucfirst($leads->forward_to()->first()->last_name)."(".$leads->forward_to()->first()->superuser()->first()->is_superuser.")" }}
                    @endif @endif
                </td>

            </tr>
            <tr>
                <td><strong>Meeting Medium:</strong></td>
                <td>{{ $leads->meeting_medium }}</td>
                <td><strong>Lead Arrival Time:</strong></td>
                <td>{{ date('d, M y h:i',strtotime($leads->created_at)) }}</td>
            </tr>
        </table>
        <a href="{{ URL::to('admin/lead/edit/'.$leads->id)}}"><button class="btn btn-info">Update</button></a>
    </div>
    <div class="tab-pane" id="add_meeting">
        <div class="profile-header"> Following

        </div>

            <form class="orb-form" id="registration-form" action="{{ URL::to('admin/lead/leadtimeline/'.$leads->id)}}" method="post" novalidate="novalidate">
                Meeting Date
                <label class="input">
                    <i class="icon-append fa fa-calendar"></i>
                    <input type="text" class="datetimepicker6" name="meeting_date" id="" placeholder="">
                    <b class="tooltip tooltip-bottom-right">Fixed Meeting Date</b>
                    {{ $errors->first('meeting_date','<em class="invalid text-danger" > :message </em>') }}
                </label>
                </br>
                Meeting Medium
                <label class="select">
                    <select name="meeting_medium">
                        <option value="">--Please Select Expected Meeting Medium--</option>
                        <option value="Face To Face">Face To Face</option>
                        <option value="Email">Email</option>
                        <option value="Phone">Phone</option>
                    </select>
                    {{ $errors->first('meeting_medium','<em class="invalid text-danger" > :message </em>') }}
                </label>
                </br>
                Meeting Topic
                <label class="input">
                    <i class="icon-append fa fa-text-height"></i>
                    <input type="text" name="meeting_topic" id="" placeholder="Meeting Topic">
                    <b class="tooltip tooltip-bottom-right">Meeting Topic</b>
                    {{ $errors->first('meeting_topic','<em class="invalid text-danger" > :message </em>') }}
                </label>
                </br>



                <input type="hidden" name="lead_agent_id" value='@if($leads->forward_to != "" && $leads->lead_to != "") @if($leads->forward_to == '0'){{ $leads->lead_to }}@else {{ $leads->forward_to }} @endif @endif'>
                <input="text" name="modal_input" value="no">


                <footer>
                    <button class="btn btn-info" type="submit">Add Meeting or disscussion</button>
                </footer>
            </form>

        </div>




    <div class="tab-pane" id="meetings">
        <div class="profile-header">Meetings</div>
        @if(Session::has('message'))
        <div class="callout callout-warning">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong >Please Check! </strong>{{ Session::get('message')}}
        </div>

        @endif
        <ul class="tmtimeline">
            @foreach($meetings as $meeting)
            <li>
                <time class="tmtime" datetime="2013-04-10 18:30"><span>{{ date('d/m/y',strtotime($meeting->meeting_date)) }}</span> <span>{{ date('h:i A',strtotime($meeting->meeting_date)) }}</span></time>
                <div class="tmicon bg-blue fa fa-comment    "></div>
                <div class="tmlabel">
                    <button  id="{{ $meeting->id }}" type="button"  class="close close-meeting"><i class="fa fa-times text-dark-blue"></i></button>
                    <h2>{{ ucfirst($meeting->meeting_topic) }}</h2>
                    <p><strong>Discussion: </strong>{{ $meeting->meeting_discussion }}</p>
                    <p><strong class="text text-info">Remarks: </strong>{{ $meeting->remarks }}</p>
                    <p><strong>Status: </strong>{{ $meeting->meeting_status }}</p>
                    <p>Had <i class="text text-info">{{ $meeting->meeting_medium }}</i> meeting with <i>@if($meeting->lead_agent_id != '') {{ ucfirst($meeting->lead()->first()->first_name)." ".ucfirst($meeting->lead()->first()->last_name)."(".$meeting->lead()->first()->superuser()->first()->is_superuser.")" }} @endif</i></p>
                    <button type="button" class="btn btn-info discussion_meeting btn-sm" id="discussion_{{ $meeting->id }}"  >Add Discussion</button>
                    <button type="button" class="btn btn-warning remark_meeting btn-sm" id="remark_{{ $meeting->id }}" value="{{ $meeting->remarks }}" >Add Remarks</button>
                </div>
            </li>





            @endforeach
        </ul>
    </div>

 </div>

    <!--/discussion modal-->

        <div class="modal" id="add_discussion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Meeting Discussion</h4>
                    </div>
                    <div class="modal-body">
                        <form class="orb-form cmxform" id="discussion_form" action='{{ URL::to("admin/lead/discussion") }}' method="post" novalidate="novalidate">
                            <input type="hidden" name="meeting_id" id="meeting_id"/>
                            <section class="form-group">

                                <label class="textarea"> <i class="icon-append fa fa-comment"></i>
                                    <textarea name="meeting_discussion" class="phone-group" id="comment" required placeholder="Meeting Discussion" rows="3"></textarea>
                                    <b class="tooltip tooltip-right">Discussionn</b> </label>
                            </section>
                            <section class="form-group">
                                <label class="select">
                                    <select name="meeting_status" id="">
                                        <option value="">--Please Select Meeting Status--</option>
                                        <option value="PLI" >PLI</option>
                                        <option value="NotInterested">Not Interested</option>                                        
                                    </select>
                                </label>
                            </section>


                            <input type="hidden" name="lead_agent_id" value='@if($leads->forward_to != "" && $leads->lead_to != "") @if($leads->forward_to == '0'){{ $leads->lead_to }}@else {{ $leads->forward_to }} @endif @endif'>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary btn-sm" id="add_next_meeting">Add Meeting</button>
                                <button type="submit" class="btn btn-info btn-sm">Save changes</button>
                            </div>

                        </form>

                        <form class="orb-form" id="next_meeting_form" style="display:none" action="{{ URL::to('admin/lead/leadtimeline/'.$leads->id)}}" method="post" novalidate="novalidate">
                            <h4 class="modal-title" id="myModalLabel">Add Meeting</h4>
                            <input type="hidden" name="modal_input" value="yes">
                            Meeting Date
                            <label class="input form-group">
                                <i class="icon-append fa fa-calendar"></i>
                                <input type="text" class="datetimepicker6" name="meeting_date" id="" placeholder="">
                                <b class="tooltip tooltip-bottom-right">Fixed Meeting Date</b>
                                {{ $errors->first('meeting_date','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                            </br>
                            Meeting Medium
                            <label class="select form-group">
                                <select name="meeting_medium">
                                    <option value="">--Please Select Expected Meeting Medium--</option>
                                    <option value="Face To Face">Face To Face</option>
                                    <option value="Email">Email</option>
                                    <option value="Phone">Phone</option>
                                </select>

                            </label>

                            </br>
                            Meeting Topic
                            <label class="input form-group">
                                <i class="icon-append fa fa-text-height"></i>
                                <input type="text" name="meeting_topic" id="" placeholder="Meeting Topic">
                                <b class="tooltip tooltip-bottom-right">Meeting Topic</b>
                                {{ $errors->first('meeting_topic','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                            </br>



                            <input type="hidden" name="lead_agent_id" value='@if($leads->forward_to != "" && $leads->lead_to != "") @if($leads->forward_to == '0'){{ $leads->lead_to }}@else {{ $leads->forward_to }} @endif @endif'>



                            <footer>
                                <button class="btn btn-info btn-sm" type="submit">Add Meeting or disscussion</button>
                            </footer>
                        </form>

                    </div>

                </div>
            </div>
        </div>



        <!--Remarks-->
    <div class="modal" id="add_remarks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Meeting Remarks</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="remarks_form" action='{{ URL::to("admin/lead/remarks") }}' method="post" novalidate="novalidate">
                        <input type="hidden" name="remark_id" id="remark_id"/>
                        <section class="form-group">

                            <label class="textarea"> <i class="icon-append fa fa-comment"></i>
                                <textarea name="remarks" class="phone-group" id="remarks" required placeholder="Meeting Remarks" rows="3"></textarea>
                                <b class="tooltip tooltip-right">Remarks</b> </label>

                        </section>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm remarks_close" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info btn-sm">Save changes</button>

                        </div>

                    </form>



                </div>

            </div>
        </div>
    </div>


    </div>
    </div>
    </div>
    </div>

    <!--/Profile-->
    </div>
    <!-- /Inner Row Col-md-12 -->


    </div>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->

<!----------Modal For discussion ------->


<script>

    $("#add_next_meeting").click(function(){
       $('#next_meeting_form').slideToggle();
    });

    $().ready(function() {

        $(".discussion_meeting").click(function(){
            var meeting_id = $(this).attr("id");
            //alert(meeting_id);
            var id  = meeting_id.split("_");
            $("#meeting_id").val(id[1]);
            $('#add_discussion').modal('show');

        });

        $(".remark_meeting").click(function(){
            var meeting = $(this).attr("id").split("_");
            //alert(meeting[1]);
            var remark_val = $(this).attr("value");
            if(remark_val != ""){
                $("#remarks").val(remark_val);
            }
            $("#remark_id").val(meeting[1]);
            $('#add_remarks').modal('show');
        });

        $(".remarks_close").click(function(){

            $('#remarks_form')[0].reset();
        });

       $('#discussion_form').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                           console.log(data);

                             if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                               // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');

                                $('#add_discussion').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                location.reload();
                            }
                            else {

                                for (var key in data.errors) {
                                    // toastr['error'](data.errors[key]);
                                }
                                $('[type=submit]', form).removeAttr('disabled');
                            }
                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    meeting_discussion: {
                        required: true
                    }

                },
                messages: {}
            }
        );

        $('#next_meeting_form').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);

                            if (data.status == 'success') {
                                //alert(data.message);
                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');

                                $('#add_discussion').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                location.reload();
                            }
                            if(data.status == 'fail') {
                                //alert(data.message);

                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');

                                //$('#add_discussion').modal('hide');
                                $('#proof-text').text(data.message);
                                $('#proof_message').modal('show');
                               // location.reload();
                            }
                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    meeting_medium: {
                        required: true
                    },
                    meeting_date:{
                        required:true
                    },
                    meeting_topic:{
                        required:true
                    }

                },
                messages: {}
            }
        );



        $('#remarks_form').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);

                            if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');

                                $('#add_remarks').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                location.reload();
                            }
                            else {

                                for (var key in data.errors) {
                                    // toastr['error'](data.errors[key]);
                                }
                                $('[type=submit]', form).removeAttr('disabled');
                            }
                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    remarks: {
                        required: true
                    }

                },
                messages: {}
            }
        );


    });

</script>
@stop



