
@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Today Schedule</li>
    </ul>
</div>

<div class="page-header">




    <form id="agent" class="orb-form">

       <div class="row">
            <section class="col col-md-4">
                <label class="label">Select Agent to forward/print task list</label>
                <label class="select ">
                    <select name="assigned" id="assigned">
                        <option value="">Choose name</option>
                        @foreach($admins as $admin)
                            <option value="{{ $admin->id }}">{{ $admin->first_name." ".$admin->last_name}}</option>
                        @endforeach
                    </select>
                    <i></i> </label>

            </section>
           <section class="col col-md-4">
               <label class="label">Select</label>
               <label class="select ">
                   <select name="forward" id="forward">
                       <option value="">Choose name</option>
                       @foreach($admins as $admin)
                       <option value="{{ $admin->id }}">{{ $admin->first_name." ".$admin->last_name}}</option>
                       @endforeach
                   </select>
                   <i></i> </label>

           </section>
            <section class="col col-md-3" style="margin-top: 30px;">
                @if(!role_permission('13','todayschedule_assignlead'))
                <button  type="button" disabled='' class="btn btn-info" >Assign</button>
                @elseif(role_permission('13','todayschedule_assignlead'))
                <button  type="submit" class="btn btn-info" >Assign</button>
                @endif

                @if(!role_permission('13','todayschedule_print'))
                <button  type="button" class="btn btn-info" disabled=''>Print</button>
                @elseif(role_permission('13','todayschedule_print'))
                <button  type="button" class="btn btn-info" onclick="task_print()">Print</button>
                @endif
            </section>
        </div>

    </form>

</div>
   
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

    <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{ $data->render() }}
                <script>
                    $('#test')
                        .on('preXhr.dt', function (e, settings, data) {

                            // on start of ajax call
                        }).on( 'draw.dt', function () {
                                $(".status").click(function(){
                                    var id = $(this).attr("id");
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/state/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".state").click(function(){
                                   var id = $(this).attr("id");
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/state/edit') }}",
                                        success:function(data){
                                            data = JSON.parse(data)
                                            $('#edit_form').modal('show');
                                            document.getElementById("state_name").value=data.record.state;
                                            document.getElementById("state_id").value=data.record.id;
                                            }
                                        },'json'
                                    );
                                });
                                $('.lead_to').change(function(){
                                    var id=$(this).attr('id');
                                    //alert('id='+id);
                                    var leadid=$("#"+id).val();
                                   // alert('leadid='+leadid);
                                    $.ajax({
                                        method:"POST",
                                        data:{id:id,leadid:leadid},
                                        url:"{{ URL::to('admin/todayschedule/update') }}",
                                        success:function(data){
                                            /*data = JSON.parse(data)
                                            $('#edit_form').modal('show');
                                            document.getElementById("state_name").value=data.record.state;
                                            document.getElementById("state_id").value=data.record.id;*/
                                            }
                                        },'json'
                                    );
                                });
                                $(".forward_agent").change(function(){
                                    var id = $(this).attr("id");
                                    var lead_agent_id = $(this).val();

                                    $.post("{{ URL::to('admin/todayschedule/agent')}}",{'id':id,'lead_agent_id':lead_agent_id},function(data){

                                        $('#test').dataTable()._fnAjaxUpdate();
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');

                                    },'json');
                                });

                        });
                </script>

                {{ $data->script() }}
            </div>
        </div>

    </div>


    <div class="modal fade" id="edit_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit State</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="state_edit" action='{{ URL::to("admin/state/update") }}' method="post" novalidate="novalidate">

                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>


                                    <input type="text" placeholder="State" name="state" value="" id="state_name"/>
                                    <input type="hidden" placeholder="" name="id" value="" id="state_id"/>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter state</b>
                                    {{ $errors->first('state','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="state" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add State</h4>
                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="state_add" action='{{ URL::to("admin/state/save") }}' method="post" novalidate="novalidate">

                        <fieldset>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="State" name="state" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter state</b>
                                    {{ $errors->first('state','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
<script>

    function task_print(){
        var assigned = $("#assigned").val();
        if(assigned == ""){
            $('#proof-text').text("Please Select agent First !");
            $('#proof_message').modal('show');
            return false;
        }
        else{
            window.location = "{{ URL::to('admin/todayschedule/print') }}/"+assigned;
        }

    }

    $().ready(function() {




        $("#agent").submit(function(e){
            e.preventDefault();
            var assigned = $("#assigned").val();
            var forward = $("#forward").val();
            if(assigned == "")
            {
                $('#proof-text').text("Please select a agent whose lead you want to forward !");
                $('#proof_message').modal('show');
                return false;
            }
            if(forward == "")
            {
                $('#proof-text').text("Please select a agent whome you want to forward leads !");
                $('#proof_message').modal('show');
                return false;
            }
            if(assigned == forward)
            {
                $('#proof-text').text("Oops you have selected same agent !");
                $('#proof_message').modal('show');
                return false;
            }
            else{
                $("#confirm_message").modal("show");
                $("#confirm_text").text("Confirm Submit !");
                $("#yesconfirm").click(function(){
                    $.get("{{ URL::to('admin/todayschedule/agent') }}",{'assigned':assigned,'forward':forward},function(data){

                        if(data.status = "success"){

                            $('#test').dataTable()._fnAjaxUpdate();
                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                        }
                        if(data.status == "error"){

                        }
                    },"json");
                })
            }
        });





        $('#state_add').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('#state').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#state').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    state: {
                        required: true
                    }

                },
                messages: {}
            }
        );
        //eidt district



        $('#state_edit').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {

                                $('#edit_form').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {


                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#edit_form').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    state: {
                        required: true
                    }

                },
                messages: {

                }
            }
        );


    });

</script>

    <!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->


@stop
