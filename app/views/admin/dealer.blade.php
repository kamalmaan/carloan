@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li class="active">Dealer</li>
    </ul>
</div>

<div class="page-header">

    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#add">Add Dealer</button>
    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong >Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>

<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

<div class="col-md-12 bootstrap-grid">

    <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
        <header>
            <h2>Dealer<small>Show Hide Columns</small></h2>
        </header>
        <div class="inner-spacer">
            {{ $data->render() }}
            <script>
                $('#test')
                    .on('preXhr.dt', function (e, settings, data) {
                        // on start of ajax call
                    }).on( 'draw.dt', function () {
                        $(".status").click(function(){
                            var id = $(this).attr("id");
                            $.ajax({
                                method:"GET",
                                data:{id:id},
                                url:"{{ URL::to('admin/dealer/status') }}",
                                success:function(data){
                                    $('#test').dataTable()._fnAjaxUpdate();
                                }
                            });
                        });
                        $(".dealer").click(function(){
                            var id = $(this).attr("id");
                            //alert(id);
                            $.ajax({
                                    method:"GET",
                                    data:{id:id},
                                    url:"{{ URL::to('admin/dealer/edit') }}",
                                    success:function(data){
                                        data = JSON.parse(data)
                                        $('#edit').modal('show');
                                        $('#id').val(data.record.id);
                                        $('#dealer').val(data.record.dealer);
                                        $('#gm_name').val(data.record.gm_name);
                                        $('#gm_phone').val(data.record.gm_phone);
                                        $('#gm_email').val(data.record.gm_email);
                                        $('#rc_name').val(data.record.rc_name);
                                        $('#rc_phone').val(data.record.rc_phone);
                                        $('#rc_email').val(data.record.rc_email);
                                        $('#ac_name').val(data.record.ac_name);
                                        $('#ac_phone').val(data.record.ac_phone);
                                        $('#ac_email').val(data.record.ac_email);
                                    }
                                },'json'
                            );
                        });
                    });
            </script>

            {{ $data->script() }}
        </div>
    </div>

</div>


<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Dealer</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="dealer_add" action='{{ URL::to("admin/dealer/save") }}' method="post" novalidate="novalidate">

                    <fieldset>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append glyphicon glyphicon-user"></i>
                                <input type="text" placeholder="Dealer" name="dealer" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Dealer</b>
                                {{ $errors->first('dealer','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>

                        <section class="form-group">
                            <label class="input"> <i class="icon-append glyphicon glyphicon-user"></i>
                                <input type="text" placeholder="Gm Name" name="gm_name" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Gm Name</b>
                                {{ $errors->first('gm_name','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="GM Mobile" name="gm_phone" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Gm Mobile</b>
                                {{ $errors->first('gm_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="GM Email" name="gm_email" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Gm Email</b>
                                {{ $errors->first('gm_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append glyphicon glyphicon-user"></i>
                                <input type="text" placeholder="RC Executive Name" name="rc_name" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter RC Executive Name</b>
                                {{ $errors->first('rc_name','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="RC Executive Mobile" name="rc_phone" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter RC Executive Mobile</b>
                                {{ $errors->first('rc_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="RC Executive Email" name="rc_email" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter RC Email</b>
                                {{ $errors->first('rc_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append glyphicon glyphicon-user"></i>
                                <input type="text" placeholder="Accountant Name" name="ac_name" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Accountant Name</b>
                                {{ $errors->first('ac_name','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="Accountant Mobile" name="ac_phone" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Accountant Mobile</b>
                                {{ $errors->first('ac_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="Accountant Email" name="ac_email" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Accountant Email</b>
                                {{ $errors->first('ac_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Dealer</h4>
            </div>
            <div class="modal-body">
                <form class="orb-form cmxform" id="dealer_edit" action='{{ URL::to("admin/dealer/update") }}' method="post" novalidate="novalidate">
                    <input type="hidden" placeholder="" name="id" id="id" value="">
                    <fieldset>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append glyphicon glyphicon-user"></i>
                                <input type="text" placeholder="Dealer" name="dealer" id="dealer" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Dealer</b>
                                {{ $errors->first('dealer','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>

                        <section class="form-group">
                            <label class="input"> <i class="icon-append glyphicon glyphicon-user"></i>
                                <input type="text" placeholder="Gm Name" name="gm_name" id="gm_name" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Gm Name</b>
                                {{ $errors->first('gm_name','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="GM Mobile" name="gm_phone" id="gm_phone" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Gm Mobile</b>
                                {{ $errors->first('gm_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="GM Email" name="gm_email" id="gm_email" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Gm Email</b>
                                {{ $errors->first('gm_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append glyphicon glyphicon-user"></i>
                                <input type="text" placeholder="RC Executive Name" name="rc_name" id="rc_name" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter RC Executive Name</b>
                                {{ $errors->first('rc_name','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="RC Executive Mobile" name="rc_phone" id="rc_phone" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter RC Executive Mobile</b>
                                {{ $errors->first('rc_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="RC Executive Email" name="rc_email" id="rc_email" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter RC Email</b>
                                {{ $errors->first('rc_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append glyphicon glyphicon-user"></i>
                                <input type="text" placeholder="Accountant Name" name="ac_name" id="ac_name" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Accountant Name</b>
                                {{ $errors->first('ac_name','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="number" placeholder="Accountant Mobile" name="ac_phone" id="ac_phone" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Accountant Mobile</b>
                                {{ $errors->first('ac_phone','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                        <section class="form-group">
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" placeholder="Accountant Email" name="ac_email" id="ac_email" value="">
                                <b class="tooltip tooltip-bottom-right">Needed to enter Accountant Email</b>
                                {{ $errors->first('ac_email','<em class="invalid text-danger" > :message </em>') }}
                            </label>
                        </section>
                    </fieldset>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save changes</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>



<script>

    $().ready(function() {

        $('#dealer_add').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {
                                $('#add').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');

                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#add').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    dealer: {
                        required: false
                    },
                    gm_name:{
                        required:false
                    },
                    gm_phone:{
                        required:false
                    },
                    gm_email:{
                        required:false
                    },
                    rc_name:{
                        required:false
                    },
                    rc_phone:{
                        required:false
                    },
                    rc_email:{
                        required:false
                    },
                    ac_name:{
                        required:false
                    },
                    ac_phone:{
                        required:false
                    },
                    ac_email:{
                        required:false
                    }

                },
                messages: {}
            }
        );
        //eidt district



        $('#dealer_edit').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    //uiLoader('#form-add-body', 'show');
                    //var l = Ladda.create($('[type=submit]', form)[0]);
                    //l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            console.log('data')

                            if (data.status == 'fail') {

                                $('#edit').modal('hide');
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                form[0].reset();
                                $('.form-group', form).removeClass('has-error');
                                //$('input', form).iCheck('update');

                                // toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                $('#edit').modal('hide');
                                $('#test').dataTable()._fnAjaxUpdate();
                                $('#message_text').text(data.message);
                                $('#message').modal('show');
                            } else {

                            }

                            //l.stop();
                            //uiLoader('#form-add-body', 'hide');
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    dealer: {
                        required: false
                    },
                    gm_name:{
                        required:false
                    },
                    gm_phone:{
                        required:false
                    },
                    gm_email:{
                        required:false
                    },
                    rc_name:{
                        required:false
                    },
                    rc_phone:{
                        required:false
                    },
                    rc_email:{
                        required:false
                    },
                    ac_name:{
                        required:false
                    },
                    ac_phone:{
                        required:false
                    },
                    ac_email:{
                        required:false
                    }
                },
                messages: {

                }
            }
        );


    });

</script>

<!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->


@stop
