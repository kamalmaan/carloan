@extends('admin/layout')
@section('content')

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
    <ul>
        <li><a href="{{ URL::to('admin') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
        <li><a href="{{ URL::to('admin/lead') }}">Co-Applicant</a></li>
        <li class="active">Add Co-Appplicant</li>
    </ul>
</div>
<!--/Breadcrumb-->

<div class="page-header">



    <br>
    @if(Session::has('message'))
    <div class="callout callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Well done ! </strong>{{ Session::get('message')}}
    </div>
    @endif

</div>
<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">


<div class="col-md-12 bootstrap-grid">


<div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">

<div data-widget-editbutton="false" id="registration-form-validation-widget"
     class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
<header role="heading">
    <h2>
        <Lead></Lead>
        <small>You Can Add Your Lead Type</small>
    </h2>
    <div class="powerwidget-ctrls" role="menu"><a class="button-icon powerwidget-delete-btn" href="#"><i
                class="fa fa-times-circle"></i></a> <a class="button-icon powerwidget-fullscreen-btn" href="#"><i
                class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i
                class="fa fa-chevron-circle-up "></i></a></div>
    <span class="powerwidget-loader"></span></header>
<div class="inner-spacer" role="content">
<form class="orb-form" id="registration-form" action="{{ URL::to('admin/co-applicant/save/'.$lead_id)}}"method="post"
      novalidate="novalidate" enctype="multipart/form-data">
<header></header>



<fieldset>

    <div class="row">

            <section class="col col-4 form-group firm_container">
            <strong class="text text-dark-blue">Customer First Name</strong>
            <label class="input">
                <i class="icon-append fa fa-user"></i>
                <input type="text" name="first_name" id="" placeholder="First Name"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter First Name</b>

            </label>
        </section>
        <section class=" form-group col col-4 firm_container " >
            <strong class="text text-dark-blue">Customer Middle Name</strong>
            <label class="input">
                <i class="icon-append fa fa-user"></i>
                <input type="text" name="middle_name" id="" placeholder="Middle Name"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Last Name</b>

            </label>
        </section>
        <section class="  form-group  col col-4 firm_container " >
            <strong class="text text-dark-blue">Customer Last Name</strong>
            <label class="input">
                <i class="icon-append fa fa-user"></i>
                <input type="text" name="last_name" id="" placeholder="Last Name"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Last Name</b>

            </label>
        </section>
    </div>


    <div class="row">

        <section class="col col-4 form-group">
            <h5 class="text text-dark-blue ">Choose The Type Of User</h5>
            <div class="inline-group">
                <label class="radio">
                    <input type="radio" value = "person" id="person_type" class="type" name="type" checked>
                    <i></i>Single Person</label>
                <label class="radio">
                    <input type="radio" value="firm" id="firm_type" class="type" name="type" >
                    <i></i>Firm</label>
            </div>


        </section>
        <section class="col col-4 form-group">
            <label class="input">
                <i class="icon-append fa fa-whatsapp"></i>
                <input type="number" name="whatsapp_number" id="" placeholder="Whatsapp Number" >
                <b class="tooltip tooltip-bottom-right">Needed to enter your Whatsapp Number</b>

            </label>
        </section>

    </div>

    <div class="row" id="firm"  style="{{($errors->has('company_name') || $errors->has('constitution') || $errors->has('company_category'))? 'display:block':'display:none' }}">
        <section class="col col-4 form-group">
            <label class="input">
                <i class="icon-append fa fa-user"></i>
                <input type="text" name="company_name" id="" placeholder="Company Name" >
                <b class="tooltip tooltip-bottom-right">Needed to enter Your Company Name</b>

            </label>
        </section>
        <section class="col col-4 form-group">
            <label class="select">
                <select name="constitution" id="">
                    <option value="">--Please Select Constitution Type--</option>
                    <option value="Partnership" >Partnership</option>
                    <option value="Public Ltd. Co." >Public Ltd. Co.</option>
                    <option value="PVT Ltd. Co." >PVT Ltd. Co.</option>
                    <option value="Salaried" >Salaried</option>
                    <option value="Self Employed" >Self Employed</option>
                    <option value="Society" >Society</option>
                    <option value="Sole Propriertors" >Sole Propriertors</option>
                    <option value="Student" >Student</option>
                </select>

            </label>
        </section>
        <section class="col col-4 form-group">
            <label class="select">
                <select name="company_category" id="">
                    <option value="">--Please Select Company Category--</option>
                    <option value="CAT D" >CAT D</option>
                    <option value="CAT DEN WITH CLINIC" >CAT DEN WITH CLINIC</option>
                    <option value="CAT DEN WITHOUT CLINIC" >CAT DEN WITHOUT CLINIC</option>
                    <option value="CAT DOC HOSPITAL" >CAT DOC HOSPITAL</option>
                    <option value="CAT DOC AYURVEDA" >CAT DOC AYURVEDA</option>
                    <option value="CAT DOC CONSULTANT" >CAT DOC CONSULTANT</option>
                    <option value="CAT DOC DIAGNOSTICS" >CAT DOC DIAGNOSTICS</option>
                    <option value="CAT DOC GP WITH CLINIC" >CAT DOC GP WITH CLINIC</option>
                </select>

            </label>
        </section>
    </div>
    <div id="guardian" style="{{($errors->has('guardian') || $errors->has('guardian_first_name') || $errors->has('guardian_last_name'))? 'display:block':'display:none' }}">
        <div class="row">
            <section class="col col-4 form-group firm_container">
                <label class="input">
                    <i class="icon-append fa fa-user"></i>
                    <input type="text" name="occupation" id="" placeholder="Customer Occuptaion" >
                    <b class="tooltip tooltip-bottom-right">Needed to enter Your Occupation</b>

                </label>
            </section>

            <section class="col col-3 form-group firm_container">
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" value = "S/O" name="guardian" >
                        <i></i>S/O</label>
                    <label class="radio">
                        <input type="radio" value="W/O" name="guardian" >
                        <i></i>W/O</label>
                    <label class="radio">
                        <input type="radio" value="D/O" name="guardian" >
                        <i></i>D/O</label><br>

                </div>
            </section>

        </div>
        <div class="row" >
            <section class="col col-4 form-group firm_container ">
                <label class="input">
                    <i class="icon-append fa fa-user"></i>
                    <input type="text" name="guardian_first_name" id="" placeholder="Guardian First Name" >
                    <b class="tooltip tooltip-bottom-right">Needed to enter Your Gaurdian's First Name</b>

                </label>
            </section>
            <section class="col col-4 form-group firm_container ">
                <label class="input">
                    <i class="icon-append fa fa-user"></i>
                    <input type="text" name="guardian_middle_name" id="" placeholder="Guardian Middle Name" >
                    <b class="tooltip tooltip-bottom-right">Needed to enter Your Gaurdian's Middle Name</b>

                </label>
            </section>
            <section class="col col-4 form-group firm_container">
                <label class="input">
                    <i class="icon-append fa fa-user"></i>
                    <input type="text" name="guardian_last_name" id="" placeholder="Guardian Last Name" >
                    <b class="tooltip tooltip-bottom-right">Needed to enter Your Gaurdian's Last Name</b>

                </label>
            </section>
        </div>
    </div>
    <div class="row">
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Customer Contact Number</strong>
            <label class="input">
                <i class="icon-append fa fa-phone"></i>
                <input type="number" name="phone" id="" placeholder="Phone" >
                <b class="tooltip tooltip-bottom-right">Needed to enter Your Contact Number</b>

            </label>
        </section>
        <section class=" form-group col col-4 firm_container ">
            <strong class="text text-dark-blue">Date of Birth</strong>
            <label class="input">
                <i class="icon-append fa fa-calendar"></i>
                <input type="date" class="dob" name="dob" id="date" placeholder="" >
                <b class="tooltip tooltip-bottom-right">Enter Your Date of Birth</b>

            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Customer Email</strong>
            <label class="input">
                <i class="icon-append fa fa-envelope"></i>
                <input type="text" name="email_id" id="" placeholder="E-mail" >
                <b class="tooltip tooltip-bottom-right">Needed to enter Your Email Address</b>

            </label>
        </section>
    </div>
</fieldset>
<!------------address---------------->
<fieldset>



    <div class="user-profile-info" id="address_div">
        <div class="tabs-white">
            <ul id="myTabs" class="nav nav-tabs nav-justified">
                <li class="active tab firm_container "><a href="#permanent_residence" data-toggle="tab">Permanent Residence Address</a></li>
                <li class="firm_container"><a href="#current_residence" data-toggle="tab">Current Residence Address</a></li>
                <li><a href="#current_office" data-toggle="tab">Current Office Address</a></li>
                <li><a href="#permanent_office" data-toggle="tab">Permanent Office Address</a></li>
            </ul>
            <div id="myTabContents" class="tab-content">


                <div class="tab-pane in active firm_container" id="permanent_residence">
                    <div class="profile-header">Permanent Residence Address</div>

                    <div class="row">
                        <section class="  form-group  col col-4 ">
                            <strong class="text text-dark-blue">permanent residence address(House Number and
                                Street)</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="text" name="permanent_resi_street" id=""
                                       placeholder="Permanent residence house number and street"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Residence House number
                                    and Street Number</b>

                            </label>
                        </section>
                        <section class="  form-group  col col-4 ">
                            <strong class="text text-dark-blue">City</strong>
                            <label class="select">
                                <select name="pemanent_resi_city_id" id="pemanent_resi_city_id">
                                    <option value="">--Please Select City Name--</option>
                                    @foreach($city as $cities) {
                                    <option value="{{ $cities->id }}"  >{{ $cities->city}}</option>
                                    }
                                    @endforeach
                                </select>

                            </label>
                        </section>
                        <section class="  form-group  col col-4 ">
                            <strong class="text text-dark-blue">District</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="text" name="permanent_resi_district_id" id="permanent_resi_district_id"
                                      >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Residence District
                                </b>


                            </label>
                        </section>
                    </div>

                    <div class="row">
                        <section class="  form-group  col col-4 ">
                            <strong class="text text-dark-blue">State</strong>
                            <label class="input">

                                <i class="icon-append fa fa-phone"></i>
                                <input type="text" name="permanent_resi_state_id" id="permanent_resi_state_id" readonly
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Residence District
                                </b>


                            </label>
                        </section>
                        <section class="  form-group  col col-4 ">
                            <strong class="text text-dark-blue">Pin Code</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="permanent_resi_pincode" id="" placeholder="Permanent residence city Pin Code"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Residence PinCode Number
                                </b>

                            </label>
                        </section>
                        <section class="  form-group  col col-4 ">
                            <strong class="text text-dark-blue">Phone</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="permanent_resi_phone" id=""
                                       placeholder="Permanent residence house number and street"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter permanent Residence Phone Number</b>

                            </label>
                        </section>
                    </div>

                </div>
                <div class="tab-pane firm_container" id="current_residence">
                    <div class="profile-header">Current Residence Address</div>
                    <div class="row">
                        <section class="form-group col col-4 ">
                            <strong class="text text-dark-blue">Current residence address(House Number and
                                Street)</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="text" name="current_resi_street" id=""
                                       placeholder="Current residence house number and street"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Residence House number
                                    and Street Number</b>

                            </label>
                        </section>
                        <section class=" form-group col col-4 ">
                            <strong class="text text-dark-blue">City</strong>
                            <label class="select">
                                <select name="current_resi_city_id" id="current_resi_city_id">
                                    <option value="">--Please Select City Name--</option>
                                    @foreach($city as $cities) {
                                    <option value="{{ $cities->id }}" >{{ $cities->city}}</option>
                                    }
                                    @endforeach
                                </select>

                            </label>
                        </section>
                        <section class="form-group  col col-4 ">
                            <strong class="text text-dark-blue">District</strong>
                            <label class="input">
                                <i class="icon-append fa fa-road"></i>
                                <input type="text" name="current_resi_district_id" readonly id="current_resi_district_id"
                                       placeholder="Current Residence District"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Residence District</b>
                            </label>
                        </section>

                    </div>
                    <div class="row">
                        <section class="form-group  col col-4 ">
                            <strong class="text text-dark-blue">District</strong>
                            <label class="input">
                                <i class="icon-append fa fa-road"></i>
                                <input type="text" name="current_resi_state_id" readonly id="current_resi_state_id"
                                       placeholder="Current Residence State"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Residence State</b>
                            </label>
                        </section>
                        <section class="  form-group  col col-4 ">
                            <strong class="text text-dark-blue">Pin Code</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="current_resi_pincode" id="" placeholder="Current residence city Pin Code"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Residence PinCode Number
                                </b>

                            </label>
                        </section>
                        <section class="  form-group  col col-4 ">
                            <strong class="text text-dark-blue">Phone</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="current_resi_phone" id=""
                                       placeholder="Current residence house number and street"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Residence Phone Number</b>

                            </label>
                        </section>
                    </div>


                </div>


                <div class="tab-pane " id="current_office">
                    <div class="profile-header">Current Office Address</div>

                    <div class="row">
                        <section class="  form-group col col-4">
                            <strong class="text text-dark-blue">Official Name</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="text" name="current_office_name" id=""
                                       placeholder="current office house number and street"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter current office Name</b>

                            </label>
                        </section>
                        <section class="  form-group  col col-4">
                            <strong class="text text-dark-blue">Current address(House Number and
                                Street)</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="text" name="current_office_street" id=""
                                       placeholder="current office house number and street"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter current office House number
                                    and Street Number</b>

                            </label>
                        </section>
                        <section class="  form-group col col-4">
                            <strong class="text text-dark-blue">City</strong>
                            <label class="select">
                                <select name="current_office_city_id" id="current_office_city_id">
                                    <option value="">--Please Select City Name--</option>

                                    @foreach($city as $cities) {
                                    <option value="{{ $cities->id }}" >{{ $cities->city}}</option>
                                    }
                                    @endforeach
                                </select>

                            </label>
                        </section>

                    </div>
                    <div class="row">
                        <section class="  form-group col col-4">
                            <strong class="text text-dark-blue">District</strong>
                            <label class="input">
                                <i class="icon-append fa fa-road"></i>
                                <input type="text" name="current_office_district_id" readonly id="current_office_district_id"
                                       placeholder="Current Office District"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office District</b>
                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">State</strong>
                            <label class="input">
                                <i class="icon-append fa fa-road"></i>
                                <input type="text" name="current_office_state_id" readonly id="current_office_state_id"
                                       placeholder="Current Office State"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office Sate</b>
                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Email</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="email" name="current_office_email" id=""
                                       placeholder="Current Office Email"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office Email</b>

                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Phone</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="current_office_phone" id=""
                                       placeholder="Current Office Phone"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office Phone Number</b>

                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Alternate Phone</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="current_office_alternate_phone" id=""
                                       placeholder="Current Office Alternate Phone Number"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office Alternate Phone Number</b>

                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Mobile</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="current_office_mobile" id=""
                                       placeholder="Current Office Mobile"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office Mobile</b>
                                {{ $errors->first('current_office_mobile','<em class="invalid text-danger">
                                    :message </em>') }}
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Pin Code</strong>
                            <label class="input">
                                <i class="icon-append fa fa-road"></i>
                                <input type="number" name="current_office_pincode" id="" placeholder="Current Office Pin Code"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office PinCode Number
                                </b>
                                {{ $errors->first('current_office_pincode','<em class="invalid text-danger">
                                    :message </em>') }}
                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Fax</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="current_office_fax" id="" placeholder="Current Office Pin Code"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office Fax
                                </b>

                            </label>
                        </section>
                    </div>

                </div>
                <div class="tab-pane" id="permanent_office">
                    <div class="profile-header">Permanent Office Address</div>

                    <div class="row">
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Official Name</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="text" name="permanent_office_name" id=""
                                       placeholder="Permanent Office Name"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent office Name</b>

                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Permanent Office Street</strong>
                            <label class="input">
                                <i class="icon-append fa fa-road"></i>
                                <input type="text" name="permanent_office_street" id=""
                                       placeholder="Permanent office street"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent office Street</b>

                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">City</strong>
                            <label class="select">
                                <select name="permanent_office_city_id" id="permanent_office_city_id">
                                    <option value="">--Please Select City Name--</option>
                                    @foreach($city as $cities) {
                                    <option value="{{ $cities->id }}" >{{ $cities->city}}</option>
                                    }
                                    @endforeach
                                </select>

                            </label>
                        </section>

                    </div>
                    <div class="row">
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">District</strong>
                            <label class="input">
                                <i class="icon-append fa fa-road"></i>
                                <input type="text" name="permanent_office_district_id" readonly id="permanent_office_district_id"
                                       placeholder="Permanent Office State"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office District</b>
                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">State</strong>
                            <label class="input">
                                <i class="icon-append fa fa-road"></i>
                                <input type="text" name="permanent_office_state_id" readonly id="permanent_office_state_id"
                                       placeholder="Permanent Office State"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office State</b>
                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Email</strong>
                            <label class="input">
                                <i class="icon-append fa fa-envelope"></i>
                                <input type="email" name="permanent_office_email" id=""
                                       placeholder="Permanent Office Email"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Current Office Email</b>

                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Phone</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="permanent_office_phone" id=""
                                       placeholder="Permanent Office house number and street"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Office Phone Number</b>

                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Alternate Phone</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="permanent_office_alternate_phone" id=""
                                       placeholder="Permanent Office Alternate Phone Number"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Office Alternate Phone Number</b>

                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Mobile</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="permanent_office_mobile" id=""
                                       placeholder="Permanent Office Mobile"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Office Mobile</b>

                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Pin Code</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="permanent_office_pincode" id="" placeholder="Permanent Office Pin Code"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Office PinCode Number
                                </b>

                            </label>
                        </section>
                        <section class=" form-group col col-4">
                            <strong class="text text-dark-blue">Fax</strong>
                            <label class="input">
                                <i class="icon-append fa fa-phone"></i>
                                <input type="number" name="permanent_office_fax" id="" placeholder="Permanent Office Pin Code"
                                       >
                                <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Office Fax
                                </b>

                            </label>
                        </section>
                    </div>

                </div>


            </div>
        </div>

        <!--/Chat Tab-->


    </div>


</fieldset>

<fieldset>

   <div class="row">
        <section class=" form-group col col-4 firm_container">
            <strong class="text text-dark-blue">Profile Picture</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="profile_picture" id="profile_picture_1" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>

                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_1" src="" alt="No Preview" height="50" width="100" >
            </label>

        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Signature Image</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="signature" id="profile_signature_2" onchange="readURL(this);" multiple=""  >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_2" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
</fieldset>
<!-------------- Customer verifications and proof -------------->
<fieldset>


<div class="user-profile-info" id="proof_div">
<div class="tabs-white">
<ul id="myTab" class="nav nav-tabs nav-justified ">
    <li class="active"><a style="padding: 13px 1px;" href="#id_proofs" data-toggle="tab">Id Proofs</a></li>
    <li class="firm_container"><a style="padding: 13px 1px;" href="#residential_proofs" data-toggle="tab">Residential Proofs</a></li>
    <li><a style="padding: 13px 1px;" href="#income_proofs" data-toggle="tab">Income Proofs</a></li>
    <li><a style="padding: 13px 1px;" href="#bank_status" data-toggle="tab">Bank Status</a></li>
    <li><a style="padding: 13px 1px;" href="#signature_verification" data-toggle="tab">Signature Verification</a></li>
    <li><a style="padding: 13px 1px;" href="#ownership_proofs" data-toggle="tab">Ownership Proofs</a></li>
</ul>
<div id="myTabContent" class="tab-content">
<div class="tab-pane in active" id="id_proofs">
    <div class="profile-header">Id Proofs</div>
    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Pan Card Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="proof_pan_card_number" class="3" id="1_pan_3" onchange="getvalue(this);" placeholder="Pan Card Number"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Pan Card Number</b>

            </label>
        </section>
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Pan Card Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="proof_pan_card_proof" id="id_pan_3"  onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="" readonly="">

                <img class="img-thumbnail blah_3" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6 firm_container">
            <strong class="text text-dark-blue">Voter card Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="proof_voter_card_number" class="4" id="1_voter_4" onchange="getvalue(this);" placeholder="Voter Card Number"
                      >
                <b class="tooltip tooltip-bottom-right">Needed to enter Voter Card Number</b>

            </label>
        </section>
        <section class=" form-group col col-6 firm_container">
            <strong class="text text-dark-blue">Voter Card Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="proof_voter_card_proof" id="id_voter_4" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_4" src="" alt="No Image" height="50" width="100"class="img-thumbnail">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6 firm_container ">
            <strong class="text text-dark-blue">Adhar Card Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="proof_adhar_card_number" id="1_adhar_5" placeholder="Adhar Card Number"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter adhar Card Number</b>

            </label>
        </section>
        <section class=" form-group col col-6 firm_container">
            <strong class="text text-dark-blue">Adhar Card Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="proof_adhar_card_proof" id="id_adhar_5" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_5" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6 firm_container">
            <strong class="text text-dark-blue">Driving License Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="proof_driving_license_number" class="6" id="1_license_6" onchange="getvalue(this);" placeholder="Driving License Number"
                     >
                <b class="tooltip tooltip-bottom-right">Needed to enter Driving License Number</b>

            </label>
        </section>
        <section class=" form-group col col-6 firm_container">
            <strong class="text text-dark-blue">Driving License Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="proof_driving_license_proof" id="id_driving_6" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_6" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6 firm_container">
            <strong class="text text-dark-blue">Passport Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="prrof_passport_number" class="7" id="1_passport_7" onchange="getvalue(this);" placeholder="Passport Number"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Passport Number</b>

            </label>
        </section>
        <section class=" form-group col col-6 firm_container ">
            <strong class="text text-dark-blue">Passport Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="proof_passport_proof" id="id_passport_7" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_7" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6 firm_container">
            <strong class="text text-dark-blue">Arm License Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="proof_arm_license_number" id="1_arm_8" placeholder="Arm License Number"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Arm License Number</b>

            </label>
        </section>
        <section class=" form-group col col-6 firm_container">
            <strong class="text text-dark-blue">Arm License Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="proof_arm_license_proof" id="id_arm_8" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_8" src="" alt="No Preview" height="50" width="100">

            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Bank Account Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="proof_bank_account_number" class="91" id="3bank_number_91" placeholder="Bank Account Number" onchange="getvalue(this);"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Bank Account Number</b>

            </label>
        </section>
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Bank Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="proof_bank_proof" id="id_bank_9" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_9" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Other Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="other_proof" id="id_other_10" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_10" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>

</div>
<!------ Residential Proof --------->
<div class="tab-pane" id="residential_proofs">
    <div class="profile-header"> Residential Proofs</div>

    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Rashan Card Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="resi_rashan_id_proof" id="residential_rashan_11" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_11" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Driving License Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="resi_driving_license__number" class="6" id="2_license_6" onchange="getvalue(this);" placeholder="Driving License Number"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Driving License Number</b>

            </label>
        </section>
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Driving License Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="resi_driving_license_proof" id="residential_driving_6" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_6" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Passport Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="resi_passport__number" class="7" id="1_passport_7" onchange="getvalue(this);" placeholder="Passport Number"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Passport Number</b>

            </label>
        </section>
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Passport Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="resi_passport_proof" id="residential_passport_7" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file"  readonly="">
                <img class="img-thumbnail blah_7" src="" alt="No Preview" height="50" width="100"class="img-thumbnail">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Voter card Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="resi_voter_car_number" class="4" id="2_voter_4" onchange="getvalue(this);" placeholder="Voter Card Number"
                      >
                <b class="tooltip tooltip-bottom-right">Needed to enter Voter Card Number</b>

            </label>
        </section>
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Voter Card Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="resi_voter_card_proof" id="residential_voter_4" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_4" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>

</div>
<!---------------Income Proof--------------->
<div class="tab-pane" id="income_proofs">
    <div class="profile-header">Income Proofs</div>

    <div class="row">
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">ITR Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="income_itr_number" id="1income_itr_15" placeholder="ITR Number"
                   >
                <b class="tooltip tooltip-bottom-right">Needed to enter ITR Number</b>

            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">ITR Proof 1<sup>st</sup></strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="income_itr_1_proof" id="income_itr_15" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_15" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">ITR Proof 2<sup>nd</sup></strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="income_itr_2_proof" id="income_itr2_16" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_16" src="" alt="No Preview" height="50" width="100"class="img-thumbnail">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Computation Number 1<sup>st</sup></strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="income_computation_number" id="1income_computation_17" placeholder="Computation Number"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Computation Number</b>

            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Computation Proof 1<sup>st</sup></strong>
            <label for="file" class="input input-file">
                <div class="button">
                    <input type="file" name="income_computaion_1_proof" id="income_computation_17" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_17" src="" alt="No Preview" height="50" width="100"class="img-thumbnail">
            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Computation Proof 2<sup>nd</sup></strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="income_computation_2_proof" id="income_computation2_18" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_18" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>

    <div class="row">
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Bank Name</strong>
            <label class="input">
                <i class="icon-append fa fa-bank"></i>
                <input type="text" name="income_bank_name" class="9" id="1bank_name_9" placeholder="Bank Name" onchange="getvalue(this);"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Bank Name</b>

            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Bank Account Number</strong>
            <label class="input">
                <i class="icon-append fa fa-bank"></i>
                <input type="text" name="income_bank_account_number" class="91" id="1bank_number_91" onchange="getvalue(this);" placeholder="Bank Account Number"
                        >
                <b class="tooltip tooltip-bottom-right">Needed to enter Bank Account Number</b>

            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Bank Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="income_bank_1_proof" id="income_bank_9" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_9" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Attach Bank Copy Id Proof 2<sup>nd</sup></strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="income_bank_2_proof" id="income_bank2_20" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class ="img-thumbnail blah_20" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Attach Fard</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="income_fard" id="income_fard_21" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_21" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Other Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="income_other_proof" id="income_other_10" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_10" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>

</div>

<!-------Bank Status ----------->
<div class="tab-pane" id="bank_status">
    <div class="profile-header">Bank Status</div>
    <div class="row">
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Bank Name</strong>
            <label class="input">
                <i class="icon-append fa fa-bank"></i>
                <input type="text" name="bank_bank_name" class="9" id="2bank_name_9" placeholder="Bank Name" onchange="getvalue(this);"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Bank Name</b>

            </label>
        </section>
        <section class=" form-group col col-4">
            <strong class="text text-dark-blue">Bank Account Number</strong>
            <label class="input">
                <i class="icon-append fa fa-bank"></i>
                <input type="text" name="bank_bank_account_number" class="91" id="2bank_number_91" onchange="getvalue(this);" placeholder="Bank Account Number"
                        >
                <b class="tooltip tooltip-bottom-right">Needed to enter Bank Account Number</b>

            </label>
        </section>
    </div>
</div>

<!---------- Signature verification----------->
<!--Chat Tab-->
<div class="tab-pane in" id="signature_verification">
    <div class="profile-header">Signature Verification</div>

    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Pan Card Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="sign_pan_card_number" class="3" id="2_pan_3" onchange="getvalue(this);" placeholder="Pan Card Number"
                       >
                <b class="tooltip tooltip-bottom-right">Needed to enter Pan Card Number</b>

            </label>
        </section>
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Pan Card Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="sign_pan_card_proof" id="signature_pan_3" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_3" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Passport Number</strong>
            <label class="input">
                <i class="icon-append fa fa-file-text"></i>
                <input type="text" name="sign_passport__number" class="7" id="1_passport_7" onchange="getvalue(this);" placeholder="Passport Number"
                      >
                <b class="tooltip tooltip-bottom-right">Needed to enter Passport Number</b>

            </label>
        </section>
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Passport Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="sign_passport_proof" id="signature_passport_7" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_7" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Other Id Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="sign_other_proof" id="signature_other_10" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_10" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
    </div>
</div>

<!--/ owner Chat Tab-->
<div class="tab-pane in" id="ownership_proofs">
    <div class="profile-header">Ownership Proofs</div>

    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Electricity Bill</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="owner_electricity_bill" id="ownership_electricity_26" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_26" src="" alt="No Preview" height="50" width="100">
            </label>
        </section>
        <div class="row">
            <section class=" form-group col col-6">
                <strong class="text text-dark-blue">Sevrage Bill</strong>
                <label for="file" class="input input-file">
                    <div class="button"><input type="file" name="owner_sevrage_bill" id="owner_sevrage_27" onchange="readURL(this);" multiple=""
                                               >Browse
                    </div>
                    <input type="text" placeholder="" readonly="">
                    <img class="img-thumbnail blah_27" src="" alt="No Preview" height="50" width="100">
                </label>
            </section>
        </div>
    </div>
    <div class="row">
        <section class=" form-group col col-6">
            <strong class="text text-dark-blue">Property Papers Proof</strong>
            <label for="file" class="input input-file">
                <div class="button"><input type="file" name="owner_property_paper" id="owner_property_28" onchange="readURL(this);" multiple=""
                                           >Browse
                </div>
                <input type="text" placeholder="Include some file" readonly="">
                <img class="img-thumbnail blah_28" src="" alt="No preview" height="50" width="100">
            </label>
        </section>
        <div class="row">
            <section class=" form-group col col-6">
                <strong class="text text-dark-blue">Other Ownership Bill Proof</strong>
                <label for="file" class="input input-file">
                    <div class="button"><input type="file" name="owner_other_ownership_proof" id="owner_other_10" onchange="readURL(this);" multiple=""
                                               >Browse
                    </div>
                    <input type="text" placeholder="Include some file" readonly="">
                    <img class="img-thumbnail blah_10" src="" alt="No Preview" height="50" width="100">
                </label>
            </section>
        </div>
    </div>

</div>


</div>

</fieldset>
<footer>
    <button class="btn btn-default" type="submit" >Add Co-Applicant</button>
</footer>
</form>
</div>
</div>

</div>
<!-- /Inner Row Col-md-12 -->


</div>

<!-- /End Widget -->

</div>
<!-- /Inner Row Col-md-6 -->
<script>




    function readURL(input) {
        //console.log(input);
       var id = input.id;
        var t = id.split("_");
        //alert(t[2]);
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.blah_'+t[2]).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);

        }

    }

    function getvalue(input){
        var id = input.id;
        var val_u = input.value;
        //alert(val_u);
        var t = id.split("_");
        //alert(t[2]);

        $("input."+t[2]).each(function(){

            $(this).val(val_u);
        })
    }

    $(document).ready(function () {



        $("#existing_id").change(function(){

            var exist_check = $(this).val();
            //alert(exist_check);
            if(exist_check == 1){

                document.getElementById("check_existing").style.display="block";
            }
            else{

                document.getElementById("check_existing").style.display="none";
            }

        })


        $(".type").click(function(){
            var type_check =  $(this).val();
            if(type_check == 'person'){
                $('#guardian').slideToggle(500);
                $(".firm_container").show();
                document.getElementById("firm").style.display="none";

            }
            else{
                $("#firm").slideToggle(500);
                $(".firm_container").hide();
                document.getElementById("guardian").style.display="none";
            }
        });

                                               /* $(".type").click(function(){
                                                    var type_check =  $(this).val();
                                                    //alert(type_check);
                                                    if(type_check == 'firm'){
                                                        $(".firm_container").hide();
                                                    }


                                                });
                                                      */ //document.getElementsById(".btn btn-info btn-sm").style.display="block";
                                                      // alert(type_check);
                                                      //  $('#dob').slideToggle(500);

                                                        //document.getElementsById("date").style.display="none";


                                                   /* else{
                                                        //$("#date").slideToggle(500);
                                                        //document.getElementByName("dob").style.display="none";
                                                    }*/


        $('#pemanent_resi_city_id').change(function (){

            var city_id = $(this).val();
            //alert(city_id);
            $.ajax({
                method:"POST",
                data:{city_id:city_id},
                url:"{{ URL::to('admin/approve/district') }}",
                success:function(data){

                    //alert(data.district);
                    var data = JSON.parse(data);

                    $('#permanent_resi_district_id').val(data.district);
                    $('#permanent_resi_state_id').val(data.state);

                }
            });
        });


        $('#current_resi_city_id').change(function (){

            var city_id = $(this).val();

            $.ajax({
                method:"POST",
                data:{city_id:city_id},
                url:"{{ URL::to('admin/approve/district') }}",
                success:function(data){

                    //alert(data.district);
                    var data = JSON.parse(data);

                    $('#current_resi_district_id').val(data.district);
                    $('#current_resi_state_id').val(data.state);

                }
            });
        });

        $('#permanent_office_city_id').change(function (){

            var city_id = $(this).val();

            $.ajax({
                method:"POST",
                data:{city_id:city_id},
                url:"{{ URL::to('admin/approve/district') }}",
                success:function(data){

                    //alert(data.district);
                    var data = JSON.parse(data);

                    $('#permanent_office_district_id').val(data.district);
                    $('#permanent_office_state_id').val(data.state);

                }
            });
        });

        $('#current_office_city_id').change(function (){

            var city_id = $(this).val();

            $.ajax({
                method:"POST",
                data:{city_id:city_id},
                url:"{{ URL::to('admin/approve/district') }}",
                success:function(data){

                    //alert(data.district);
                    var data = JSON.parse(data);

                    $('#current_office_district_id').val(data.district);
                    $('#current_office_state_id').val(data.state);

                }
            });
        });


        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        }),

        $('#registration-form').validate({
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                $('#proof_div').slideDown(700);
                $('#address_div').slideDown(700);
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-add-body', 'show');
                //var l = Ladda.create($('[type=submit]', form)[0]);
                //l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        console.log('data')

                        if (data.status == 'fail') {
                            //$('#add').modal('hide');

                            $('#proof-text').text(data.message);
                            $('#proof_message').modal('show');

                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {
                            //alert(data.status);
                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            //$('input', form).iCheck('update');

                            // toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#message_text').text(data.message);
                            $('#message').modal('show');
                                window.location.replace("{{ URL::to('admin/co-applicant/index/"+data.id+"') }}")
                        } else {

                        }

                        //l.stop();
                        //uiLoader('#form-add-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },

            rules: {
                first_name:{required: '#person_type:checked'},
                phone:{required:true},

                //occupation: { required: '#person_type:checked' },
                //guardian: { required: '#person_type:checked' },
                //guardian_first_name:{required:'#person_type:checked'},
                //guardian_middle_name: { required: '#person_type:checked' },
                //guardian_last_name: { required: '#person_type:checked' },
                company_name: { required: '#firm_type:checked' },
                //constitution: { required: '#firm_type:checked' },
                //company_category: { required: '#firm_type:checked' },




                profile_picture: {
                    required: false,
                    accept: "image/*",
                    remote: {
                        url: '{{ URL::to("admin/validator/validationempty") }}',
                        type: 'POST',
                        data:{
                            lead_id:$('#hidden').val()
                        }
                    }
                },
                signature: {
                    required: false,
                    accept: "image/*"
                },
                /*proof_pan_card_number:{
                    required:{
                        depends:function(element){
                            return $('#id_voter_2').val() != ""
                        }
                    }
                },*/
                proof_pan_card_number:{
                    required: {
                        depends:function(element){
                            if($("#id_pan_3").val() != "" || $(".blah_3").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                proof_pan_card_proof: {
                    required: function(element){
                            if($('#1_pan_3').val() != "" && $(".blah_3").attr("src") == ""){
                                return true;
                            }

                            else{
                            return false;
                        }


                    },
                    accept:"image/*"
                },
                proof_voter_card_number:{
                    required: {
                        depends:function(element){
                            if($("#id_voter_4").val() != "" || $(".blah_4").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                proof_voter_card_proof: {
                    required: {
                        depends:function(element){
                            if($("#1_voter_4").val() != "" && $(".blah_4").attr("src") == ""){
                                return true;
                            }
                            else{
                               return false;
                            }
                        }
                    },
                    accept: "image/*"
                },
                proof_adhar_card_number:{
                    required: {
                        depends:function(element){
                            if($("#id_adhar_5").val() != "" || $(".blah_5").attr("src") != ""){
                                return true;
                            }
                        }
                    }

                },
                proof_adhar_card_proof: {
                    required: {
                        depends:function(element){
                            if($("#1_adhar_5").val() != "" && $(".blah_5").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                proof_driving_license_number:{
                    required: {
                        depends:function(element){
                            if($("#id_driving_6").val() != "" || $(".blah_6").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                proof_driving_license_proof: {
                    required: {
                        depends:function(element){
                            if($("#1_license_6").val() != "" && $(".blah_6").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                proof_passport_number:{
                    required: {
                        depends:function(element){
                            if($("#id_passport_7").val() != "" || $(".blah_7").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                proof_passport_proof: {
                    required: {
                        depends:function(element){
                            if($("#1_passport_7").val() != "" && $(".blah_7").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                proof_arm_license_number:{
                    required: {
                        depends:function(element){
                            if($("#id_arm_8").val() != "" || $(".blah_8").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                proof_arm_license_proof: {
                    required: {
                        depends:function(element){
                            if($("#1_arm_8").val() != "" && $(".blah_8").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                proof_bank_account_number:{
                    required: {
                        depends:function(element){
                            if($("#id_bank_9").val() != "" || $(".blah_9").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                proof_bank_proof: {
                    required: {
                        depends:function(element){
                            if($("#3bank_number_91").val() != "" && $(".blah_9").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                other_proof: {
                    required: false,
                    accept: "image/*"
                },
                resi_rashan_id_proof: {
                    required: false,
                    accept: "image/*"
                },
                resi_driving_license__number:{
                    required: {
                        depends:function(element){
                            if($("#residential_driving_6").val() != "" || $(".blah_6").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                resi_driving_license_proof: {
                    required: {
                        depends:function(element){
                            if($("#2_license_6").val() != "" && $(".blah_6").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                resi_passport__number:{
                    required: {
                        depends:function(element){
                            if($("#residential_passport_7").val() != "" || $(".blah_7").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                resi_passport_proof: {
                    required: {
                        depends:function(element){
                            if($("#1_passport_7").val() != "" && $(".blah_7").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },

                resi_voter_car_number:{
                    required: {
                        depends:function(element){
                            if($("#residential_voter_4").val() != "" || $(".blah_4").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                resi_voter_card_proof: {
                    required: {
                        depends:function(element){
                            if($("#2_voter_4").val() != "" && $(".blah_4").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                income_itr_number:{
                    required: {
                        depends:function(element){
                            if($("#income_itr_15").val() != "" || $(".blah_15").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                income_itr_1_proof: {
                    required: {
                        depends:function(element){
                            if($("#1income_itr_15").val() != "" && $(".blah_15").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                income_itr_2_proof: {
                    required: false,
                    accept: "image/*"
                },
                income_computation_number:{
                    required: {
                        depends:function(element){
                            if($("#income_computation_17").val() != "" || $(".blah_17").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                income_computaion_1_proof: {
                    required: {
                        depends:function(element){
                            if($("#1income_computation_17").val() != "" && $(".blah_17").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                income_computation_2_proof: { required: false,accept: "image/*" },
                /*income_bank_name:{
                    required: {
                        depends:function(element){
                            if($("#1bank_number_91").val() != "" || $(".blah_9").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },*/
                income_bank_account_number:{
                    required: {
                        depends:function(element){
                            if($("#1bank_name_9").val() != "" || $(".blah_9").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                income_bank_1_proof: {
                    required: {
                        depends:function(element){
                            if($("#1bank_number_91").val() != "" && $(".blah_9").attr("src") == ""){
                                return true;
                            }
                            if($("#1bank_name_9").val() != "" && $(".blah_9").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                income_bank_2_proof: {
                    required: false ,
                    accept: "image/*"
                },
                income_fard: { required: false,accept: "image/*" },
                income_other_proof: {
                    required: false,
                    accept: "image/*"
                },
                sign_pan_card_number:{
                    required: {
                        depends:function(element){
                            if($("#signature_pan_3").val() != "" || $(".blah_3").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                sign_pan_card_proof: {
                    required: {
                        depends:function(element){
                            if($("#2_pan_3").val() != "" && $(".blah_3").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                sign_passport__number:{
                    required: {
                        depends:function(element){
                            if($("#signature_passport_7").val() != "" || $(".blah_7").attr("src") != ""){
                                return true;
                            }
                        }
                    }
                },
                sign_passport_proof: {
                    required: {
                        depends:function(element){
                            if($("#1_passport_7").val() != "" && $(".blah_7").attr("src") == ""){
                                return true;
                            }
                        }
                    },
                    accept: "image/*"
                },
                sign_other_proof: {
                    required: false,
                    accept: "image/*"
                },
                owner_electricity_bill: {
                    required: false,
                    accept: "image/*"
                },
                owner_sevrage_bill: { required: false,accept: "image/*" },
                owner_property_paper: { required: false,accept: "image/*" },
                owner_other_ownership_proof: {
                    required: false,
                    accept: "image/*"
                }
            },
            messages: {}
        });
    });
</script>
@stop



