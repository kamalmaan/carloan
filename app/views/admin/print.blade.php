<html>

<head>
  <meta name="viewport" content="width=device-width"/>
    {{ HTML::style('css/styles.css') }}
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::script('js/vendors/jquery/jquery.min.js') }}
    {{ HTML::script('js/vendors/jquery/jquery-ui.min.js') }}
    <style>

        @media print {

            #get_print {
                padding-top:-40px;
                position:absolute;
                z-index:2;
                background:;
            }
            #print_task{
                display:none
            }
        }
    </style>
</head>
<body class="table_watermark">
<button type="button" class="btn btn-info pull-right btn-sm" id="print_task">Print Task List</button>
<script>
    $("button").click(function(){
        print();


    });
</script>

<div class="container-fluid" id="get_print">

    <div class="row">
        <div class="col-sm-12">
          <div class="img_center" style="">
            <img src="{{ URL::to('images/shree_balaji.png') }}" id="" class="img-responsive pull-right">
          </div>
          <div style="width: 23%;float: right;margin-right: -77px;">
            <span><strong class="text text-info">Follow up Date:</strong></span>
              <br><p><span><strong class="text text-info">Agent:</strong></span>
              @if(count($today_schedules) && $today_schedules!= 0)
                  {{ $today_schedules[0]['lead_assigned']}}
                  @elseif(count($daily_tasks))
                  {{ ucwords($daily_tasks[0]->assign_to()->first()->first_name." ". $daily_tasks[0]->assign_to()->first()->last_name) }}
                  @else
                  {{ " "}}
              @endif
              </p>
          </div>
        </div>
    </div>
    <br>
    <p class="p-head">Follow-up-list(Shree Balaji Enterprises)</p>



    <div class="table-responsive margin20">

        <!--table---->
        @if(count($today_schedules) && $today_schedules != 0)

        @foreach($today_schedules as $today_schedule)

        <table class="table table-bordered table table-striped">
            <tr>
                <td ><strong>Customer Name: </strong><span class="text text-info">{{ $today_schedule['customer_name']}} </span></td>

                <td colspan="2"><strong>Conatct No.: </strong>{{ $today_schedule['phone'] }}</td>
                <td colspan="5"><strong>Lead Arrival: </strong>{{ date('l d,F Y', strtotime($today_schedule['lead_arrival'])) }} </td>
            </tr>
            <tr>
                <td >
                    <p><strong>Meeting Topic:</strong> {{ ucfirst($today_schedule['meeting_topic']) }}</p>


                </td>
                <td colspan="5">
                    <p><strong>Meeting Medium:</strong> {{ $today_schedule['meeting_medium'] }}</p>
                </td>
            </tr>


            <tr>
                <td style="width:45%"><strong>Product: </strong>{{ $today_schedule['product'] }}</td>
                <td colspan="3" class="table_small_fonts"><strong>Lead Type:</strong> &nbsp; <input type="checkbox" @if($today_schedule['enquiry_type'] == "Hot") {{ "checked" }} @endif>Hot &nbsp; <input type="checkbox" @if($today_schedule['enquiry_type'] == "Cold") {{ "checked" }} @endif>Cold &nbsp; <input type="checkbox" @if($today_schedule['enquiry_type'] == "Warm") {{ "checked" }} @endif>Warm</td>
                <td colspan="2"><strong>Lead Form: </strong>{{ $today_schedule['lead_from'] }}</td>
            </tr>

            <tr >
                <td colspan="6">
                  <p class='text text-info'><u>Detail Previous Meetings</u> </p>
                  @if(count($today_schedule['previous_meeting']) >0)
                  <?php $i = 0?>
                      @foreach($today_schedule['previous_meeting'] as $key=>$value)

                          <span class='text text-info'>{{ ++$i }}) Meeting Date: </span>
                          {{ date('l d,F Y', strtotime( $value->meeting_date)) }},
                          <p><span class='text text-info'>Meeting Discussion: </span>
                            {{ $value->meeting_discussion }}</p>

                    @endforeach

                    @else
                    {{ "First Meeting" }}
                    @endif

                </td>
            </tr>

            <tr >
              <td colspan="6" style="height:130px;"><strong> Discussion: </strong> </td>
            </tr>

        </table>
        <hr>

        @endforeach
        @endif

        @if(count($daily_tasks))
        <p><strong class="text text-info">Task For Agent:</strong> {{ ucwords($daily_tasks[0]->assign_to()->first()->first_name." ". $daily_tasks[0]->assign_to()->first()->last_name) }}</p>
        @foreach($daily_tasks as $daily_task)
        <table class="table table-bordered table table-striped">
            <tr>
                <td ><strong>Customer Name: </strong> <span class="text text-info">{{ ucwords($daily_task->client) }}</span> </td>
                <td colspan="2"><strong>Contact No.:</strong> {{ $daily_task->phone }}</td>
                <td colspan="3"><strong>Assign By :</strong> {{ ucwords($daily_task->assign_by()->first()->first_name ." ".$daily_task->assign_by()->first()->last_name) }}</td>

            </tr>
            <tr>
                <td ><strong>Task:  </strong> {{ ucfirst($daily_task->task) }}</td>
                <td colspan="2"><strong>Assigned On: </strong> {{ date('l d,F Y', strtotime($daily_task->assign_on)) }}</td>
                <td colspan="3"><strong>Completion On: </strong> {{ date('l d,F Y', strtotime($daily_task->completion_on)) }}</td>

            </tr>
            <tr >
                <th colspan="6"> Meeting Remarks: </th>
            </tr>
        </table>
        <hr>

        @endforeach
        @endif
    </div>
</div>

</body>
</html>
