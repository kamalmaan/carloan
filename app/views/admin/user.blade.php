@extends('admin/layout')
@section('content')

    <!--Breadcrumb-->
    <div class="breadcrumb clearfix">
        <ul>
            <li><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li><a href="index.html">Dashboard</a></li>
            <li class="active">Profile</li>
        </ul>
    </div>


    <div class="page-header">

        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#add">Add User</button>

        <br>
        @if(Session::has('message'))
            <div class="callout callout-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Well done ! </strong>{{ Session::get('message')}}
            </div>
        @endif

    </div>

    {{--
        user registration model
    --}}
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add User</h4>

                </div>
                <div class="modal-body">
                    <form class="orb-form cmxform" id="add_user" action='{{ URL::to("admin/users/add") }}' method="post"
                          novalidate="novalidate">
                        <fieldset>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-automobile"></i>
                                    <input type="text" placeholder="User name" name="username" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter username</b>
                                    {{ $errors->first('username','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-pied-piper"></i>
                                    <input type="password" id="password" placeholder="password" name="password"
                                           name="make" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter password</b>
                                    {{ $errors->first('password','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-pied-piper"></i>
                                    <input type="password" id="confirm_password" placeholder="confirm password"
                                           name="confirm_password" name="make" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter confirm password</b>
                                    {{ $errors->first('confirm_password','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="select">
                                    <select name="user_type" class="newuser_role">
                                        <option value="">--Please Select User type--</option>
                                        @foreach($total_users as $row)
                                            <option value="{{$row->id}}">{{ $row->is_superuser  }}</option>
                                        @endforeach
                                    </select>
                                    {{ $errors->first('user_type','<em class="invalid text-danger" > :message </em>') }}
                                    </label>
                            </section>
                            <section class="form-group">
                                <label class="select">
                                    <select name="branch" id="role_branch">
                                        <option value="">--Please Select branch--</option>
                                        @foreach($branches as $row)
                                        <option value="{{$row->id}}">{{ $row->address."(".$row->city()->first()->city .")"  }}</option>
                                        @endforeach
                                    </select>
                                    {{ $errors->first('branch','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <section class="form-group">
                                 <label class="select">
                                    <select name="parent_user_type" class="parent_role" >

                                    </select>
                                 </label>
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-info"></i>
                                    <input type="text" placeholder="First name" name="first_name" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter First name</b>
                                    {{ $errors->first('first_name','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="last name" name="last_name" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter last name</b>
                                    {{ $errors->first('last_name','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="email" name="email" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter email</b>
                                    {{ $errors->first('email','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>

                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="Phone number" name="phone" value="">
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Phone number</b>
                                    {{ $errors->first('phone','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="input"> <i class="icon-append fa fa-car"></i>
                                    <input type="text" placeholder="Alternate Phone number" name="phone1" value="">                                    
                                </label>
                            </section>
                            <div class="row">
                                <section class="form-group col col-6">                                                                                              
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="profile_pic_1" name="profile_pic" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Profile picture">
                                        
                                    </label>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Profile Picture</b>
                                        {{ $errors->first('profile_pic','<em class="invalid text-danger" > :message </em>') }}
                                </section>
                                <section class="form-group col col-6"> 
                                    <img class="img-thumbnail attachment_1"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="additional_pic_2" name="additional_pic" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Additional picture">
                                    </label>  
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Additional Picture</b>
                                        {{ $errors->first('additional_pic','<em class="invalid text-danger" > :message </em>') }}                              
                                </section>
                                <section class="form-group col col-6"> 
                                    <img class="img-thumbnail attachment_2"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <section class="form-group">                                
                                <label class="textarea"> <i class="icon-append fa fa-car"></i>
                                    <textarea name="permanent_address" placeholder="Permanent Address" id="permanent_address" ></textarea>                                    
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Permanent Address</b>
                                    {{ $errors->first('permanent_address','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <section class="form-group">
                                <label class="checkbox">
                                    <input type="checkbox" name="same_permanent" id="same_permanent"><i></i>Same as Permanent Address
                                </label>
                                <label class="textarea"> <i class="icon-append fa fa-car"></i>
                                    <textarea name="temporary_address" placeholder="Temporary Address"  id="temporary_address"></textarea>                                    
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Temporary Address</b>
                                    {{ $errors->first('temporary_address','<em class="invalid text-danger" > :message </em>') }}
                                </label>
                            </section>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="agreement_proof_3" name="agreement" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Agreement">
                                    </label>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('agreement','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6">
                                    <img class="img-thumbnail attachment_3"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">                                    
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="id_proof1_4" name="id_proof1" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Id proof1">                                   
                                    </label>   
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('id_proof1','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6">
                                    <img class="img-thumbnail attachment_4"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="id_proof2_5" name="id_proof2" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Id proof2">                                       
                                    </label>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('id_proof2','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6">
                                    <img class="img-thumbnail attachment_5"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="id_proof3_6" name="id_proof3" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Id proof3">
                                    </label>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('id_proof3','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6">
                                    <img class="img-thumbnail attachment_6"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                            </div>
                            <div class="row">
                                <section class="form-group col col-6">
                                   <label for="file" class="input input-file">
                                        <div class="button">
                                        <input type="file" id="id_proof4_7" name="id_proof4" onchange="readURL(this);">Browse
                                        </div>
                                        <input type="text" readonly="" placeholder="Id proof4">
                                    </label>
                                    <b class="tooltip tooltip-bottom-right">Needed to enter Proof</b>
                                    {{ $errors->first('id_proof4','<em class="invalid text-danger" > :message </em>') }}                             
                                </section>
                                <section class="form-group col col-6"> 
                                    <img class="img-thumbnail attachment_7"  src=""  alt="No Preview" height="50" width="100">
                                </section>
                               
                            </div>
                            
               
                        </fieldset>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Widget Row Start grid -->
    <div class="row" id="powerwidgets">
        <div class="col-md-12 bootstrap-grid">
            <div class="powerwidget powerwidget-as-portlet-white" id="serverstatuz-indexpage2">
                <header><h2>User Record</h2></header>
                <div>
                    <div class="inner-spacer">
                        <div class="row">
                            <!--Row-->
                            <div class="col-md-12">
                                <div class="row margin-bottom-10px">
                                    <ul class="countries-demo" id="choices">
                                        @foreach($total_users as $row)
                                            <li class="col-md-2 col-sm-4">
                                                <h3>{{ $row->is_superuser  }} <span
                                                            class="label bg-marine"> {{ $row->count  }} </span></h3>

                                                <div class="orb-form">
                                                    <label for="iduk">
                                                        <a href="{{URL::to('admin/users/user/'.$row->id)}}"> View All </a>
                                                    </label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!--/Row--->
                            </div>
                            <!--/Col-md-12--->
                        </div>
                        <!--/Row-->
                    </div>
                </div>
            </div>
        </div>
        <!-- /End Widget -->
    </div>
    <!-- /Inner Row Col-md-6 -->
<!-- </div>
</div> -->
    <!-- Widget Row Start grid -->
    <div class="row" id="powerwidgets">
        <div class="col-md-12 bootstrap-grid">
            <div class="powerwidget powerwidget-as-portlet-white" id="serverstatuz-indexpage2">
                <header><h2>Search User Record</h2></header>
                <div>
                    <div class="inner-spacer">
                        <div class="row">
                            <!--Row-->
                            <div class="col-md-12">
                                <div class="row margin-bottom-10px">
                                    <form class="orb-form"   method="post" novalidate="novalidate">  
                                    <section class="col col-3 form-group">                                        
                                        <label class="select">Select Branch
                                            <select name="branch" id="branch">                                                
                                                <option value="">--Please Select branch--</option>
                                                @foreach($branches as $branch)
                                                <option value="{{ $branch->id }}" >{{ $branch->address }}</option>                                                
                                                @endforeach
                                            </select>
                                        </label>
                                        <span id="error" style="color:red"></span>
                                    </section> 

                                     <section class="col col-3 form-group">                                        
                                        <label class="select">Select Main Role
                                            <select name="main_role" id="main_role">                                                
                                                <option value="">--Please Select Role--</option>
                                                @foreach($total_users as $total_user)
                                                <option value="{{ $total_user->id }}" >{{ $total_user->is_superuser }}</option>                                                
                                                @endforeach
                                            </select>
                                        </label>
                                    </section>
                                    <section class="col col-3 form-group">                                        
                                        <label class="select">Select Sub Role
                                            <select name="sub_role" id="sub_role">                                                
                                                <option value="">--Please Select Sub-Role--</option>
                                               <!--  @foreach($total_users as $total_user)
                                                <option value="{{ $total_user->id }}" >{{ $total_user->is_superuser }}</option>                                                
                                                @endforeach -->
                                            </select>
                                        </label>
                                    </section>
                                    <section class="col col-3 form-group">  
                                        <label class="select"><br>
                                            <input type='button' name='' id="search" value='Search' class='btn btn-sm btn-info'/>
                                        </label>
                                    </section>

                                    </form>
                                </div>
                                <!--/Row--->
                            </div>
                            <!--/Col-md-12--->
                        </div>
                        <!--/Row-->
                    </div>
                </div>
                <div class="row">
                   <div class="col-md-12 bootstrap-grid">

        <div class="powerwidget" id="datatable-with-colvis" data-widget-editbutton="false">
            <header>
                <h2>Bank<small>Show Hide Columns</small></h2>
            </header>
            <div class="inner-spacer">
                {{$data->render()}}
                <script>
                    $('#test')
                            .on('preXhr.dt', function (e, settings, data) {
                                // on start of ajax call
                            }).on( 'draw.dt', function () {
                                $(".status").click(function(){
                                    var id = $(this).attr("id");
                                    $.ajax({
                                        method:"GET",
                                        data:{id:id},
                                        url:"{{ URL::to('admin/users/status') }}",
                                        success:function(data){
                                            $('#test').dataTable()._fnAjaxUpdate();
                                        }
                                    });
                                });
                                $(".user_edit").click(function(){
                                    var id = $(this).attr("id");
                                    $.ajax({
                                                method:"GET",
                                                data:{id:id},
                                                url:"{{ URL::to('admin/users/edit') }}",
                                                success:function(data){
                                                    data = JSON.parse(data)
                                                    $('#edit').modal('show');                                                    
                                                    $('#username').val(data.record.username);
                                                    $('#id').val(data.record.id);
                                                    $('#first_name').val(data.record.first_name);
                                                    $('#last_name').val(data.record.last_name);
                                                    $('#phone').val(data.record.phone);
                                                    $('#user_type').val(data.record.is_superuser);
                                                    if(data.record.is_superuser==6||data.record.is_superuser==7||data.record.is_superuser==8){
                                                        $("#branch").hide();
                                                    }else{
                                                        $("#branch").show();
                                                    }

                                                    $('#branch').val(data.record.branch_id);
                                                    if(data.options==""){
                                                        $('#parent_role').hide();
                                                    }else{
                                                        $('#parent_role').html(data.options);    
                                                    }
                                                    
                                                    //$('#parent_id').val(data.record.parent_id);
                                                    //alert(data.record.parent_id);
                                                    $('#phone1').val(data.record.phone1);
                                                    $(".attachment_1").attr('src','{{ asset("upload/'+data.record.profile_pic+'") }}');
                                                    $(".attachment_2").attr('src','{{ asset("upload/'+data.record.additional_pic+'") }}');
                                                    $("#permanent_address").val(data.record.permanent_address);
                                                    $("#temporary_address").val(data.record.temporary_address);
                                                    $(".attachment_3").attr('src','{{ asset("upload/'+data.record.agreement+'") }}');
                                                    $(".attachment_4").attr('src','{{ asset("upload/'+data.record.id_proof1+'") }}');
                                                    $(".attachment_5").attr('src','{{ asset("upload/'+data.record.id_proof2+'") }}');
                                                    $(".attachment_6").attr('src','{{ asset("upload/'+data.record.id_proof3+'") }}');
                                                    $(".attachment_7").attr('src','{{ asset("upload/'+data.record.id_proof4+'") }}');                                                                                                  
                                                }
                                            },'json'
                                    );

                                
                                });
                            });
                </script>
                {{ $data->script() }}
                </div>
        </div>
    </div> 
                </div>
            </div>
        </div>
        <!-- /End Widget -->
    </div>
    <!-- /Inner Row Col-md-6 -->
</div>
</div>
<script>
$(function(){    
    $("#main_role").click(function(event){
        var main_role=$('#main_role').val();
        if(main_role!=""){            
            $.ajax({
                method:"POST",
                data:{"main_role":main_role},
                url:"{{ URL::to('admin/users/subrole') }}",
                success:function(data){                                
                    $("#sub_role").html(data);                    
                }
            });
        }
    });

    $("#search").click(function(event){
        
        var main_role=$('#main_role').val();
        var sub_role=$('#sub_role').val();
        var branch=$('#branch').val();
        //console.log('in func'+main_role+'-'+sub_role+'-'+branch);
        if(main_role==""||sub_role==""||branch==""){
            $("#error").html('*All fields are required');
        }else{           
            $.get("{{ URL::to('admin/users') }}",{'main_role':main_role,'sub_role':sub_role,'branch':branch},function(data){                            
                $('#test').dataTable()._fnAjaxUpdate();          
            });
        }      
    });
});
</script>
    <script>
    function readURL(input) {          
           var id = input.id;
            var t = id.split("_");           
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.attachment_'+t[2]).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }

        $(document).ready(function () {
             
            $(".parent_role,#role_branch").hide();
            $(".newuser_role").click(function(){

                var id = $(this).attr("id");
                var role_id=$(this).val();
                if(role_id==3||role_id==2||role_id==4){
                    $("#role_branch").show();
                    var branch=$("#branch").val();
                    
                    if(branch==""){
                        alert('Select Branch');
                    }else{                        
                        $.ajax({
                            method:"POST",
                            data:{role_id:role_id,branch_id:branch},
                            url:"{{ URL::to('admin/users/role') }}",
                            success:function(data){
                                //alert(data);
                                $(".parent_role").html(data);
                                $("#branch").show();
                            }
                        });

                    }
                }else if(role_id==5){                    
                        $("#role_branch").show();
                        $(".parent_role").hide(); 
                }else{
                    $("#role_branch,.parent_role").hide();                    
                }
            });

            $("#role_branch").click(function(){

                var branch= $("#role_branch").val();;                
                var role_id=$(".newuser_role").val();
                if(role_id==3||role_id==2||role_id==4){
                    
                        $.ajax({
                            method:"POST",
                            data:{role_id:role_id,branch_id:branch},
                            url:"{{ URL::to('admin/users/role') }}",
                            success:function(data){                                
                                $(".parent_role").html(data);
                                $(".parent_role").show();
                            }
                        });

                } else if(role_id==5){
                        $("#role_branch").show();
                        $(".parent_role").hide();
                }else{
                     $("#role_branch,.parent_role").hide();
                }                

            });
            //set temporary same as permanent
            $("#same_permanent").click(function(){
                var permanent_address=$("#permanent_address").val();                
                $("#temporary_address").val(permanent_address);
            });

            $('#add_user').validate(
                    {
                        ignore: [],
                        errorElement: 'span',
                        errorClass: 'help-block',
                        highlight: function (element) { // hightlight error inputs
                            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        submitHandler: function (form) {
                            form = $(form);
                            $('[type=submit]', form).attr('disabled', 'disabled');
                            //uiLoader('#form-add-body', 'show');
                            //var l = Ladda.create($('[type=submit]', form)[0]);
                            //l.start();
                            form.ajaxSubmit({
                                dataType: 'json',
                                success: function (data) {
                                    console.log('data');

                                    if (data.status == 'fail') {
                                        //alert('fail');
                                        $('#add').modal('hide');
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                        $('[type=submit]', form).removeAttr('disabled');
                                    } else if (data.status == 'success') {
                                        //alert('success');
                                        form[0].reset();
                                        
                                        $('.form-group', form).removeClass('has-error');
                                        //$('input', form).iCheck('update');

                                        //toastr['success'](data.message);
                                        $('[type=submit]', form).removeAttr('disabled');
                                        $('#add').modal('hide');
                                        $('#test').dataTable()._fnAjaxUpdate();
                                        $('#message_text').text(data.message);
                                        $('#message').modal('show');
                                    } else {

                                    }

                                    //l.stop();
                                    //uiLoader('#form-add-body', 'hide');
                                }
                            });

                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                        },
                        rules: {
                            username: {
                                required: true,
                                minlength: 6
                            },
                            password: {
                                required: true,
                                minlength: 6

                            },
                            confirm_password: {
                                required: true,
                                minlength: 6,
                                equalTo: "#password"
                            },
                            user_type: {
                                required: true
                            },
                            first_name: {
                                required: true
                            },
                            last_name: {
                                required: true
                            },
                            email: {
                                required: true,
                                email: true
                            },
                            phone: {
                                required: true
                            },
                            permanent_address:{
                                required: true
                            },
                            temporary_address:{
                                required: true
                            },
                            profile_pic:{
                                required:true,
                                accept: "image/*"
                            }, 
                            additional_pic:{
                                required:false,
                                accept: "image/*"
                            },                          
                            id_proof1:{
                                required:true,
                                accept: "image/*"
                            },                            
                            id_proof2:{
                                required:true,
                                accept: "image/*"
                            },
                            id_proof3:{
                                required:false,
                                accept: "image/*"
                            },
                            id_proof4:{
                                required:false,
                                accept: "image/*"
                            },
                            agreement:{
                                required:true                                
                            }

                        },
                        messages: {}
                    }
            );
        })



    </script>

@stop
