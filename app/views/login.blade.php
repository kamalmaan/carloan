﻿<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBE | Login</title>
    {{ HTML::style('css/styles.css') }}


<link rel="shortcut icon" type="image/x-icon" href="{{ URL('favicon.ico') }}" />
    {{ HTML::script('js/vendors/horisontal/modernizr.custom.js') }}

</head>

<body>
<div class="colorful-page-wrapper">
  <div class="center-block">
    <div class="login-block">
<!--      <form action="{{ URL::to('') }}" id="login-form" class="orb-form">-->
       {{ Form::open(array('url'=>'login','class'=>'orb-form','id'=>'login-form')) }}
        <header>
          <div class="image-block"><img src="{{ URL::to('images/logo.png') }}" alt="User" /></div>
          Login to SBE </header>
        <fieldset>
            @if(Session::has('danger'))
            <em class="text-danger text-center">
                {{ Session::get('danger') }}
            </em>
            @endif
          <section>
            <div class="row">
              <label class="label col col-4">Username</label>
              <div class="col col-8">
                <label class="input"> <i class="icon-append fa fa-user"></i>
                  <input type="email" name="username" value="{{ Input::old('username') }}">
                    {{ $errors->first('username','<span class="text-danger" > :message </span>') }}
                </label>

              </div>

            </div>
          </section>
          <section>
            <div class="row">
              <label class="label col col-4">Password</label>
              <div class="col col-8">
                <label class="input"> <i class="icon-append fa fa-lock"></i>
                  <input type="password" name="password">
                    {{ $errors->first('password','<span class="text-danger" > :message </span>') }}
                </label>
<!--                <div class="note"><a href="#">Forgot password?</a></div>-->
              </div>
            </div>
          </section>
          <section>
            <div class="row">
              <div class="col col-4"></div>
              <div class="col col-8">
                <label class="checkbox">
                  <input type="checkbox" name="remember" checked>

              </div>
            </div>
          </section>
        </fieldset>
        <footer>
          <button type="submit" class="btn btn-default">Log in</button>
        </footer>

       {{ Form::close() }}
    </div>


    <div class="copyrights"> SBE Loan <br>
       &copy; 2015 </div>
  </div>
</div>

<!--Scripts-->
<!--JQuery-->

{{ HTML::script('js/vendors/jquery/jquery.min.js') }}
{{ HTML::script('js/vendors/jquery/jquery-ui.min.js') }}
{{ HTML::script('js/vendors/forms/jquery.form.min.js') }}
{{ HTML::script('js/vendors/forms/jquery.validate.min.js') }}
{{ HTML::script('js/vendors/forms/jquery.maskedinput.min.js') }}
{{ HTML::script('js/vendors/jquery-steps/jquery.steps.min.js') }}

{{ HTML::script('js/vendors/sparkline/jquery.sparkline.min.js') }}
{{ HTML::script('js/scripts.js') }}


<!--Forms-->


<!--NanoScroller-->
{{ HTML::script('js/vendors/nanoscroller/jquery.nanoscroller.min.js') }}
<!--Sparkline-->

<!--Main App-->




<!--/Scripts-->

</body>
</html>