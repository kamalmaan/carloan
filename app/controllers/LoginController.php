<?php
class LoginController extends BaseController{

    function __construct()
    {
        //parent::__construct();

    }

   public function getIndex(){

       if (Auth::user()->check()) {
            return Redirect::to('user/');
       }
       else if(Auth::admin()->check()){
           return Redirect::to('admin/');
       }
       return View::make('login');
   }
   public function postIndex(){

       $rules = array(
           'password' => 'required',
           'username' => 'required'
       );

       $validator = Validator::make(Input::all(), $rules);
       if ($validator->fails()) {

           return Redirect::to('login')->withInput(Input::except('password'))->withErrors($validator);
       } else {
           $credentials = array(
               'username' => Input::get('username')
           );

          $data =User::where($credentials)->count();
          if($data == 0){
              return Redirect::back()->withInput(Input::except('password'))
                  ->with('danger', 'Invalid username or password');
          }
          else{

          $data =User::where($credentials)->first();

          // if($data->is_superuser == 8 || $data->is_superuser == 4){
               $credentials = array(
                   'username' => Input::get('username'),
                   'password' => Input::get('password'),
                );

               if(Auth::admin()->attempt($credentials)){                  

                   return Redirect::to('admin/');
               }
               else{
                   return Redirect::back()->withInput(Input::except('password'))
                       ->with('danger', 'Invalid username or password or Account deactivated');
               }
            //}

          /* else if($data->is_superuser == 5){
               $credentials = array(
                   'username' => Input::get('username'),
                   'password' => Input::get('password'),
               );
               if(Auth::user()->attempt($credentials)){
                   //echo Auth::user()->id();
                   return Redirect::to('user/');
               }
               else{
                   return Redirect::back()->withInput(Input::except('password'))
                       ->with('danger', 'Invalid username or password');
               }
           }
           else {
               return Redirect::back()->withInput(Input::except('password'))
               ->with('danger', 'Invalid username or password');
           }*/
       }
      }
   }
}