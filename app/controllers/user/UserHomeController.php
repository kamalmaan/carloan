<?php

class UserHomeController extends BaseController{
    function __construct(){

        //parent::__construct();

        $this->beforeFilter('user');
    }
    public function getIndex(){
        return View::make('user/index');
    }
    public function getProfile(){
        $data = Auth::user()->get();
        return View::make('user/profile',array('data' =>$data));
    }
    public function postProfile(){

        $rules = array(
            'new_password' => 'min:6',
            'confirm_password' =>'required_with:new_password|same:new_password',
            'phone' =>'required',
            'first_name' => 'required',
            'last_name' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {

            return Redirect::back()->withInput(Input::except('new_password'))->withErrors($validator);
        }
        else{
            $user = Auth::user()->id();

            $data = array(
                //'password' => Input::get('new_password'),
                'phone' => Input::get('phone'),
                'first_name' => Input::get('first_name'),
                'last_name' => Input::get('last_name'),
            );
            if(Input::get('new_password') != '')
            {
                $data = array(
                    'password' => Hash::make(Input::get('new_password')),
                );
            }
            User::where('id','=',$user)->update($data);
            return Redirect::back()->with('success',"You profiled is updated successfully!");
        }

    }
}