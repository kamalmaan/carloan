<?php

/**
 * Created by PhpStorm.
 * User: john
 * Date: 22/5/15
 * Time: 3:16 PM
 *
 * this controller is for to add user from super admin states
 */
class AdminUserController extends BaseController
{

    function __construct()
    {
        $this->beforeFilter('admin');

        if(!module_permission('1'))
        {
            return Redirect::to('admin/')->send();
        }
    }

    /*
     * show index page content
     * */
    public function getIndex()
    {    
        $total_users=array();
        //get different roles comes under current user role
        $role_id=Auth::admin()->get()->is_superuser;
        if($role_id==8){
            $total_users = IsSuperuser::whereNotIn('id',[8])->groupBy('id')->get();
        }elseif($role_id==7||$role_id==6){
            $total_users = IsSuperuser::where('id','<=',[5])->groupBy('id')->get();
        }elseif($role_id==5){
            $total_users = IsSuperuser::where('id','<=',[4])->groupBy('id')->get();
        }elseif($role_id==4){
            $total_users = IsSuperuser::where('id','<=',[3])->groupBy('id')->get();
        }elseif($role_id==3||$role_id==2){
            $total_users = IsSuperuser::where('id',$role_id)->groupBy('id')->get();
        }
        
        //get details of roles with condition under current user role,branch
        foreach ($total_users as $row) {  

            if($role_id==8||$role_id==7||$role_id==6){
                $row->count = User::where('is_superuser', $row->id)->count();
            }elseif($role_id==5){                
               $row->count = User::where('is_superuser',$row->id)->where('branch_id',Auth::admin()->get()->branch_id)->count();
            }elseif($role_id==4){
                $row->count = User::where('is_superuser', $row->id)->where('branch_id',Auth::admin()->get()->branch_id)->where('parent_id',Auth::admin()->get()->id)->count();
            }elseif($role_id==3||$role_id==2){                
                $row->count = User::where('is_superuser', $row->id)->where('branch_id',Auth::admin()->get()->branch_id)->where('created_by',Auth::admin()->get()->id)->count();
            }
        }
        //get branch of current user
        if(Auth::admin()->get()->branch_id==0){
            $branches = Branch::where('status', '1')->get();
        }else{
            $branches = Branch::where('status', '1')->where('id',Auth::admin()->get()->branch_id)->get();
        }
       
        Session::put('sub_role',Input::get('sub_role'));
        Session::put('branch',Input::get('branch'));
        
        $data = Datatable::table()
            ->addColumn('Username', 'First name', 'Last Name', 'Phone', 'User Type', 'Branch'/*, 'Status', 'Action'*/)            
            ->setUrl(URL::to('admin/users/usersearch'))
            ->noScript();

        return View::make('admin/user')->with('data', $data)->with('total_users', $total_users)->with('branches', $branches);             
    }
    public function postSubrole(){
        $main_role=Input::get('main_role');
        if($main_role==8){
            $total_users = IsSuperuser::whereNotIn('id',[8])->groupBy('id')->get();
        }elseif($main_role==7||$main_role==6){
            $total_users = IsSuperuser::where('id','<=',[5])->groupBy('id')->get();
        }elseif($main_role==5){
            $total_users = IsSuperuser::where('id','<=',[4])->groupBy('id')->get();
        }elseif($main_role==4){
            $total_users = IsSuperuser::where('id','<=',[3])->groupBy('id')->get();
        }elseif($main_role==3||$main_role==2){
            $total_users = IsSuperuser::where('id',$main_role)->groupBy('id')->get();
        }
        echo '<option value="">--Please Select Sub-Role--</option>';
        foreach ($total_users as $row) {
            echo '<option value="'.$row->id.'">'.$row->is_superuser.'</option>';
        }

    }
    public function getUsersearch()
    {   
        $sub_role=Session::get('sub_role');
        $branch=Session::get('branch');        
        
        $query = User::where('is_superuser',$sub_role)->where('branch_id',$branch)->get();                
        return Datatable::collection($query)

            ->addColumn('Username', function ($model) {
                return ucfirst($model->username);
            })
            ->addColumn('First Name', function ($model) {
                return ucfirst($model->first_name);
            })
            ->addColumn('Last Name', function ($model) {
                return ucfirst($model->last_name);
            })
            ->addColumn('Phone', function ($model) {
                if(!empty($model->phone)){
                    return ucfirst($model->phone);
                }else{
                    return '';
                }
            })
           ->addColumn('User Type', function ($model) {
                return ucfirst($model->superuser()->first()->is_superuser);
            })
            ->addColumn('Branch', function ($model) {
                if(!empty($model->branch()->first()->branch)){
                    //return ucfirst($model->branch()->first()->branch);
                    return ucfirst($model->branch()->first()->address);

                }else{
                    return '';
                }
            })
            /*->addColumn('Status', function ($model) {
                if ($model->active == 1) {
                    return "<button class='btn btn-success btn-xs '>Enable</button>";
                } else {
                    return "<button class='btn btn-danger btn-xs '>Disabled</button>";
                }
            })
            ->addColumn('action', function ($model) {

                if ($model->active == 1) {
                    return "<button class='btn btn-danger btn-xs status' id='status_" . $model->id . "'>Disable</button> <button type='button' class='btn btn-xs btn-info user_edit' id='user_" . $model->id . "'>Edit</button>";
                } else {
                    return "<button class='btn btn-success btn-xs status' id='status_" . $model->id . "'>Enable</button> <button type='button' class='btn btn-xs btn-info user_edit' id='user_" . $model->id . "'>Edit</button>";
                }
            })*/
            ->searchColumns('Id', 'Username', 'First name', 'Last Name', 'Phone', 'Type', 'Branch',/* 'Status',*/ 'Verified', 'Created_by')
            ->orderColumns('id')
            ->make();
    }

    /*
     *  datatable
     *  type_id is is_superuer id in user table
     *
     */
    public function getUser($type_id)
    {   
        $total_users=array();
        //get different roles comes under current user role
        $role_id=Auth::admin()->get()->is_superuser;
        if($role_id==8){
            $total_users = IsSuperuser::whereNotIn('id',[8])->groupBy('id')->get();
        }elseif($role_id==7||$role_id==6){
            $total_users = IsSuperuser::where('id','<=',[5])->groupBy('id')->get();
        }elseif($role_id==5){
            $total_users = IsSuperuser::where('id','<=',[4])->groupBy('id')->get();
        }elseif($role_id==4){
            $total_users = IsSuperuser::where('id','<=',[3])->groupBy('id')->get();
        }elseif($role_id==3||$role_id==2){
            $total_users = IsSuperuser::where('id',$role_id)->groupBy('id')->get();
        }        

        //get details of roles with condition under current user role,branch
        foreach ($total_users as $row) {
            //$row->count = User::where('is_superuser', $row->id)->count();
            if($role_id==8||$role_id==7||$role_id==6){
                $row->count = User::where('is_superuser', $row->id)->count();
            }elseif($role_id==5){                
               $row->count = User::where('is_superuser',$row->id)->where('branch_id',Auth::admin()->get()->branch_id)->count();
            }elseif($role_id==4){
                $row->count = User::where('is_superuser', $row->id)->where('branch_id',Auth::admin()->get()->branch_id)->where('parent_id',Auth::admin()->get()->id)->count();
            }elseif($role_id==3||$role_id==2){                
                $row->count = User::where('is_superuser', $row->id)->where('branch_id',Auth::admin()->get()->branch_id)->where('created_by',Auth::admin()->get()->id)->count();
            }
        }
        if(Auth::admin()->get()->branch_id==0){
            $branches = Branch::where('status', '1')->get();
        }else{
            $branches = Branch::where('status', '1')->where('id',Auth::admin()->get()->branch_id)->get();
        }

        //get branch of current user
        //$branches = Branch::where('status', '1')->where('id',Auth::admin()->get()->branch_id)->get();
        $data = Datatable::table()
            ->addColumn('Username', 'First name', 'Last Name', 'Phone', 'User Type', 'Branch', 'Status', 'Action')
            ->setUrl(URL::to('admin/users/userlist/' . $type_id))
            ->noScript();
            // 'Verified',
        return View::make('admin/user_list')->with('data', $data)->with('total_users', $total_users)->with('branches', $branches);        
    }

    /*
     * get user list of usertype through is_superuser type
    */

    public function getUserlist($is_superuser)
    {   
        $query=array();
        $role_id=Auth::admin()->get()->is_superuser;

        if($role_id==8||$role_id==7||$role_id==6){
            $query = User::where('is_superuser', $is_superuser)->get();
        }elseif($role_id==5){   
            $query = User::where('is_superuser', $is_superuser)->where('branch_id',Auth::admin()->get()->branch_id)->get();                         
        }elseif($role_id==4){
            $query = User::where('is_superuser', $is_superuser)->where('branch_id',Auth::admin()->get()->branch_id)->where('parent_id',Auth::admin()->get()->id)->get();
        }elseif($role_id==3||$role_id==2){                
            $query = User::where('is_superuser', $is_superuser)->where('branch_id',Auth::admin()->get()->branch_id)->where('created_by',Auth::admin()->get()->id)->get();
        }

        return Datatable::collection($query)

            ->addColumn('Username', function ($model) {
                return ucfirst($model->username);
            })
            ->addColumn('First Name', function ($model) {
                return ucfirst($model->first_name);
            })
            ->addColumn('Last Name', function ($model) {
                return ucfirst($model->last_name);
            })
            ->addColumn('Phone', function ($model) {
                if(!empty($model->phone)){
                    return ucfirst($model->phone);
                }else{
                    return '';
                }
            })

           ->addColumn('User Type', function ($model) {
                return ucfirst($model->superuser()->first()->is_superuser);
            })
            ->addColumn('Branch', function ($model) {
                if(!empty($model->branch()->first()->branch)){
                    //return ucfirst($model->branch()->first()->branch);
                    return ucfirst($model->branch()->first()->address);

                }else{
                    return '';
                }

            })
            ->addColumn('Status', function ($model) {
                if ($model->active == 1) {
                    return "<button class='btn btn-success btn-xs '>Enable</button>";
                } else {
                    return "<button class='btn btn-danger btn-xs '>Disabled</button>";
                }
            })

            ->addColumn('action', function ($model) {

                if ($model->active == 1) {
                    return "<button class='btn btn-danger btn-xs status' id='status_" . $model->id . "'>Disable</button> <button type='button' class='btn btn-xs btn-info user_edit' id='user_" . $model->id . "'>Edit</button>";
                } else {
                    return "<button class='btn btn-success btn-xs status' id='status_" . $model->id . "'>Enable</button> <button type='button' class='btn btn-xs btn-info user_edit' id='user_" . $model->id . "'>Edit</button>";
                }
            })
            ->searchColumns('Id', 'Username', 'First name', 'Last Name', 'Phone', 'Type', 'Branch', 'Status', 'Verified', 'Created_by')
            ->orderColumns('id')
            ->make();
    }

    /**
     * change status of user
     */
    public function getStatus()
    {
        $id = Input::get('id');
        $user = explode('_', $id);
        $check = $user[1];
        $status = User::where('id', $check)->first();

        if ($status->active == 1) {
            $checkstatus = array(
                'active' => 0
            );
        } else {
            $checkstatus = array(
                'active' => 1
            );
        }
        DB::table('users')->where('id', $check)->update($checkstatus);
    }
    /*
     * add new use
     * such as agent,admin
    */
    public function postAdd()
    {
        $username = Input::get('username');
        
        $count = User::where('username', $username)->count();
        if ($count > 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'username already Exists.',
            );
            echo json_encode($data);
        } else {

            $profile_pic="";$additional_pic="";$agreement="";$id_proof1="";
            $id_proof2="";$id_proof3="";$id_proof4="";$parent_user_type="";
            if (Input::hasFile('profile_pic'))
            { $profile_pic = str_random(10).Input::file('profile_pic')->getClientOriginalName(); 
            Input::file("profile_pic")->move('upload/',$profile_pic);}

            if (Input::hasFile('additional_pic'))
            { $additional_pic= str_random(10).Input::file('additional_pic')->getClientOriginalName(); 
            Input::file("additional_pic")->move('upload/',$additional_pic);}

            if (Input::hasFile('agreement'))
            { $agreement= str_random(10).Input::file('agreement')->getClientOriginalName(); 
            Input::file("agreement")->move('upload/',$agreement);}

            if (Input::hasFile('id_proof1'))
            { $id_proof1= str_random(10).Input::file('id_proof1')->getClientOriginalName(); 
            Input::file("id_proof1")->move('upload/',$id_proof1);}

            if (Input::hasFile('id_proof2'))
            { $id_proof2= str_random(10).Input::file('id_proof2')->getClientOriginalName(); 
            Input::file("id_proof2")->move('upload/',$id_proof2);}

            if (Input::hasFile('id_proof3'))
            { $id_proof3= str_random(10).Input::file('id_proof3')->getClientOriginalName(); 
            Input::file("id_proof3")->move('upload/',$id_proof3);}

            if (Input::hasFile('id_proof4'))
            { $id_proof4= str_random(10).Input::file('id_proof4')->getClientOriginalName(); 
            Input::file("id_proof4")->move('upload/',$id_proof4);}

            if(Input::get('parent_user_type')=="")
            {$parent_user_type=0;}
            else{$parent_user_type=Input::get('parent_user_type');}
            
            $auth=Auth::admin()->get()->is_superuser;
            $user=new User();
            $user->username=Input::get('username');
            $user->password=Hash::make(Input::get('password'));
            $user->first_name=Input::get('first_name');
            $user->last_name=Input::get('last_name');
            //User type
            $user->is_superuser=Input::get('user_type');
            $user->branch_id=Input::get('branch');
            $user->phone=Input::get('phone');
            //new fields
            $user->phone1=Input::get('phone1');
            $user->profile_pic=$profile_pic;
            $user->additional_pic=$additional_pic;
            $user->permanent_address=Input::get('permanent_address');
            $user->temporary_address=Input::get('temporary_address');
            $user->agreement=$agreement;
            $user->id_proof1=$id_proof1;
            $user->id_proof2=$id_proof2;
            $user->id_proof3=$id_proof3;
            $user->id_proof4=$id_proof4;
            //$user->created_by=8;
            $user->created_by=Auth::admin()->get()->id;
            $user->parent_id=$parent_user_type;
            $user->save();

            $data = array(
                'status' => 'success',
                'message' =>'User Successfully Created.',

            );
            echo json_encode($data);

        }

    }

    /**
     * Edit user
     */
    public function getEdit(){
        Input::get('id');
        $check = explode('_',Input::get('id'));
        $id = $check[1];

        $data = User::find($id);
        //code for get parent role
        $options=array();$parent_role="";$options1="";
        $role_id= $data->is_superuser;
        $branch_id= $data->branch_id;
        $parent_id=$data->parent_id;
        if($role_id==2||$role_id==3){
            $parent_role=User::where('is_superuser','4')->where('branch_id',$branch_id)->where('active','1')->get();
            $options1="Select Team Leader";
        }elseif($role_id==4){
            $options1="Select Branch manager";
            $parent_role=User::where('is_superuser','5')->where('branch_id',$branch_id)->where('active','1')->get();
        }

        $selected='';
        if(!empty($parent_role)){
            $options="<option value=''>".$options1."</option>";
            foreach($parent_role as $role){

                if($role->id==$parent_id) {
                    $options.="<option value='".$role->id."'  selected >".$role->username."</option>";
                }else{
                    $options.="<option value='".$role->id."'  >".$role->username."</option>"; 
                }
            }
            $resp = array(
            'record' => $data,
            'options'=>$options
            );
        }else{
            $resp = array(
            'record' => $data,
            'options'=>""
            );
        }
        
        //end code for get parent role
        //echo $data;
        $resp = array(
            'record' => $data,
            'options'=>$options
        );

        

        header('content-type: application/json');
        echo json_encode($resp);
    }

    /**
     * User update
    */

    public function postUpdate(){
                        
        $user_role=Input::get('user_type');
        
        $branch_id="";$parent_id="";
        if($user_role==6||$user_role==7||$user_role==8)
        { 
            $branch_id=0;$parent_id=8;
        }
        elseif($user_role==5){
            $branch_id=Input::get('branch');
            $parent_id=8;
        }
        else{ 
            $branch_id=Input::get('branch');
            $parent_id=Input::get('parent_user_type');
        }
        
        $user=array(
            'first_name'=>Input::get('first_name'),
            'last_name'=>Input::get('last_name'),
            'phone'=>Input::get('phone'),
            'is_superuser'=>$user_role,
            'branch_id'=>$branch_id,
            'parent_id'=>$parent_id,
            'phone1'=>Input::get('phone1'),                    
            'permanent_address'=>Input::get('permanent_address'),
            'temporary_address'=>Input::get('temporary_address')            
        );

        if(Input::has('password')){
            $user['password']=Hash::make(Input::get('password'));
        }
        if (Input::hasFile('profile_pic'))
        {   
            $profile_pic = str_random(10).Input::file('profile_pic')->getClientOriginalName(); 
            Input::file("profile_pic")->move('upload/',$profile_pic);
            $user['profile_pic']=$profile_pic;
        }
        if (Input::hasFile('additional_pic'))
        {   
            $additional_pic= str_random(10).Input::file('additional_pic')->getClientOriginalName(); 
            Input::file("additional_pic")->move('upload/',$additional_pic);
            $user['additional_pic']=$additional_pic;
        }
        if (Input::hasFile('agreement'))
        { $agreement= str_random(10).Input::file('agreement')->getClientOriginalName(); 
            Input::file("agreement")->move('upload/',$agreement);
            $user['agreement']=$agreement;
            
        }

        if (Input::hasFile('id_proof1'))
        { $id_proof1= str_random(10).Input::file('id_proof1')->getClientOriginalName(); 
            Input::file("id_proof1")->move('upload/',$id_proof1);
            $user['id_proof1']=$id_proof1;
        }

        if (Input::hasFile('id_proof2'))
        { $id_proof2= str_random(10).Input::file('id_proof2')->getClientOriginalName(); 
            Input::file("id_proof2")->move('upload/',$id_proof2);
            $user['id_proof2']=$id_proof2;
        }

        if (Input::hasFile('id_proof3'))
        { $id_proof3= str_random(10).Input::file('id_proof3')->getClientOriginalName(); 
            Input::file("id_proof3")->move('upload/',$id_proof3);
            $user['id_proof3']=$id_proof3;
        }

        if (Input::hasFile('id_proof4'))
        { $id_proof4= str_random(10).Input::file('id_proof4')->getClientOriginalName(); 
            Input::file("id_proof4")->move('upload/',$id_proof4);
            $user['id_proof4']=$id_proof4;
        }


        User::where('id',Input::get('id'))->update($user);

        $data = array(
            'status' => 'success',
            'message' =>'User Successfully Updated.',

        );

        echo json_encode($data);
    }
    public function postRole()
    {
        $options="";$parent_role="";
        $role_id= Input::get('role_id');
        $branch_id= Input::get('branch_id');        

        if($role_id==2||$role_id==3){
            $parent_role=User::where('is_superuser','4')->where('branch_id',$branch_id)->where('active','1')->get();
            $options="Select Team Leader";
        }elseif($role_id==4){
            $options="Select Branch manager";
            $parent_role=User::where('is_superuser','5')->where('branch_id',$branch_id)->where('active','1')->get();
        }        
        echo "<option value=''>".$options."</option>";        
        if(!$parent_role->isEmpty()){                  
            foreach($parent_role as $role){
            echo "<option value='".$role->id."'>".$role->username."</option>";
            }
        }else{             
            echo "<option value=''>No record</option>";
        }
                
    }


}