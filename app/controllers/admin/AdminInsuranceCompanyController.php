<?php
class AdminInsuranceCompanyController extends BaseController{
    function __construct(){
        $this->beforeFilter('admin');
        if(!role_permission('5','insurance_insurancecompany'))
        {
            return Redirect::to('admin/')->send();
        }
    }

    public function getIndex(){
        $insurance = Datatable::table()
            ->addColumn('Comapny name','Phone','Alternate Phone Numbers','Email','Alternate Emails','Action')
            ->setUrl(URL::to('admin/insurancecompany/insurance'))
            ->noScript();
    return View::make('admin/insurancecompany')->with('data',$insurance);
    }
    public function getInsurance(){

        $query = InsuranceCompany::get();

        return Datatable::collection($query)            
            ->addColumn('name',function($model){
                return ucfirst($model->name);
            })
            ->addColumn('phone',function($model){
                return ucfirst($model->phone);
            })
            ->addColumn('alternate_phone',function($model){
                return ucfirst($model->alternate_phone);
            })
            ->addColumn('email',function($model){
                return ucfirst($model->email);
            })
            ->addColumn('alternate_email',function($model){
                return ucfirst($model->alternate_email);
            })         
            ->addColumn('action', function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs status' id='status_".$model->id."'>Disable</button>
                        <button type='button' class='btn btn-xs btn-info company' id='company_".$model->id."'>Edit</button> <a href='insurancecompany/branch/".$model->id."'><button type='button' class='btn btn-xs btn-info company' id='page_".$model->id."'>Branches</button></a>";
                }
                else{
                    return "<button class='btn btn-success btn-xs status' id='status_".$model->id."'>Enable</button>
                    <button type='button' class='btn btn-xs btn-info company' id='company_".$model->id."'>Edit</button> <a href='insurancecompany/branch/".$model->id."'><button type='button' class='btn btn-xs btn-info company' id='page_".$model->id."'>Branches</button></a>";
                }
            
            })


           ->searchColumns('name','email','status')
            ->orderColumns('name')
            ->make();

    }
    public function postSave(){
        $rules = array(
            'name' => 'required|unique:insurance_companies'            
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            $data = array(
                'status' => 'fail',
                'message' => 'Company Name Already Exists'
            );
            echo json_encode($data);
        }
        else{
            $insurancecompany = new InsuranceCompany;
            $insurancecompany->name = Input::get('name');
            $insurancecompany->phone = Input::get('phone');
            $insurancecompany->alternate_phone = Input::get('alternate_phone');
            $insurancecompany->email = Input::get('email');
            $insurancecompany->alternate_email = Input::get('alternate_email');

            $insurancecompany->save();
            $data = array(
                'status' => 'success',
                'message' =>'Insurance Company Successfully Saved.',

            );
            echo json_encode($data);

            //return Redirect::to('admin/di')->with('message','Lead Type successfully saved.');
        }
    }
    public function getStatus(){
        $id = Input::get('id');
        $state = explode('_',$id);
        $check = $state[1];
        $status = InsuranceCompany::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('insurance_companies')
            ->where('id',$check)
            ->update($checkstatus);
    }
    public function getEdit(){

        Input::get('id');
        $check = explode('_',Input::get('id'));
        $id = $check[1];

        $data = InsuranceCompany::find($id);
       //echo $data;
        $resp = array(
            'record' => $data
        );
        header('content-type: application/json');
       echo json_encode($resp);


    }
    public function postUpdate(){


            $id = Input::get('id');

            $insurancecompany = array(
                'name' => Input::get('name'),
                'phone'=>Input::get('phone'),
                'alternate_phone'=>Input::get('alternate_phone'),
                'email'=>Input::get('email'),
                'alternate_email'=>Input::get('alternate_email')
            );

            DB::table('insurance_companies')
                ->where('id',$id)
                ->update($insurancecompany);
            $data = array(
                'status' => 'success',
                'message' =>'Insurance Company Successfully Updated.',

            );

            echo json_encode($data);

    }    
    public function getBranch($id){

        $insurancebranch = Datatable::table()
            ->addColumn('Id', 'Branch','Branch Name','Phone','Status','Action')
            ->setUrl(URL::to("admin/insurancecompany/branchrecord/$id"))
            ->noScript();

        return View::make('admin/insurancebranch')->with('data',$insurancebranch)->with('id',$id);

    }
    public function getBranchrecord($id){

        $query = InsuranceBranch::where('insurance_id',$id)->get();

        return Datatable::collection($query)
            ->showColumns('id')
            ->addColumn('branch',function($model){
                return ucfirst($model->branch);
            })
            ->addColumn('name',function($model){
                return ucfirst($model->name);
            })
            ->addColumn('phone',function($model){
                return ucfirst($model->phone);
            })
            ->addColumn('status',function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs status' id='status_".$model->id."'>Disable</button>";
                }
                else{
                    return "<button class='btn btn-success btn-xs status' id='status_".$model->id."'>Enable</button>";
                }

            })  
            ->addColumn('action', function($model){
                return "<button type='button' class='btn btn-xs btn-info branch' id='district_".$model->id."'>Edit</button>";
            })        

            ->searchColumns('id','branch','name','phone','status')
            ->orderColumns('branch')
            ->make();

    }
        public function getBranchstatus(){
        $id = Input::get('id');
        $city = explode('_',$id);
        $check = $city[1];
        $status = InsuranceBranch::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('insurance_branches')
            ->where('id',$check)
            ->update($checkstatus);
    }

    public function postBranchsave(){

        $rules = array(
            'name' => 'required|Unique:insurance_branches'
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){

            $data = array(
                'status' => 'fail',
                'message' =>'Branch Name Already exist.',

            );
            
            echo json_encode($data);
            /*return Redirect::back()
                ->withErrors($validator);*/
        }
        else{
            $insurancebranch = new InsuranceBranch;
            $insurancebranch->branch= Input::get('branch');
            $insurancebranch->name= Input::get('name');
            $insurancebranch->phone= Input::get('phone');
            $insurancebranch->insurance_id= Input::get('id');

            $insurancebranch->save();
            $data = array(
                'status' => 'success',
                'message' =>'Insurance Branch Name Successfully Saved',

            );
            echo json_encode($data);

            //return Redirect::to('admin/di')->with('message','Lead Type successfully saved.');
        }
    }
    public function getBranchedit(){


        Input::get('id');

        $check = explode('_',Input::get('id'));
        $id = $check[1];

        $data = InsuranceBranch::find($id);
      
        $resp = array(
            'record' => $data
        );
       
        header('content-type: application/json');
        echo json_encode($resp);


    }
    public function postBranchupdate(){



        $id = Input::get('id');

        $insurancebranch = array(
            'branch' => Input::get('branch'),
            'name'=>Input:: get('name'),
            'phone'=>Input::get('phone')
        );

        DB::table('insurance_branches')
            ->where('id',$id)
            ->update($insurancebranch);
        $data = array(
            'status' => 'success',
            'message' =>'Insurance Branch Successfully Updated',

        );

        echo json_encode($data);

        }
}