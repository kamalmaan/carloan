<?php
class AdminCoApplicantController extends BaseController{
    function __construct(){
        $this->beforefilter('admin');
    }
    public function getIndex($id){

        $coapplicant = Datatable::table()
                ->addColumn('Name', 'Information','Mobile No.','EMail','Decline')
                ->setUrl(URL::to("admin/co-applicant/record/$id"))
                ->noScript();

            return View::make('admin/coapplicant')->with(array('data'=>$coapplicant,'lead_id' => $id));
        }
    public function getRecord($id){

        $query = Coapplicant::where('lead_id',$id)->get();


        return Datatable::collection($query)
            //->showColumns('name', 'lead_type')
            ->addColumn('name', function($model){
                if(!empty($model->company_name)){
                    $user = ucfirst($model->company_name) ." ".$model->constitution;
                }
                else{
                    $user = ucfirst($model->first_name)." ".ucfirst($model->last_name);
                }
                //(!empty($model->company_name))? ucfirst($model->lead_first_name)." ".ucfirst($model->lead_last_name):ucfirst($model->company_name) ."(".$model->constiution." - ".$model->company_category.")"



                return "<span><a href='../view/".$model->id."'><button class='btn btn-info btn-sm'>".$user." </button></a>";
            })

            ->addcolumn('information',function($model){
                if($model->type == "person" )  {
                    return $model->guardian." ".ucwords($model->guardian_first_name." ".$model->guardian_middle_name." ".$model->guardian_last_name);
                }
                else{
                    return $model->company_category;
                }
            })
            ->addcolumn('phone',function($model){

                    return $model->phone;

            })
            ->addcolumn('email_id',function($model){

                return $model->email_id;

            })

            ->addcolumn('decline',function($model){
                return "<button class='btn btn-danger btn-xs delete' id='".$model->id."' >Delete</button> <a href='../edit/".$model->id."'><button class='btn btn-success btn-xs' id='".$model->id."' >Edit</button</a>";

            })




            ->searchColumns('name','created_at')


            ->orderColumns('name')
            ->make();

    }
    public function getAdd($id){


        $banks = Bank::where('status','1')->get();
        $vehicles = Vehicle::where('status','1')->get();
        $dealers = Dealer::where('status','1')->get();
        $cities = City::where('status','1')->get();
        $districts = District::where('status','1')->get();




        return View::make('admin/coapplicant_add')->with(array('lead_id'=>$id,'bank' => $banks,'vehicle' =>$vehicles,'dealer' =>$dealers,'city' =>$cities,'district' => $districts));
    }
    public function postSave($id){
        //echo $id; exit();

        $array_proof = array(
            'proof_pan_card_proof',
            'proof_voter_card_proof',
            'proof_adhar_card_proof',
            'proof_driving_license_proof',
            'proof_passport_proof',
            'proof_arm_license_proof',
            'proof_bank_proof',
            'other_proof',
            'resi_rashan_id_proof',
            'resi_driving_license_proof',
            'resi_passport_proof',
            'resi_voter_card_proof',
            'income_itr_1_proof',
            'income_itr_2_proof',
            'income_computaion_1_proof',
            'income_computation_2_proof',
            'income_bank_1_proof',
            'income_bank_2_proof',
            'income_fard',
            'income_other_proof',
            'sign_pan_card_proof',
            'sign_passport_proof',
            'sign_other_proof',
            'owner_electricity_bill',
            'owner_sevrage_bill',
            'owner_property_paper',
            'owner_other_ownership_proof'

        );
        /*$i = 0;


        foreach($array_proof as $count_proof ){

            if(Input::hasFile($count_proof)){
                $i = $i+1;
            }

        }*/

        /*if($i >= 2 ){*/
            $lead = array(
                'lead_id'=> $id,
                'first_name'=> Input::get('first_name'),

                'whatsapp_number'=> Input::get('whatsapp_number'),
                'middle_name'=> Input::get('middle_name'),
                'last_name'=> Input::get('last_name'),
                'phone'=> Input::get('phone'),
                'email_id'=> Input::get('email_id'),
                'dob'=> Input::get('dob'),
                'current_resi_street'=> Input::get('current_resi_street'),
                'current_resi_city_id'=> Input::get('current_resi_city_id'),
                'current_resi_district_id'=> Input::get('current_resi_district_id'),
                'current_resi_state_id'=> Input::get('current_resi_state_id'),
                'current_resi_pincode'=> Input::get('current_resi_pincode'),
                'current_resi_phone'=> Input::get('current_resi_phone'),
                'permanent_resi_street'=> Input::get('permanent_resi_street'),
                'pemanent_resi_city_id'=> Input::get('pemanent_resi_city_id'),
                'permanent_resi_district_id'=> Input::get('permanent_resi_district_id'),
                'permanent_resi_state_id'=> Input::get('permanent_resi_state_id'),
                'permanent_resi_pincode'=> Input::get('permanent_resi_pincode'),
                'permanent_resi_phone'=> Input::get('permanent_resi_phone'),
                'current_office_name'=> Input::get('current_office_name'),
                'current_office_street'=> Input::get('current_office_street'),
                'current_office_city_id'=> Input::get('current_office_city_id'),
                'current_office_district_id'=> Input::get('current_office_district_id'),
                'current_office_state_id'=> Input::get('current_office_state_id'),
                'current_office_email'=> Input::get('current_office_email'),
                'current_office_phone'=> Input::get('current_office_phone'),
                'current_office_alternate_phone'=> Input::get('current_office_alternate_phone'),
                'current_office_mobile'=> Input::get('current_office_mobile'),
                'current_office_pincode'=> Input::get('current_office_pincode'),
                'current_office_fax'=> Input::get('current_office_fax'),
                'permanent_office_name'=> Input::get('permanent_office_name'),
                'permanent_office_street'=> Input::get('permanent_office_street'),
                'permanent_office_city_id'=> Input::get('permanent_office_city_id'),
                'permanent_office_district_id'=> Input::get('permanent_office_district_id'),
                'permanent_office_state_id'=> Input::get('permanent_office_state_id'),
                'permanent_office_email'=> Input::get('permanent_office_email'),
                'permanent_office_phone'=> Input::get('permanent_office_phone'),
                'permanent_office_alternate_phone'=> Input::get('permanent_office_alternate_phone'),
                'permanent_office_mobile'=> Input::get('permanent_office_mobile'),
                'permanent_office_pincode'=> Input::get('permanent_office_pincode'),
                'permanent_office_fax'=> Input::get('permanent_office_fax'),
                'type' => Input::get('type'),


            );





            if(Input::get('type') == 'person'){

                $person = array(
                    'occupation' => Input::get('occupation'),
                    'guardian' => Input::get('guardian'),
                    'guardian_first_name' => Input::get('guardian_first_name'),
                    'guardian_middle_name' => Input::get('guardian_middle_name'),
                    'guardian_last_name' => Input::get('guardian_last_name'),
                    'company_name' => "",
                    'constitution' => "",
                    'company_category' => ""
                );

                $lead = array_merge($lead,$person);
            }
            else{
                $person = array(
                    'company_name' => Input::get('company_name'),
                    'constitution' => Input::get('constitution'),
                    'company_category' => Input::get('company_category'),
                    'guardian' => "",
                    'guardian_first_name' => "",
                    'guardian_middle_name' => "",
                    'guardian_last_name' => ""
                );
                $lead = array_merge($lead,$person);

            }

            $array_lead = array('profile_picture','signature');
            foreach($array_lead as $lead_array){

                if(Input::hasFile($lead_array)){

                    $name = str_random(10).Input::file($lead_array)->getClientOriginalName();
                    Input::file("$lead_array")->move('upload/',$name);

                    $leads = array(
                        $lead_array => $name,
                    );
                    $lead = array_merge($lead,$leads);

                }
            }
            /*"yougetsignal.com"*/
            DB::table('co_applicants')
                ->insert($lead);

            $co_applicant_id = DB::getPdo()->lastInsertId();



            //------- Add Proof---------------

            $proof = array(

                'co_applicant_id'=> $co_applicant_id,
                'proof_pan_card_number'=> Input::get('proof_pan_card_number'),
                'proof_voter_card_number'=> Input::get('proof_voter_card_number'),
                'proof_adhar_card_number'=> Input::get('proof_adhar_card_number'),
                'proof_driving_license_number'=> Input::get('proof_driving_license_number'),
                'prrof_passport_number'=> Input::get('prrof_passport_number'),
                'proof_arm_license_number'=> Input::get('proof_arm_license_number'),
                'proof_bank_account_number'=> Input::get('proof_bank_account_number'),
                'resi_driving_license__number'=> Input::get('resi_driving_license__number'),
                'resi_passport__number'=> Input::get('resi_passport__number'),
                'resi_voter_car_number'=> Input::get('resi_voter_car_number'),
                'income_itr_number'=> Input::get('income_itr_number'),
                'income_computation_number'=> Input::get('income_computation_number'),
                'income_bank_name'=> Input::get('income_bank_name'),
                'income_bank_account_number'=> Input::get('income_bank_account_number'),
                'bank_bank_name'=> Input::get('bank_bank_name'),
                'bank_bank_account_number'=> Input::get('bank_bank_account_number'),
                'sign_pan_card_number'=> Input::get('sign_pan_card_number'),
                'sign_passport__number'=> Input::get('sign_passport__number'),
                'co_applicant_id' =>$co_applicant_id



            );


            foreach($array_proof as $proof_array){

                if(Input::hasFile($proof_array)){

                    if($proof_array == "proof_pan_card_proof" || $proof_array == 'sign_pan_card_proof'){


                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_pan_card_proof' =>  $names,
                            'sign_pan_card_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'proof_voter_card_proof' || $proof_array == 'resi_voter_card_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_voter_card_proof' =>  $names,
                            'resi_voter_card_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'proof_driving_license_proof' || $proof_array == 'resi_driving_license_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_driving_license_proof' =>  $names,
                            'resi_driving_license_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'proof_passport_proof' || $proof_array == 'resi_passport_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_passport_proof' =>  $names,
                            'resi_passport_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }
                    else if($proof_array == 'proof_bank_proof' || $proof_array == 'income_bank_1_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_bank_proof' =>  $names,
                            'income_bank_1_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'other_proof' || $proof_array == 'income_other_proof' || $proof_array == 'sign_other_proof' || $proof_array == 'owner_other_ownership_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'other_proof' =>  $names,
                            'income_other_proof' =>  $names,
                            'sign_other_proof' =>  $names,
                            'owner_other_ownership_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }
                    else {
                        $name = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$name);

                        $proofs = array(
                            $proof_array => $name,
                        );
                        $proof = array_merge($proof,$proofs);

                    }

                }
            }

            //print_r($proof);exit;

            DB::table('coapplicant_proofs')->insert($proof);



            $data = array(
                'status' => 'success',
                'message' =>"Co-Applicant Information is Updated with proof and head towards the approval" ,
                'id' =>$id
            );

            echo json_encode($data);

        /*}
        else{

            $data = array(
                'status' => 'fail',
                'message' =>"Lead's Information cannot be Updated, please provide atleast 2 proves" ,
                'id' =>$id
            );

            echo json_encode($data);

        }*/
    }
    public function getEdit($id){

        $banks = Bank::where('status','1')->get();
        $vehicles = Vehicle::where('status','1')->get();
        $dealers = Dealer::where('status','1')->get();
        $cities = City::where('status','1')->get();
        $districts = District::where('status','1')->get();
        $coapplicant = Coapplicant::find($id);
        return View::make('admin/coapplicant_edit')->with(array('lead'=>$coapplicant,'bank' => $banks,'vehicle' =>$vehicles,'dealer' =>$dealers,'city' =>$cities,'district' => $districts));
    }
    public function postUpdate($id){
        //echo $id; exit();

        $array_proof = array(
            'proof_pan_card_proof',
            'proof_voter_card_proof',
            'proof_adhar_card_proof',
            'proof_driving_license_proof',
            'proof_passport_proof',
            'proof_arm_license_proof',
            'proof_bank_proof',
            'other_proof',
            'resi_rashan_id_proof',
            'resi_driving_license_proof',
            'resi_passport_proof',
            'resi_voter_card_proof',
            'income_itr_1_proof',
            'income_itr_2_proof',
            'income_computaion_1_proof',
            'income_computation_2_proof',
            'income_bank_1_proof',
            'income_bank_2_proof',
            'income_fard',
            'income_other_proof',
            'sign_pan_card_proof',
            'sign_passport_proof',
            'sign_other_proof',
            'owner_electricity_bill',
            'owner_sevrage_bill',
            'owner_property_paper',
            'owner_other_ownership_proof'

        );
        $i = 0;

        //print_r($array_proof);
        $proof_check = CoapplicantProof::where("co_applicant_id",$id)->first();
        foreach($array_proof as $count_proof ){

            if(Input::hasFile($count_proof) || !empty($proof_check->$count_proof)){

                $i = $i+1;
            }

        }

        if($i >= 2 ){
            $lead = array(
                'lead_id'=> Input::get('lead_id'),

                'first_name'=> Input::get('first_name'),

                'whatsapp_number'=> Input::get('whatsapp_number'),
                'middle_name'=> Input::get('middle_name'),
                'last_name'=> Input::get('last_name'),
                'phone'=> Input::get('phone'),
                'email_id'=> Input::get('email_id'),
                'dob'=> Input::get('dob'),
                'current_resi_street'=> Input::get('current_resi_street'),
                'current_resi_city_id'=> Input::get('current_resi_city_id'),
                'current_resi_district_id'=> Input::get('current_resi_district_id'),
                'current_resi_state_id'=> Input::get('current_resi_state_id'),
                'current_resi_pincode'=> Input::get('current_resi_pincode'),
                'current_resi_phone'=> Input::get('current_resi_phone'),
                'permanent_resi_street'=> Input::get('permanent_resi_street'),
                'pemanent_resi_city_id'=> Input::get('pemanent_resi_city_id'),
                'permanent_resi_district_id'=> Input::get('permanent_resi_district_id'),
                'permanent_resi_state_id'=> Input::get('permanent_resi_state_id'),
                'permanent_resi_pincode'=> Input::get('permanent_resi_pincode'),
                'permanent_resi_phone'=> Input::get('permanent_resi_phone'),
                'current_office_name'=> Input::get('current_office_name'),
                'current_office_street'=> Input::get('current_office_street'),
                'current_office_city_id'=> Input::get('current_office_city_id'),
                'current_office_district_id'=> Input::get('current_office_district_id'),
                'current_office_state_id'=> Input::get('current_office_state_id'),
                'current_office_email'=> Input::get('current_office_email'),
                'current_office_phone'=> Input::get('current_office_phone'),
                'current_office_alternate_phone'=> Input::get('current_office_alternate_phone'),
                'current_office_mobile'=> Input::get('current_office_mobile'),
                'current_office_pincode'=> Input::get('current_office_pincode'),
                'current_office_fax'=> Input::get('current_office_fax'),
                'permanent_office_name'=> Input::get('permanent_office_name'),
                'permanent_office_street'=> Input::get('permanent_office_street'),
                'permanent_office_city_id'=> Input::get('permanent_office_city_id'),
                'permanent_office_district_id'=> Input::get('permanent_office_district_id'),
                'permanent_office_state_id'=> Input::get('permanent_office_state_id'),
                'permanent_office_email'=> Input::get('permanent_office_email'),
                'permanent_office_phone'=> Input::get('permanent_office_phone'),
                'permanent_office_alternate_phone'=> Input::get('permanent_office_alternate_phone'),
                'permanent_office_mobile'=> Input::get('permanent_office_mobile'),
                'permanent_office_pincode'=> Input::get('permanent_office_pincode'),
                'permanent_office_fax'=> Input::get('permanent_office_fax'),
                'type' => Input::get('type'),


            );




            if(Input::get('type') == 'person'){

                $person = array(
                    'occupation' => Input::get('occupation'),
                    'guardian' => Input::get('guardian'),
                    'guardian_first_name' => Input::get('guardian_first_name'),
                    'guardian_middle_name' => Input::get('guardian_middle_name'),
                    'guardian_last_name' => Input::get('guardian_last_name'),
                    'company_name' => "",
                    'constitution' => "",
                    'company_category' => ""
                );

                $lead = array_merge($lead,$person);
            }
            else{
                $person = array(
                    'company_name' => Input::get('company_name'),
                    'constitution' => Input::get('constitution'),
                    'company_category' => Input::get('company_category'),
                    'guardian' => "",
                    'guardian_first_name' => "",
                    'guardian_middle_name' => "",
                    'guardian_last_name' => ""
                );
                $lead = array_merge($lead,$person);

            }

            $array_lead = array('profile_picture','signature');
            foreach($array_lead as $lead_array){

                if(Input::hasFile($lead_array)){

                    $name = str_random(10).Input::file($lead_array)->getClientOriginalName();
                    Input::file("$lead_array")->move('upload/',$name);

                    $leads = array(
                        $lead_array => $name,
                    );
                    $lead = array_merge($lead,$leads);

                }
            }
            /*"yougetsignal.com"*/
            DB::table('co_applicants')
                ->where('id',$id)->update($lead);





            //------- Add Proof---------------

            $proof = array(

                'co_applicant_id'=> $id,
                'proof_pan_card_number'=> Input::get('proof_pan_card_number'),
                'proof_voter_card_number'=> Input::get('proof_voter_card_number'),
                'proof_adhar_card_number'=> Input::get('proof_adhar_card_number'),
                'proof_driving_license_number'=> Input::get('proof_driving_license_number'),
                'prrof_passport_number'=> Input::get('prrof_passport_number'),
                'proof_arm_license_number'=> Input::get('proof_arm_license_number'),
                'proof_bank_account_number'=> Input::get('proof_bank_account_number'),
                'resi_driving_license__number'=> Input::get('resi_driving_license__number'),
                'resi_passport__number'=> Input::get('resi_passport__number'),
                'resi_voter_car_number'=> Input::get('resi_voter_car_number'),
                'income_itr_number'=> Input::get('income_itr_number'),
                'income_computation_number'=> Input::get('income_computation_number'),
                'income_bank_name'=> Input::get('income_bank_name'),
                'income_bank_account_number'=> Input::get('income_bank_account_number'),
                'bank_bank_name'=> Input::get('bank_bank_name'),
                'bank_bank_account_number'=> Input::get('bank_bank_account_number'),
                'sign_pan_card_number'=> Input::get('sign_pan_card_number'),
                'sign_passport__number'=> Input::get('sign_passport__number'),
                'co_applicant_id' =>$id



            );


            foreach($array_proof as $proof_array){

                if(Input::hasFile($proof_array)){

                    if($proof_array == "proof_pan_card_proof" || $proof_array == 'sign_pan_card_proof'){


                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_pan_card_proof' =>  $names,
                            'sign_pan_card_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'proof_voter_card_proof' || $proof_array == 'resi_voter_card_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_voter_card_proof' =>  $names,
                            'resi_voter_card_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'proof_driving_license_proof' || $proof_array == 'resi_driving_license_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_driving_license_proof' =>  $names,
                            'resi_driving_license_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'proof_passport_proof' || $proof_array == 'resi_passport_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_passport_proof' =>  $names,
                            'resi_passport_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }
                    else if($proof_array == 'proof_bank_proof' || $proof_array == 'income_bank_1_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_bank_proof' =>  $names,
                            'income_bank_1_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'other_proof' || $proof_array == 'income_other_proof' || $proof_array == 'sign_other_proof' || $proof_array == 'owner_other_ownership_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'other_proof' =>  $names,
                            'income_other_proof' =>  $names,
                            'sign_other_proof' =>  $names,
                            'owner_other_ownership_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }
                    else {
                        $name = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$name);

                        $proofs = array(
                            $proof_array => $name,
                        );
                        $proof = array_merge($proof,$proofs);

                    }

                }
            }

            //print_r($proof);exit;

            DB::table('coapplicant_proofs')->where('co_applicant_id',$id)->update($proof);




            $data = array(
                'status' => 'success',
                'message' =>"Lead's Information is Updated with proof and head towards the approval" ,
                'id' =>Input::get('lead_id')
            );

            echo json_encode($data);

        }
        else{

            $data = array(
                'status' => 'fail',
                'message' =>"Lead's Information cannot be Updated, please provide atleast 2 proves" ,
                'id' =>$id
            );

            echo json_encode($data);

        }
    }
    public function getDelete(){
        $id = Input::get('id');
        Coapplicant::where('id',$id)->delete();
        CoapplicantProof::where('co_applicant_id',$id)->delete();
        $data = array(
            'status' => 'success',
            'message' =>"Co-Applicant successfully deleted !" ,
        );
        echo json_encode($data);
    }

    public function getView($id){
        $banks = Bank::where('status','1')->get();
        $vehicles = Vehicle::where('status','1')->get();
        $dealers = Dealer::where('status','1')->get();
        $cities = City::where('status','1')->get();
        $districts = District::where('status','1')->get();


        $coapplicant = Coapplicant::find($id);
     /*   $asset=AssetDetail::find($id);*/
        $coapplicant_proof = CoapplicantProof::where('co_applicant_id',$id)->first();
        //print_r($coapplicant);exit();
        return View::make('admin/coapplicant_view')->with(array('coapplicants'=>$coapplicant,'coapplicant_proofs'=>$coapplicant_proof));
    }
   /* public function getasset($id){
        $banks = Bank::where('status','1')->get();
        $vehicles = Vehicle::where('status','1')->get();
        $dealers = Dealer::where('status','1')->get();
        $cities = City::where('status','1')->get();
        $districts = District::where('status','1')->get();


        $asset = asset::find($id);
       /* $coapplicant_proof = CoapplicantProof::where('co_applicant_id',$id)->first();*/
        //print_r($coapplicant);exit();
       // return View::make('admin/coapplicant_view/asset')->with(array('asset_details'=>$asset));









}