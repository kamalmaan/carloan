<?php
class AdminDistrictController extends BaseController{
    function __construct(){
        $this->beforeFilter('admin');
    }
    public function getIndex(){

        $district = Datatable::table()
            ->addColumn('Id', 'District','Status','Action')
            ->setUrl(URL::to('admin/district/district'))
            ->noScript();
        return View::make('admin/district')->with('data',$district);
    }
    public function getDistrict(){

        $query = District::get();


        return Datatable::collection($query)
            ->showColumns('id')
            ->addColumn('district',function($model){
                return ucfirst($model->district);
            })
            ->addColumn('status',function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs status' id='status_".$model->id."'>Disable</button>";
                }
                else{
                    return "<button class='btn btn-success btn-xs status' id='status_".$model->id."'>Enable</button>";
                }

            })
            ->addColumn('action', function($model){
                return "<button type='button' class='btn btn-xs btn-info district' id='district_".$model->id."'>Edit</button> <a href='district/city/".$model->id."'><button type='button' class='btn btn-xs btn-info district' id='page_".$model->id."'>Cities</button></a>";
            })


            ->searchColumns('id','district')
            ->orderColumns('district')
            ->make();

    }
    public function postSave(){
        $rules = array(
            'district' => 'required|unique:districts'
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            $data = array(
                'status' => 'fail',
                'message' => 'District Already Exists'
            );
            echo json_encode($data);
        }
        else{
            $district = new District;
            $district->district = Input::get('district');

            $district->save();
            $data = array(
                'status' => 'success',
                'message' =>'District Successfully Saved.',

            );
            echo json_encode($data);

            //return Redirect::to('admin/di')->with('message','Lead Type successfully saved.');
        }
    }
    public function getStatus(){
        $id = Input::get('id');
        $district = explode('_',$id);
        $check = $district[1];
        $status = District::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('districts')
            ->where('id',$check)
            ->update($checkstatus);
    }
    public function getEdit(){

        Input::get('id');
        $check = explode('_',Input::get('id'));
        $id = $check[1];

        $data = District::find($id);
       //echo $data;
        $resp = array(
            'record' => $data
        );
        header('content-type: application/json');
       echo json_encode($resp);


    }
    public function postUpdate(){


            $id = Input::get('id');

            $district = array(
                'district' => Input::get('district')
            );

            DB::table('districts')
                ->where('id',$id)
                ->update($district);
            $data = array(
                'status' => 'success',
                'message' =>'District Successfully Updated.',

            );

            echo json_encode($data);

    }
    public function getCity($id){

        $city = Datatable::table()
            ->addColumn('Id', 'city','Status','Action')
            ->setUrl(URL::to("admin/district/cityrecord/$id"))
            ->noScript();

        return View::make('admin/city')->with('data',$city)->with('id',$id);

    }
    public function getCityrecord($id){

        $query = City::where('district_id',$id)->get();

        return Datatable::collection($query)
            ->showColumns('id')
            ->addColumn('city',function($model){
                return ucfirst($model->city);
            })
            ->addColumn('status',function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs status' id='status_".$model->id."'>Disable</button>";
                }
                else{
                    return "<button class='btn btn-success btn-xs status' id='status_".$model->id."'>Enable</button>";
                }

            })
            ->addColumn('action', function($model){
                return "<button type='button' class='btn btn-xs btn-info city' id='".$model->id."_".$model->district_id."'>Edit</button>";
            })


            ->searchColumns('id','city')
            ->orderColumns('city')
            ->make();

    }
    public function getCitystatus(){
        $id = Input::get('id');
        $city = explode('_',$id);
        $check = $city[1];
        $status = City::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('cities')
            ->where('id',$check)
            ->update($checkstatus);
    }
    public function postCitysave(){

        $rules = array(
            'city' => 'required|Unique:cities'
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){

            $data = array(
                'status' => 'fails',
                'message' =>'City Aleready exist.',

            );
            echo json_encode($data);
            /*return Redirect::back()
                ->withErrors($validator);*/
        }
        else{
            $city = new City;
            $city->city= Input::get('city');
            $city->district_id= Input::get('id');

            $city->save();
            $data = array(
                'status' => 'success',
                'message' =>'City Name Successfully Updated',

            );
            echo json_encode($data);

            //return Redirect::to('admin/di')->with('message','Lead Type successfully saved.');
        }
    }
    public function getCityedit(){

        Input::get('id');
        $check = explode('_',Input::get('id'));
        $id = $check[0];

        $data = City::find($id);
        //echo $data;
        $resp = array(
            'record' => $data
        );
        header('content-type: application/json');
        echo json_encode($resp);


    }
    public function postCityupdate(){



        $id = Input::get('id');

        $city = array(
            'city' => Input::get('city')
        );

        DB::table('cities')
            ->where('id',$id)
            ->update($city);
        $data = array(
            'status' => 'success',
            'message' =>'City Name Successfully Updated',

        );

        echo json_encode($data);

        }



}