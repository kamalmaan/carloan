<?php
class AdminApproveController extends BaseController{

    function __construct(){
        $this->beforefilter('admin');

    }
    public function getIndex($id){

       $button ="no";
       $leads = Lead::find($id);
       $banks = Bank::where('status','1')->get();
       $vehicles = Vehicle::where('status','1')->get();
       $dealers = Dealer::where('status','1')->get();
       $cities = City::where('status','1')->get();
       $districts = District::where('status','1')->get();

       if($leads->existing_customer == 0 || $leads->update == "yes" || $leads->update == "") {

           $proof_lead_id = Proof::where('lead_id',$id)->count();
           $asset_lead_id =AssetDetail::where('lead_id',$id)->count();

           return View::make('admin/approve')->with(array('lead'=>$leads,'bank' => $banks,'vehicle' =>$vehicles,'dealer' =>$dealers,'city' =>$cities,'district' => $districts,'id' =>"0"));

       }
        if($leads->existing_customer != 0 || $leads->update == ""){

            return View::make('admin/approve')->with(array('lead'=>$leads,'bank' => $banks,'vehicle' =>$vehicles,'dealer' =>$dealers,'city' =>$cities,'district' => $districts,'id' =>"0"));
        }
        if($leads->existing_customer != 0 || $leads->update == "no"){

            $exist_customer_id = $leads->existing_customer;
            $leads = Lead::find($exist_customer_id);
            $id = Lead::find($id);

            //existing customer
            return View::make('admin/approve')->with(array('lead'=>$leads,'bank' => $banks,'vehicle' =>$vehicles,'dealer' =>$dealers,'city' => $cities,'district' =>$districts,'id' => $id));
        }


    }
    public function postUpdate($id){
        //echo $id; exit();

        $array_proof = array(
            'proof_pan_card_proof',
            'proof_voter_card_proof',
            'proof_adhar_card_proof',
            'proof_driving_license_proof',
            'proof_passport_proof',
            'proof_arm_license_proof',
            'proof_bank_proof',
            'other_proof',
            'resi_rashan_id_proof',
            'resi_driving_license_proof',
            'resi_passport_proof',
            'resi_voter_card_proof',
            'income_itr_1_proof',
            'income_itr_2_proof',
            'income_computaion_1_proof',
            'income_computation_2_proof',
            'income_bank_1_proof',
            'income_bank_2_proof',
            'income_fard',
            'income_other_proof',
            'sign_pan_card_proof',
            'sign_passport_proof',
            'sign_other_proof',
            'owner_electricity_bill',
            'owner_sevrage_bill',
            'owner_property_paper',
            'owner_other_ownership_proof',
            'income_balancesheet_proof',
            'income_pl_proof',
            'income_form_3cb',
            'income_form_3cd',
            'address_te_bill',
            'bankstatement_firm_bill',
            'other_address_proof1',
            'other_address_proof2',
            'establishment_pancard_firm',
            'establishment_registration_certificate',
            'establishment_service_tax',            
            'establishment_other_proof1',            
            'establishment_other_proof2'
        );
        $i = 0;
        $proof_check = Proof::where("lead_id",$id)->first();

        foreach($array_proof as $count_proof ){

            if(Input::hasFile($count_proof) || !empty($proof_check->$count_proof) ){
                $i = $i+1;
            }

        }

        if($i >= 2 ){
            $lead = array(
                'lead_first_name'=> Input::get('lead_first_name'),
                'update'=> Input::get('update'),
                'whatsapp_number'=> Input::get('whatsapp_number'),
                'lead_middle_name'=> Input::get('lead_middle_name'),
                'lead_last_name'=> Input::get('lead_last_name'),
                //'guardian'=> Input::get('guardian'),
                //'guardian_first_name'=> Input::get('guardian_first_name'),
                //'guardian_middle_name'=> Input::get('guardian_middle_name'),
                //'guardian_last_name'=> Input::get('guardian_last_name'),
                'phone'=> Input::get('phone'),
                'email_id'=> Input::get('email_id'),
                'dob'=> Input::get('dob'),
                'current_resi_street'=> Input::get('current_resi_street'),
                'current_resi_city_id'=> Input::get('current_resi_city_id'),
                'current_resi_district_id'=> Input::get('current_resi_district_id'),
                'current_resi_state_id'=> Input::get('current_resi_state_id'),
                'current_resi_pincode'=> Input::get('current_resi_pincode'),
                'current_resi_phone'=> Input::get('current_resi_phone'),
                'permanent_resi_street'=> Input::get('permanent_resi_street'),
                'pemanent_resi_city_id'=> Input::get('pemanent_resi_city_id'),
                'permanent_resi_district_id'=> Input::get('permanent_resi_district_id'),
                'permanent_resi_state_id'=> Input::get('permanent_resi_state_id'),
                'permanent_resi_pincode'=> Input::get('permanent_resi_pincode'),
                'permanent_resi_phone'=> Input::get('permanent_resi_phone'),
                'current_office_name'=> Input::get('current_office_name'),
                'current_office_street'=> Input::get('current_office_street'),
                'current_office_city_id'=> Input::get('current_office_city_id'),
                'current_office_district_id'=> Input::get('current_office_district_id'),
                'current_office_state_id'=> Input::get('current_office_state_id'),
                'current_office_email'=> Input::get('current_office_email'),
                'current_office_phone'=> Input::get('current_office_phone'),
                'current_office_alternate_phone'=> Input::get('current_office_alternate_phone'),
                'current_office_mobile'=> Input::get('current_office_mobile'),
                'current_office_pincode'=> Input::get('current_office_pincode'),
                'current_office_fax'=> Input::get('current_office_fax'),
                'permanent_office_name'=> Input::get('permanent_office_name'),
                'permanent_office_street'=> Input::get('permanent_office_street'),
                'permanent_office_city_id'=> Input::get('permanent_office_city_id'),
                'permanent_office_district_id'=> Input::get('permanent_office_district_id'),
                'permanent_office_state_id'=> Input::get('permanent_office_state_id'),
                'permanent_office_email'=> Input::get('permanent_office_email'),
                'permanent_office_phone'=> Input::get('permanent_office_phone'),
                'permanent_office_alternate_phone'=> Input::get('permanent_office_alternate_phone'),
                'permanent_office_mobile'=> Input::get('permanent_office_mobile'),
                'permanent_office_pincode'=> Input::get('permanent_office_pincode'),
                'permanent_office_fax'=> Input::get('permanent_office_fax'),
                'bank_id'=> Input::get('bank_id'),
                'account_number'=> Input::get('account_number'),
                'loan_amount'=> Input::get('loan_amount'),
                'tenure'=> Input::get('tenure'),
                'vehicle_id'=> Input::get('vehicle_id'),
                'dealer_id'=> Input::get('dealer_id'),
                'executive_name'=> Input::get('executive_name'),
                'executive_email'=> Input::get('executive_email'),
                'executive_phone'=> Input::get('executive_phone'),
                'vehicle_cost'=> Input::get('vehicle_cost'),
                'emi_amount'=> Input::get('emi_amount'),
                'type' => Input::get('type'),
                'password' => Input::get('phone'),

            );


            if(Input::get('existing_customer') == 1){

                $existing = array(
                    'bank_id' => Input::get('bank_id'),
                    'account_number' => Input::get('account_number')
                );

                $lead = array_merge($lead,$existing);

            }
            else{
                $existing = array(
                    'bank_id' => "",
                    'account_number' => ""
                );

                $lead = array_merge($lead,$existing);
            }
            if(Input::get('type') == 'person'){

                $person = array(
                    'occupation' => Input::get('occupation'),
                    'guardian' => Input::get('guardian'),
                    'guardian_first_name' => Input::get('guardian_first_name'),
                    'guardian_middle_name' => Input::get('guardian_middle_name'),
                    'guardian_last_name' => Input::get('guardian_last_name'),
                    'company_name' => "",
                    'constitution' => "",
                    'company_category' => ""
                );

                $lead = array_merge($lead,$person);
            }
            else{
                $person = array(
                    'company_name' => Input::get('company_name'),
                    'constitution' => Input::get('constitution'),
                    'company_category' => Input::get('company_category'),
                    'guardian' => "",
                    'guardian_first_name' => "",
                    'guardian_middle_name' => "",
                    'guardian_last_name' => ""
                );
                $lead = array_merge($lead,$person);

            }

            $array_lead = array('profile_picture','signature');
            foreach($array_lead as $lead_array){

                if(Input::hasFile($lead_array)){

                    $name = str_random(10).Input::file($lead_array)->getClientOriginalName();
                    Input::file("$lead_array")->move('upload/',$name);

                    $leads = array(
                        $lead_array => $name,
                    );
                    $lead = array_merge($lead,$leads);

                }
            }
                /*"yougetsignal.com"*/
            DB::table('leads')
                ->where('id',$id)
                ->update($lead);





            //------- Add Proof---------------

            $proof = array(

                'proof_pan_card_number'=> Input::get('proof_pan_card_number'),
                'proof_voter_card_number'=> Input::get('proof_voter_card_number'),
                'proof_adhar_card_number'=> Input::get('proof_adhar_card_number'),
                'proof_driving_license_number'=> Input::get('proof_driving_license_number'),
                'prrof_passport_number'=> Input::get('prrof_passport_number'),
                'proof_arm_license_number'=> Input::get('proof_arm_license_number'),
                'proof_bank_account_number'=> Input::get('proof_bank_account_number'),
                'resi_driving_license__number'=> Input::get('resi_driving_license__number'),
                'resi_passport__number'=> Input::get('resi_passport__number'),
                'resi_voter_car_number'=> Input::get('resi_voter_car_number'),
                'income_itr_number'=> Input::get('income_itr_number'),
                'income_computation_number'=> Input::get('income_computation_number'),
                'income_bank_name'=> Input::get('income_bank_name'),
                'income_bank_account_number'=> Input::get('income_bank_account_number'),
                'bank_bank_name'=> Input::get('bank_bank_name'),
                'bank_bank_account_number'=> Input::get('bank_bank_account_number'),
                'sign_pan_card_number'=> Input::get('sign_pan_card_number'),
                'sign_passport__number'=> Input::get('sign_passport__number'),
                'establishment_other_proofname1'=>Input::get('establishment_other_proofname1'),
                'establishment_other_proofname2'=>Input::get('establishment_other_proofname2'),
                'lead_id' =>$id
            );

            //for same proofs in different sections
            foreach($array_proof as $proof_array){

                if(Input::hasFile($proof_array)){

                    if($proof_array == "proof_pan_card_proof" || $proof_array == 'sign_pan_card_proof'){


                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                           'proof_pan_card_proof' =>  $names,
                           'sign_pan_card_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'proof_voter_card_proof' || $proof_array == 'resi_voter_card_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_voter_card_proof' =>  $names,
                            'resi_voter_card_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'proof_driving_license_proof' || $proof_array == 'resi_driving_license_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_driving_license_proof' =>  $names,
                            'resi_driving_license_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'proof_passport_proof' || $proof_array == 'resi_passport_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_passport_proof' =>  $names,
                            'resi_passport_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }
                    else if($proof_array == 'proof_bank_proof' || $proof_array == 'income_bank_1_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'proof_bank_proof' =>  $names,
                            'income_bank_1_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }

                    else if($proof_array == 'other_proof' || $proof_array == 'income_other_proof' || $proof_array == 'sign_other_proof' || $proof_array == 'owner_other_ownership_proof'){

                        $names = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$names);
                        $test = array(
                            'other_proof' =>  $names,
                            'income_other_proof' =>  $names,
                            'sign_other_proof' =>  $names,
                            'owner_other_ownership_proof' =>  $names,
                        );
                        $proof = array_merge($proof,$test);

                    }
                    else {
                        $name = str_random(10).Input::file($proof_array)->getClientOriginalName();
                        Input::file("$proof_array")->move('upload/',$name);

                        $proofs = array(
                            $proof_array => $name,
                        );
                        $proof = array_merge($proof,$proofs);

                    }

                }
            }

            //print_r($proof);exit;
            $check_lead_id = Proof::where('lead_id',$id)->count();

            if($check_lead_id == 0){

                $proof['lead_id'] =$id;
                DB::table('proofs')->insert($proof);

            }
            else{

                DB::table('proofs')->where('lead_id',$id)->update($proof);
            }



            $data = array(
                'status' => 'success',
                'message' =>"Lead's Information is Updated with proof and head towards the approval" ,
                'id' =>$id
            );

            echo json_encode($data);

        }
        else{

            $data = array(
                'status' => 'fail',
                'message' =>"Lead's Information cannot be Updated, please provide atleast 2 proves" ,
                'id' =>$id
            );

            echo json_encode($data);

        }


    }
    public function getAsset($id){
        //still need to check if existing customer and then to be done

        $leads = Lead::find($id);

        $vehicles = Vehicle::where('status','1')->get();
        $insurances = InsuranceCompany::where('status','1')->get();
        $products = Product::where('status','1')->get();

        $asset = AssetDetail::where('lead_id',$id)->first();
        $update = $asset->update;

        if($leads->existing_customer == 0 || $update == ""){
            return View::make('admin/asset_detail')->with(array('lead' => $leads,'vehicle' =>$vehicles,'insurance' =>$insurances,'product' =>$products,'id' => ""));
        }
        if($leads->existing_customer != 0 || $update == "no"){

            $exist_customer_id = $leads->existing_customer;
            $leads = Lead::find($exist_customer_id);
            $id = Lead::find($id);

            return View::make('admin/asset_detail')->with(array('lead' => $leads,'vehicle' =>$vehicles,'insurance' =>$insurances,'product' =>$products, 'id' => $id));
        }

    }
    public function postAsset($id){

        $asset = array(
            'lead_id' =>$id,
            'product_id' =>Input::get('product_id'),
            'vehicle_id' =>Input::get('vehicle_id'),
            'asset_description' =>Input::get('asset_description'),
            'chasis_number' =>Input::get('chasis_number'),
            'asset_age' =>Input::get('asset_age'),
            'engine_number' =>Input::get('engine_number'),
            'manufacture_year' =>Input::get('manufacture_year'),
            'insurance_id' =>Input::get('insurance_id'),
            'vehicle_number' =>Input::get('vehicle_number'),
            'update' =>"",
        );

        if(AssetDetail::where('lead_id',$id)->count() == 0){
            DB::table('asset_details')->insert($asset);
        }
        else{
            DB::table('asset_details')->where('lead_id',$id)->update($asset);
        }

        $lead = array(
            'product_id' =>Input::get('product_id'),
            'vehicle_id' =>Input::get('vehicle_id')
        );
        DB::table('leads')->where('id',$id)->update($lead);

        $data = array(
            'status' => 'success',
            'message' =>"Asset Details for lead is updated" ,
            'id' =>$id
        );

        echo json_encode($data);


    }
    public function postInsurance(){
        $id = Input::get('id');

        $insurance = InsuranceBranch::where('status','1')->where('insurance_id',$id)->get();


        $data = "<option value=''>---Please Select Branch of Insurance Company</option>";
        foreach($insurance as $insurances){
            $data .= "<option value='".$insurances->id."'>".$insurances->branch."</option>";
        }

        echo json_encode($data);
    }
    public function getReference($id){

        //still need to check if existing customer and then to be done

        $reference = "";

        if(Reference::where('lead_id',$id)->count() != 0){
            $reference = Reference::where('lead_id',$id)->get();
        }
        $leads = Lead::find($id);
        $cities = City::where('status','1')->get();

        $references = Reference::where('lead_id',$id)->first();

        $update = $references->update;
        if($leads->existing_customer == 0 || $update == ""){

            if(Reference::where('lead_id',$id)->count() != 0){
                $reference = Reference::where('lead_id',$id)->get();
            }

            return View::make('admin/reference')->with(array('lead' => $leads,'city' =>$cities,'references' =>$reference,'id' => "", 'r_id' => ""));
         }
        if($leads->existing_customer != 0 || $update == "no"){


            $exist_customer_id = $leads->existing_customer;
            $leads = Lead::find($exist_customer_id);


            if(Reference::where('lead_id',$id)->count() != 0){
                $reference = Reference::where('lead_id',$leads->id)->get();
                $reference_id = Reference::where('lead_id',$id)->first();
            }
            $id = Lead::find($id);
            return View::make('admin/reference')->with(array('lead' => $leads,'city' =>$cities,'references' =>$reference,'id' => $id,'r_id' => $reference_id));
        }

    }
    public function postReference($id){

        $limit = count(Input::get('name'));

        $ref=  Input::get('id');
        $name=  Input::get('name');
        $relation=  Input::get('relation');
        $address_street=  Input::get('address_street');
        $city=  Input::get('city_id');
        $district = Input::get('district_id');
        $state = Input::get('state_id');
        $phone = Input::get('phone');
        $mobile = Input::get('mobile');
        $alternate_mobile = Input::get('alternate_mobile');
        $pincode = Input::get('pincode');
        $std_code = Input::get('std_code');
        $email = Input::get('email');

        for($i=0; $i<$limit; $i++){
            $data = array(
                'name' =>$name[$i],
                'relation' =>$relation[$i],
                'address_street' =>$address_street[$i],
                'city_id' =>$city[$i],
                'district_id' =>$district[$i],
                'state_id' =>$state[$i],
                'phone' =>$phone[$i],
                'mobile' =>$mobile[$i],
                'alternate_mobile' =>$alternate_mobile[$i],
                'pincode' =>$pincode[$i],
                'std_code' =>$std_code[$i],
                'email' =>$email[$i],
                'lead_id' =>$id,
                'update' =>""
            );

            if($ref[$i] == ""){
                DB::table('references')->insert($data);
            }
            else{
                $ref_id = $ref[$i];
                DB::table('references')->where('id',$ref_id)->update($data);
            }
        }

        //approve lead after all personal detail, proof and reference detail

        $lead = array(
            'approve_lead' => 1,
            'fi_initiation' =>1
        );
        DB::table('leads')
            ->where('id',$id)
            ->update($lead);

        $data = array(
            'status' => 'success',
            'message' =>"Reference Details Have Been Added "

        );

        echo json_encode($data);

    }

    public function getRemovereference(){
        $id = Input::get('id');
        Reference::destroy($id);

        $data = array(
            'status' => 'success',

        );

        echo json_encode($data);

    }

    public function getBank(){
        $id = Input::get('id');

        $bank = Bank::find($id);
        echo $bank->bank;
    }

    public function getFinance($id){

        //still need to check if existing customer and then to be done

        $leads = Lead::find($id);
        $bank = Bank::where('status',1)->get();

        $asset = Finance::where('lead_id',$id)->first();
        $update = $asset->update;

        if($leads->existing_customer == 0 || $update == ""){
            return View::make('admin/financedetail')->with(array('lead' => $leads,'banks' => $bank,'id' => ""));
        }
        if($leads->existing_customer != 0 || $update == "no"){

            $exist_customer_id = $leads->existing_customer;
            $leads = Lead::find($exist_customer_id);
            $id = Lead::find($id);

            return View::make('admin/financedetail')->with(array('lead' => $leads,'banks' => $bank,'id' => $id));
        }
    }
    public function postSavefinance($id){
        //echo $id;exit();

        $advance_emi = Input::get('advance_emi');
        $bank_id = Input::get('bank_id');
        //$delivery = Input::get('delivery');
        $disbursement = Input::get('disbursement');
        $first_installment = Input::get('first_installment');
        $tenure = Input::get('tenure');
        $last_installment = Input::get('last_installment');
        $finance_id = Input::get('id');

        $finance = array(
            'advance_emi' =>$advance_emi,
            'bank_id' =>$bank_id,
            //'delivery' => $delivery,
            'disbursement' => $disbursement,
            'first_installment' => date('Y-m-d',strtotime($first_installment)),
            'tenure' => $tenure,
            'last_installment' => date('Y-m-d',strtotime($last_installment)),
            'lead_id' => $id,
            'update' => "",

        );
        //print_r($finance);exit();
        $array_upload = array(
            'upload_1',
            'upload_2',
            'upload_3',
            'upload_4',
        );
        foreach($array_upload as $upload_array){

            if(Input::hasFile($upload_array)){

                $name = str_random(10).Input::file($upload_array)->getClientOriginalName();
                Input::file($upload_array)->move('upload/',$name);

                $finances = array(
                    $upload_array => $name,
                );

                $finance = array_merge($finance,$finances);
            }



        }


        if($finance_id == ""){
            DB::table('finacial_details')->insert($finance);
        }
        else{
            DB::table('finacial_details')->where('id',$finance_id)->update($finance);
            $check = Finance::find($finance_id);
            if($check->advance_emi == "" || $check->bank_id == "" || $check->delivery == "" || $check->disbursement == "" || $check->first_installment == "" || $check->tenure == "" || $check->last_installment == "")
            {
                $final = array(
                    'finally_save' => 0
                );
                DB::table('temporary_leads')->where('lead_id',$id)->update($final);
            }
        }
        $data = array(
            'status' => 'success',
            'message' => 'Financial Detail Updated Successfully !'
        );

        echo json_encode($data);

    }

    public function postDistrict(){

        $id = Input::get('city_id');
        $city = City::find($id);
        $district_id = $city->district_id;

        $district = $city->district()->first()->district;

        $state = $city->state()->first()->state;

        $data = array(

            'district' => $district,
            'state' => $state
        );
        echo json_encode($data);

    }
}