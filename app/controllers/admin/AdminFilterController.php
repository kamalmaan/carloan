<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 3/8/15
 * Time: 2:17 PM
 */
class AdminFilterController extends BaseController{
    function __construct(){
        $this->beforefilter('admin');
    }
    public function getIndex(){

        return View::make('admin/filter');
    }
}