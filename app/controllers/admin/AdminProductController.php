<?php
class AdminProductController extends BaseController{
    function __construct(){
        $this->beforefilter('admin');
        if(!role_permission('4','component_product'))
        {
            return Redirect::to('admin/')->send();
        }
    }
    public function getIndex(){


        $res = Datatable::table()
            ->addColumn('Id', 'Product Type','Status','Action')
            ->setUrl(URL::to('admin/product/product'))
            ->noScript();

      return View::make('admin.product',array('data' => $res));


//        return View::make('admin/product')->with('data',$table);
    }
    public function getProduct()
    {
        $query = Product::get();


        return Datatable::collection($query)
            ->showColumns('id')
            ->addColumn('product',function($model){
                return ucfirst($model->product);
            })
            //->setClass('table-striped table-hover')
            ->addColumn('status',function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs' id='status_".$model->id."'>Disable</button>";
                }
                else{
                    return "<button class='btn btn-success btn-xs' id='status_".$model->id."'>Enable</button>";
                }

            })
            ->addColumn('action', function($model){
                return "<a href='product/edit/" . $model->id . "'><button class='btn btn-xs btn-info'>Edit</button></a>";
            })

            ->searchColumns('id','product')
            ->orderColumns('id','product')
            ->make();

    }
    public function getAdd(){

        return View::Make('admin/product_add');
    }
    public function postSave(){
        $rules = array(
            'product' => 'required|unique:products'
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return Redirect::back()
                ->withErrors($validator);
        }
        else{
            $product = new Product;

            $product->product = Input::get('product');

            $product->save();

            return Redirect::to('admin/product')->with('message','Product Type successfully saved.');
        }
    }
    public function getEdit($id){

        $product = Product::where('id',$id)->first();
        return View::Make('admin/product_edit')->with('data',$product);
    }
    public function postUpdate($id){

        $rules = array(
            'product'    => 'required|unique:products'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator); // send back all errors to the form
        }
        else
        {
            $product = array(
                'product' => Input::get('product')
            );

            DB::table('products')
                ->where('id',$id)
                ->update($product);

            return Redirect::to('admin/product')->with('message','Product Type successfully updated.');
        }
    }
    public function getStatus(){
        $id = Input::get('id');
        $product = explode('_',$id);
        $check = $product[1];
        $status = Product::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('products')
            ->where('id',$check)
            ->update($checkstatus);
    }
    public function getDelete($id){

        Product::where('id',$id)->delete();

        return Redirect::to('admin/product')->with('message','Product Type successfully deleted.');

    }
}