<?php
Class AdminDealerController extends BaseController{
    function __contruct(){
        $this->beforeFilter('admin');
        if(!role_permission('4','component_dealer'))
        {
            return Redirect::to('admin/')->send();
        }
    }
    public function getIndex(){

        $dealer = Datatable::table()
            ->addColumn('Id', 'Dealer','Gm','RC Executive','Accountant','Action')
            ->setUrl(URL::to('admin/dealer/dealer'))
            ->noScript();
        return View::make('admin/dealer')->with('data',$dealer);
    }
    public function getDealer(){

        $query = Dealer::get();


        return Datatable::collection($query)
            ->showColumns('id')
            ->addColumn('dealer',function($model){
                return ucfirst($model->dealer);
            })
            ->addColumn('gm',function($model){
                return "<Strong class='text-info'>Name:</strong> ".ucfirst($model->gm_name)."<br><strong class='text-info'>Phone:</strong> ".ucfirst($model->gm_phone)."<br><strong class='text-info'>Email:</strong> ".ucfirst($model->gm_email);
            })
            ->addColumn('rc',function($model){
                return "<Strong class='text-info'>Name:</strong> ".ucfirst($model->rc_name)."<br><strong class='text-info'>Phone:</strong> ".ucfirst($model->rc_phone)."<br><strong class='text-info'>Email:</strong> ".ucfirst($model->rc_email);
            })
            ->addColumn('accountant',function($model){
                return "<Strong class='text-info'>Name:</strong> ".ucfirst($model->ac_name)."<br><strong class='text-info'>Phone:</strong> ".ucfirst($model->ac_phone)."<br><strong class='text-info'>Email:</strong> ".ucfirst($model->ac_email);
            })
            ->addColumn('status',function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs status' id='status_".$model->id."'>Disable</button> <button type='button' class='btn btn-xs btn-info dealer' id='dealer_".$model->id."'>Edit</button> <a href='dealer/dealerbranch/".$model->id."'><button type='button' class='btn btn-xs btn-info dealer' id='page_".$model->id."'>Branch</button></a>";
                }
                else{
                    return "<button class='btn btn-success btn-xs status' id='status_".$model->id."'>Enable</button> <button type='button' class='btn btn-xs btn-info dealer' id='dealer_".$model->id."'>Edit</button> <a href='dealer/dealerbranch/".$model->id."'><button type='button' class='btn btn-xs btn-info dealer' id='page_".$model->id."'>Branch</button></a>";
                }

            })

            ->searchColumns('id','dealer','email','gm_name','gm_phone','gm_email','rc_name','rc_phone','rc_email','ac_name','ac_phone','ac_email','status')
            ->orderColumns('model')
            ->make();
    }
    public function getStatus(){
        $id = Input::get('id');
        $dealer = explode('_',$id);
        $check = $dealer[1];
        $status = Dealer::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('dealers')
            ->where('id',$check)
            ->update($checkstatus);
    }
    public function postSave(){


        $dealer = new Dealer;
        $dealer->dealer = Input::get('dealer');
        $dealer->gm_name = Input::get('gm_name');
        $dealer->gm_phone = Input::get('gm_phone');
        $dealer->gm_email = Input::get('gm_email');
        $dealer->rc_name = Input::get('rc_name');
        $dealer->rc_phone = Input::get('rc_phone');
        $dealer->rc_email = Input::get('rc_email');
        $dealer->ac_name = Input::get('ac_name');
        $dealer->ac_phone = Input::get('ac_phone');
        $dealer->ac_email = Input::get('ac_email');

        $dealer->save();
        $data = array(
           'status' => 'success',
           'message' =>'Vehicle Successfully Saved.',

        );
        echo json_encode($data);

    }
    public function getEdit(){

        Input::get('id');
        $check = explode('_',Input::get('id'));
        $id = $check[1];

        $data = Dealer::find($id);
        //echo $data;
        $resp = array(
            'record' => $data
        );
        header('content-type: application/json');
        echo json_encode($resp);


    }
    public function postUpdate(){


        $id = Input::get('id');
        $dealer = array(
            'dealer' => Input::get('dealer'),
            'gm_name' => Input::get('gm_name'),
            'gm_phone' => Input::get('gm_phone'),
            'gm_email' => Input::get('gm_email'),
            'rc_name' => Input::get('rc_name'),
            'rc_phone' => Input::get('rc_phone'),
            'rc_email'=> Input::get('rc_email'),
            'ac_name' => Input::get('ac_name'),
            'ac_phone' => Input::get('ac_phone'),
            'ac_email' => Input::get('ac_email'),
        );

        DB::table('dealers')
            ->where('id',$id)
            ->update($dealer);
        $data = array(
            'status' => 'success',
            'message' =>'vehicle Successfully Updated.',

        );

        echo json_encode($data);

    }
    public function getDealerbranch($id){

        $dealerbranch = Datatable::table()
            ->addColumn('Id', 'Branch','BM Name','BM Email','BM Phone','Action')
            ->setUrl(URL::to("admin/dealer/branchrecord/$id"))
            ->noScript();

        return View::make('admin/dealerbranch')->with('data',$dealerbranch)->with('id',$id);

    }
    public function getBranchrecord($id){

        $query = DealerBranch::where('dealer_id',$id)->get();

        return Datatable::collection($query)
            ->showColumns('id')
            ->addColumn('branch_id',function($model){
                return ucfirst($model->city()->first()->city);
            })
            ->addColumn('bm_name',function($model){
                return ucfirst($model->bm_name);
            })
            ->addColumn('bm_phone',function($model){
                return ucfirst($model->bm_phone);
            })
            ->addColumn('bm_email',function($model){
                return ucfirst($model->bm_email);
            })
            ->addColumn('status',function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs status' id='status_".$model->id."'>Disable</button> <button type='button' class='btn btn-xs btn-info dealerbranch' id='".$model->id."_".$model->dealer_id."'>Edit</button>";
                }
                else{
                    return "<button class='btn btn-success btn-xs status' id='status_".$model->id."'>Enable</button> <button type='button' class='btn btn-xs btn-info dealerbranch' id='".$model->id."_".$model->dealer_id."'>Edit</button>";
                }

            })

            ->searchColumns('id','branch','bm_name','bm_phone','bm_email','status')
            ->orderColumns('branch')
            ->make();

    }
    public function getBranchdealerstatus(){

        $id = Input::get('id');
        $dealerbranch = explode('_',$id);
        $check = $dealerbranch[1];
        $status = DealerBranch::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('dealer_branches')
            ->where('id',$check)
            ->update($checkstatus);

    }
    public function getBranchdealeradd(){

        $city = City::get();

        $data = "";

         foreach($city as $cities){
             echo $data = "<option value='".$cities->id."'>".$cities->city."</option>";
         }

        header('content-type: application/json');
        echo json_encode($data);


    }
    public function postDealerbranchsave(){

        $dealerbranch = new DealerBranch;
        $dealerbranch->dealer_id = Input::get('dealer_id');
        $dealerbranch->branch_id = Input::get('branch_id');
        $dealerbranch->bm_name = Input::get('bm_name');
        $dealerbranch->bm_phone = Input::get('bm_phone');
        $dealerbranch->bm_email = Input::get('bm_email');


        $dealerbranch->save();
        $data = array(
            'status' => 'success',
            'message' =>'Branch For Dealer Successfully Saved.',

        );
        echo json_encode($data);

    }
    public function getBranchdealeredit(){
        header('content-type: application/json');
        Input::get('id');
        $check = explode('_',Input::get('id'));
        $id = $check[0];
        $arr = array();
        $data = DealerBranch::find($id);

        $city = City::get();

        $citydata = "";

        foreach($city as $cities){

            $citydata .= "<option value='".$cities->id."'";
            if($cities->id==$data->branch_id){

                $citydata .= "selected";

            }
            $citydata .= ">".$cities->city."</option>";
        }

        //$arr[] = $data;
        //$arr[] = $citydata;
        $res = array(
            'record' => $data,
            'city' => $citydata
        );

        echo json_encode($res);


    }
    public function postDealerbranchupdate(){


        $id = Input::get('id');
        $dealerbranch = array(
            'branch_id' => Input::get('branch_id'),
            'bm_name' => Input::get('bm_name'),
            'bm_phone' => Input::get('bm_phone'),
            'bm_email' => Input::get('bm_email'),
        );

        DB::table('dealer_branches')
            ->where('id',$id)
            ->update($dealerbranch);
        $data = array(
            'status' => 'success',
            'message' =>'Branch For Dealer Successfully Updated.',

        );

        echo json_encode($data);

    }

}