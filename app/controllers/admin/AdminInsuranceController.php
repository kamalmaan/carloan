<?php
class AdminInsuranceController extends BaseController
{
    function __construct()
    {
        $this->beforefilter('admin');
        if(!role_permission('5','insurance_insurance'))
        {
            return Redirect::to('admin/')->send();
        }
    }


    public function getIndex(){

        $data = Datatable::table()
            ->addColumn('Customer','Comp.Vehicle','Insurance Amount','DOB','Issue Date','Manufacturing','Nominee','Action')
            ->setUrl(URL::to('admin/insurance/insurancelist'))
            ->noScript();
        $cities=City::all();
        $district=District::all();
        return View::make('admin/insurance')->with('data',$data)->with('cities',$cities)->with('district',$district);
    }
    public function getInsurancelist(){

        $query = Insurance::get();


        return Datatable::collection($query)
            ->addColumn('Customer',function($model){
                return ucfirst('<strong class="text-info">Name:</strong>'.$model->customer_name.'<br><strong class="text-info">Phone:</strong> :'.$model->customer_phone);
            })

            ->addColumn('Comp.Vehicle',function($model){
                return ucfirst($model->company_vehicle);
            })
            ->addColumn('Insurance Amount',function($model){
                return ucfirst($model->insurance_amount);
            })
            ->addColumn('DOB',function($model){
                return ucfirst(date('d-m-Y',strtotime($model->dob)));
            })
            ->addColumn('Issue Date',function($model){
                return ucfirst(date('d-m-Y',strtotime($model->issue_date)));
            })
            ->addColumn('Manufacturing',function($model){
                return ucfirst($model->manufacturing_year);
            })
            ->addColumn('Nominee',function($model){
                return ucfirst('<strong class="text-info">Name:</strong>'.$model->nominee_name.'<br><strong class="text-info">Relation:</strong>'.$model->nominee_relation.'<br><strong class="text-info">DOB:</strong>'.date('d-m-y',strtotime($model->nominee_dob)));
            })
            ->addColumn('Action',function($model){
                return ucfirst('<button class="btn btn-xs btn-info insurance_edit" id="insurance_'.$model->id.'">Edit</button> <button class="btn btn-xs delete btn-danger" id="insurance_'.$model->id.'">Delete</button>  ');
            })
            ->searchColumns('Customer')
            ->orderColumns('Insurance Amount')
            ->make();
    }


    /**
     * Add Insurance
    */
    public function postAdd(){
         Insurance::create(Input::all());
        $data = array(
            'status' => 'success',
            'message' =>'Insurance Successfully Saved.',
        );
        header('content-type: application/json');
        echo json_encode($data);
    }

    /**
     * Destroy Insurance
    */

    public function getDestroy(){

        $check = explode('_',Input::get('id'));
        $id = $check[1];

        Insurance::destroy($id);
        $data = array(
            'status' => 'success',
            'message' =>'Error In Deletion.',

        );
        echo json_encode($data);
    }

    /**
     * Insurance Edit
    */

    public function getEdit(){
        $check = explode('_',Input::get('id'));
        $id = $check[1];

        $data=array(
            'record'=>Insurance::find($id)
        );
        echo json_encode($data);
    }

    /**
     *  Updating Insurance Record
    */

    public function postUpdate(){

        $id=Input::get('id');
        $data=Input::all();
        unset($data['id']);

        /* update */
        Insurance::where('id',$id)->update($data);

        $data = array(
            'status' => 'success',
            'message' =>'Insurance Successfully Updated.',
        );
        echo json_encode($data);
    }

    /**
     * City and District Id get
    */

    public function getCity(){

        $id=Input::get('id');
        $data=City::find($id);

        $data=District::find($data->district_id);
        echo json_encode($data);
    }


}