<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 6/8/15
 * Time: 11:29 AM
 */
class AdminBranchController extends BaseController
{
    function __construct()
    {
        $this->beforefilter('admin');
        if(!module_permission('15'))
        {
            return Redirect::to('admin/')->send();
        }
    }


    public function getIndex(){

        $data = Datatable::table()
            ->addColumn('Branch','Address','Action')
            ->setUrl(URL::to('admin/branch/branchlist'))
            ->noScript();
        $cities=City::all();
        return View::make('admin/branch')->with('data',$data)->with('city', $cities);
    }
    public function getBranchlist(){

        $query = Branch::get();
        return Datatable::collection($query)
            ->addColumn('Branch',function($model){

                if (!empty($model->city()->first()->city)) {
                return ucfirst($model->city()->first()->city);
                }
            })


            ->addColumn('Address',function($model){
                return ucfirst($model->address);
            })
            ->addColumn('Action',function($model){
                return ucfirst('<button class="btn btn-xs btn-info branch_edit" id="branch_'.$model->id.'">Edit</button> <button class="btn btn-xs delete btn-danger" id="branch_'.$model->id.'">Delete</button>');
            })
            ->searchColumns('Branch')
            ->orderColumns('Branch')
            ->make();
    }



    /**
     * Add Branch
     */
    public function getAdd(){

        $city = City::get();

        foreach($city as $cities ) {
            $data = "<option value='".$cities->id."'>".$cities->city."</option>";
        }

        header('content-type: application/json');
        echo json_encode($data);
    }


    public function postAdd(){
        Branch::create(Input::all());
        $data = array(
            'status' => 'success',
            'message' =>'Branch Successfully Saved.',
        );
        header('content-type: application/json');
        echo json_encode($data);
    }


    /**
     *  Edit Branch
     */

    public function getEdit(){
        $check = explode('_',Input::get('id'));
        $id = $check[1];

        $data=array(
            'record'=>Branch::find($id),

        );
        echo json_encode($data);
    }

    /**
     *  Updating Branch Record
     */

    public function postUpdate(){
        $id=Input::get('id');

        $data=Input::all();
        unset($data['id']);

        /* update */
        Branch::where('id',$id)->update($data);


        $data = array(
            'status' => 'success',
            'message' =>'Branch Successfully Updated.',
        );
        echo json_encode($data);
    }


    /**
     * Destroy Branch
     */

    public function getDestroy(){

        $check = explode('_',Input::get('id'));
        $id = $check[1];

        Branch::destroy($id);
        $data = array(
            'status' => 'success',
            'message' =>'Error In Deletion.',

        );
        echo json_encode($data);
    }
    /**
     * City and District Id get
     */

    /*public function getCity(){

         $id=Input::get('id');
         $data=City::find($id);

         /*$data=District::find($data->district_id);
         echo json_encode($data);
     }*/
}


