<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 4/6/15
 * Time: 1:00 PM
 */
class AdminUnderwritingApproved extends BaseController{
    function __construct(){
        $this->beforeFilter('admin');
        if(!module_permission('8'))
        {
            return Redirect::to('admin/')->send();
        }

    }
    public function getIndex(){

        $temporary = Datatable::table()
            ->addColumn('Lead','Guardian Name','Email','Phone','LOS Number','Decline','Status')
            ->setUrl(URL::to('admin/underwritingapprovedlead/temporary'))
            ->noScript();

        $breadcrum = "Declined Temporary Lead";
        return View::make('admin/temporarylead')->with('data',$temporary)->with('breadcrum',$breadcrum);
    }
    public function getTemporary(){

        $query = TemporaryLead::select('temporary_leads.*')->join('temporary_customers','temporary_leads.lead_id','=','temporary_customers.lead_id')->where('temporary_leads.decline','1')->where('finally_save',0)->where("temporary_customers.decline",1)->get();
        /*$queries = DB::getQueryLog();
        $last_query = end($queries);
        print_r($last_query);exit;*/

        return Datatable::collection($query)


            ->addColumn('lead',function($model){
                if(!role_permission('8','underwritingapproved_profile')){
                    return "<button title='Edit Lead' class='btn btn-info btn-xs' disabled=''>".ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name)."</button>";
                }
                elseif(role_permission('8','underwritingapproved_profile')){
                    return "<a href='approve/finance/" . $model->lead_id . "'><button title='Edit Lead' class='btn btn-info btn-xs' >".ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name)."</button></a>";
                }

            })
            ->addColumn('guardian',function($model){
                return $model->guardian." Of ".ucfirst($model->lead()->first()->guardian_first_name)." ".ucfirst($model->lead()->first()->guardian_last_name);
            })
            ->addColumn('Email',function($model){
                return ucfirst($model->lead()->first()->email_id);
            })
            ->addColumn('Phone',function($model){
                return ucfirst($model->lead()->first()->phone);
            })
            ->addColumn('los',function($model){
                return ucfirst($model->los_number);
            })
            ->addColumn('decline',function($model){
                if(!role_permission('8','underwritingapproved_decline')){
                    return  "<button class='btn btn-danger btn-xs ' disabled=''>Decline</button>";
                }
                else if(role_permission('8','underwritingapproved_decline')){
                    return  "<button class='btn btn-danger btn-xs decline_temporary' id='".$model->id."'>Decline</button>";
                }


            })
            ->addColumn('status',function($model){
                if(!role_permission('8','underwritingapproved_status')){
                    return "<button class='btn btn-success btn-xs final' disabled=''>Disburse</button>";
                }
                elseif(role_permission('8','underwritingapproved_status')){
                    return "<button class='btn btn-success btn-xs final' id='".$model->id."'>Disburse</button>";
                }



                //return "<button class='btn btn-success btn-xs los_number' id='".$model->id."'>LOS Number</button>";



            })
            /*->addColumn('action', function($model){

                return "<div class='btn-group-xs'></div>";
            })*/
            ->searchColumns('id','lead_id','status')
            ->orderColumns('id','lead_type')
            ->make();

    }
    public function postLosnumber(){

        $lead_id = Input::get('lead_id');
        $fi_id = Input::get('fi_id');
        $applied_loan_amount = Input::get('applied_loan_amount');
        $approve_loan_amount = Input::get('approve_loan_amount');
        $approve_los_number = Input::get('approve_los_number');
        $special_condition = Input::get('special_condition');


        $temporary = array(
            'lead_id' => $lead_id,
            'applied_loan_amount' => $applied_loan_amount,
            'approve_loan_amount' => $approve_loan_amount,
            'los_number' => $approve_los_number,
            'special_condition' => $special_condition
        );

        $fi = FiInitiation::find($fi_id);
        $fi->status = 'los';
        $fi->update();

        DB::table('temporary_leads')->insert($temporary);

        $data = array(
            'status' => 'success',
            'message' => "NOS Number Is Successfully Assigned"
        );
        header('content-type:application/json');
        echo json_encode($data);
    }
    public function getStatus(){
        $id = Input::get('id');

        $temporary = TemporaryLead::find($id);
        if(Input::get('status') == 'decline'){
            $temporary->decline = 0;
        }
        if(Input::get('status') == 'recycle'){
            $temporary->decline = 1;
        }
        $temporary->update();

        $data = array(
            'status' => 'success',
            'message' => "Temporary Lead Is Declined Successfully"
        );
        header('content-type:application/json');
        echo json_encode($data);


    }
    public function getFinal(){
        $id = Input::get('id');
        $temporary = TemporaryLead::find($id);
        $temporary->finally_save = 1;
        $temporary->update();

        $lead_id = $temporary->lead_id;

        $lead = Lead::find($lead_id);
        $lead->fi_initiation = 3;
        $lead->update();

        $data = array(
            'status' => 'success',
            'message' => "Lead has become permanent customer"
        );
        header('content-type:application/json');
        echo json_encode($data);

    }
    public function getTemporarydecline(){
        if(!module_permission('9'))
        {
            return Redirect::to('admin/')->send();
        }
        $temporary = Datatable::table()
            ->addColumn('Lead','Guardian Name','Email','Phone','LOS Number','Decline')
            ->setUrl(URL::to('admin/underwritingapprovedlead/decline'))
            ->noScript();
        $breadcrum = "Declined Temporary Lead";
        return View::make('admin/temporarylead')->with('data',$temporary)->with('breadcrum',$breadcrum);
    }

    public function getDecline(){
        $query = TemporaryLead::where('decline','0')->get();


        return Datatable::collection($query)


            ->addColumn('lead',function($model){
                return ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name);
            })
            ->addColumn('guardian',function($model){
                return $model->guardian." Of ".ucfirst($model->lead()->first()->guardian_first_name)." ".ucfirst($model->lead()->first()->guardian_last_name);
            })
            ->addColumn('Email',function($model){
                return ucfirst($model->lead()->first()->email_id);
            })
            ->addColumn('Phone',function($model){
                return ucfirst($model->lead()->first()->phone);
            })
            ->addColumn('los',function($model){
                return ucfirst($model->los_number);
            })
            ->addColumn('recycle',function($model){
                if(!role_permission('9','underapproveddecline_recycle')){
                    return  "<button class='btn btn-success btn-xs' disabled=''>Recycle</button>";
                }
                else if(role_permission('9','underapproveddecline_recycle')){
                    return  "<button class='btn btn-success btn-xs recycle_temporary' id='".$model->id."'>Recycle</button>";
                }

            })

            /*->addColumn('action', function($model){

                return "<div class='btn-group-xs'></div>";
            })*/
            ->searchColumns('id','lead_id','status')
            ->orderColumns('id','lead_type')
            ->make();

    }


}