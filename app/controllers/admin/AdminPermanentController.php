<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 10/6/15
 * Time: 10:40 AM
 */
class AdminPermanentController extends BaseController{
    function __construct(){
        $this->beforefilter('admin');
        if(!module_permission('12'))
        {
            return Redirect::to('admin/')->send();
        }
    }
    public function getIndex(){
        //,'Calculation'
        $temporary = Datatable::table()
            ->addColumn('Lead','Guardian Name','Email','Phone','LOS Number','Rc','Customer End Date','manage lead')
            ->setUrl(URL::to('admin/permanent/record'))
            ->noScript();

        $breadcrum = "Permanent Lead";
        return View::make('admin/temporarylead')->with('data',$temporary)->with('breadcrum',$breadcrum);
    }
    public function getRecord(){

        $query = TemporaryLead::where('decline','1')->where('finally_save',1)->get();


        return Datatable::collection($query)


            ->addColumn('lead',function($model){

                if(!empty($model->lead()->first()->company_name)){
                    $user = ucfirst($model->lead()->first()->company_name) ."(".$model->lead()->first()->constitution." - ".$model->lead()->first()->company_category.")";
                }
                else{
                    $user = ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name);
                }
                if(!role_permission('12','permanentcustomer_profile')){
                    return "<span><button title='Edit Lead' class='btn btn-info btn-xs' disabled='' >".$user."</button><sup><span class='badge badges_up label-success'>Permanent</span></sup></span>";
                }
                elseif(role_permission('12','permanentcustomer_profile')){
                    return "<span><a href='lead/leadtimeline/" . $model->lead_id . "'><button title='Edit Lead' class='btn btn-info btn-xs' >".$user."</button></a><sup><span class='badge badges_up label-success'>Permanent</span></sup></span>";
                }


            })
            ->addColumn('guardian',function($model){
                return $model->guardian." Of ".ucfirst($model->lead()->first()->guardian_first_name)." ".ucfirst($model->lead()->first()->guardian_last_name);
            })
            ->addColumn('Email',function($model){
                return ucfirst($model->lead()->first()->email_id);
            })
            ->addColumn('Phone',function($model){
                return ucfirst($model->lead()->first()->phone);
            })
            ->addColumn('los',function($model){
                return ucfirst($model->los_number);
            })
            ->addColumn('RC',function($model){
                if($model->rc == ""){
                	$rc = "Pending";
                }
                else{
                	$rc = $model->rc;
                }
                if(!role_permission('12','permanentcustomer_rc')){
                    return  "<button class='btn btn-warning btn-xs ' disabled='' value = '".$model->rc."'>".$rc."</button>";
                }
                elseif(role_permission('12','permanentcustomer_rc')){
                    return  "<button class='btn btn-warning btn-xs rc' id='".$model->id."' value = '".$model->rc."'>".$rc."</button>";
                }

                
                
            })
            ->addColumn('customer_end_date',function($model){


                return "customer_end_date";


            })
            /*->addColumn('calculation',function($model){

                if(!role_permission('12','permanentcustomer_calculation')){
                    return "<button class='btn btn-success btn-xs ' disabled=''>Calculation</button>";
                }elseif(role_permission('12','permanentcustomer_calculation')){
                    return "<button class='btn btn-success btn-xs calculation' id='".$model->id."'>Calculation</button>";
                }


                //return "<button class='btn btn-success btn-xs los_number' id='".$model->id."'>LOS Number</button>";
            })*/
            ->addColumn('manage_lead',function($model){

                $ci=Coapplicant::where('lead_id','=',$model->lead_id )->count();
                $update_info="";
                if(!role_permission('12','permanentcustomer_update')){
                    $update_info="<button class='btn btn-info btn-xs' disabled=''>Update Info</button>";
                }
                elseif(role_permission('12','permanentcustomer_update')){
                    $update_info="<a href='approve/index/" . $model->lead_id . "'><button class='btn btn-info btn-xs' >Update Info</button></a>";
                }

                $coapplicant="";
                if(!role_permission('12','permanentcustomer_coapplicant')){
                    $coapplicant="<button class='btn btn-info btn-xs' disabled=''>Co-Applicant<sup><span class='badge badges_up label-success'>$ci</span></sup></button>";
                }
                elseif(role_permission('12','permanentcustomer_coapplicant')){
                    $coapplicant="<a href='co-applicant/index/" . $model->lead_id . "'><button class='btn btn-info btn-xs'>Co-Applicant<sup><span class='badge badges_up label-success'>$ci</span></sup></button></a>";
                }

                return "<div class='btn-group-xs'>".$update_info."<br>".$coapplicant;

            })
            /*->addColumn('action', function($model){

                return "<div class='btn-group-xs'></div>";
            })*/
            ->searchColumns('id','lead_id','status')
            ->orderColumns('id','lead_type')
            ->make();

    }
    public function postRc(){

        $id = Input::get('id');
        $rc = Input::get('rc');

        $tem = TemporaryLead::find($id);
        $tem->rc = $rc;
        $tem->update();

        $data = array(
            'status' => 'success',
            'message' => 'RC successfully saved'
        );

        header('content-type:application-json');
        echo json_encode($data);

    }

}