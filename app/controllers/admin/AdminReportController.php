<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
class AdminReportController extends BaseController{
    public function getIndex($id,$bank,$branch_id){
    

        //$data=Lead::find($id);
        //http://localhost/car/public/admin/report/index/10/djgh@fhj.ovcm/2/7
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        /** Include PHPExcel */
        require_once dirname(__FILE__) . '/Classes/PHPExcel.php';


// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

// Set document properties
        $objPHPExcel->getProperties()->setCreator("Shree Balaji Enterprise")
            ->setLastModifiedBy("Ranjit Aulakh")
            ->setTitle("FI Initiation Document")
            ->setSubject("FI Initiation Lead Detail Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");

        /**/

        $data = Lead::find($id);


        //PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

        $styleArray = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '00BFFF')
            ),
            'font' => array(
                'bold' => true,
                'size' => 14,
                'color' => array('rgb' => 'FFFFFF'),
                'name' => 'Times New Roman'
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle("A29")->applyFromArray($styleArray);


        $styleArrayhead = array(
            'font' => array(
                'bold' => true,
                'size' => 12,
                //'color' => array('rgb' => '808080'),
                'name' => 'Times New Roman'
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle("A")->applyFromArray($styleArrayhead);
        $objPHPExcel->getActiveSheet()->getStyle("C30")->applyFromArray($styleArrayhead);
        $objPHPExcel->getActiveSheet()->getStyle("E30")->applyFromArray($styleArrayhead);
        $objPHPExcel->getActiveSheet()->getStyle("G30")->applyFromArray($styleArrayhead);
        $objPHPExcel->getActiveSheet()->getStyle("B31")->applyFromArray($styleArrayhead);
        $objPHPExcel->getActiveSheet()->getStyle("C31")->applyFromArray($styleArrayhead);
        $objPHPExcel->getActiveSheet()->getStyle("D31")->applyFromArray($styleArrayhead);
        $objPHPExcel->getActiveSheet()->getStyle("E31")->applyFromArray($styleArrayhead);
        $objPHPExcel->getActiveSheet()->getStyle("F31")->applyFromArray($styleArrayhead);

        $styleArraydata = array(
            'font' => array(
                'bold' => false,
                'size' => 12,
                //'color' => array('rgb' => '808080'),
                'name' => 'Times New Roman'
            )
        );
        for($x = 6;$x<=28;$x++){
            $objPHPExcel->getActiveSheet()->getStyle("B".$x)->applyFromArray($styleArraydata);
        }
        $objPHPExcel->getActiveSheet()->getStyle("32")->applyFromArray($styleArraydata);


        $objPHPExcel->setActiveSheetIndex(0)
             ->mergeCells("A1:J1")->setCellValue('A1','FI Initiation Detail')
            ->mergeCells('A2:J2')->setCellValue('A2', 'Reference Number:')
            ->mergeCells('A3:J3')->setCellValue('A3', 'DMA/DSA Name:Shree Balaji Enterprise')
            ->mergeCells('A4:J4')->setCellValue("A4", "Date And Time:")
            ->mergeCells("A5:J5")->setCellValue("A5", "Co-Applicant/Guarantor")
            ->setCellValue("A6", "Applicant's Name")
            ->mergeCells("B6:J6")->setCellValue("B6",  ($data->type != "firm")?ucfirst($data->lead_first_name)." ".ucfirst($data->lead_last_name)."(Single Person)":ucfirst($data->compnay_name)."(Firm)")
            ->setCellValue("A7", "Date of Birth")
            ->mergeCells("B7:J7")->setCellValue("B7", date('d-m-Y',strtotime($data->dob)))
            ->setCellValue("A8", "Phone Number")
            ->mergeCells("B8:J8")->setCellValue("B8", (string)$data->phone)

            ->setCellValue("A9", "PAN")
            ->mergeCells("B9:J9")->setCellValue("B9", (!empty($data->proof()->first()->sign_pan_card_number))?$data->proof()->first()->sign_pan_card_number:null)
            ->setCellValue("A10", "Passport Number")
            ->mergeCells("B10:J10")->setCellValue("B10", (!empty($data->proof()->first()->sign_passport__number))?$data->proof()->first()->sign_passport__number:null)
            ->setCellValue("A11", "Voter Id Card Number")
            ->mergeCells("B11:J11")->setCellValue("b11", (!empty($data->proof()->first()->resi_voter_car_number))?$data->proof()->first()->resi_voter_car_number:null)

            ->setCellValue("A12", "Adhar Card Number")
            ->mergeCells("B12:J12")->setCellValue("b12", (!empty($data->proof()->first()->proof_adhar_card_number))?$data->proof()->first()->proof_adhar_card_number:null)

            ->setCellValue("A13", ($data->type == "person")?$data->guardian: "Constituion/CompanyCategory")
            ->mergeCells("B13:J13")->setCellValue("B13", ($data->type == "person")? ucfirst($data->guardian_first_name)." ".ucfirst($data->guardian_last_name):$data->constitution." - ".ucfirst($data->company_category))
            ->setCellValue("A14", "Residential Address")
            ->mergeCells("B14:J14")->setCellValue("B14", (!empty($data->current_resi_city_id))?$data->current_resi_street.", ".$data->current_resi_city()->first()->city.",".$data->current_resi_district_id.", ".$data->current_resi_state_id:null)
            ->setCellValue("A15", "Phone Number(Landline)")
            ->mergeCells("B15:J15")->setCellValue("B15", $data->crrent_resi_phone)
            ->setCellValue("A16", "Permanent Address")
            ->mergeCells("B16:J16")->setCellValue("B16", (!empty($data->pemanent_resi_city_id))?$data->permanent_resi_street.", ".$data->permanent_resi_city()->first()->city.",".$data->permanent_resi_district_id.", ".$data->permanent_resi_state_id:null)
            ->setCellValue("A17", "Office/Bussiness Name")
            ->mergeCells("B17:J17")->setCellValue("B17", $data->permanent_office_name)
            ->setCellValue("A18", "Office Address(Permanent)")
            ->mergeCells("B18:J18")->setCellValue("B18", (!empty($data->permanent_office_city_id))?$data->permanent_office_street.", ".$data->permanent_office_city()->first()->city.",".$data->permanent_office_district_id.", ".$data->permanent_office_state_id:null)
            ->setCellValue("A19", "Phone")
            ->mergeCells("B19:J19")->setCellValue("B19", $data->permanent_office_phone)
            ->setCellValue("A20", "Office/Bussiness Name")
            ->mergeCells("B20:J20")->setCellValue("B20", $data->current_office_name)
            ->setCellValue("A21", "Office Address(Current)")
            ->mergeCells("B21:J21")->setCellValue("B21", (!empty($data->current_office_city_id))?$data->current_office_street.", ".$data->current_office_city()->first()->city.",".$data->current_office_district_id.", ".$data->current_office_state_id:null)
            ->setCellValue("A22", "Phone")
            ->mergeCells("B22:J22")->setCellValue("B22", $data->current_office_phone)

            ->setCellValue("A23", "Vehicle Required")
            ->mergeCells("B23:J23")->setCellValue("B23", (!empty($data->vehicle_id))? $data->vehicle()->first()->make ." ".$data->vehicle()->first()->model ." (". $data->vehicle()->first()->variant . "," . $data->vehicle()->first()->category .")":null)
            ->setCellValue("A24", "Tenure")
            ->mergeCells("B24:J24")->setCellValue("B24", $data->tenure)
            ->setCellValue("A25", "Amount Fianced")
            ->mergeCells("B25:J25")->setCellValue("B25", (string)$data->loan_amount)
            ->setCellValue("A26", "Previous Vehicle")
            ->mergeCells("B26:J26")->setCellValue("B26", "")
            ->setCellValue("A27", "Track Details")
            ->mergeCells("B27:J27")->setCellValue("B27", "")
            ->setCellValue("A28", "PDC Form")
            ->mergeCells("B28:J28")->setCellValue("B28", "")

            ->mergeCells('A29:J29')->setCellValue("A29", "For Office Use Only")
            ->mergeCells('A30:B30')->setCellValue("A30", "FI Request to Agency Given On")
            ->setCellValue("A31", "Date")
            ->setCellValue("B31", "Time")

            ->mergeCells('C30:D30')->setCellValue("C30", "FIR Recieved On")
            ->setCellValue("C31", "Date")
            ->setCellValue("D31", "Time")

            ->mergeCells('E30:F30')->setCellValue("E30", "Recievers Signature")
            ->mergeCells('E31:F31')->setCellValue("E31", "")

            ->mergeCells('G30:J30')->setCellValue("G30", "Signature Of FI Agency Representative")
            ->mergeCells('G31:J31')->setCellValue("G31", "")

            ->setCellValue("A32", "")
            ->setCellValue("B32", "")
            ->setCellValue("C32", "")
            ->setCellValue("D32", "")
            ->mergeCells('E32:F32')->setCellValue("E32", "")
            ->mergeCells('G32:J32')->setCellValue("G32", "");

            /*->setCellValue('E1', 'Answer')
            ->setCellValue('F1', 'FreeText')
            ->setCellValue('G1', 'Created');*/
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
            array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,

            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A29')->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
        );
        $objPHPExcel->getActiveSheet()->getStyle('A30')->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
        );
        $objPHPExcel->getActiveSheet()->getStyle('A31')->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
        );
        $objPHPExcel->getActiveSheet()->getStyle('A31')->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
        );
        $objPHPExcel->getActiveSheet()->getStyle('C30')->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
        );
        $objPHPExcel->getActiveSheet()->getStyle('C31')->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
        );
        $objPHPExcel->getActiveSheet()->getStyle('D31')->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
        );
        $objPHPExcel->getActiveSheet()->getStyle('E30')->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
        );
        $objPHPExcel->getActiveSheet()->getStyle('G30')->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(20);

        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);

        for($i = 2; $i<=28; $i++){
            $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(20);
        }


        $objPHPExcel->getActiveSheet()->getRowDimension(29)->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getRowDimension(30)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getRowDimension(31)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getRowDimension(32)->setRowHeight(100);


                /*$objPHPExcel->setActiveSheetIndex(0)

                    ->setCellValue("A2", ucfirst($data->lead_first_name))
                    ->setCellValue("A3", $data->phone);*/




// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');

        header('Content-Disposition: attachment;filename="'.$data->lead_first_name.'_'.$id.'01simple.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        //$objWriter->setIncludeCharts(true);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $dir = 'document/';$file = $data->lead_first_name.'_'.$id.'01simple.xls';
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        //$objWriter->save($path);
       $objWriter->save($dir.$file);
        	$lead = Lead::find($id);
	        $lead->fi_initiation = '2';
	        $lead->update();

            if(Temporary::where('lead_id',$id)->count() == 0){
                $temporary = New Temporary;
                $temporary->lead_id = $id;
                $temporary->save();
            }

	        $check = FiInitiation::where('lead_id', $id)->count();
	
	        if($check == 0){
	
	            $fi = New FiInitiation();
	
	            $fi->lead_id = $id;
	            $fi->bank_id = $bank;
	            $fi->branch_id = $branch_id;
	            $fi->mail = 'send';
	
	
	            $fi->save();
	        }
	        else{
	
	            $fi = array(
	                'bank_id' => $bank,
	                'status' => 0,
	                'branch_id' => $branch_id,
	                'decline' =>1,
	                'mail' =>'send'
	
	            );
	            FiInitiation::where('lead_id',$id)->update($fi);
	        }
        
        

        $data = array(
            'status' => 'success',
            'message' => "FI Detail are done! You Can Check it in FI Status section"

        );
        header('content-type:apllication-json');
        echo json_encode($data);
        //return Redirect::to("admin/fi/mail/$id/$email/$file/$bank/$branch");






    }


}