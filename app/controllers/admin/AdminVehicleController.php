<?php
Class AdminVehicleController extends BaseController{
    function __contruct(){
        $this->beforeFilter('admin');
        if(!role_permission('4','component_vehicle'))
        {
            return Redirect::to('admin/')->send();
        }
    }
    public function getIndex(){

        $vehicle = Datatable::table()
            ->addColumn('Id', 'Model','Make','Variant','Category','Segment','CC','Action')
            ->setUrl(URL::to('admin/vehicle/vehicle'))
            ->noScript();
        return View::make('admin/vehicle')->with('data',$vehicle);
    }
    public function getVehicle(){

        $query = Vehicle::get();


        return Datatable::collection($query)
            ->showColumns('id')
            ->addColumn('model',function($model){
                return ucfirst($model->model);
            })
            ->addColumn('make',function($model){
                return ucfirst($model->make);
            })
            ->addColumn('variant',function($model){
                return ucfirst($model->variant);
            })
            ->addColumn('category',function($model){
                return ucfirst($model->category);
            })
            ->addColumn('segment',function($model){
                return ucfirst($model->segment);
            })
            ->addColumn('cc',function($model){
                return ucfirst($model->cc);
            })
            /*->addColumn('status',function($model){


            })*/
            ->addColumn('action', function($model){

                    if($model->status == 0){
                        return "<button class='btn btn-danger btn-xs status' id='status_".$model->id."'>Disable</button> <button type='button' class='btn btn-xs btn-info vehicle' id='vehicle_".$model->id."'>Edit</button>";
                    }
                    else{
                        return "<button class='btn btn-success btn-xs status' id='status_".$model->id."'>Enable</button> <button type='button' class='btn btn-xs btn-info vehicle' id='vehicle_".$model->id."'>Edit</button>";
                    }

            })


            ->searchColumns('id','model','make','variant','category','segment','cc','status')
            ->orderColumns('model')
            ->make();
    }
    public function getStatus(){
        $id = Input::get('id');
        $vehicle = explode('_',$id);
        $check = $vehicle[1];
        $status = Vehicle::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('vehicles')
            ->where('id',$check)
            ->update($checkstatus);
    }
    public function postSave(){
        /*$rules = array(
            'district' => 'required'
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            $data = array(
                'status' => 'fail',
                'message' => 'District Already Exists'
            );
            echo json_encode($data);
        }
        else{*/

            $vehicle = new Vehicle;
            $vehicle->model = Input::get('model');
            $vehicle->make = Input::get('make');
            $vehicle->variant = Input::get('variant');
            $vehicle->category = Input::get('category');
            $vehicle->segment = Input::get('segment');
            $vehicle->cc = Input::get('cc');

            $vehicle->save();
            $data = array(
                'status' => 'success',
                'message' =>'Vehicle Successfully Saved.',

            );
            echo json_encode($data);

       /* }*/
    }
    public function getEdit(){

        Input::get('id');
        $check = explode('_',Input::get('id'));
        $id = $check[1];

        $data = Vehicle::find($id);
        //echo $data;
        $resp = array(
            'record' => $data
        );
        header('content-type: application/json');
        echo json_encode($resp);


    }
    public function postUpdate(){


        $id = Input::get('id');

        $vehicle = array(
            'model' => Input::get('model'),
            'make' => Input::get('make'),
            'variant' => Input::get('variant'),
            'category' => Input::get('category'),
            'segment' => Input::get('segment'),
            'cc' => Input::get('cc')
        );

        DB::table('vehicles')
            ->where('id',$id)
            ->update($vehicle);
        $data = array(
            'status' => 'success',
            'message' =>'vehicle Successfully Updated.',

        );

        echo json_encode($data);

    }
}