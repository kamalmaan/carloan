<?php
class AdminBankController extends BaseController{
    function __construct(){
        $this->beforefilter('admin');
         if(!role_permission('4','component_bank'))
          {
              return Redirect::to('admin/')->send();
          }
    }
    public function getIndex(){


        $res = Datatable::table()
            ->addColumn('Id', 'Bank','Status','Action')
            ->setUrl(URL::to('admin/bank/bank'))
            ->noScript();

      return View::make('admin.bank',array('data' => $res))->with('title','BANK');


//        return View::make('admin/product')->with('data',$table);
    }
    public function getBank()
    {
        $query = Bank::get();

        return Datatable::collection($query)
            ->showColumns('id')
            ->addColumn('bank',function($model){
                return ucfirst($model->bank);
            })
            ->addColumn('status',function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs' id='status_".$model->id."'>Disable</button>";
                }
                else{
                    return "<button class='btn btn-success btn-xs' id='status_".$model->id."'>Enable</button>";
                }

            })
            ->addColumn('action', function($model){
                return "<a href='bank/edit/" . $model->id . "'><button class='btn btn-xs btn-info'>Edit</button></a> <a href='bank/bankbranch/" . $model->id . "'><button class='btn btn-xs btn-info'>Branch</button></a>";
            })

            ->searchColumns('id','bank')
            ->orderColumns('id','bank')
            ->make();

    }
    public function getAdd(){

        return View::Make('admin/bank_add');
    }
    public function postSave(){
        $rules = array(
            'bank'    => 'required',
            /*'bank_form' => 'mimes:jpeg,bmp,png,jpg,doc,pdf,docx'*/
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator); // send back all errors to the form
        }
        else{
            $bank = new Bank;
         /*   if(Input::hasFile('bank_form')){
                $bank_form = str_random(10).Input::file('bank_form')->getClientOriginalName();
                $bank->bank_form = $bank_form;
                Input::file('bank_form')->move('upload/doc',$bank_form);
            }*/
            $bank->bank = Input::get('bank');
            /*$bank->firm_tag = Input::get('firm_tag');
            $bank->single_tag = Input::get('single_tag');*/


            $bank->save();

            return Redirect::to('admin/bank')->with('message','Bank Type successfully saved.');
        }
    }
    public function getEdit($id){

        $bank = Bank::where('id',$id)->first();
        return View::Make('admin/bank_edit')->with('data',$bank);
    }
    public function postUpdate($id){
        $rules = array(
            'bank'    => 'required',
            /*'bank_form' => 'mimes:jpeg,bmp,png,jpg,doc,pdf,docx'*/
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator); // send back all errors to the form
        }
        else
        {
            $bank = array(
                'bank' => Input::get('bank'),
                /*'firm_tag' =>Input::get('firm_tag'),
                'single_tag' =>Input::get('single_tag')*/
            );
           /* if(Input::hasFile('bank_form')) {
                $qry = DB::select(DB::raw("SELECT * FROM banks WHERE id = '$id'"));
                $file = base_path('public/upload/doc/' . $qry[0]->bank_form);
                if (file_exists($file)) {
                    unlink(base_path('public/upload/doc/' . $qry[0]->bank_form));
                }*/

                /*$bank_form = str_random(10) . Input::file('bank_form')->getClientOriginalName();
                $bank['bank_form'] = $bank_form;
                Input::file('bank_form')->move('upload/doc', $bank_form);
            }*/
            DB::table('banks')
                ->where('id',$id)
                ->update($bank);
            return Redirect::to('admin/bank')->with('message','Bank Type successfully updated.');
        }
    }
    public function getStatus(){
        $id = Input::get('id');
        $bank = explode('_',$id);
        $check = $bank[1];
        $status = Bank::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('banks')
            ->where('id',$check)
            ->update($checkstatus);
    }
    public function getDelete($id){

        Bank::where('id',$id)->delete();

        return Redirect::to('admin/bank')->with('message','Bank Type successfully deleted.');

    }
    public function getBankbranch($id){
        $bankbranch = Datatable::table()
            ->addColumn('Branch','BM Name','BM Email','BM Phone','Action')
            ->setUrl(URL::to("admin/bank/bankbranchrecord/$id"))
            ->noScript();

        return View::make('admin/bankbranch')->with('data',$bankbranch)->with('id',$id);

    }
    public function getBankbranchrecord($id){

        $query = BankBranch::where('bank_id',$id)->get();

        return Datatable::collection($query)


            ->addColumn('branch',function($model){
                return ucfirst($model->city()->first()->city);
            })
            ->addColumn('bm_name',function($model){
                return ucfirst($model->contact_person);
            })
            ->addColumn('bm_email',function($model){
                return ucfirst($model->email);
            })
            ->addColumn('bm_phone',function($model){
                return ucfirst($model->phone);
            })
            
            ->addColumn('status',function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs status' id='status_".$model->id."'>Disable</button> <button type='button' class='btn btn-xs btn-info bankbranch' id='".$model->id."_".$model->dealer_id."'>Edit</button>";
                }
                else{
                    return "<button class='btn btn-success btn-xs status' id='status_".$model->id."'>Enable</button> <button type='button' class='btn btn-xs btn-info bankbranch' id='".$model->id."_".$model->dealer_id."'>Edit</button>";
                }

            })

            ->searchColumns('id','branch','conatct_person','phone','email','status')
            ->orderColumns('branch')
            ->make();

    }
    public function getBankbranchstatus(){

        $id = Input::get('id');
        $bankbranch = explode('_',$id);
        $check = $bankbranch[1];
        $status = BankBranch::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('bank_branches')
            ->where('id',$check)
            ->update($checkstatus);

    }
    public function getBankbranchadd(){

        $city = City::get();

        $data = "";


        foreach($city as $cities){
            echo $data = "<option value='".$cities->id."'>".$cities->city."</option>";
        }

        header('content-type: application/json');
        echo json_encode($data);


    }
    public function postBankbranchsave(){


        $bankbranch = new BankBranch;
        $bankbranch->bank_id = Input::get('bank_id');
        $bankbranch->branch = Input::get('branch');
        $bankbranch->contact_person = Input::get('contact_person');
        $bankbranch->phone = Input::get('phone');
        $bankbranch->email = Input::get('email');


        $bankbranch->save();
        $data = array(
            'status' => 'success',
            'message' =>'Branch For Dealer Successfully Saved.',

        );
        echo json_encode($data);

    }
    public function getBankbranchedit(){
        header('content-type: application/json');
        Input::get('id');
        $check = explode('_',Input::get('id'));
        $id = $check[0];
        $arr = array();
        $data = BankBranch::find($id);

        $city = City::get();

        $citydata = "";

        foreach($city as $cities){

            $citydata .= "<option value='".$cities->id."'";
            if($cities->id==$data->branch){

                $citydata .= "selected";

            }
            $citydata .= ">".$cities->city."</option>";
        }

        //$arr[] = $data;
        //$arr[] = $citydata;
        $res = array(
            'record' => $data,
            'city' => $citydata
        );

        echo json_encode($res);


    }
    public function postBankbranchupdate(){


        $id = Input::get('id');
        $bankbranch = array(
            'bank_id' => Input::get('bank_id'),
            'branch' => Input::get('branch'),
            'contact_person' => Input::get('contact_person'),
            'phone' => Input::get('phone'),
            'email' => Input::get('email')
        );

        DB::table('bank_branches')
            ->where('id',$id)
            ->update($bankbranch);
        $data = array(
            'status' => 'success',
            'message' =>'Branch For Dealer Successfully Updated.',

        );

        echo json_encode($data);

    }



    /*public function postTestbank(){


         foreach($_POST as $key =>$value){
              $key_value=$key;
             break;
        }




        $rule=array(
            $key_value=>$_POST['rules']
        );

        print_r($rule);
        exit;
        unset($_POST['rules']);

        $v=Validator::make($_POST,$rule);
        print_r($v);
        if($v->fails()){
            print_r($v);

        }


    }*/


}