<?php
class AdminHomeController extends BaseController{
    function __construct(){

        //parent::__construct();

        $this->beforeFilter('admin');
    }
    public function getIndex(){
        return View::make('admin/index');
    }
    public function getProfile(){
        $data = Auth::admin()->get();
        return View::make('admin/profile',array('data' =>$data));
    }
    public function postProfile(){

        $rules = array(
            'new_password' => 'min:6',
            'confirm_password' =>'required_with:new_password|same:new_password',
            'phone' =>'required',
            'first_name' => 'required',
            'profile_pic' => 'mimes:jpeg,bmp,png,jpg'

        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {

            return Redirect::back()->withInput(Input::except('new_password'))->withErrors($validator);
        }
        else{
            $profile_picture="";
            if(Input::hasFile('profile_pic')){
                echo $profile_picture = str_random(10).Input::file('profile_pic')->getClientOriginalName();
                Input::file('profile_pic')->move('upload/',$profile_picture);
            }
                $user = Auth::admin()->id();

                $data = array(
                    //'password' => Input::get('new_password'),
                    'phone' => Input::get('phone'),
                    'first_name' => Input::get('first_name'),
                    'last_name' => Input::get('last_name'),
                    'profile_pic' => $profile_picture,
                );
                if(Input::get('new_password') != '')
                {
                    $data = array(
                        'password' => Hash::make(Input::get('new_password')),
                    );
                }
                User::where('id','=',$user)->update($data);
                return Redirect::back()->with('message',"You profiled is updated successfully!");
        }

    }
}