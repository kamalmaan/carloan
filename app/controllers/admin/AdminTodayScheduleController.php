<?php
class AdminTodayScheduleController extends BaseController{

    function __construct(){
        $this->beforeFilter('admin');
        if(!module_permission('13'))
        {
            return Redirect::to('admin/')->send();
        }
    }

    public function getIndex(){
        $todayschedule = Datatable::table()
            ->addColumn('Name','Phone','Disccusion Topic','Previous Meeting Details','Meeting Assign','Forward Meeting','Lead From')
            //->addColumn('Meeting Assign')
            ->setUrl(URL::to('admin/todayschedule/schedule'))
            ->noScript();

        $today=date('Y-m-d');
        $nxtday=date('Y-m-d',strtotime("+1 days $today")).' 00-00-00';


        $leads = Meeting::join('users','meetings.lead_agent_id','=','users.id')
            ->select('meetings.*','users.first_name as user_first_name','users.id as user_id')
            ->where('meeting_date','>',$today.' 00-00-00')->where('meeting_date','<',$nxtday)->get();


        $admin = User::get();
        return View::make('admin/todayschedule')->with('data',$todayschedule)->with('admins',$admin)->with('lead',$leads);
    }
    public function getSchedule(){

       $today=date('Y-m-d');
       $nxtday=date('Y-m-d',strtotime("+1 days $today")).' 00-00-00';


      $query = Meeting::join('users','meetings.lead_agent_id','=','users.id')

           ->select('meetings.*','users.first_name as user_first_name','users.id as user_id')
           ->where('meeting_date','>',$today.' 00-00-00')->where('meeting_date','<',$nxtday)->get();




        return Datatable::collection($query)
            ->addColumn('name',function($model){
                return ucfirst($model->leads()->first()->lead_first_name.' '.
                    $model->leads()->first()->lead_middle_name.' '.
                    $model->leads()->first()->lead_last_name);
            })
            ->addColumn('phone',function($model){
                return ucfirst($model->leads()->first()->phone);
            })
            ->addColumn('current_meeting_topic',function($model){
                return ucfirst($model->meeting_topic);
            })
            ->addColumn('prevous_meeting',function($model){
                $today=date('Y-m-d');
                $nxtday=date('Y-m-d',strtotime("+1 days $today")).' 00-00-00';

                $query1=Meeting::where('lead_id',$model->lead_id)
                ->where('meeting_date','<',$today.' 00-00-00')
                ->orderBy('id','desc')
                ->first();

                if(!empty($query1)){
                   return "<strong class='text text-info'>Topic: </strong> ".ucfirst($query1->meeting_topic)."<br>
                            <strong class='text text-info'>Discussion: </strong> ".ucfirst($query1->meeting_discussion)."<br>
                            <strong class='text text-info'>Date: </strong> ".ucfirst($query1->meeting_date)."<br>
                            <strong class='text text-info'>Meeting Date: </strong> ".ucfirst($query1->meeting_medium);
                }else{
                return "First Meeting";
                }
            })
            ->addColumn('assign_lead',function($model){
                return $model->user()->first()->first_name." ".$model->user()->first()->last_name;
            })
            ->addColumn('forward_lead',function($model){

                $admin = User::get();
                if(!role_permission('13','todayschedule_forwardmeeting')){
                    $options = "<select class='forward_agent' disabled=''><option value=''>Select Name</option>";
                }
                elseif(role_permission('13','todayschedule_forwardmeeting')){
                    $options = "<select class='forward_agent' id='".$model->id."'><option value=''>Select Name</option>";
                }

                foreach($admin as $admins){
                    if($admins->id == $model->lead_agent_id){
                        $opt = "selected";
                    }
                    else{
                        $opt = "";
                    }
                    $options .= "<option value='".$admins->id."' $opt>".$admins->first_name." ".$admins->last_name."</option>";
                }
                $options .= "</select>";
                return $options;
                //return $model->user()->first()->first_name." ".$model->user()->first()->last_name;


            })
            ->addColumn('lead_from',function($model){
                $query8=$model->leads()->first()->lead_from;
                $query9=User::where('id',$query8)->first();
                   return ucfirst($query9->first_name.' '.$query9->last_name);
            })

        ->searchColumns('assign_lead','name','meeting_medium')
        ->orderColumns('lead_agent_id')
        ->make();

    }
    public function getAgent(){
        $assigned = Input::get("assigned");
        $forward = Input::get("forward");

        $update_agent = array(
            'lead_agent_id' => $forward
        );
        if(Meeting::where("lead_agent_id",'=',$assigned)->count() == 0){
            $data = array(
                'status' => 'error',
                'message' => 'No lead is assigned to agent !'
            );
            echo json_encode($data);
        }
        else{
            Meeting::where("lead_agent_id",'=',$assigned)->update($update_agent);
            $data = array(
                'status' => 'success',
                'message' => 'Lead Successfully Forward !'
            );
            echo json_encode($data);
        }

    }
    public function getPrint($lead_agent_id){

        $today=date('Y-m-d');
        $nxtday=date('Y-m-d',strtotime("+1 days $today")).' 00-00-00';

	$check  = Meeting::where('lead_agent_id',$lead_agent_id)
            ->where('meeting_date','>',$today.' 00-00-00')
            ->where('meeting_date','<',$nxtday)
            ->count();
	if($check > 0){
        $today_meeting = Meeting::where('lead_agent_id',$lead_agent_id)
            ->where('meeting_date','>',$today.' 00-00-00')
            ->where('meeting_date','<',$nxtday)
            ->get();
        $todays = array();
        foreach($today_meeting as $today_meetings){

            if($today_meetings->leads()->first()->type == "person"){

                $customer_name= ucwords($today_meetings->leads()->first()->lead_first_name ." ". $today_meetings->leads()->first()->lead_last_name);
            }
            else{
                $customer_name = ucwords($today_meetings->leads()->first()->company_name ." ". $today_meetings->leads()->first()->constitution);
            }
            if(!empty($today_meetings->leads()->first()->product_id)){

                $product = $today_meetings->leads()->first()->product()->first()->product;
            }
            else{
                $product = "";
            }
            if(!empty($today_meetings->leads()->first()->enquiry_type)){
                $enquiry_type = $today_meetings->leads()->first()->enquiry_type;
            }
            else{
                $enquiry_type = "";
            }
            if(!empty($today_meetings->leads()->first()->lead_from)){
                $lead_from = ucwords($today_meetings->leads()->first()->lead_from()->first()->first_name ." ".$today_meetings->leads()->first()->lead_from()->first()->last_name);
            }
            else{
                $lead_from = "";
            }

            $current[] = array(
                'customer_name' => $customer_name,
                'phone' => $today_meetings->leads()->first()->phone,
                'meeting_topic' => $today_meetings->meeting_topic,
                'meeting_medium' => $today_meetings->meeting_medium,
                'product' => $product,
                'enquiry_type' => $enquiry_type,
                'lead_from' => $lead_from,
                'lead_assigned' => ucwords($today_meetings->user()->first()->first_name." ". $today_meetings->user()->first()->last_name),
                'lead_arrival' => $today_meetings->leads()->first()->created_at,
                'lead_id' => $today_meetings->lead_id
            );


        }

        foreach($today_meeting as $previous_meetings){
            $lead_id = $previous_meetings->lead_id;
            $previous[] = Meeting::where('lead_id',$lead_id)->where('meeting_date','<',$today.' 00-00-00')->get();

        }

        //echo "<pre>";print_r($previous);exit;
        for($i = 0; $i<= count($previous)-1;$i++)
        {
            if(!empty($previous[$i])){
                $previous_meeting_topic = $previous[$i];
            }
            else{
                $previous_meeting_topic =  "First Meeting";
            }
            $previouss[] = array(
                'previous_meeting'=> $previous_meeting_topic
            );



        }

        for($j= 0;$j<= count($current)-1; $j++){
            $today_schedule[]  = array_merge($current[$j],$previouss[$j]);
        }
	}

	else{
		$today_schedule = 0;
	}
  //echo "<pre>";print_r($today_schedule);exit;
        $daily_task = DailyTask::where('assign_to',$lead_agent_id)

            ->get();

        return View::make('admin/print')->with(array('today_schedules'=>$today_schedule,'daily_tasks'=>$daily_task));
    }
    public function postAgent(){
        $id = Input::get('id');
        $lead_agent_id = Input::get('lead_agent_id');
        $meeting = Meeting::find($id);
        $meeting->lead_agent_id = $lead_agent_id;
        $meeting->update();
        $data = array(
            'status' => 'success',
            'message' => 'Lead Successfully Forwarded !'
        );
        echo json_encode($data);
    }

}
