<?php
class AdminLeadTypeController extends BaseController{
    function __construct(){
        $this->beforefilter('admin');
        if(!role_permission('4','component_leadtype'))
        {
            return Redirect::to('admin/')->send();
        }
    }
    public function getIndex(){

        $lead_type = Datatable::table()
            ->addColumn('Id', 'Lead Type','Status','Delete')
            ->setUrl(URL::to('admin/lead_type/leadtype'))
            ->noScript();
       return View::make('admin/lead_type')->with('data',$lead_type);
    }
    public function getLeadtype(){

        $query = LeadType::get();


        return Datatable::collection($query)
            ->showColumns('id')
            ->addColumn('lead_type',function($model){
                return ucfirst($model->lead_type);
            })
            ->addColumn('status',function($model){
                if($model->status == 0){
                    return "<button class='btn btn-danger btn-xs' id='status_".$model->id."'>Disable</button>";
                }
                else{
                    return "<button class='btn btn-success btn-xs' id='status_".$model->id."'>Enable</button>";
                }

            })
            ->addColumn('action', function($model){
                return "<div class='btn-group-xs'><a href='lead_type/edit/" . $model->id . "'><button type='button' class='btn btn-xs btn-info'>Edit</button></a></div>";
            })
            ->searchColumns('id','lead_type')
            ->orderColumns('id','lead_type')
            ->make();

    }
    public function getAdd(){

        return View::Make('admin/lead_type_add');
    }
    public function postSave(){
        $rules = array(
            'lead_type' => 'required|unique:lead_types'
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return Redirect::back()
                ->withErrors($validator);
        }
        else{
            $lead_type = new LeadType;
            $lead_type->lead_type = Input::get('lead_type');

            $lead_type->save();

            return Redirect::to('admin/lead_type')->with('message','Lead Type successfully saved.');
        }
    }
    public function getEdit($id){

        $lead_type = LeadType::where('id',$id)->first();
        return View::Make('admin/lead_type_edit')->with('data',$lead_type);
    }
    public function postUpdate($id){

        $rules = array(
            'lead_type'    => 'required|unique:lead_types'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator); // send back all errors to the form
        }
        else
        {
            $lead_type = array(
                'lead_type' => Input::get('lead_type')
            );

            DB::table('lead_types')
                ->where('id',$id)
                ->update($lead_type);

            return Redirect::to('admin/lead_type')->with('message','Lead Type successfully updated.');
        }
    }
    public function getStatus(){

        $id = Input::get('id');
        $lead_type = explode('_',$id);
        $check = $lead_type[1];
        $status = LeadType::where('id',$check)->first();

        if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('lead_types')
            ->where('id',$check)
            ->update($checkstatus);
    }
    public function getDelete($id){

        LeadType::where('id',$id)->delete();

        return Redirect::to('admin/lead_type')->with('message','Lead Type successfully deleted.');

    }
}