<?php

/**
 * Created by PhpStorm.
 * User: john
 * Date: 29/5/15
 * Time: 3:02 PM
 */
class AdminRoleController extends BaseController
{
    /**
     * Constructor
     * checking admin has logged in or not
     */

    function __construct()
    {
        $this->beforeFilter('admin');
    }

    public function getIndex()
    {

        $roles = IsSuperuser::orderBy('id','DESC')->get();
        $res = Datatable::table()
            ->addColumn('Id', 'Module Name','No Access','View Only','Full Access','Additional')
            ->setUrl(URL::to("admin/role/module/0"))
            ->noScript();

        return View::make('admin.role',array('data' => $res))->with('roles',$roles);
    }

    /* Set Role and Permission */
    public function postSetpermission()
    {
        $data = Input::all();
        $role = $data['role'];

        unset($data['role']);

        foreach ($data as $row) {
            if ($row != 0) {

                $role_perm = new RolePermission();
                $role_perm->role_id = $role;
                $role_perm->permission_id = $row;
                $role_perm->save();

            }
        }
    }

    /* Update Role and Permission */
    public function getSetpermission() {

        $id=Input::get('id');
        $data=explode('_',$id);

        /*user id*/
        $role_id=$data[2];
        $perm_id=$data[4];

        $count=RolePermission::where('role_id',$role_id)->where('permission_id',$perm_id)->count();
        if($count>0){
            RolePermission::where('role_id',$role_id)->where('permission_id',$perm_id)->delete();
        } else {
            RolePermission::insert(array(
                'permission_id'=>$perm_id,
                'role_id'=>$role_id
            ));
        }
    }
    public function postModules(){
        $roles = IsSuperuser::orderBy('id','DESC')->get();
        $role_id=Input::get('role');
        Session::put('role_id', $role_id);
       /* $lead=ModuleRoles::where('module_id','2')->where('role_id',$role_id)->first();
        $leads=explode(",",$lead->sub_permissions);
        echo $leads[0];
        exit;*/
        $res = Datatable::table($role_id)
            ->addColumn('Id', 'Module Name','No Access','View Only','Full Access','Additional')
            ->setUrl(URL::to("admin/role/module/$role_id"))
            ->noScript();

        return View::make('admin.role',array('data' => $res))->with('roles',$roles);
    }
    /* Get Modules */
    public function getModule($role_id) {

        $query=ModuleRoles::where('role_id',$role_id)->get();

       return Datatable::collection($query)
            ->showColumns('module_role_id')
            ->addColumn('module_id',function($model){
                return $model->module()->first()->module_name;
            })
            ->addColumn('No Access',function($model){
                if($model->permissions == 0){
                    return '<input type="radio" value="0" class="permissions" checked name="permissions_'.$model->module_role_id.'"  id="noaccess_'.$model->module_role_id.'" />';
                }
                else{
                    return '<input type="radio" value="0" class="permissions" name="permissions_'.$model->module_role_id.'" id="noaccess_'.$model->module_role_id.'" />';
                }
            })
            ->addColumn('View Only',function($model){
                if($model->permissions == 1){
                    return '<input type="radio" value="1" class="permissions" checked name="permissions_'.$model->module_role_id.'" id="viewonly_'.$model->module_role_id.'" />';
                }
                else{
                    return '<input type="radio" value="1"  class="permissions" name="permissions_'.$model->module_role_id.'" id="viewonly_'.$model->module_role_id.'"/>';
                }

            })
            ->addColumn('Full Access',function($model){
                if($model->permissions == 2){
                    return '<input type="radio" value="2" class="permissions" checked name="permissions_'.$model->module_role_id.'" id="fullaccess_'.$model->module_role_id.'" />';
                }
                else{
                    return '<input type="radio" value="2" class="permissions" name="permissions_'.$model->module_role_id.'" id="fullaccess_'.$model->module_role_id.'"/>';
                }
            })
            ->addColumn('sub_permissions', function($model){

                if($model->module_id==2){
                    //lead
                    $lead=ModuleRoles::where('module_id','2')->where('role_id',$model->role_id)->first();
                    $leads=explode(",",$lead->sub_permissions);
                    $lead1="";$lead2="";$lead3="";$lead4="";$lead5="";$lead6="";$lead7="";$lead8="";
                    for($i=0;$i<count($leads);$i++){
                        if($leads[$i]=="lead_add"){ $lead1="checked"; }
                        else if($leads[$i]=="lead_profile"){ $lead2="checked"; }
                        else if($leads[$i]=="lead_assign") {$lead3="checked";}
                        else if($leads[$i]=="lead_forward") {$lead4="checked";}
                        else if($leads[$i]=="lead_approve") {$lead5="checked";}
                        else if($leads[$i]=="lead_coapplicant") {$lead6="checked";}
                        else if($leads[$i]=="lead_fiinitiation") {$lead7="checked";}
                        else if($leads[$i]=="lead_decline") {$lead8="checked";}
                    }
                    return '<p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-lead_add"  name="lead_'.$model->module_role_id.'[]" value="lead_add" '.$lead1.'/>Add Lead</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-lead_profile"  name="lead_'.$model->module_role_id.'[]" value="lead_profile" '.$lead2.'/> Profile</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-lead_assign"  name="lead_'.$model->module_role_id.'[]" value="lead_assign" '.$lead3.'/> Assign Lead</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-lead_forward"  name="lead_'.$model->module_role_id.'[]" value="lead_forward" '.$lead4.'/> Forward Lead</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-lead_approve"  name="lead_'.$model->module_role_id.'[]" value="lead_approve" '.$lead5.'/> Approve</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-lead_coapplicant"  name="lead_'.$model->module_role_id.'[]" value="lead_coapplicant" '.$lead6.'/> Co-Applicant</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-lead_fiinitiation"  name="lead_'.$model->module_role_id.'[]" value="lead_fiinitiation" '.$lead7.'/> FI Initiation</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-lead_decline"  name="lead_'.$model->module_role_id.'[]" value="lead_decline"  '.$lead8.'/> Decline</p>';

                }else if($model->module_id==3){
                    //decline lead
                    $declinelead=ModuleRoles::where('module_id','3')->where('role_id',$model->role_id)->first();
                    $declineleads=explode(",",$declinelead->sub_permissions);
                    $declinelead1="";$declinelead2="";
                    for($i=0;$i<count($declineleads);$i++){
                        if($declineleads[$i]=="declinelead_delete"){$declinelead1="checked";}
                        elseif($declineleads[$i]=="declinelead_recycle"){$declinelead2="checked";}
                    }

                    return '<p><input type="checkbox" name="lead_delete'.$model->module_role_id.'[]" class="sub_permissions" id="'.$model->module_role_id.'-declinelead_delete"  value="declinelead_delete" '.$declinelead1.'/>Delete</p>
                            <p><input type="checkbox" name="lead_delete'.$model->module_role_id.'[]" class="sub_permissions" id="'.$model->module_role_id.'-declinelead_recycle"  value="declinelead_recycle" '.$declinelead2.'/>Recycle</p>';
                }else if($model->module_id==4){
                    //components of lead

                    $component=ModuleRoles::where('module_id','4')->where('role_id',$model->role_id)->first();
                    $components=explode(",",$component->sub_permissions);
                    $component1="";$component2="";$component3="";$component4="";$component5="";$component6="";
                    for($i=0;$i<count($components);$i++){
                        if($components[$i]=="component_leadtype"){ $component1="checked"; }
                        else if($components[$i]=="component_bank"){ $component2="checked"; }
                        else if($components[$i]=="component_state") {$component3="checked";}
                        else if($components[$i]=="component_vehicle") {$component4="checked";}
                        else if($components[$i]=="component_product") {$component5="checked";}
                        else if($components[$i]=="component_dealer") {$component6="checked";}
                    }
                    return '<p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-component_leadtype"  name="component[]" value="component_leadtype" '.$component1.'/> Lead Type</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-component_bank"  name="component[]" value="component_bank" '.$component2.'/> Bank</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-component_state"  name="component[]" value="component_state" '.$component3.'/> State</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-component_vehicle"  name="component[]" value="component_vehicle" '.$component4.'/> Vehicle</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-component_product"  name="component[]" value="component_product" '.$component5.'/> Product</p>
                            <p><input type="checkbox" class="sub_permissions" id="'.$model->module_role_id.'-component_dealer"  name="component[]" value="component_dealer" '.$component6.'/> Dealer</p>';

                }else if($model->module_id==5){
                    //insurance
                    $insurance=ModuleRoles::where('module_id','5')->where('role_id',$model->role_id)->first();
                    $insurances=explode(",",$insurance->sub_permissions);
                    $insurance1="";$insurance2="";
                    for($i=0;$i<count($insurances);$i++){
                        if($insurances[$i]=="insurance_insurancecompany"){ $insurance1="checked"; }
                        else if($insurances[$i]=="insurance_insurance"){ $insurance2="checked"; }
                    }
                    return '<p><input type="checkbox" class="sub_permissions" name="insurance[]" id="'.$model->module_role_id.'-insurance_insurancecompany"  value="insurance_insurancecompany" '.$insurance1.'/> Insurance Company</p>
                            <p><input type="checkbox" class="sub_permissions" name="insurance[]" id="'.$model->module_role_id.'-insurance_insurance"  value="insurance_insurance" '.$insurance2.'/> Insurance</p>';
                }else if($model->module_id==6){
                    //FI Status
                    $fistatus=ModuleRoles::where('module_id','6')->where('role_id',$model->role_id)->first();
                    $fistatuss=explode(",",$fistatus->sub_permissions);
                    $fistatus1="";$fistatus2="";$fistatus3="";$fistatus4="";$fistatus5="";$fistatus6="";$fistatus7="";
                    for($i=0;$i<count($fistatuss);$i++){
                        if($fistatuss[$i]=="fistatus_downloadcustomize"){ $fistatus1="checked"; }
                        else if($fistatuss[$i]=="fistatus_uploadsendfi"){ $fistatus2="checked"; }
                        else if($fistatuss[$i]=="fistatus_sendmaildirectly"){ $fistatus3="checked"; }
                        else if($fistatuss[$i]=="fistatus_applyagain"){ $fistatus4="checked"; }
                        else if($fistatuss[$i]=="fistatus_filerejected"){ $fistatus5="checked"; }
                        else if($fistatuss[$i]=="fistatus_accept"){ $fistatus6="checked"; }
                        else if($fistatuss[$i]=="fistatus_reject"){ $fistatus7="checked"; }
                    }

                    return '<p><input type="checkbox" class="sub_permissions" name="fistatus" id="'.$model->module_role_id.'-fistatus_downloadcustomize"  value="fistatus_downloadcustomize" '.$fistatus1.'/> Download & Customize</p>
                            <p><input type="checkbox" class="sub_permissions" name="fistatus" id="'.$model->module_role_id.'-fistatus_uploadsendfi"  value="fistatus_uploadsendfi" '.$fistatus2.'/> Upload & Send FI</p>
                            <p><input type="checkbox" class="sub_permissions" name="fistatus" id="'.$model->module_role_id.'-fistatus_sendmaildirectly"  value="fistatus_sendmaildirectly" '.$fistatus3.'/> Send Mail Directly</p>
                            <p><input type="checkbox" class="sub_permissions" name="fistatus" id="'.$model->module_role_id.'-fistatus_applyagain"  value="fistatus_applyagain" '.$fistatus4.'/> Apply Again</p>
                            <p><input type="checkbox" class="sub_permissions" name="fistatus" id="'.$model->module_role_id.'-fistatus_filerejected"  value="fistatus_filerejected" '.$fistatus5.'/> File Rejected </p>
                            <p><input type="checkbox" class="sub_permissions" name="fistatus" id="'.$model->module_role_id.'-fistatus_accept"  value="fistatus_accept" '.$fistatus6.'/> Accept</p>
                            <p><input type="checkbox" class="sub_permissions" name="fistatus" id="'.$model->module_role_id.'-fistatus_reject"  value="fistatus_reject" '.$fistatus7.'/> Reject</p>';

                }else if($model->module_id==7){
                    //under writing
                    $underwriting=ModuleRoles::where('module_id','7')->where('role_id',$model->role_id)->first();
                    $underwritings=explode(",",$underwriting->sub_permissions);
                    $underwriting1="";$underwriting2="";
                    for($i=0;$i<count($underwritings);$i++){

                    if($underwritings[$i]=="underwriting_declineunderwriting"){ $underwriting1="checked"; }
                    elseif($underwritings[$i]=="underwriting_losnumber"){ $underwriting2="checked"; }

                    }

                    return '<p><input type="checkbox" class="sub_permissions" name="underwriting" id="'.$model->module_role_id.'-underwriting_declineunderwriting"  value="underwriting_declineunderwriting" '.$underwriting1.'/> Decline Under Writing</p>
                            <p><input type="checkbox" class="sub_permissions" name="underwriting" id="'.$model->module_role_id.'-underwriting_losnumber"  value="underwriting_losnumber" '.$underwriting2.'/>Under Writing LOS Number</p>';
                }else if($model->module_id==8){
                    //under writing approved
                    $underwritingapproved=ModuleRoles::where('module_id','8')->where('role_id',$model->role_id)->first();
                    $underwritingapproveds=explode(",",$underwritingapproved->sub_permissions);
                    $underwritingapproved1="";$underwritingapproved2="";$underwritingapproved3="";
                    for($i=0;$i<count($underwritingapproveds);$i++){
                        if($underwritingapproveds[$i]=="underwritingapproved_profile"){ $underwritingapproved1="checked"; }
                        else if($underwritingapproveds[$i]=="underwritingapproved_decline"){ $underwritingapproved2="checked"; }
                        else if($underwritingapproveds[$i]=="underwritingapproved_status"){ $underwritingapproved3="checked"; }
                    }
                    return '<p><input type="checkbox" class="sub_permissions" name="underwritingapproved" id="'.$model->module_role_id.'-underwritingapproved_profile"  value="underwritingapproved_profile" '.$underwritingapproved1.'/> Profile</p>
                            <p><input type="checkbox" class="sub_permissions" name="underwritingapproved" id="'.$model->module_role_id.'-underwritingapproved_decline"  value="underwritingapproved_decline" '.$underwritingapproved2.'/> Decline</p>
                            <p><input type="checkbox" class="sub_permissions" name="underwritingapproved" id="'.$model->module_role_id.'-underwritingapproved_status"  value="underwritingapproved_status" '.$underwritingapproved3.'/> Disburse</p>';
                }else if($model->module_id==9){
                    //under approved decline
                    $underapproveddecline=ModuleRoles::where('module_id','9')->where('role_id',$model->role_id)->first();
                    $underapproveddeclines=explode(",",$underapproveddecline->sub_permissions);
                    $underapproveddecline1="";
                    for($i=0;$i<count($underapproveddeclines);$i++){
                        if($underapproveddeclines[$i]=="underapproveddecline_recycle"){ $underapproveddecline1="checked"; }
                    }
                    return '<p><input type="checkbox" class="sub_permissions" name="underapproveddecline[]" id="'.$model->module_role_id.'-underapproveddecline_recycle"  value="underapproveddecline_recycle" '.$underapproveddecline1.'/> Decline</p>';
                }else if($model->module_id==10){
                    //Temporary customer
                    $temporarycustomer=ModuleRoles::where('module_id','10')->where('role_id',$model->role_id)->first();
                    $temporarycustomers=explode(",",$temporarycustomer->sub_permissions);
                    $temporarycustomer1="";$temporarycustomer2="";$temporarycustomer3="";$temporarycustomer4="";
                    for($i=0;$i<count($temporarycustomers);$i++){
                        if($temporarycustomers[$i]=="temporarycustomer_profile"){ $temporarycustomer1="checked"; }
                        else if($temporarycustomers[$i]=="temporarycustomer_decline"){ $temporarycustomer2="checked"; }
                        else if($temporarycustomers[$i]=="temporarycustomer_update"){ $temporarycustomer3="checked"; }
                        else if($temporarycustomers[$i]=="temporarycustomer_coapplicant"){ $temporarycustomer4="checked"; }
                    }
                    return '<p><input type="checkbox" class="sub_permissions" name="temporarycustomer[]" id="'.$model->module_role_id.'-temporarycustomer_profile"  value="temporarycustomer_profile"  '.$temporarycustomer1.'/> Profile</p>
                            <p><input type="checkbox" class="sub_permissions" name="temporarycustomer[]" id="'.$model->module_role_id.'-temporarycustomer_decline"  value="temporarycustomer_decline" '.$temporarycustomer2.'/> Decline</p>
                            <p><input type="checkbox" class="sub_permissions" name="temporarycustomer[]" id="'.$model->module_role_id.'-temporarycustomer_update"  value="temporarycustomer_update"  '.$temporarycustomer3.'/> Update info</p>
                            <p><input type="checkbox" class="sub_permissions" name="temporarycustomer[]" id="'.$model->module_role_id.'-temporarycustomer_coapplicant"  value="temporarycustomer_coapplicant" '.$temporarycustomer4.'/> Co Applicant</p>';
                }else if($model->module_id==11){
                    //Temporary decline customer
                    $temporarydeclinecustomer=ModuleRoles::where('module_id','11')->where('role_id',$model->role_id)->first();
                    $temporarydeclinecustomers=explode(",",$temporarydeclinecustomer->sub_permissions);
                    $temporarydeclinecustomer1="";
                    for($i=0;$i<count($temporarydeclinecustomers);$i++){
                        if($temporarydeclinecustomers[$i]=="temporarydeclinecustomer_recycle"){ $temporarydeclinecustomer1="checked"; }
                    }
                    return '<p><input type="checkbox" class="sub_permissions" name="temporarydeclinecustomer[]" id="'.$model->module_role_id.'-temporarydeclinecustomer_recycle"  value="temporarydeclinecustomer_recycle" '.$temporarydeclinecustomer1.' /> Recycle</p>
                            ';
                }else if($model->module_id==12){
                    //permanent customer
                    $permanentcustomer=ModuleRoles::where('module_id','12')->where('role_id',$model->role_id)->first();
                    $permanentcustomers=explode(",",$permanentcustomer->sub_permissions);
                    $permanentcustomer1="";$permanentcustomer2=""; $permanentcustomer3=""; $permanentcustomer4="";$permanentcustomer5="";
                    for($i=0;$i<count($permanentcustomers);$i++){
                        if($permanentcustomers[$i]=="permanentcustomer_profile"){ $permanentcustomer1="checked"; }
                        else if($permanentcustomers[$i]=="permanentcustomer_rc"){ $permanentcustomer2="checked"; }
                        else if($permanentcustomers[$i]=="permanentcustomer_calculation"){ $permanentcustomer3="checked"; }
                        else if($permanentcustomers[$i]=="permanentcustomer_update"){ $permanentcustomer4="checked"; }
                        else if($permanentcustomers[$i]=="permanentcustomer_coapplicant"){ $permanentcustomer5="checked"; }
                    }
                    return '<p><input type="checkbox" class="sub_permissions" name="permanentcustomer[]" id="'.$model->module_role_id.'-permanentcustomer_profile"  value="permanentcustomer_profile" '.$permanentcustomer1.'/> Profile</p>
                            <p><input type="checkbox" class="sub_permissions" name="permanentcustomer[]" id="'.$model->module_role_id.'-permanentcustomer_rc"  value="permanentcustomer_rc" '.$permanentcustomer2.'/> RC</p>
                            <p><input type="checkbox" class="sub_permissions" name="permanentcustomer[]" id="'.$model->module_role_id.'-permanentcustomer_calculation"  value="permanentcustomer_calculation"'.$permanentcustomer3.' /> Calculation</p>
                            <p><input type="checkbox" class="sub_permissions" name="permanentcustomer[]" id="'.$model->module_role_id.'-permanentcustomer_update"  value="permanentcustomer_update" '.$permanentcustomer4.' /> Update Information</p>
                            <p><input type="checkbox" class="sub_permissions" name="permanentcustomer[]" id="'.$model->module_role_id.'-permanentcustomer_coapplicant"  value="permanentcustomer_coapplicant" '.$permanentcustomer5.' /> Co applicant</p>';

                }else if($model->module_id==13){
                    //today schedule
                    $todayschedule=ModuleRoles::where('module_id','13')->where('role_id',$model->role_id)->first();
                    $todayschedules=explode(",",$todayschedule->sub_permissions);
                    $todayschedule1="";$todayschedule2="";$todayschedule3="";
                    for($i=0;$i<count($todayschedules);$i++){
                        if($todayschedules[$i]=="todayschedule_assignlead"){ $todayschedule1="checked"; }
                        else if($todayschedules[$i]=="todayschedule_print"){ $todayschedule2="checked"; }
                        else if($todayschedules[$i]=="todayschedule_forwardmeeting"){ $todayschedule3="checked"; }
                    }
                    return '<p><input type="checkbox" class="sub_permissions" name="todayschedule[]" id="'.$model->module_role_id.'-todayschedule_assignlead"  value="todayschedule_assignlead" '.$todayschedule1.'/> Global Assign Lead</p>
                            <p><input type="checkbox" class="sub_permissions" name="todayschedule[]" id="'.$model->module_role_id.'-todayschedule_print"  value="todayschedule_print" '.$todayschedule2.'/> Print</p>
                            <p><input type="checkbox" class="sub_permissions" name="todayschedule[]" id="'.$model->module_role_id.'-todayschedule_forwardmeeting"  value="todayschedule_forwardmeeting" '.$todayschedule3.'/> Forward Meeting</p>';
                }else if($model->module_id==14){
                    //Daily task
                    $dailytask=ModuleRoles::where('module_id','14')->where('role_id',$model->role_id)->first();
                    $dailytasks=explode(",",$dailytask->sub_permissions);
                    $dailytask1="";$dailytask2="";$dailytask3="";$dailytask4="";$dailytask5="";
                    for($i=0;$i<count($dailytasks);$i++){
                        if($dailytasks[$i]=="dailytask_assignlead"){ $dailytask1="checked"; }
                        else if($dailytasks[$i]=="dailytask_print"){ $dailytask2="checked"; }
                        else if($dailytasks[$i]=="dailytask_status"){ $dailytask3="checked"; }
                        else if($dailytasks[$i]=="dailytask_edit"){ $dailytask4="checked"; }
                        else if($dailytasks[$i]=="dailytask_enabledisable"){ $dailytask5="checked"; }
                    }
                    return '<p><input type="checkbox" class="sub_permissions" name="dailytask[]" id="'.$model->module_role_id.'-dailytask_assignlead"  value="dailytask_assignlead" '.$dailytask1.'/> Global Assign Lead</p>
                            <p><input type="checkbox" class="sub_permissions" name="dailytask[]" id="'.$model->module_role_id.'-dailytask_print"  value="dailytask_print" '.$dailytask2.'/> Print</p>
                            <p><input type="checkbox" class="sub_permissions" name="dailytask[]" id="'.$model->module_role_id.'-dailytask_status"  value="dailytask_status" '.$dailytask3.'/> Status(Pending/Completed)</p>
                            <p><input type="checkbox" class="sub_permissions" name="dailytask[]" id="'.$model->module_role_id.'-dailytask_edit"  value="dailytask_edit" '.$dailytask4.'/> Edit</p>
                            <p><input type="checkbox" class="sub_permissions" name="dailytask[]" id="'.$model->module_role_id.'-dailytask_enabledisable"  value="dailytask_enabledisable" '.$dailytask5.'/> Enable/Disable</p>';
                }
                //return '<p><input type="checkbox" name="" value=""/></p>';
                return '-';
            })

            ->searchColumns('module_role_id')
            ->orderColumns('module_role_id')
            ->make();

    }
    function postUpdatepermission(){
        $module_role_id=Input::get('module_role_id');
        $role_id=Input::get('role_id');
        $permissions=Input::get('permissions');
        DB::table('module_roles')
            ->where('module_role_id',$module_role_id)
            ->where('role_id',$role_id)
            ->update(array('permissions' => $permissions));
    }
    function postUpdatesubpermission(){

        $module_role_id=Input::get('module_role_id');
        $role_id=Input::get('role_id');
        $permissions=Input::get('permissions');
        $module_role_id.$role_id.$permissions;
        $sub_permission=ModuleRoles::where('module_role_id',$module_role_id)->where('role_id',$role_id)->first();
        $sub_permissions=explode(",",$sub_permission->sub_permissions);
       /* echo "<pre>";
        print_r($sub_permissions);*/
        $arr=array();
        $st=0;
        for($i=0;$i<count($sub_permissions);$i++){
            if($sub_permissions[$i]==$permissions){
                $st=1;
                $sub_permissions[$i]="";
               // array_push($arr,$permissions);
              //  unset($sub_permissions[$i]);
            }else if($sub_permissions[$i]!=""){
            array_push($arr,$sub_permissions[$i]);
            }
        }
        if($st==0){
            //array_push($sub_permissions,$permissions);
            array_push($arr,$permissions);
        }
        /*echo "<pre>";
        print_r($arr);*/
        $sp="";
        for($i=0;$i<count($arr);$i++){
            $sp.=$arr[$i].',';
        }
        /*for($i=0;$i<count($sub_permissions);$i++){
            $sp.=$sub_permissions[$i].',';
        }*/

        DB::table('module_roles')
            ->where('module_role_id',$module_role_id)
            ->where('role_id',$role_id)
            ->update(array('sub_permissions' => $sp));
    }
}