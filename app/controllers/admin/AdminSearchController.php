<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 12/6/15
 * Time: 12:44 PM
 */
class AdminSearchController extends BaseController{
    function __construct(){
        $this->beforeFilter('admin');
       /*if(!module_permission('16'))
        {
            return Redirect::to('admin/')->send();
        }*/

    }
    public function getIndex(){

      $lead = Datatable::table()
            ->addColumn('ID','Name','Mobile','Product','Assign Lead','Forward Lead','Manage Lead')
            ->setUrl(URL::to('admin/search/fetchrecord'))
            ->noScript();

      return View::make('admin/search')->with(array('data'=>$lead));

      // return View::make('admin/search');
    }
    public function getFetchrecord()
    {
        $datam=SearchFilter::select('lead_id')->get();
                  
        $data_nm=array();
        foreach($datam as $data){          
            $data_nm[]=$data->lead_id;          
        }
        
      $query = Lead::whereIn('id',$data_nm)->get();
       
        return Datatable::collection($query)
            
            ->addColumn('id',function($model){            
                return $model->customer_id;
            })

            ->addColumn('name', function($model){
                if(!empty($model->company_name)){
                    $user = ucfirst($model->company_name) ."(".$model->constitution." - ".$model->company_category.")";
                }
                else{
                    $user = ucfirst($model->lead_first_name)." ".ucfirst($model->lead_last_name);
                }               
                $label = "";
                if($model->fi_initiation == "" && $model->approve_lead == "0"){
                    $label = "<sup class='stages_up'><span class='badge badges_up label-success'>New</span></sup>";

                }
                if($model->fi_initiation == "1" && $model->approve_lead == "1"){
                    $label = "<sup class='stages_up'><span class='badge badges_up label-warning'>FI</span></sup>";
                }
                if(!role_permission('2','lead_profile')){
                    return "<span><button disabled='' class='btn btn-info btn-sm'>".$user." </button>$label</span>";
                }elseif(role_permission('2','lead_profile')){
                return "<span><a href='lead/leadtimeline/".$model->id."'><button class='btn btn-info btn-sm'>".$user." </button></a>$label</span>";
                }
            })

            ->addColumn('mobile', function($model){
              if(!empty($model->phone)){
                  return ucfirst($model->phone);
                  }
            })

            ->addColumn('product', function($model){
              if(!empty($model->product()->first()->product)){
                  return ucfirst($model->product()->first()->product);
                  }
            })

            ->addColumn('lead_to', function($model){
              $lead_from = User::where('active','1')->get();   
              if(!empty($lead_from)){
              foreach($lead_from as $lead_froms){
                if($lead_froms->id == $model->lead_to)
                {
                  return $lead_froms->first_name." ".$lead_froms->last_name." - ".$lead_froms->superuser()->first()->is_superuser;
                }
              }     
              }                     
            })

            ->addColumn('forward_to', function($model){
              $lead_from = User::where('active','1')->get();  
              if(!empty($lead_from)){
              foreach($lead_from as $lead_froms){
                if($lead_froms->id == $model->forward_to)
                {
                  return $lead_froms->first_name." ".$lead_froms->last_name." - ".$lead_froms->superuser()->first()->is_superuser;
                }
              }  
              }                        
            })

            ->addColumn('action', function($model){

                $lead = "";

                if($model->approve_lead == 1 && $model->fi_initiation >= 1){
                    $fi = FiInitiation::where('lead_id',$model->id)->count();
                    if($fi == 0){
                        if(!role_permission('2','lead_fiinitiation')){
                            $lead = "<button class='btn btn-success btn-xs fi_initiation' disabled='' type='button' id='".$model->id."'>FI Initiation</button>";
                        }elseif(role_permission('2','lead_fiinitiation')){
                            $lead = "<button class='btn btn-success btn-xs fi_initiation' type='button' id='".$model->id."'>FI Initiation</button>";
                        }
                    }
                }
                $add_perm='';            
                $ci = Coapplicant::where('lead_id',$model->id)->count();
                if(!role_permission('2','lead_approve')){
                    $approve="<button class='btn btn-info btn-xs' disabled=''>Approve</button>";
                }elseif(role_permission('2','lead_approve')){
                    $approve="<a href='approve/index/" . $model->id . "'><button class='btn btn-info btn-xs' >Approve</button></a>";
                }

                if(!role_permission('2','lead_coapplicant')){
                    $coapplicant="<button class='btn btn-info btn-xs' disabled=''>Co-Applicant
                    <sup class='stages_up'><span class='badge badges_up label-success'>$ci</span></sup></button>";
                }elseif(role_permission('2','lead_coapplicant')){
                    $coapplicant="<a href='co-applicant/index/" . $model->id . "'><button class='btn btn-info btn-xs'>Co-Applicant
                    <sup class='stages_up'><span class='badge badges_up label-success'>$ci</span></sup></button></a>";
                }
                return "<div class='btn-group-xs'>$approve.$coapplicant.$lead";                
            })            
            ->searchColumns('id','name','phone','product','created_at')
            ->orderColumns('name')
            ->make();

    }
    public function postOptions(){
      
      $mode=Input::get("mode");          
      //stage
      $stages = Modules::whereNotIn('module_id',[1,4,5,13,14,15,16])->get();      
      
      echo "<section class='col col-md-3'><label class='select'>Select Stage
            <select class='' id='module_name' name='' onchange='filter(this)' >
            <option value=''>Select stage</option>";
        foreach($stages as $stage){          
          echo "<option value='".$stage->module_table."'>".$stage->module_name."</option>";
        }
      echo "</select></label></section>";

      //Lead Type
      $lead_types=LeadType::get();
      echo "<section class='col col-md-3'><label class='select'>Select Lead Type
            <select class='' name='' id='lead_type' onchange='filter(this)' >
            <option value=''>Select Lead Type</option>";
        foreach($lead_types as $lead_type){          
          echo "<option value='".$lead_type->id."'>".$lead_type->lead_type."</option>";
        }
      echo "</select></label></section>";

      //Enquiry Type
      echo '<section class="col col-3">
              <label class="select">Please Select Enquiry Type
                <select name="enquiry_type" id="enquiry_type" onchange="filter(this)">
                  <option value="">Select Enquiry Type</option>                                   
                  <option value="Warm" >Warm</option>
                  <option value="Hot">Hot</option>
                  <option value="Cold">Cold</option>
                </select>                
              </label>
            </section>';

       //Banks
      $banks=Bank::get();
      echo "<section class='col col-md-3'><label class='select'>Select Bank
            <select class='' name='' id='bank' onchange='filter(this)'>
            <option value=''>Select Bank</option>";
        foreach($banks as $bank){          
          echo "<option value='".$bank->id."'>".$bank->bank."</option>";
        }
      echo "</select></label></section>";

      //loan amount range      
      echo "<section class='col col-md-3'><label class='select'>Select applied loan amount
            <select class='' name='' id='loan_amount' onchange='filter(this)'>
            <option value=''>Select Loan amount</option>";
        for($l=50000;$l<=500000;$l=$l+50000){          
          echo "<option value='".$l."'>".'<'.$l."</option>";
        }
      echo "</select></label></section>";

      //loan approved amount range      
      echo "<section class='col col-md-3'><label class='select'>Select approved loan amount
            <select class='' name='' id='loan_approved' onchange='filter(this)'>
            <option value=''>Select Loan amount</option>";
        for($l=50000;$l<=500000;$l=$l+50000){          
          echo "<option value='".$l."'>".'<'.$l."</option>";
        }
      echo "</select></label></section>";

      //Address type
      echo '<section class="col col-3">
              <label class="select">Please Select Address Type
                <select name="address_type" onchange="filter(this)" id="address_type" > 
                  <option value="">Select Address Type</option>                
                  <option value="permanent_office" >Permanent Office Address</option>
                  <option value="current_office">Current Office Address</option>
                  <option value="permanent_residence">Permanent Residence Address</option>
                  <option value="current_residence">Current Residence Address</option>
                </select>                
              </label>
            </section>';

      //cities
      $cities=City::get();
      echo "<section class='col col-md-3'><label class='select'>Select City
            <select class='' name='' id='city' onchange='filter(this)'>
            <option value=''>Select city</option>";
        foreach($cities as $city){          
          echo "<option value='".$city->id."'>".$city->city."</option>";
        }
      echo "</select></label></section>";

       //Produts
      $products=Product::get();
      echo "<section class='col col-md-3'><label class='select'>Select Product
            <select class='' name='' id='product' onchange='filter(this)'>
            <option value=''>Select product</option>";
        foreach($products as $product){          
          echo "<option value='".$product->id."'>".$product->product."</option>";
        }
      echo "</select></label></section>";

      //Vehicle Model
      $vehicle_models=Vehicle::groupBy('model')->get();
      echo "<section class='col col-md-3'><label class='select'>Select Vehicle Model
            <select class='' name='' id='vehicle_model' onchange='filter(this)'>
            <option value=''>Select Vehicle Model</option>";
        foreach($vehicle_models as $vehicle_model){          
          echo "<option value='".$vehicle_model->id."'>".$vehicle_model->model."</option>";
        }
      echo "</select></label></section>";

      //Vehicle Make
      $vehicle_makes=Vehicle::groupBy('make')->get();
      echo "<section class='col col-md-3'><label class='select'>Select Vehicle Make
            <select class='' name='' id='vehicle_make' onchange='filter(this)' >
            <option value=''>Select Vehicle Make</option>";
        foreach($vehicle_makes as $vehicle_make){          
          echo "<option value='".$vehicle_make->id."'>".$vehicle_make->make."</option>";
        }
      echo "</select></label></section>";

      //Vehicle Category
      $vehicle_categories=Vehicle::groupBy('category')->get();
      echo "<section class='col col-md-3'><label class='select'>Select Category
            <select class='' name='' id='vehicle_category' onchange='filter(this)'>
            <option value=''>Select Category </option>";
        foreach($vehicle_categories as $vehicle_category){          
          echo "<option value='".$vehicle_category->id."'>".$vehicle_category->category."</option>";
        }
      echo "</select></label></section>";
     
      //date range
      /*echo  '<section class=" form-group col col-3" id="person_dob">
            <strong class="text text-dark-blue">From</strong>
            <label class="input">
                <i class="icon-append fa fa-calendar"></i>
                <input type="date" class="dob" name="from_date" id="from_date" placeholder="" value="">
                <b class="tooltip tooltip-bottom-right">Enter From date</b>
            </label>
        </section>';
      echo  '<section class=" form-group col col-3" id="person_dob">
            <strong class="text text-dark-blue">To</strong>
            <label class="input">
                <i class="icon-append fa fa-calendar"></i>
                <input type="date" class="dob" name="to_date" id="to_date" placeholder="" value="">
                <b class="tooltip tooltip-bottom-right">Enter to date</b>
            </label>
        </section>';
*/
      /*echo "<br><br><br><input type='submit' name='' value='Search' class='btn btn-sm btn-info'/>";
      echo "</form>";*/
     /* //User roles
      $user_roles=IsSuperuser::get();
      echo "<section class='col col-md-3'><label class='select'>Select User Role
            <select class='' name='' onchange='filter(this)' id='user_role'>
            <option value=''>Select option</option>";
        foreach($user_roles as $user_role){          
          echo "<option value='".$user_role->id."'>".$user_role->is_superuser."</option>";
        }
      echo "</select></label></section>";*/
    }  
    public function postFetch(){
      //1.selected  options
      $module_ele=json_decode(Input::get('module'));      
      $lead_types=json_decode(Input::get('lead_type'),true);     
      $enquiry_types=json_decode(Input::get('enquiry_type'));
      $banks=json_decode(Input::get('bank'));
      $loan_amount=json_decode(Input::get('loan_amount'));
      $loan_approved=json_decode(Input::get('loan_approved'));
      $address_type=json_decode(Input::get('address_type'),true);
      $city=json_decode(Input::get('city'),true);
      $product=json_decode(Input::get('product'));
      $vehicle_model=json_decode(Input::get('vehicle_model'));
      $vehicle_make=json_decode(Input::get('vehicle_make'));
      $vehicle_category=json_decode(Input::get('vehicle_category'));
      $from_date=Input::get('from_date');
      $to_date=Input::get('to_date');
      
      //2. module to search
      $lead_ids=array();$lead_id_arr=array();
      $date_exist=0;


        //2.1 lead ids
        if($module_ele[0]=="Lead"){
          if($from_date=="" || $to_date==""){            
            $lead_ids=Lead::select('id')->get();
          }else{ $date_exist=1;           
            $lead_ids=Lead::whereBetween('created_at',array($from_date,$to_date))->select('id')->get();
          }
          foreach($lead_ids as $lead_id){
            $lead_id_arr[]=$lead_id->id;
          }                 
        }//2.2 decline leads
        elseif($module_ele[0]=="Decline"){
          if($from_date=="" || $to_date==""){            
            $lead_ids=Decline::select('lead_id')->get();
          }else{ $date_exist=1;           
            $lead_ids=Decline::whereBetween('created_at',array($from_date,$to_date))->select('lead_id')->get();
          }          
          foreach($lead_ids as $lead_id){
            $lead_id_arr[]=$lead_id->lead_id;
          }           
        }// 2.3 fiinitiation
        elseif($module_ele[0]=="FiInitiation"){
          if($from_date=="" || $to_date==""){            
            $fiinitiation_leads=FiInitiation::select('lead_id')->get();
          }else{ $date_exist=1;           
            $fiinitiation_leads=FiInitiation::whereBetween('created_at',array($from_date,$to_date))->select('lead_id')->get();
          }          
          foreach($fiinitiation_leads as $fiinitiation_lead){
            $lead_id_arr[]=$fiinitiation_lead->lead_id;
          }          
        } //2.4 underwriting      
        elseif($module_ele[0]=="underwriting"){
          if($from_date=="" || $to_date==""){            
            $underwriting_leads = FiInitiation::join("temporary_customers",'fi_initiations.lead_id','=','temporary_customers.lead_id')->where('fi_initiations.decline',1)->where('status','accept')->where('temporary_customers.decline',1)->select('fi_initiations.*','temporary_customers.id as temp_cus')->get();          
          }else{ $date_exist=1;           
            $underwriting_leads=FiInitiation::join("temporary_customers",'fi_initiations.lead_id','=','temporary_customers.lead_id')->where('fi_initiations.decline',1)->where('status','accept')->where('temporary_customers.decline',1)->select('fi_initiations.*','temporary_customers.id as temp_cus')->whereBetween('fi_initiations.created_at',array($from_date,$to_date))->get();
          }          
          foreach($underwriting_leads as $$underwriting_lead){
            $lead_id_arr[]=$$underwriting_lead->lead_id;
          }          
        }//2.5 underwritingapprovedlead
        elseif($module_ele[0]=="underwritingapprovedlead"){
          if($from_date=="" || $to_date==""){            
            $underwritingapprovedlead_leads = TemporaryLead::select('temporary_leads.lead_id')->join('temporary_customers','temporary_leads.lead_id','=','temporary_customers.lead_id')->where('temporary_leads.decline','1')->where('finally_save',0)->where("temporary_customers.decline",1)->get();
          }else{ $date_exist=1;           
            $underwritingapprovedlead_leads=TemporaryLead::select('temporary_leads.lead_id')->join('temporary_customers','temporary_leads.lead_id','=','temporary_customers.lead_id')->where('temporary_leads.decline','1')->where('finally_save',0)->where("temporary_customers.decline",1)->whereBetween('temporary_leads.created_at',array($from_date,$to_date))->get();            
          }         
          foreach($underwritingapprovedlead_leads as $underwritingapprovedlead_lead){
            $lead_id_arr[]=$underwritingapprovedlead_lead->lead_id;
          }          
        } //2.6under approved temporarydecline
        elseif($module_ele[0]=="temporarydecline"){
          if($from_date=="" || $to_date==""){            
            $temporarydecline_leads= TemporaryLead::where('decline','0')->get();
          }else{ $date_exist=1;           
            $temporarydecline_leads=TemporaryLead::where('decline','0')->whereBetween('created_at',array($from_date,$to_date))->get();
          }          
          foreach($temporarydecline_leads as $temporarydecline_lead){
            $lead_id_arr[]=$temporarydecline_lead->lead_id;
          }         
        } // 2.7 temporarylead
        elseif($module_ele[0]=="temporarylead"){
          if($from_date=="" || $to_date==""){            
            $temporarylead_leads=Temporary::join("leads",'temporary_customers.lead_id' ,'=','leads.id')
                ->leftJoin("temporary_leads",'temporary_customers.lead_id' ,'=','temporary_leads.lead_id')
            ->select('temporary_customers.lead_id')
            ->where('temporary_customers.decline','=',1)->where('fi_initiation','=',2)->orwhere('finally_save',0)
            ->get(); 
          }else{ $date_exist=1; 
            $temporarylead_leads=Temporary::join("leads",'temporary_customers.lead_id' ,'=','leads.id')
                ->leftJoin("temporary_leads",'temporary_customers.lead_id' ,'=','temporary_leads.lead_id')
            ->select('temporary_customers.lead_id')
            ->whereBetween('temporary_customers.created_at',array($from_date,$to_date))
            ->where('temporary_customers.decline','=',1)->where('fi_initiation','=',2)->orwhere('finally_save',0)            
            ->get();                              
          }
                   
          foreach($temporarylead_leads as $temporarylead_lead){
            $lead_id_arr[]=$temporarylead_lead->lead_id;
          }          
        } //2.8 temporarydeclinecustomer
        elseif($module_ele[0]=="temporarydeclinecustomer"){
          if($from_date=="" || $to_date==""){            
            $temporarydeclinecustomer_leads=Temporary::select('lead_id')->where('decline','0')->get();
          }else{ $date_exist=1;  
            $temporarydeclinecustomer_leads=Temporary::select('lead_id')->where('decline','0')->whereBetween('created_at',array($from_date,$to_date))->get();                     
          }              
          foreach($temporarydeclinecustomer_leads as $temporarydeclinecustomer_lead){
            $lead_id_arr[]=$temporarydeclinecustomer_lead->lead_id;
          }          
        } //2.9 permanent customer
        elseif($module_ele[0]=="permanent"){
          if($from_date=="" || $to_date==""){            
            $permanent_leads=TemporaryLead::select('lead_id')->where('decline','1')->where('finally_save',1)->get();
          }else{ $date_exist=1;  
            $permanent_leads=TemporaryLead::select('lead_id')->where('decline','1')->where('finally_save',1)->whereBetween('created_at',array($from_date,$to_date))->get();                    
          }         
          foreach($permanent_leads as $permanent_lead){
            $lead_id_arr[]=$permanent_lead->lead_id;
          }          
        } 
       
        // 3. multiple search
        $no_of_options=0;$intersection=0;

        //3.1 lead type
        $lead_type_lead=array();$lead_type_lead_arr=array();
        if(count($lead_types) > 0){         
          $no_of_options++;
          $lead_type_lead = Lead::select('id')->whereIn('lead_type_id', $lead_types)->get();       
          if(count($lead_type_lead)>0){             
            foreach($lead_type_lead as $lead_type_lead1){          
              $lead_type_lead_arr[]=$lead_type_lead1->id;          
            }
          }else{ 
              $intersection=1;
          }
        }
       
        //3.2 enquiry type
        $enquiry_type_lead=array();$enquiry_type_lead_arr=array();        
        if(count($enquiry_types)>0){
         
          $no_of_options++;
          $enquiry_type_lead = Lead::select('id')->whereIn('enquiry_type', $enquiry_types)->get();
          if(count($enquiry_type_lead)>0){            
            foreach($enquiry_type_lead as $enquiry_type_lead1){          
              $enquiry_type_lead_arr[]=$enquiry_type_lead1->id;          
            }
          }else{
              $intersection=1;
          }
        }

        //3.3 banks
        $bank_lead=array();$bank_lead_arr=array();        
        if(count($banks)>0){
           $no_of_options++;
          $bank_lead = Lead::select('id')->whereIn('bank_id', $banks)->get();          
          if(count($bank_lead)>0){
            foreach($bank_lead as $bank_lead1){          
              $bank_lead_arr[]=$bank_lead1->id;          
            }
          }else{
              $intersection=1;
          }
        }
        // 3.4 loan amount
        $loanamount_lead=array();$loanamount_lead_arr=array();        
        if(!empty($loan_amount)){
          $loan_amt = (int)$loan_amount[0];
          $loanamount_lead  = TemporaryLead::where('applied_loan_amount','<=',$loan_amt)->get();                      
        //  $loanamount_lead = Temporary::select('lead_id')->where('applied_loan_amount','<=',$loan_amount)->get();
          foreach($loanamount_lead as $loanamount_lead1){          
            $loanamount_lead_arr[]=$loanamount_lead1->lead_id;          
          }
        }
        // 3.4 loan amount
        $loanapproved_lead=array();$loanapproved_lead_arr=array();        
        if(!empty($loan_approved)){
          
          $loan_approve = (int)$loan_approved[0];         
          $loanapproved_lead  = TemporaryLead::where('approve_loan_amount','<=',$loan_approve)->get();                                
        //  $loanamount_lead = Temporary::select('lead_id')->where('applied_loan_amount','<=',$loan_amount)->get();
          foreach($loanapproved_lead as $loanapproved_lead1){          
            $loanapproved_lead_arr[]=$loanapproved_lead1->lead_id;          
          }
        }      
      //3.5 address type ,city                        
        $address_type_lead=array();$address_type_lead_arr=array();        
        if(!empty($address_type)){
          if($address_type[0]=="permanent_office"){
            $address_type_lead = Lead::select('id')->whereIn('permanent_office_city_id',$city)->get();
          }else if($address_type[0]=="current_office"){
            $address_type_lead = Lead::select('id')->whereIn('current_office_city_id',$city)->get();
          }else if($address_type[0]=="permanent_residence"){
            $address_type_lead = Lead::select('id')->whereIn('pemanent_resi_city_id',$city)->get();
          }else if($address_type[0]=="current_residence"){
            $address_type_lead = Lead::select('id')->whereIn('current_resi_city_id',$city)->get();
          }
          foreach($address_type_lead as $address_type_lead1){          
            $address_type_lead_arr[]=$address_type_lead1->id;          
          }
        }
        
      //3.6 product
        $product_lead=array();$product_lead_arr=array();        
        if(!empty($product)){
          $product_lead = Lead::select('id')->whereIn('product_id', $product)->get();
          foreach($product_lead as $product_lead1){          
            $product_lead_arr[]=$product_lead1->id;          
          }
        }
      //3.7 vehicle model
        $vehicle_model_lead=array();$vehicle_model_lead_arr=array();        
        if(!empty($vehicle_model)){
          $vehicle_model_lead = Lead::select('id')->whereIn('vehicle_id', $vehicle_model)->get();
          foreach($vehicle_model_lead as $vehicle_model_lead1){          
            $vehicle_model_lead_arr[]=$vehicle_model_lead1->id;          
          }
        }
       
      // 3.8 vehicle make
        $vehicle_make_lead=array();$vehicle_make_lead_arr=array();        
        if(!empty($vehicle_make)){
          $vehicle_make_lead = Lead::select('id')->whereIn('vehicle_id', $vehicle_make)->get();
          foreach($vehicle_make_lead as $vehicle_make_lead1){          
            $vehicle_make_lead_arr[]=$vehicle_make_lead1->id;          
          }
        }

      //3.9 vehicle category
        $vehicle_category_lead=array();$vehicle_category_lead_arr=array();        
        if(!empty($vehicle_category)){
          $vehicle_category_lead = Lead::select('id')->whereIn('vehicle_id', $vehicle_category)->get();
          foreach($vehicle_category_lead as $vehicle_category_lead1){          
            $vehicle_category_lead_arr[]=$vehicle_category_lead1->id;          
          }
        }        
        

        //4.Final result
        $arr=array();
        $arr_merge=array_merge($lead_type_lead_arr,$enquiry_type_lead_arr,$bank_lead_arr,$loanamount_lead_arr,$loanapproved_lead_arr,$address_type_lead_arr,$product_lead_arr,$vehicle_model_lead_arr,$vehicle_make_lead_arr,$vehicle_category_lead_arr);
      
        if($no_of_options>=2){
            //echo "if op>2";
            //$arr_unique = array_unique($arr_merge);           
            if($intersection==1){
                $arr=array_intersect($lead_type_lead_arr,$enquiry_type_lead_arr,$bank_lead_arr,$loanamount_lead_arr,$loanapproved_lead_arr,$address_type_lead_arr,$product_lead_arr,$vehicle_model_lead_arr,$vehicle_make_lead_arr,$vehicle_category_lead_arr);
             }else{
                $arr=array_unique( array_diff_assoc($arr_merge, array_unique( $arr_merge) ));
             }
            
          }else{
            //echo "else op<2";
            if($no_of_options==0&&$intersection==0&&$date_exist==1){
              //echo "else op<2".$no_of_options.$intersection;              
              $arr=array_unique($lead_id_arr);
            }else{
              $arr=array_unique($arr_merge);
            }
          }  
        /*  echo "array merge";
          print_r($arr_merge);
          echo "array";
          print_r($arr);
          echo "lead id array";
          print_r($lead_id_arr);*/
                  
        $result=array_intersect($lead_id_arr,$arr);      
        /*echo "result array";
          print_r($result);*/
       
        DB::table("search_filter")->truncate();
        DB::statement('ALTER TABLE search_filter AUTO_INCREMENT =1');
        if(!empty($result)){
        foreach($result as $res){
          DB::table('search_filter')->insert(
          array('lead_id' => $res, 'created_at' =>date('Y-m-d'),'created_by'=>Auth::admin()->get()->id)
          );
        }
      }
      $msg=array(
          'message'=>'success'
        );
      echo json_encode($msg)  ;

    }
    /*$queries = DB::getQueryLog();
          $last_query = end($queries);
          print_r($last_query);*/

        
}