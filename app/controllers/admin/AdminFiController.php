<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 29/5/15
 * Time: 5:11 PM
 */
class AdminFiController extends BaseController{

    function __construct(){
        $this->beforeFilter('admin');
        if(!module_permission('6'))
        {
            return Redirect::to('admin/')->send();
        }

    }
    public function getIndex(){


        $fi = Datatable::table()
            ->addColumn('Bank & Bank Branch','Lead','Email','Phone','Status','Action')
            ->setUrl(URL::to('admin/fi/firecord'))
            ->noScript();
        $breadcrum = 'FI Initiation';
        return View::make('admin/fi_status')->with('data',$fi)->with('breadcrum',$breadcrum);

    }
    public function getFirecord(){
        $query = FiInitiation::join("temporary_customers",'fi_initiations.lead_id','=','temporary_customers.lead_id')->where(function ($query){$query->where('status',0)->where('temporary_customers.decline',1);})->orwhere(function($query){$query->where('status','reject')->where('temporary_customers.decline',1);})->select('fi_initiations.*','temporary_customers.id as temp_cus')->get();

        /*$queries = DB::getQueryLog();
        $last_query = end($queries);
        print_r($last_query);exit;*/
        return Datatable::collection($query)

            ->addColumn('bank',function($model){
                return ucfirst($model->bank()->first()->bank)."<br><strong class='text-info'>Branch: </strong> ".$model->branch()->first()->city()->first()->city."<br><strong class='text-info'>Branch Manager:</strong> ".$model->branch()->first()->contact_person."<br><strong class='text-info'>Branch Manager Contact:</strong> ".$model->branch()->first()->email.", ".$model->branch()->first()->phone;
            })
            ->addColumn('lead',function($model){
                return ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name);
            })
            ->addColumn('Email',function($model){
                return ucfirst($model->lead()->first()->email_id);
            })
            ->addColumn('Phone',function($model){
                return ucfirst($model->lead()->first()->phone);
            })
            ->addColumn('status',function($model){

                if($model->status === "reject"){
                    if(!role_permission('6','fistatus_filerejected')){
                        return "<button class='btn btn-danger btn-xs' disabled=''>File Rejected</button>";
                    }
                    elseif(role_permission('6','fistatus_filerejected')){
                        return "<button class='btn btn-danger btn-xs' id='".$model->id."'>File Rejected</button>";
                    }
                }
                if($model->status == 0 && $model->mail == 'sent'){
                    $accept="";
                    if(!role_permission('6','fistatus_accept')){
                        $accept="<button class='btn btn-success btn-xs accept' disabled=''>Accept</button>";
                    }
                    elseif(role_permission('6','fistatus_accept')){
                        $accept="<button class='btn btn-success btn-xs accept' id='".$model->id."'>Accept</button>";
                    }
                    $reject="";
                    if(!role_permission('6','fistatus_reject')){
                        $reject="<button class='btn btn-warning btn-xs reject' disabled=''>Reject</button>";
                    }
                    elseif(role_permission('6','fistatus_reject')){
                        $reject="<button class='btn btn-warning btn-xs reject' id='".$model->id."'>Reject</button>";
                    }


                    return $accept. "|".$reject;
                }
                if($model->mail == 'send'){

                    $download="";
                   if(!role_permission('6','fistatus_downloadcustomize')){
                        $download="<button type='button' disabled='' class='btn btn-xs btn-info'>Download & Customize</button>";
                    }
                    elseif(role_permission('6','fistatus_downloadcustomize')){
                        $download="<a href='".asset("document/".$model->lead()->first()->lead_first_name."_".$model->lead_id."01simple.xls")."'><button type='button' class='btn btn-xs btn-info'>Download & Customize</button></a>";
                    }

                    $upload_sendfi="";
                    if(!role_permission('6','fistatus_uploadsendfi')){
                        $upload_sendfi="<button class='btn btn-xs btn-info send_mail' disabled=''>Upload & Send FI</button>";
                    }
                    elseif(!role_permission('6','fistatus_uploadsendfi')){
                        $upload_sendfi="<button class='btn btn-xs btn-info send_mail' id='".$model->lead_id."'>Upload & Send FI</button>";
                    }
                    return  $download."|".$upload_sendfi;
                }

            })
            ->addColumn('action', function($model){

                $link = "<a href='#'><button type='button' class='btn btn-xs btn-success'>FI Initiation Under Process</button></a>";
                if($model->decline == 0 || $model->status == "reject")
                {
                    if(!role_permission('6','fistatus_applyagain')){
                        $link = "<button type='button' class='btn btn-xs btn-info apply_again' >Apply Again</button></a>";
                    }else if(!role_permission('6','fistatus_applyagain')){
                        $link = "<button type='button' class='btn btn-xs btn-info apply_again' id='".$model->lead_id."'>Apply Again</button></a>";
                    }


                }
                if($model->status == 'accept'){
                    $link = "<a href='#'><button type='button' class='btn btn-xs btn-info'>File Accepted</button></a>";
                }
                if($model->mail == 'send'){
                    //$link = "<a href='fi/mail/".$model->lead_id."'><button class='btn btn-info btn-xs'>Send Mail</button></a>";
                    if(!role_permission('6','fistatus_sendmaildirectly')){
                        $link = "<button class='btn btn-info btn-xs mail_direct' disabled=''>Send Mail Directly</button>";
                    }
                    elseif(role_permission('6','fistatus_sendmaildirectly')){
                        $link = "<button class='btn btn-info btn-xs mail_direct' id='".$model->lead_id."'>Send Mail Directly</button>";
                    }

                } 	

                return "<div class='btn-group-xs'>$link</div>";
            })
            ->searchColumns('id','bank_id','lead_id','status')
            ->orderColumns('id','lead_type')
            ->make();
    }
    public function getStatus(){
    
       $id = Input::get('id');
       $status = Input::get('status');
       $fi = FiInitiation::find($id);
       if($status == 'accept'){
            $fi->status = 'accept';
       }
       if($status == 'reject'){
           $fi->status = 'reject';
       }
        if($status == 'decline'){

            $fi->decline = 0;
            $fi->status = 'reject';
        }
       $fi->update();


    }
    public function getBank(){

        $bank = Bank::where('status',1)->get();
        
        $data = "";
        $data ="<option>---SELECT BANK FOR FI INITIATION---</option>";
        foreach($bank as $banks){
            $data .= "<option value=".$banks->id.">".$banks->bank."</option>";
        }
        
        header('content-type:application/json');
        echo json_encode($data);
    }
    public function getBankbranches(){

        $branch_id = Input::get('branch');

        $branches = BankBranch::where('bank_id',$branch_id)->get();

        header("content-type:application/json");

        echo json_encode($branches);
    }
    public function postSendfi(){
        $id = Input::get('lead_id');
        $bank = Input::get('branch');
        $branch_id = Input::get('bankbranch_id');

        $branch = BankBranch::find($branch_id);
        $email = $branch->email;
		
		return Redirect::to("admin/report/index/$id/$bank/$branch_id");




    }
    public function getMail(){
    
    
        $id = Input::get('id');
        $file = Lead::find($id);
        $src = $file->lead_first_name."_".$id."01simple.xls"; 
        $email_get = FiInitiation::where('lead_id',$id)->first();


        $user=array(
            'src'=>$src,
            'to' =>$email_get->branch()->first()->email
        );

        Mail::send('emails.fi',$user,function($message) use ($user)
        {

            $message->to($user['to'])
                ->cc('ranjit00923@gmail.com')
                ->cc('kunalkhurana.it@gmail.com')
                ->subject("FI Initiation file, Shree Balajee Enterprises");

        });

        if(count(Mail::failures()) > 0){

            $data = array(
                'status' => 'fail',
                'message' => "There was an error sending mail!"

            );
        }
        else{
            $fi = array(
                'mail' => 'sent'

            );
            FiInitiation::where('lead_id',$id)->update($fi);



            $data = array(
                'status' => 'success',
                'message' => "Mail has been send to bank"

            );

        }
        header('content-type:application/json');
        echo json_encode($data);



    }
    public function postMail(){

        $id = Input::get("lead_id");
        $file = Lead::find($id);

        $email_get = FiInitiation::where('lead_id',$id)->first();

        $src = $file->lead_first_name."_".$id."01simple.xls";

        $user=array(
            'src'=>$src,
            'to' =>$email_get->branch()->first()->email
        );

        Mail::send('emails.fi',$user,function($message) use ($user)
        {
            $message->to($user['to'])
                ->cc('ranjit00923@gmail.com')
                ->cc('kunalkhurana.it@gmail.com')
                ->subject("FI Initiation file");

        });

        if(count(Mail::failures()) > 0){

            $data = array(
                'status' => 'fail',
                'message' => "There was an error sending mail!"

            );
        }
        else{
            $fi = array(
                'mail' => 'sent'

            );
            FiInitiation::where('lead_id',$id)->update($fi);



            $data = array(
                'status' => 'success',
                'message' => "Mail has been send to bank"

            );

        }
        header('content-type:application/json');
        echo json_encode($data);

    }
    public function getUnderwriting(){
        if(!module_permission('7'))
        {
            return Redirect::to('admin/')->send();
        }

        $fi = Datatable::table()
            ->addColumn('Lead','Email','Phone','Decline','Status')
            ->setUrl(URL::to('admin/fi/underwritingrecord'))
            ->noScript();
            
        $breadcrum = 'Under Writing';
        return View::make('admin/fi_status')->with('data',$fi)->with('breadcrum',$breadcrum);

    }
    public function getUnderwritingrecord(){
    
        //$query = FiInitiation::where('decline','1')->where('status','accept')->get();

        $query = FiInitiation::join("temporary_customers",'fi_initiations.lead_id','=','temporary_customers.lead_id')->where('fi_initiations.decline',1)->where('status','accept')->where('temporary_customers.decline',1)->select('fi_initiations.*','temporary_customers.id as temp_cus')->get();
		
        return Datatable::collection($query)


          ->addColumn('lead',function($model){
                return ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name).$model->status;
                
            })
           /*->addColumn('guardian',function($model){
                return $model->guardian." Of ".ucfirst($model->lead()->first()->guardian_first_name)." ".ucfirst($model->lead()->first()->guardian_last_name);
            })*/
            ->addColumn('Email',function($model){
                return ucfirst($model->lead()->first()->email_id);
            })
            ->addColumn('Phone',function($model){
                return ucfirst($model->lead()->first()->phone);
            })
            ->addColumn('decline',function($model){

                if($model->status == 'los'){
                    return  "<button class='btn btn-danger btn-xs decline_temporary' id='".$model->id."'></button>";
                }
                if($model->status != 'los'){

                    if(!role_permission('7','underwriting_declineunderwriting')){
                        return "<button class='btn btn-danger btn-xs' disabled=''>Decline Under Writing</button>";
                    }
                    elseif(role_permission('7','underwriting_declineunderwriting')){
                        return "<button class='btn btn-danger btn-xs decline' id='".$model->id."'>Decline Under Writing</button>";
                    }

		}

            })
            ->addColumn('status',function($model){

                    if($model->status == 'los'){
                        return "<button class='btn btn-success btn-xs' id='".$model->id."'></button>";
                    }
                   if($model->status != 'los'){

                       if(!role_permission('7','underwriting_losnumber')){
                           return "<button class='btn btn-success btn-xs' disabled=''>LOS Number</button>";
                       }
                       else if(role_permission('7','underwriting_losnumber')){
                           return "<button class='btn btn-success btn-xs los_number' id='".$model->id."'>LOS Number</button>";
                       }

                    }


            })
            /*->addColumn('action', function($model){

                return "<div class='btn-group-xs'></div>";
            })*/
            ->searchColumns('id','lead_id','status')
            ->orderColumns('id','lead_type')
            ->make();
    }
    public function getLead(){

        $id = Input::get('id');

        $fi = FiInitiation::find($id);

        $amount = $fi->lead()->first()->loan_amount;

        $data = array(
            'lead_id' => $fi->lead_id,
            'amount' => $amount,
            'fi_id' => $id
        );

        header('content-type:application/json');
        echo json_encode($data);
    }

}