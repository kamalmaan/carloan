<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 4/6/15
 * Time: 1:00 PM
 */
class AdminTemporaryLeadController extends BaseController{
    function __construct(){
        $this->beforeFilter('admin');
        if(!module_permission('10'))
        {
            return Redirect::to('admin/')->send();
        }

    }
    public function getIndex(){

        $temporary = Datatable::table()
            ->addColumn('Lead','Guardian Name','Email','Phone','Decline','Status')
            ->setUrl(URL::to('admin/temporarylead/temporary'))
            ->noScript();

        $breadcrum = "Declined Temporary Lead";
        return View::make('admin/temporarylead')->with('data',$temporary)->with('breadcrum',$breadcrum);
    }
    public function getTemporary(){
        //$query = Temporary::where('fi_initiation','1')->where('approve_lead',1)->get();
        $query = Temporary::join("leads",'temporary_customers.lead_id' ,'=','leads.id')
                ->leftJoin("temporary_leads",'temporary_customers.lead_id' ,'=','temporary_leads.lead_id')
            ->select('temporary_customers.*','leads.fi_initiation')
            ->where('temporary_customers.decline','=',1)->where('fi_initiation','=',2)->orwhere('finally_save',0)
            ->get();
        //,'temporary_leads.finally_save'
        
       	/*$queries = DB::getQueryLog();
        $last_query = end($queries);
        print_r($last_query);exit();*/

        return Datatable::collection($query)


            ->addColumn('lead',function($model){

                if(!empty($model->lead()->first()->company_name)){
                    $user = ucfirst($model->lead()->first()->company_name)." ".$model->lead()->first()->constitution;
                }
                else{
                    $user = ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name);
                }
                $label ="";
                if($model->fi_initiation()->first()->status == 0  ){
                    $label = "<sup class='stages_up'><span class='badge badges_up label-warning'>FI</span></sup>";
                }
                if($model->fi_initiation()->first()->decline == 1 && $model->fi_initiation()->first()->status == 'accept'){
                    $label = "<sup class='stages_up'><span class='badge badges_up label-success'>UW</span></sup>";
                }
                if(TemporaryLead::where('lead_id','=',$model->lead_id )->count() > 0)
                    if($model->temporary_lead()->first()->decline == 1 && $model->temporary_lead()->first()->finally_save == 0){
                        $label = "<sup class='stages_up'><span class='badge badges_up label-success'>UW Approved</span></sup>";
                    }

                if($model->fi_initiation()->first()->status == 'reject'){
                    $label ="<sup class='stages_up'><span class='badge badges_up label-danger'>FI Rejected</span></sup>";
                }
                if(!role_permission('10','temporarycustomer_profile')){
                    return "<span><button title='Edit Lead' class='btn btn-info btn-xs' disabled='' >".$user."</button>$label</span>";
                }
                elseif(role_permission('10','temporarycustomer_profile')){

                    return "<span><a href='lead/leadtimeline/".$model->lead_id."'><button title='Edit Lead' class='btn btn-info btn-xs' >".$user."</button></a>$label</span>";
                }


            })
            ->addColumn('guardian',function($model){
                if($model->lead()->first()->type == "person"){
                    return $model->guardian." ".ucfirst($model->lead()->first()->guardian_first_name)." ".ucfirst($model->lead()->first()->guardian_last_name);
                }

            })
            ->addColumn('Email',function($model){
                return ucfirst($model->lead()->first()->email_id);
            })
            ->addColumn('Phone',function($model){
                return ucfirst($model->lead()->first()->phone);
            })

            ->addColumn('decline',function($model){
                if(!role_permission('10','temporarycustomer_decline')){
                    return  "<button class='btn btn-danger btn-xs ' disabled=''>Decline</button>";
                }
                else if(role_permission('10','temporarycustomer_decline')){
                    return  "<button class='btn btn-danger btn-xs decline_temporary_customer' id='".$model->id."'>Decline</button>";
                }


            })
            ->addColumn('manage_lead',function($model){

                $ci=Coapplicant::where('lead_id','=',$model->lead_id )->count();
                $update_info="";$coapplicant="";
                if(!role_permission('10','temporarycustomer_update')){
                    $update_info="<button class='btn btn-info btn-xs' disabled=''>Update Info</button>";
                }
                elseif(role_permission('10','temporarycustomer_update')){
                    $update_info="<a href='approve/index/" . $model->lead_id . "'><button class='btn btn-info btn-xs' >Update Info</button></a>";
                }


                if(!role_permission('10','temporarycustomer_coapplicant')){
                    //return "<div class='btn-group-xs'><button class='btn btn-info btn-xs' >Update Info</button></a><br><a href='co-applicant/index/" . $model->lead_id . "'><button class='btn btn-info btn-xs'>Co-Applicant<sup class='stages_up'><span class='badge badges_up label-warning'>$ci</span></sup></button>";
                    $coapplicant="<button class='btn btn-info btn-xs' disabled=''>Co-Applicant<sup class='stages_up'><span class='badge badges_up label-warning'>$ci</span></sup></button>";
                }
                elseif(role_permission('10','temporarycustomer_coapplicant')){
                    $coapplicant="<a href='co-applicant/index/" . $model->lead_id . "'><button class='btn btn-info btn-xs'>Co-Applicant<sup class='stages_up'><span class='badge badges_up label-warning'>$ci</span></sup></button></a>";
                }

                return "<div class='btn-group-xs'>".$update_info."<br>".$coapplicant;
            })
            /*->addColumn('action', function($model){

                return "<div class='btn-group-xs'></div>";
            })*/
            ->searchColumns('id','lead_id','manage_lead')
            ->orderColumns('id','lead_type')
            ->make();

    }

    public function getStatus(){
        $id = Input::get('id');

        $temporary = Temporary::find($id);

        if(Input::get('status') == 'decline'){

            $temporary->decline = 0;
        }
        if(Input::get('status') == 'recycle'){
            $temporary->decline = 1;
        }
        $temporary->update();

        $data = array(
            'status' => 'success',
            'message' => "Temporary Lead Is Declined Successfully"
        );
        header('content-type:application/json');
        echo json_encode($data);


    }

    public function getTemporarydecline(){

        if(!module_permission('11'))
        {
            return Redirect::to('admin/')->send();
        }

        $temporary = Datatable::table()
            ->addColumn('Lead','Guardian Name','Email','Phone','LOS Number','Decline')
            ->setUrl(URL::to('admin/temporarylead/decline'))
            ->noScript();
        $breadcrum = "Declined Temporary Lead";
        return View::make('admin/temporarylead')->with('data',$temporary)->with('breadcrum',$breadcrum);
    }

    public function getDecline(){
        $query = Temporary::where('decline','0')->get();


        return Datatable::collection($query)


            ->addColumn('lead',function($model){
                return ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name);
            })
            ->addColumn('guardian',function($model){
                return $model->guardian." Of ".ucfirst($model->lead()->first()->guardian_first_name)." ".ucfirst($model->lead()->first()->guardian_last_name);
            })
            ->addColumn('Email',function($model){
                return ucfirst($model->lead()->first()->email_id);
            })
            ->addColumn('Phone',function($model){
                return ucfirst($model->lead()->first()->phone);
            })
            ->addColumn('los',function($model){
                return ucfirst($model->los_number);
            })
            ->addColumn('recycle',function($model){
                if(!role_permission('11','temporarydeclinecustomer_recycle')){
                    return  "<button class='btn btn-success btn-xs recycle_temporary_customer' disabled=''>Recycle</button>";
                }
                elseif(role_permission('11','temporarydeclinecustomer_recycle')){
                    return  "<button class='btn btn-success btn-xs recycle_temporary_customer' id='".$model->id."'>Recycle</button>";
                }


            })

            /*->addColumn('action', function($model){

                return "<div class='btn-group-xs'></div>";
            })*/
            ->searchColumns('id','lead_id','status')
            ->orderColumns('id','lead_type')
            ->make();

    }


}