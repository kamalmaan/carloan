<?php
class AdminDailyTaskController extends BaseController{

    function __construct(){
        $this->beforeFilter('admin');
        if(!module_permission('14'))
        {
            return Redirect::to('admin/')->send();
        }
    }

    public function getIndex(){
        $dailytask = Datatable::table()
            ->addColumn('Client','Assigned To','Task','Assigned On','Completion On','Assigned By','Status','Action')
            //->addColumn('Client','Assigned To')
            ->setUrl(URL::to('admin/dailytask/schedule'))
            ->noScript();
        $user=User::all();
      /* echo "<pre>";
      // print_r($dailytask);
        print_r($user);
        exit;*/
        return View::make('admin/dailytask')->with('data',$dailytask)->with('user',$user);
    }
    public function getSchedule(){

        $query=DailyTask::join('users','daily_tasks.assign_to','=','users.id')

            ->select('daily_tasks.*','users.first_name as user_first_name','users.id as user_id')->get();


        return Datatable::collection($query)
            ->addColumn('client',function($model){
                return ucfirst($model->client);
            })
            ->addColumn('assign_to',function($model){

                $user=User::get();
                $opt = '';
                $selected='';

                foreach($user as $users){

                    $opt .= "<option value='".$users->id ."'";

                    $branch="No Branch";
                    if($users->branch_id==0){
                        $branch="Branch Not Assigned";
                    }
                    else{
                        if($users->branch()->first()->status==1){
                            $branch=ucfirst($users->branch()->first()->branch);
                        }
                        else{
                            $branch="Assigned branch disabled";
                        }
                    }
                    if($users->id == $model->user_id)
                    {
                        $opt .= "selected";
                    }
                    $opt .= ">".ucfirst($users->superuser()->first()->is_superuser).' >> '.$branch.' >> '.ucfirst($users->first_name).' '.ucfirst($users->last_name)."</option>";
                }
                if(!role_permission('14','dailytask_assignlead')){
                    return "<select class='assign_to' disabled=''>"."<option value=''>Assign To</option>".$opt."</select>";
                }elseif(role_permission('14','dailytask_assignlead')){
                    return "<select class='assign_to' id='".$model->id."'>"."<option value=''>Assign To</option>".$opt."</select>";
                }
            })

            ->addColumn('task',function($model){
                return substr($model->task,0,100);
            })
            ->addColumn('assign_on',function($model){
                return ucfirst($model->assign_on);
            })
            ->addColumn('completion_on',function($model){
                return ucfirst($model->completion_on);
            })
            ->addColumn('assign_by',function($model){
                $query2=User::where('id',$model->assign_by)->first();
                return ucfirst($query2->first_name.' '.$query2->last_name);
            })
            ->addColumn('status',function($model){
                if($model->status == 0){

                    if(!role_permission('14','dailytask_status')){
                        return "<button class='btn btn-danger btn-xs ' >Pending</button>";
                    }elseif(role_permission('14','dailytask_status')){
                        return "<button class='btn btn-danger btn-xs status' id='status_".$model->id."'>Pending</button>";
                    }

                }
                else{
                    if(!role_permission('14','dailytask_status')){
                        return "<button class='btn btn-success btn-xs' >Completed</button>";
                    }
                    elseif(role_permission('14','dailytask_status')){
                        return "<button class='btn btn-success btn-xs status' id='status_".$model->id."'>Completed</button>";
                    }

                }
            })
            ->addColumn('action', function($model){
                if($model->status_delete==1){
                    $edit="";$disable="";

                    if(!role_permission('14','dailytask_edit')){
                        $edit="<button type='button' class='btn btn-xs btn-info' disabled=''>Edit</button>";
                    }elseif(role_permission('14','dailytask_edit')){
                        $edit="<button type='button' class='btn btn-xs btn-info edittask' id='state_".$model->id."'>Edit</button>";
                    }
                    if(!role_permission('14','dailytask_enabledisable')){
                        $disable="<button class='btn btn-danger btn-xs ' disabled=''>Disable</button>";
                    }
                    elseif(role_permission('14','dailytask_enabledisable')){
                        $disable="<button class='btn btn-danger btn-xs dailytask' id='status_".$model->id."'>Disable</button>";
                    }

                    return $edit." ".$disable;
                }else{
                    if(!role_permission('14','dailytask_enabledisable')){
                        return "<button class='btn btn-success btn-xs' disabled=''>Enable</button>";
                    }
                    elseif(role_permission('14','dailytask_enabledisable')){
                        return "<button class='btn btn-success btn-xs dailytask' id='status_".$model->id."'>Enable</button>";
                    }

                }
            })
            ->searchColumns("assign_to",'client','status')
            ->orderColumns('assign_to')
            ->make();

    }


    public function getAssignto(){

        $user=User::get();

        echo "<option value=''>Assign To</option>";
        foreach($user as $users){

            $branch="No Branch";
            if($users->branch_id==0){
                $branch="Branch Not Assigned";
            }else{
                if($users->branch()->first()->status==1){
                    $branch=ucfirst($users->branch()->first()->branch);
                }
                else{
                    $branch="Assigned branch disabled";
                }
            }

            echo "<option value='".$users->id."'>".ucfirst($users->superuser()->first()->is_superuser).' >> '.$branch.' >> '.ucfirst($users->first_name).' '.ucfirst($users->last_name)."</option>";
        }

    }


    public function getAgent(){
        $assigned = Input::get("assigned");
        $forward = Input::get("forward");

        $update_agent = array(
            'assign_to' => $forward
        );
        if(DailyTask::where("assign_to",'=',$assigned)->count() == 0){

            $data = array(
                'status' => 'error',
                'message' => 'No lead is assigned to agent !'
            );
            echo json_encode($data);
        }
        else{
            DailyTask::where("assign_to",'=',$assigned)->update($update_agent);
            $data = array(
                'status' => 'success',
                'message' => 'Lead Successfully Forward !'
            );
            echo json_encode($data);
        }

    }

    public function getPrint($lead_agent_id){
        $today=date('Y-m-d');
        $nxtday=date('Y-m-d',strtotime("+1 days $today")).' 00-00-00';

        $check = Meeting::where('lead_agent_id',$lead_agent_id)
            ->where('meeting_date','>',$today.' 00-00-00')
            ->where('meeting_date','<',$nxtday)
            ->count();

        if($check > 0){

            $today_meeting = Meeting::where('lead_agent_id',$lead_agent_id)
                ->where('meeting_date','>',$today.' 00-00-00')
                ->where('meeting_date','<',$nxtday)
                ->get();

            $todays = array();
            foreach($today_meeting as $today_meetings){

                if($today_meetings->leads()->first()->type == "person"){

                    $customer_name= ucwords($today_meetings->leads()->first()->lead_first_name ." ". $today_meetings->leads()->first()->lead_last_name);
                }
                else{
                    $customer_name = ucwords($today_meetings->leads()->first()->company_name ." ". $today_meetings->leads()->first()->constitution);
                }
                if(!empty($today_meetings->leads()->first()->product_id)){

                    $product = $today_meetings->leads()->first()->product()->first()->product;
                }
                else{
                    $product = "";
                }
                if(!empty($today_meetings->leads()->first()->enquiry_type)){
                    $enquiry_type = $today_meetings->leads()->first()->enquiry_type;
                }
                else{
                    $enquiry_type = "";
                }
                if(!empty($today_meetings->leads()->first()->lead_from)){
                    $lead_from = ucwords($today_meetings->leads()->first()->lead_from()->first()->first_name ." ".$today_meetings->leads()->first()->lead_from()->first()->last_name);
                }
                else{
                    $lead_from = "";
                }

                $current[] = array(
                    'customer_name' => $customer_name,
                    'phone' => $today_meetings->leads()->first()->phone,
                    'meeting_topic' => $today_meetings->meeting_topic,
                    'meeting_medium' => $today_meetings->meeting_medium,
                    'product' => $product,
                    'enquiry_type' => $enquiry_type,
                    'lead_from' => $lead_from,
                    'lead_assigned' => ucwords($today_meetings->user()->first()->first_name." ". $today_meetings->user()->first()->last_name),
                    'lead_arrival' => $today_meetings->leads()->first()->created_at
                );


            }

            foreach($today_meeting as $previous_meetings){
                $lead_id = $previous_meetings->lead_id;
                $previous[] = Meeting::where('lead_id',$lead_id)->where('meeting_date','<',$today.' 00-00-00')->get();

            }
            for($i = 0; $i<= count($previous)-1;$i++)
            {
                if(!empty($previous[$i])){
                    $previous_meeting_topic = $previous[$i];
                }
                else{
                    $previous_meeting_topic =  "First Meeting";
                }
                $previouss[] = array(
                    'previous_meeting'=> $previous_meeting_topic
                );



            }

            for($j= 0;$j<= count($current)-1; $j++){
                $today_schedule[]  = array_merge($current[$j],$previouss[$j]);
            }
        }
        else{
            $today_schedule = 0;
        }

        $daily_task = DailyTask::where('assign_to',$lead_agent_id)

            ->get();

        return View::make('admin/print')->with(array('today_schedules'=>$today_schedule,'daily_tasks'=>$daily_task));

    }


    public function postSave(){
        //echo "save";

        $rules = array(
            'client' =>'required',
            'assign_on' => 'required',
            'assign_to' => 'required',
            'task' => 'required'
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            $data = array(
                'status' => 'fail',
                'message' => 'All fields are required'
            );
            echo json_encode($data);
        }
        else{
            echo Input::get('phone');exit;
            $dailytask = new DailyTask;

            $dailytask->client = Input::get('client');
            $dailytask->phone = Input::get('phone');
            $dailytask->assign_on = Input::get('assign_on');
            $dailytask->completion_on = Input::get('completion_on');
            $dailytask->assign_to = Input::get('assign_to');
            $dailytask->task = Input::get('task');
            $dailytask->assign_by= ucfirst(Auth::admin()->get()->id);
            //$dailytask->save();
            $data = array(
                'status' => 'success',
                'message' =>'Task Successfully Saved.',
            );
            echo json_encode($data);

            // return Redirect::to('admin/lead')->with('message','Lead successfully updated.');

        }

    }
    public function getStatus(){
        $id = Input::get('id');
        $state = explode('_',$id);
        $check = $state[1];
        $status = DailyTask::where('id',$check)->delete();

        $data = array(
            'status' => "success",
            'Message' => "Task Completed !"

        );
        echo json_encode($data);
        /*if($status->status == 1){

            $checkstatus = array(
                'status' => 0
            );
        }
        else{

            $checkstatus = array(
                'status' => 1
            );
        }
        DB::table('daily_tasks')
            ->where('id',$check)
            ->update($checkstatus);*/
    }
    public function getStatusdelete(){
        $id = Input::get('id');
        $state = explode('_',$id);
        $check = $state[1];
        $status = DailyTask::where('id',$check)->first();

        if($status->status_delete == 1){

            $checkstatus = array(
                'status_delete' => 0
            );
        }
        else{

            $checkstatus = array(
                'status_delete' => 1
            );
        }
        DB::table('daily_tasks')
            ->where('id',$check)
            ->update($checkstatus);
    }
    public function postUpdateassignto(){
        $id = Input::get('id');
        $assignto = Input::get('userid');

        $data = array(
            'assign_to' => $assignto
        );

        DB::table('daily_tasks')
            ->where('id',$id)
            ->update($data);
    }
    public function getEdit(){

        Input::get('id');
        $check = explode('_',Input::get('id'));
        $id = $check[1];

        $data = DailyTask::find($id);

        //echo $data;
        $resp = array(
            'record' => $data
        );
        header('content-type: application/json');
        echo json_encode($resp);


    }
    public function postUpdate(){


        $id = Input::get('id');

        $data = array(
            'client' => Input::get('client'),
            'phone' => Input::get('phone'),
            'assign_on' => Input::get('assign_on'),
            'completion_on' => Input::get('completion_on'),
            'assign_to' => Input::get('assign_to'),
            'task' => Input::get('task')
        );

        DB::table('daily_tasks')
            ->where('id',$id)
            ->update($data);
        $data = array(
            'status' => 'success',
            'message' =>'DailyTask Successfully Updated.',

        );

        echo json_encode($data);

    }

}