<?php
class AdminLeadController extends BaseController{
    function __construct(){
        $this->beforefilter('admin');
        if(!module_permission('2'))
        {
            return Redirect::to('admin/')->send();
        }
    }
    public function getIndex(){

       $lead = Datatable::table()
            ->addColumn('Name', 'Product','Assign Lead','Forward Lead','Enquiry','Decline','Manage Lead')
            ->setUrl(URL::to('admin/lead/lead'))
            ->noScript();

        return View::make('admin/lead')->with(array('data'=>$lead));
    }
    public function getLead(){

        $query = Lead::where(function($query){$query->whereNULL('fi_initiation')->orwhere('fi_initiation',1);})->where('decline_lead','=', '0')->get();


        return Datatable::collection($query)
            //->showColumns('name', 'lead_type')
            ->addColumn('name', function($model){
                if(!empty($model->company_name)){
                    $user = ucfirst($model->company_name) ."(".$model->constitution." - ".$model->company_category.")";
                }
                else{
                    $user = ucfirst($model->lead_first_name)." ".ucfirst($model->lead_last_name);
                }
                //(!empty($model->company_name))? ucfirst($model->lead_first_name)." ".ucfirst($model->lead_last_name):ucfirst($model->company_name) ."(".$model->constiution." - ".$model->company_category.")"
                $label = "";
                if($model->fi_initiation == "" && $model->approve_lead == "0"){
                    $label = "<sup class='stages_up'><span class='badge badges_up label-success'>New</span></sup>";

                }
                if($model->fi_initiation == "1" && $model->approve_lead == "1"){
                    $label = "<sup class='stages_up'><span class='badge badges_up label-warning'>FI</span></sup>";
                }
                if(!role_permission('2','lead_profile')){
                    return "<span><button disabled='' class='btn btn-info btn-sm'>".$user." </button>$label</span>";
                }elseif(role_permission('2','lead_profile')){
                return "<span><a href='lead/leadtimeline/".$model->id."'><button class='btn btn-info btn-sm'>".$user." </button></a>$label</span>";
                }

            })
            ->addColumn('product', function($model){
            	if(!empty($model->product()->first()->product)){
                	return ucfirst($model->product()->first()->product);
                	}
            })


            ->addColumn('lead_to', function($model){
                //$lead_from = User::get();
                $role_id=Auth::admin()->get()->is_superuser;
                if($role_id==8||$role_id==7||$role_id==6){
                    $lead_from = User::where('active','1')->get();    
                }elseif($role_id==5){
                    $lead_from = User::where('branch_id',Auth::admin()->get()->branch_id)->where('is_superuser','<',5)->orwhere('id',Auth::admin()->get()->id)->where('active','1')->get();
                }elseif($role_id==4){
                    $lead_from = User::where('branch_id',Auth::admin()->get()->branch_id)->where('is_superuser','<',4)->where('parent_id',Auth::admin()->get()->id)->orwhere('id',Auth::admin()->get()->id)->where('active','1')->get();
                }elseif($role_id==3||$role_id==2){
                    $lead_from = User::where('branch_id',Auth::admin()->get()->branch_id)->where('is_superuser',$role_id)->where('created_by',Auth::admin()->get()->id)->orwhere('id',Auth::admin()->get()->id)->where('active','1')->get();
                }

                $opt = '';
                $selected='';
                $opt .="<option value='0'>None</option>";
                foreach($lead_from as $lead_froms){

                    $opt .= "<option value='".$lead_froms->id ."'";

                    if($lead_froms->id == $model->lead_to)
                    {
                        $opt .= "selected";
                    }
                    $opt .= ">".$lead_froms->first_name." ".$lead_froms->last_name." - ".$lead_froms->superuser()->first()->is_superuser."</option>";

                }
                if(!role_permission('2','lead_assign')){
                    return "<select class='lead_to' disabled='' id='".$model->id."'>".$opt."</select>";
                }elseif(role_permission('2','lead_assign')){
                    return "<select class='lead_to' id='".$model->id."'>".$opt."</select>";
                }

            })
            ->addColumn('forward_lead', function($model){
                //$lead_from = User::get();
                $role_id=Auth::admin()->get()->is_superuser;
                if($role_id==8||$role_id==7||$role_id==6){
                    $lead_from = User::where('active','1')->get();    
                }elseif($role_id==5){
                    $lead_from = User::where('branch_id',Auth::admin()->get()->branch_id)->where('is_superuser','<',5)->orwhere('id',Auth::admin()->get()->id)->where('active','1')->get();
                }elseif($role_id==4){
                    $lead_from = User::where('branch_id',Auth::admin()->get()->branch_id)->where('is_superuser','<',4)->where('parent_id',Auth::admin()->get()->id)->orwhere('id',Auth::admin()->get()->id)->where('active','1')->get();
                }elseif($role_id==3||$role_id==2){
                    $lead_from = User::where('branch_id',Auth::admin()->get()->branch_id)->where('is_superuser',$role_id)->where('created_by',Auth::admin()->get()->id)->orwhere('id',Auth::admin()->get()->id)->where('active','1')->get();
                }

                $opt = '';
                $selected='';
                $opt .="<option value='0'>None</option>";
                foreach($lead_from as $lead_froms){


                    $opt .= "<option value='".$lead_froms->id ."'";

                    if($lead_froms->id == $model->forward_to)
                    {
                        $opt .= "selected";
                    }
                    $opt .= ">".$lead_froms->first_name." ".$lead_froms->last_name." - ".$lead_froms->superuser()->first()->is_superuser."</option>";

                }
                if(!role_permission('2','lead_forward')){
                    return "<select class='forward_lead' disabled='' id='f_".$model->id."'>".$opt."</select>";
                }elseif(role_permission('2','lead_forward')){
                    return "<select class='forward_lead' id='f_".$model->id."'>".$opt."</select>";
                }


            })

            ->addColumn('enquiry_type', function($model){

                if($model->enquiry_type == 'Hot'){
                    return '<span class="text-danger" title="Hot"><i class="entypo-bell"></i>Hot</span>';

                }
                if($model->enquiry_type == 'Warm'){
                    return '<span class="text-success" title="Warm"><i class="fa fa-star"></i>Warm</span>';
                }
                if($model->enquiry_type == 'Cold'){
                    return '<span class="text-info" title="Cold"><i class="fa fa-warning"></i>Cold</span>';
                }
            })
            ->addcolumn('decline',function($model){

                if($model->approve_lead == 0){
                    if(!role_permission('2','lead_decline')){
                        return "<button class='btn btn-danger btn-xs decline' disabled='' id='".$model->id."' >Decline</button>";
                    }elseif(role_permission('2','lead_decline')){
                        return "<button class='btn btn-danger btn-xs decline' id='".$model->id."' >Decline</button>";
                    }

                }

            })

            ->addColumn('action', function($model){


                //$delete = <button class='btn btn-xs btn-danger'>Delete</button></a></div>;
                /*if($model->approve_lead == 0){
                    $lead = "<a href='approve/index/" . $model->id . "'><button class='btn btn-info btn-xs' ><i class='fa fa-folder-open'></i>Approve</button></a>";
                }*/
                $lead = "";

                if($model->approve_lead == 1 && $model->fi_initiation >= 1){
                    $fi = FiInitiation::where('lead_id',$model->id)->count();
                    if($fi == 0){
                        if(!role_permission('2','lead_fiinitiation')){
                            $lead = "<button class='btn btn-success btn-xs fi_initiation' disabled='' type='button' id='".$model->id."'>FI Initiation</button>";
                        }elseif(role_permission('2','lead_fiinitiation')){
                            $lead = "<button class='btn btn-success btn-xs fi_initiation' type='button' id='".$model->id."'>FI Initiation</button>";
                        }

                    }

                }

                $add_perm='';
                /*$role_id=Auth::admin()->get()->id;
                if(role_permission('add') AND $role_id == $model->lead_to){
                    return true;
                    //$add_perm="<a href='approve/index/" . $model->id . "'><button class='btn btn-info btn-xs' >Approve</button></a>";
                }*/
                $ci = Coapplicant::where('lead_id',$model->id)->count();

                if(!role_permission('2','lead_approve')){
                    $approve="<button class='btn btn-info btn-xs' disabled=''>Approve</button>";
                }elseif(role_permission('2','lead_approve')){
                    $approve="<a href='approve/index/" . $model->id . "'><button class='btn btn-info btn-xs' >Approve</button></a>";
                }


                if(!role_permission('2','lead_coapplicant')){
                    $coapplicant="<button class='btn btn-info btn-xs' disabled=''>Co-Applicant
                    <sup class='stages_up'><span class='badge badges_up label-success'>$ci</span></sup></button>";
                }elseif(role_permission('2','lead_coapplicant')){
                    $coapplicant="<a href='co-applicant/index/" . $model->id . "'><button class='btn btn-info btn-xs'>Co-Applicant
                    <sup class='stages_up'><span class='badge badges_up label-success'>$ci</span></sup></button></a>";
                }



                return "<div class='btn-group-xs'>$approve.$coapplicant.$lead";
                //<a href='lead/edit/" . $model->id . "'><button class='btn btn-xs btn-warning'>Edit</button></a>
            })

            ->searchColumns('name','company_name','type','product','enquiry_type','created_at')
            ->orderColumns('name')
            ->make();

    }

    public function getLeadtimeline($id){

        $lead = Lead::where('id','=',$id)->first();
        $meeting = Meeting::where('lead_id','=',$id)->get();
          return View::make('admin/lead_timeline')->with(array('leads' => $lead, 'meetings' => $meeting));
    }

    public function postLeadtimeline($id){

        $rules = array(
            'meeting_date' =>'required',
            'meeting_medium' =>'required',
            'meeting_topic' =>'required'
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return Redirect::back()
                ->withInput()->withErrors($validator);
        }
        else{
            $meeting_date =date('d',strtotime(Input::get('meeting_date')));
            $meeting_month =date('m',strtotime(Input::get('meeting_date')));
            $meeting_year =date('Y',strtotime(Input::get('meeting_date')));

            $meeting_date;
            $meeting_month;
            $meeting_year;
            //$search = DB::statement("");
            //echo $search =   DB::raw("select * from meetings where Year(meeting_date) = '$meeting_year' and Month(meeting_date) = '$meeting_month' and Day(meeting_date) = '$meeting_date' and lead_id = '$id'")->get();
            //echo $search = Meeting::where(DB::raw('DAY(meeting_date)'), $meeting_date)->where(DB::raw('MONTH(meeting_date)'),$meeting_month)->count();
            $search = Meeting::where(DB::raw('DAY(meeting_date)'), $meeting_date)->where(DB::raw('MONTH(meeting_date)'),$meeting_month)->where('lead_id',$id)->count();

            if($search == 0){

                $meeting = new Meeting;
                $meeting->lead_id = $id;
                $meeting->meeting_date = Input::get('meeting_date');
                $meeting->meeting_topic = Input::get('meeting_topic');
                $meeting->meeting_medium = Input::get('meeting_medium');
                $meeting->lead_agent_id = Input::get('lead_agent_id');

                $meeting->save();
                if(Input::get('modal_input') == "yes"){
                    $data = array(
                        'status' => 'success',
                        'message' =>'Meeting Fixed.',

                    );

                    echo json_encode($data);
                }
                else{
                    return Redirect::back()->with('message','Meeting Fixed.');
                }


            }
            else{
                if(Input::get('modal_input') == "yes"){
                    $data = array(
                        'status' => 'fail',
                        'message' =>'Meeting Already Fixed!',

                    );

                    echo json_encode($data);
                }
                else{
                return Redirect::to("admin/lead/leadtimeline/$id")
                    ->with('warning','Meeting Already Fixed!');
                }

            }

        }

    }
    public function postDiscussion(){
        $id = Input::get('meeting_id');
        $discus = Input::get('meeting_discussion');
        $meeting_status=Input::get('meeting_status');

        $edit = array(
            'meeting_discussion' => $discus,
            'meeting_status'=>$meeting_status
        );
        Meeting::where('id',$id)->update($edit);
        $data = array(
            'status' => 'success',
            'message' =>'Updated succesfully.',
            'id' => $id
        );
        echo json_encode($data);

    }

    public function postRemarks(){
        $id = Input::get('remark_id');
        $remarks = Input::get('remarks');


        $edit = array(
            'remarks' => $remarks
        );
        Meeting::where('id',$id)->update($edit);
        $data = array(
            'status' => 'success',
            'message' =>'Remarks Updated successfully',
            'id' => $id
        );
        echo json_encode($data);

    }
    public function getDelleadtimeline(){
        $id = Input::get('id');
        $lead = Meeting::where('id',$id)->first();
        $lead_id = $lead->lead_id;
        Meeting::where('id',$id)->delete();

        $data = array(
            'id' => $lead_id
        );
        header('content-type: application/json');
        echo json_encode($data);
        //return Redirect::back()->with('message','Lead successfully deleted.');

    }
    public function getAdd(){

        $product = Product::where('status','1')->get();
        $lead_type = LeadType::where('status','1')->get();

        //$lead_from = User::get();
        $lead_from="";
        $role_id=Auth::admin()->get()->is_superuser;
        if($role_id==8||$role_id==7||$role_id==6){
            $lead_from = User::where('active','1')->get();    
        }elseif($role_id==5){
            $lead_from = User::where('branch_id',Auth::admin()->get()->branch_id)->where('is_superuser','<',5)->orwhere('id',Auth::admin()->get()->id)->where('active','1')->get();
        }elseif($role_id==4){
            $lead_from = User::where('branch_id',Auth::admin()->get()->branch_id)->where('is_superuser','<',4)->where('parent_id',Auth::admin()->get()->id)->orwhere('id',Auth::admin()->get()->id)->where('active','1')->get();
        }elseif($role_id==3||$role_id==2){
            $lead_from = User::where('branch_id',Auth::admin()->get()->branch_id)->where('is_superuser',$role_id)->where('created_by',Auth::admin()->get()->id)->orwhere('id',Auth::admin()->get()->id)->where('active','1')->get();
        }

        $branch = Branch::where('status','1')->get();
        return View::make('admin/lead_add')->with(array('products' => $product,'lead_types'=>$lead_type,'lead_froms'=>$lead_from,'branches' => $branch));
    }
    public function postSave($id){
        if($id == 0){
            $rules = array(
                'lead_first_name' =>'required',
                'type' => 'required',
                //'guardian' => 'required_if:type,person',
                //'guardian_first_name' => 'required_if:type,person',
                //'guardian_last_name' => 'required_if:type,person',
                'company_name' => 'required_if:type,firm',
                //'constitution' => 'required_if:type,firm',
                //'company_category' => 'required_if:type,firm',
                //'meeting_medium' => 'required',
                //'email_id' => 'email|unique:leads',
                'phone' => 'required|numeric',
                'product_id' => 'required',
                //'lead_type_id' => 'required',
                //'lead_from' => 'required',
                /*'branch_id' => 'required'*/

            );

            $validator = Validator::make(Input::all(),$rules);
            if($validator->fails()){
                return Redirect::back()
                    ->withInput()
                    ->withErrors($validator);
            }
            else{

                // gets 2 digit branch id
                $branch_id = Input::get('branch_id');
                if($branch_id < 10){

                   $branch_id = "0".$branch_id;
                }

                //gets 2 digit product id
                $product_id = Input::get('product_id');
                if($product_id < 10){

                    $product_id = "0".$product_id;
                }

                //gets current month in alphabet
                $month = array(
                    "01" => 'A',
                    "02" => 'B',
                    "03" => 'C',
                    "04" => 'D',
                    "05" => 'E',
                    "06" => 'F',
                    "07" => 'G',
                    "08" => 'H',
                    "09" => 'I',
                    "10" => 'J',
                    "11" => 'K',
                    "12" => 'L'
                );
                $current_month = date('m');
                $month[$current_month];

                //gets last 2 digits of current month
                $current_year = date('y');

                //gets last 2 digits of current date
                $current_date = date('d');
                if($current_date < 10){
                    $current_date = "0".$current_date;
                }

                //generates 3 digit ramdom number
                $digits = 3;
                $random_num = rand(pow(10, $digits-1), pow(10, $digits)-1);

                //final calculated customer id
                $customer_id = $branch_id.$product_id.$month[$current_month].$current_year.$current_date.$random_num;

                //saves record to database with generated 12 digit customer id

                $lead = new Lead;

                $lead->lead_first_name = Input::get('lead_first_name');
                $lead->lead_middle_name = Input::get('lead_middle_name');
                $lead->lead_last_name = Input::get('lead_last_name');
                $lead->type = Input::get('type');
                $lead->company_name = Input::get('company_name');
                $lead->constitution = Input::get('constitution');
                $lead->company_category = Input::get('company_category');
                $lead->guardian = Input::get('guardian');
                $lead->guardian_first_name = Input::get('guardian_first_name');
                $lead->guardian_middle_name = Input::get('guardian_middle_name');
                $lead->guardian_last_name = Input::get('guardian_last_name');
                $lead->meeting_medium = Input::get('meeting_medium');
                $lead->enquiry_type = Input::get('enquiry_type');
                $lead->email_id = Input::get('email_id');
                $lead->phone = Input::get('phone');
                $lead->lead_type_id = Input::get('lead_type_id');
                $lead->lead_from = Input::get('lead_from');
                $lead->lead_to = Auth::admin()->id();
                /*$lead->branch_id = Input::get('branch_id');*/
                $lead->product_id = Input::get('product_id');
                $lead->customer_id = $customer_id;

                $lead->save();

                $last_id = DB::getPdo()->lastInsertId();

                $asset_exist = new AssetDetail;
                $asset_exist->lead_id = $last_id;
                $asset_exist->update = "";
                $asset_exist->save();

                $reference_exist = new Reference;

                $reference_exist->lead_id = $last_id;
                $reference_exist->update = "";
                $reference_exist->save();

                $financial_exist = new Finance;

                $financial_exist->lead_id = $last_id;
                $financial_exist->update = "";
                $financial_exist->save();

            }

                return Redirect::to('admin/lead')->with('message','Lead successfully saved.');
        }
        else{
            $lead = Lead::find($id);

            $branch_id = $lead->branch_id;
            if($branch_id < 10){

                $branch_id = "0".$branch_id;
            }

            //gets 2 digit product id
            $product_id = $lead->branch_id;
            if($product_id < 10){

                $product_id = "0".$product_id;
            }
            //gets current month in alphabet
            $month = array(
                "01" => 'A',
                "02" => 'B',
                "03" => 'C',
                "04" => 'D',
                "05" => 'E',
                "06" => 'F',
                "07" => 'G',
                "08" => 'H',
                "09" => 'I',
                "10" => 'J',
                "11" => 'K',
                "12" => 'L'
            );
            $current_month = date('m');
            $month[$current_month];

            //gets last 2 digits of current month
            $current_year = date('y');

            //gets last 2 digits of current date
            $current_date = date('d');
            if($current_date < 10){
                $current_date = "0".$current_date;
            }

            //generates 3 digit ramdom number
            $digits = 3;
            $random_num = rand(pow(10, $digits-1), pow(10, $digits)-1);

            //final calculated customer id
            $customer_id = $branch_id.$product_id.$month[$current_month].$current_year.$current_date.$random_num;

            //saves record to database with generated 12 digit customer id
            $lead_exist = new Lead;

            $lead_exist->lead_first_name = $lead->lead_first_name;
            $lead_exist->lead_middle_name = $lead->lead_middle_name;
            $lead_exist->lead_last_name = $lead->lead_last_name;
            $lead_exist->guardian = $lead->guardian;
            $lead_exist->guardian_first_name = $lead->guardian_first_name;
            $lead_exist->guardian_middle_name = $lead->guardian_middle_name;
            $lead_exist->guardian_last_name = $lead->guardian_last_name;
            $lead_exist->meeting_medium = $lead->meeting_medium;
            $lead_exist->enquiry_type = $lead->enquiry_type;
            $lead_exist->email_id = $lead->email_id;
            $lead_exist->phone = $lead->phone;
            $lead_exist->lead_type_id = $lead->lead_type_id;
            $lead_exist->lead_from = $lead->lead_from;
            $lead_exist->lead_to = Auth::admin()->id();
            /*$lead_exist->branch_id = $lead->branch_id*/;
            $lead_exist->product_id = $lead->product_id;
            $lead_exist->existing_customer = $lead->id;
            $lead_exist->update = "no";
            $lead_exist->customer_id = $customer_id;

            $lead_exist->save();

            $last_id = DB::getPdo()->lastInsertId();

            /*$proof_exist = new Proof;

            $proof_exist->lead_id = $last_id;
            $proof_exist->update = "no";
            $proof_exist->save();*/

            $asset_exist = new AssetDetail;

            $asset_exist->lead_id = $last_id;
            $asset_exist->update = "no";
            $asset_exist->save();

            $reference_exist = new Reference;

            $reference_exist->lead_id = $last_id;
            $reference_exist->update = "no";
            $reference_exist->save();

            $financial_exist = new Finance;

            $financial_exist->lead_id = $last_id;
            $financial_exist->update = "no";
            $financial_exist->save();

            return Redirect::to('admin/lead')->with('message','Lead successfully saved.');
        }

    }

    public function getEdit($id){

        $lead = Lead::where('id',$id)->first();

        $product = Product::get();
        $lead_type = LeadType::get();
        $lead_from = User::get();
        $branch = Branch::get();

        return View::make('admin/lead_edit')->with(array('leads' => $lead,'products' => $product,'lead_types'=>$lead_type,'lead_froms'=>$lead_from,'branches' => $branch));

    }
    public function postUpdate($id){

        $rules = array(
            'lead_first_name' =>'required',
            'type' => 'required',
            //'guardian' => 'required_if:type,person',
            //'guardian_first_name' => 'required_if:type,person',
            //'guardian_last_name' => 'required_if:type,person',
            'company_name' => 'required_if:type,firm',
            //'constitution' => 'required_if:type,firm',
            //'company_category' => 'required_if:type,firm',
            //'meeting_medium' => 'required',
            //'email_id' => 'email',
            'phone' => 'required|numeric',
            'product_id' => 'required',
            //'lead_type_id' => 'required',
            //'lead_from' => 'required',
            /*'branch_id' => 'required'*/
        );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return Redirect::back()
                ->withInput()->withErrors($validator);
        }
        else{

            $lead = Lead::find($id);

            $lead->lead_first_name = Input::get('lead_first_name');
            $lead->lead_middle_name = Input::get('lead_middle_name');
            $lead->lead_last_name = Input::get('lead_last_name');
            $lead->type = Input::get('type');
            if(Input::get('type') == 'person'){
                $lead->guardian = Input::get('guardian');
                $lead->guardian_first_name = Input::get('guardian_first_name');
                $lead->guardian_middle_name = Input::get('guardian_middle_name');
                $lead->guardian_last_name = Input::get('guardian_last_name');

                $lead->company_name = "";
                $lead->constitution = "";
                $lead->company_category = "";
            }
            else{
                $lead->company_name = Input::get('company_name');
                $lead->constitution = Input::get('constitution');
                $lead->company_category = Input::get('company_category');

                $lead->guardian ="";
                $lead->guardian_first_name = "";
                $lead->guardian_middle_name = "";
                $lead->guardian_last_name = "";
            }
            $lead->meeting_medium = Input::get('meeting_medium');
            $lead->enquiry_type = Input::get('enquiry_type');
            $lead->email_id = Input::get('email_id');
            $lead->phone = Input::get('phone');
            $lead->lead_type_id = Input::get('lead_type_id');
            $lead->lead_from = Input::get('lead_from');
            /*$lead->branch_id = Input::get('branch_id');*/
            $lead->product_id = Input::get('product_id');

            $lead->save();

            return Redirect::to('admin/lead')->with('message','Lead successfully updated.');

        }

    }
    public function postLeadto(){

        Input::get('user_id');
        $lead_id = Input::get('lead_id');

        $lead = Lead::find($lead_id);

        $lead->lead_to = Input::get('user_id');
        $lead->save();



    }
    public function postForwardto(){

        Input::get('user_id');
        $lead_id = Input::get('lead_id');
        //$assign_lead=Input::get('assign_lead');
        $t = explode('_',$lead_id);

        $lead = Lead::find($t[1]);

        $lead->forward_to = Input::get('user_id');
        $lead->save();

    }

    public function getDelete($id){

        Lead::where('id',$id)->delete();

        return Redirect::to('admin/lead')->with('message','Lead successfully deleted.');

    }
    public function getDecline(){
        if(!module_permission('3'))
        {
            return Redirect::to('admin/')->send();
        }
         //'Product','Arrival','Reason','Decline By','Action'
        $lead = Datatable::table()
            ->addColumn('Lead','Product','Arrival','Reason','Decline By','Action')
            ->setUrl(URL::to('admin/lead/declinerecord'))
            ->noScript();

        return View::make('admin/decline')->with(array('data'=>$lead));


    }

    public function getDeclinerecord()
    {
        
        $query = Decline::whereIn('lead_id',users())->get();
        
        return Datatable::collection($query)
            
            ->addColumn('name',function($model){

            $role_id=Auth::admin()->get()->is_superuser;
            $branch_id=Auth::admin()->get()->branch_id;
            $id=Auth::admin()->get()->id;
                if($role_id==5 || $role_id==4){
                    if($model->user()->first()->branch_id == $branch_id  &&  $model->user()->first()->is_superuser < $role_id || $model->decline_by == $id){
                        return "<Strong class='text-info'>Name:</strong> ".ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name)."<br><Strong class='text-info'>Phone:</strong> ".$model->lead()->first()->phone."<br><Strong class='text-info'>email:</strong>".$model->lead()->first()->email_id;
                    }
                }elseif($role_id==3 || $role_id==2){
                    if($model->user()->first()->branch_id == $branch_id  &&  $model->decline_by == $id ){
                        return "<Strong class='text-info'>Name:</strong> ".ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name)."<br><Strong class='text-info'>Phone:</strong> ".$model->lead()->first()->phone."<br><Strong class='text-info'>email:</strong>".$model->lead()->first()->email_id;
                    }
                }else{
                    return "<Strong class='text-info'>Name:</strong> ".ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name)."<br><Strong class='text-info'>Phone:</strong> ".$model->lead()->first()->phone."<br><Strong class='text-info'>email:</strong>".$model->lead()->first()->email_id;
                }
               
                //return "<Strong class='text-info'>Name:</strong> ".ucfirst($model->lead()->first()->lead_first_name)." ".ucfirst($model->lead()->first()->lead_last_name)."<br><Strong class='text-info'>Phone:</strong> ".$model->lead()->first()->phone."<br><Strong class='text-info'>email:</strong>".$model->lead()->first()->email_id;
                
            })

            ->addColumn('product', function($model){
                return ucfirst($model->lead()->first()->product()->first()->product);
            })
            ->addColumn('Arrival', function($model){
                return date('d M, y',strtotime($model->lead()->first()->created_at));
            })
            ->addColumn('reason', function($model){
                return "<Strong class='text-info'>Reason:</strong> ".$model->reason."<br><Strong class='text-info'>Comment:</strong>".$model->comment;
            })
            ->addColumn('decline_by', function($model){
                return ucfirst($model->user()->first()->first_name)." ".ucfirst($model->user()->first()->last_name);
            })

            ->addColumn('action', function($model){
                $delete="";
                if(!role_permission('3','declinelead_delete')){
                    $delete="<button class='btn btn-xs btn-danger' disabled >Delete</button>";
                  //  return "<button class='btn btn-xs btn-success recycle' id='".$model->id."'>Recycle</button> <button class='btn btn-xs btn-danger delete' disabled id='".$model->id."'>Delete</button>";
                 }elseif(role_permission('3','declinelead_delete')){
                    //return "<button class='btn btn-xs btn-success recycle' id='".$model->id."'>Recycle</button> <button class='btn btn-xs btn-danger delete' id='".$model->id."'>Delete</button>";
                    $delete="<button class='btn btn-xs btn-danger delete' id='".$model->id."'>Delete</button>";
                }
                $recycle="";
                if(!role_permission('3','declinelead_recycle')){
                    $recycle="<button class='btn btn-xs btn-success ' disabled >Recycle</button> ";
                }elseif(role_permission('3','declinelead_recycle')){
                    $recycle="<button class='btn btn-xs btn-success recycle' id='".$model->id."'>Recycle</button> ";
                }

                return $delete.$recycle;

            })


            ->searchColumns('name')
            ->orderColumns('id')
            ->make();

    }
    public function postDecline(){


        $id = Auth::admin()->id();

        $decline = New Decline;
        $decline->decline_by = $id;
        $decline->lead_id = Input::get('lead_id');
        $decline->reason = Input::get('reason');
        $decline->comment = Input::get('comment');

        $decline->save();

        $lead = Lead::find(Input::get('lead_id'));
        $lead->decline_lead = 1;

        $lead->save();

        $data = array(
            'status' => 'success',
            'message' => 'Lead is successfully Decline'
        );

        header('content-type:application/json');
        echo json_encode($data);
    }
    public function getRecycle(){
        $id = Input::get('id');
        $decline = Decline::find($id);

        $lead_id = $decline->lead_id;

        $lead = Lead::find($lead_id);
        $lead->decline_lead = 0;

        $lead->save();

        Decline::find($id)->delete();

        header('content-type:application/json');
        $data = array(
            'status' => 'success',
            'message' => 'Lead is Successfully Recycled'
        );
        echo json_encode($data);

    }
    public function getDeclinedelete(){
        $id = Input::get('id');
        $decline = Decline::find($id);

        $lead_id = $decline->lead_id;

        Lead::find($lead_id)->delete();
        Decline::find($id)->delete();
        header('content-type:application/json');
        $data = array(
            'status' => 'success',
            'message' => 'Lead is Successfully Recycled'
        );
        echo json_encode($data);

    }
    public function postExistingcustomer(){

        $lead_first_name = Input::get('lead_first_name');
        $lead_middle_name = Input::get('lead_middle_name');
        $lead_last_name = Input::get('lead_last_name');
        $phone = Input::get('phone');
        $guardian = Input::get('guardian');
        $guardian_first_name =Input::get('guardian_first_name');
        $guardian_middle_name =Input::get('guardian_middle_name');
        $guardian_last_name = Input::get('guardian_last_name');



        $search = Lead::Where(function($query) use($lead_first_name,$lead_last_name,$phone){


                  $query->where('lead_first_name',$lead_first_name)
                          ->where('lead_last_name',$lead_last_name)
                          ->where('phone',$phone);
                  })
                  ->orWhere(function($query) use($lead_first_name,$lead_middle_name,$lead_last_name,$phone,$guardian,$guardian_first_name,$guardian_middle_name,$guardian_last_name){
                        $query->where('lead_first_name',$lead_first_name)
                          ->where('lead_middle_name',$lead_middle_name)
                          ->where('lead_last_name',$lead_last_name)
                          ->where('phone',$phone)
                          ->where('guardian',$guardian)
                          ->where('guardian_first_name',$guardian_first_name)
                          ->where('guardian_middle_name',$guardian_middle_name)
                          ->where('guardian_last_name',$guardian_last_name);

                   })
                  ->get();


        /*$queries = DB::getQueryLog();
        $last_query = end($queries);
        print_r($last_query);*/
        header('content-type: application/json');
        echo json_encode($search);
    }

}