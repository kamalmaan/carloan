<?php
class AdminAdminController extends BaseController{

    function __construct(){
        $this->beforeFilter('admin');
    }

    public function index(){
    	
        return View::make('admin/user');
    }
}