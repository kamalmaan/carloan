-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2015 at 05:14 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `puneet9_car`
--

-- --------------------------------------------------------

--
-- Table structure for table `asset_details`
--

CREATE TABLE IF NOT EXISTS `asset_details` (
`id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `update` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `asset_description` text NOT NULL,
  `chasis_number` varchar(255) NOT NULL,
  `asset_age` varchar(255) NOT NULL,
  `engine_number` varchar(255) NOT NULL,
  `manufacture_year` date DEFAULT NULL,
  `insurance_id` int(11) NOT NULL,
  `insurance_branch_id` int(11) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_details`
--

INSERT INTO `asset_details` (`id`, `lead_id`, `update`, `product_id`, `vehicle_id`, `asset_description`, `chasis_number`, `asset_age`, `engine_number`, `manufacture_year`, `insurance_id`, `insurance_branch_id`, `vehicle_number`, `created_at`, `updated_at`) VALUES
(1, 1, '', 9, 2, '', '', '', '', '0000-00-00', 0, 0, '', '2015-07-13 07:13:19', '2015-07-13 07:13:19'),
(2, 2, '', 9, 2, '', '', '', '', '0000-00-00', 0, 0, '', '2015-07-13 09:01:59', '2015-07-13 09:01:59'),
(3, 3, '', 9, 2, '', '', '', '', '0000-00-00', 0, 0, '', '2015-07-14 05:13:10', '2015-07-14 05:13:10'),
(4, 4, '', 5, 2, '', 'g5hynh', '', 'n7unun', '0000-00-00', 0, 0, '', '2015-07-14 07:38:16', '2015-07-14 07:38:16'),
(5, 5, '', 0, 0, '', '', '', '', NULL, 0, 0, '', '2015-07-14 07:46:29', '2015-07-14 07:46:29'),
(6, 6, '', 0, 0, '', '', '', '', NULL, 0, 0, '', '2015-07-29 04:58:24', '2015-07-29 04:58:24'),
(7, 7, '', 0, 0, '', '', '', '', NULL, 0, 0, '', '2015-07-31 05:45:04', '2015-07-31 05:45:04'),
(8, 8, '', 0, 0, '', '', '', '', NULL, 0, 0, '', '2015-07-31 10:13:53', '2015-07-31 10:13:53'),
(9, 9, '', 0, 0, '', '', '', '', NULL, 0, 0, '', '2015-08-07 10:29:06', '2015-08-07 10:29:06');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE IF NOT EXISTS `banks` (
`id` int(11) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `firm_tag` text NOT NULL,
  `single_tag` text NOT NULL,
  `bank_form` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `bank`, `firm_tag`, `single_tag`, `bank_form`, `status`, `created_at`, `updated_at`) VALUES
(1, 'panjab and sind bank', '3jhj,hjhj', 'hjh,ghjgjh', 'zmwBDYacgFblog2.jpg', 0, '2015-05-01 09:50:39', '2015-05-01 09:50:39'),
(2, 'HDFC', '', '', '', 1, '2015-05-01 09:57:49', '2015-05-01 09:57:49'),
(3, 'ICICI', '', '', '', 1, '2015-05-01 09:58:05', '2015-05-01 09:58:05'),
(4, 'UCO', '', '', '', 1, '2015-05-05 07:29:27', '2015-05-05 07:29:27'),
(8, 'afasfasf', 'sfaf', 's,fsaf,sadf', 'IoMPrMdwSPtask_report.docx', 1, '2015-06-19 11:43:27', '2015-06-19 11:43:27'),
(9, 'union bank of india', 'e2kjfjfjjknjjj,rff', 'rrf,rfrf', '', 1, '2015-07-29 10:57:11', '2015-07-29 10:57:11'),
(10, 'state bank of patiala', 'ghgghd', 'udjdhj', '', 1, '2015-07-29 11:10:59', '2015-07-29 11:10:59'),
(11, 'gramin banks', '', '', '', 1, '2015-07-29 11:31:03', '2015-07-29 11:31:03');

-- --------------------------------------------------------

--
-- Table structure for table `bank_branches`
--

CREATE TABLE IF NOT EXISTS `bank_branches` (
`id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `branch` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_branches`
--

INSERT INTO `bank_branches` (`id`, `bank_id`, `status`, `branch`, `contact_person`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '5', 'kamal', 'sfhgds@gmail.comssssssssssss', '13123', '2015-05-29 09:02:00', '2015-05-29 09:02:00'),
(2, 1, 1, '5', 'hfh', 'gdg@gmail.dsfjiosdj', '4564', '2015-05-29 09:36:09', '2015-05-29 09:36:09'),
(3, 1, 1, '1', 'ghjg', 'fhgg@fmil.bom', '45', '2015-05-29 10:37:19', '2015-05-29 10:37:19'),
(4, 1, 0, '2', 'hjhj', 'df@ff.vom', '13123', '2015-05-29 10:40:57', '2015-05-29 10:40:57'),
(5, 1, 1, '5', 'jkk', 'klhkjh@fm.vom', '54545', '2015-05-29 10:43:00', '2015-05-29 10:43:00'),
(6, 1, 1, '2', 'ryty65666666666666666', 'hfgh@gmail.com', '56567', '2015-05-29 10:44:51', '2015-05-29 10:44:51'),
(7, 2, 1, '4', 'sdgdg', 'djgh@fhj.ovcm', '43645', '2015-05-29 10:52:15', '2015-05-29 10:52:15'),
(8, 2, 1, '2', 'dgdg', 'gfdgdfg@hsfg.viok', '3434', '2015-05-29 10:52:35', '2015-05-29 10:52:35'),
(9, 1, 1, '7', 'ghjgj', 'fgjs@gmail.com', '57567', '2015-05-30 11:18:14', '2015-05-30 11:18:14'),
(10, 2, 1, '38', 'pankaj', 'sbejgn@gmail.com', '9417521709', '2015-06-23 10:03:25', '2015-06-23 10:03:25'),
(11, 3, 1, '2', 'ranjit', 'ranjit00923@gmail.com', '1234567', '2015-07-03 04:50:11', '2015-07-03 04:50:11'),
(12, 3, 1, '2', 'shubham', 'shubham00923@gmail.com', '9780371204', '2015-07-30 05:37:27', '2015-07-30 05:37:27');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE IF NOT EXISTS `branches` (
`id` int(11) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `branch`, `address`, `status`, `created_at`, `updated_at`) VALUES
(31, '7', 'above mega mart', 1, '2015-08-07 07:19:17', '2015-08-07 07:19:17'),
(49, '18', 'near realiance fresh', 1, '2015-08-07 12:22:25', '2015-08-07 12:22:25'),
(66, '18', 'abc', 1, '2015-08-10 06:36:22', '2015-08-10 06:36:22'),
(82, '3', 'abcde', 1, '2015-08-10 08:41:40', '2015-08-10 11:24:24'),
(83, '156', 'ss', 1, '2015-08-10 10:33:59', '2015-08-10 10:34:05'),
(84, '5', 'rt', 1, '2015-08-10 11:36:16', '2015-08-10 11:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
`id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=269 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `state_id`, `district_id`, `city`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 'Abadi along Khairabad on Ram Tirath Road to Ajnala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 5, 'Abadi along Pandori Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 5, 'Abadi Baba Darshan Singh on Ram Tirath Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 1, 'Abadi Baba Jiwan Singh on Ram Tirath Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 2, 'Abadi Khalsa Nagar Near Kot Mit Singh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, 3, 'Abadi Mirankot on Ajnala Road', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 4, 'Abadi Naushera', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 1, 1, 'Abohar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 1, 3, 'Adampur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 1, 3, 'Ahmedgarh', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 1, 5, 'Ajnala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 1, 6, 'Akalgarh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 1, 6, 'Alawalpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 1, 6, 'Alhoran', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 1, 6, 'Alipur adjacent to Village Mithapur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 1, 6, 'Amargarh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, 5, 'Amloh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 1, 7, 'Amritsar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 1, 7, 'Anandpur Sahib', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 1, 1, 'Apra', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 1, 1, 'Aur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 1, 1, 'Baba Bakala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 1, 1, 'Baddowal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 1, 1, 'Badhni Kalan', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 1, 1, 'Bagha Purana', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 1, 0, 'Balachaur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 1, 0, 'Balongi', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 1, 0, 'Banga', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 1, 0, 'Banur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 1, 0, 'Bara Khankot Doburjee', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 1, 0, 'Bareta', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 1, 0, 'Bariwala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 1, 0, 'Barnala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 1, 0, 'Baryar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 1, 0, 'Basarka Bhaini (Attari Road)', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 1, 0, 'Bassi Pathana', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 1, 0, 'Batala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 1, 0, 'Bathinda', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 1, 0, 'Begowal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 1, 0, 'Behrampur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 1, 0, 'Bhadaur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 1, 0, 'Bhadson', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 1, 0, 'Bhagta Bhai Ka', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 1, 0, 'Bhamian Kalan', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 1, 0, 'Bhattian', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 1, 0, 'Bhawanigarh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 1, 0, 'Bhikhi', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 1, 0, 'Bhikhiwind', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 1, 0, 'Bhode - di – Khui', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 1, 0, 'Bhogpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 1, 0, 'Bhucho Mandi', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 1, 0, 'Bhulath', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 1, 0, 'Bidhipur on Amritsar Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 1, 0, 'Budha Theh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 1, 0, 'Budhlada', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 1, 0, 'Bungal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 1, 0, 'Chachoki', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 1, 0, 'Chamar Tibbi, Moranwali, Model Town (R) and Gugape', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 1, 0, 'Chamkaur Sahib', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 1, 0, 'Cheema', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 1, 0, 'Chogawan', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 1, 0, 'Chohal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 1, 0, 'Chomon', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 1, 0, 'Colony along Jail Road on Dinanagar Road (adjacent', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 1, 0, 'Dalip Nagar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 1, 0, 'Daper', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 1, 0, 'Dashmesh Nagar on Rampura - Mehraj Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 1, 0, 'Dasua', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 1, 0, 'Daulatpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 1, 0, 'Dera Baba Nanak', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 1, 0, 'Dera Bassi', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 1, 0, 'Dhaki', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 1, 0, 'Dhanaula', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 1, 0, 'Dharamkot', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 1, 0, 'Dhariwal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 1, 0, 'Dhilwan', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 1, 0, 'Dhin', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 1, 0, 'Dhuri', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 1, 0, 'Dina Nagar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 1, 0, 'Dirba', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 1, 0, 'Doraha', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 1, 0, 'Duneke (Firozpur Road)', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 1, 0, 'Extended Area Block No.22', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 1, 0, 'Faridkot', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 1, 0, 'Fatehgarh Churian', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 1, 0, 'Fateh Nangal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 1, 0, 'Fazilka', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 1, 0, 'Firozpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 1, 0, 'Firozpur Cantonment', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 1, 0, 'Gadriwala on Zira - Firozpur Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 1, 0, 'Gardhiwala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 1, 0, 'Garhshankar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 1, 0, 'Ghagga', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 1, 0, 'Ghanauli', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 1, 0, 'Ghanaur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 1, 0, 'Ghoh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 1, 0, 'Gidderbaha', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 1, 0, 'Gill', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 1, 0, 'Gobindgarh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 1, 0, 'Gobind Nagar Colony along Gurdaspur Amritsar Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 1, 0, 'Goniana', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 1, 0, 'Goraya', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 1, 0, 'Grid Colony PSEB on Bhadson Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 1, 0, 'Gumtala Colony on Jagdev Kalan Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 1, 0, 'Gurdaspur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 1, 0, 'Guru Har Sahai', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 1, 0, 'Halwara', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 1, 0, 'Handiaya', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 1, 0, 'Hariana', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 1, 0, 'Harijan Colony, I.T.I.Balmiki Graveyard & DAV Hr. ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 1, 0, 'Hazipur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 1, 0, 'Hoshiarpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 1, 0, 'Houses & Rice Shellers (Bholowal – Road)', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 1, 0, 'Hussainpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 1, 0, 'Iqbal Colony', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 1, 0, 'Jagraon', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 1, 0, 'Jaitu', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 1, 0, 'Jalalabad', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 1, 0, 'Jalandhar [Jullundur]', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 1, 0, 'Jalandhar Cantonment', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 1, 0, 'Jandiala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 1, 0, 'Jandiala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 1, 0, 'Jawahar Nagar on Rampura - Bathinda Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 1, 0, 'Jodhan', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 1, 0, 'Joshi Colony, Ram Tirath Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 1, 0, 'Jugial', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 1, 0, 'Kapurthala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 1, 0, 'Kartarpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 1, 0, 'Kathanian', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 1, 0, 'Khamanon', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 1, 0, 'Khambra on Nakodar Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 1, 0, 'Khanauri', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 1, 0, 'Khanna', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 1, 0, 'Khanpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 1, 0, 'Kharal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 1, 0, 'Kharar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 1, 0, 'Khem Karan', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 1, 0, 'Kheri Gujran', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 1, 0, 'Khilchian', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 1, 0, 'Khothran', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 1, 0, 'Korianwali', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 1, 0, 'Kot', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 1, 0, 'Kot Fatta', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 1, 0, 'Kothe Lal Premi', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 1, 0, 'Kot Ise Khan', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 1, 0, 'Kot Kapura', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 1, 0, 'Kotla Nihang', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 1, 0, 'Krishna Lane', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 1, 0, 'Kurali', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 1, 0, 'Lalru', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 1, 0, 'Lamin', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 1, 0, 'Landeke (Amritsar Road)', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 1, 0, 'Lehragaga', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 1, 0, 'Lohian Khass', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 1, 0, 'Longowal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 1, 2, 'Ludhiana', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 1, 0, 'Machhiwara', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 1, 0, 'Mahilpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 1, 0, 'Majitha', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 1, 0, 'Makhu', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 1, 0, 'Malerkotla', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 1, 0, 'Malikpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 1, 0, 'Mallanwala Khass', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 1, 0, 'Maloud', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 1, 0, 'Malout', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 1, 0, 'Mamun', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, 1, 0, 'Mansa', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, 1, 0, 'Manwal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, 1, 0, 'Maur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, 1, 0, 'Mehna', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, 1, 0, 'Mirpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, 1, 0, 'Moga', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, 1, 0, 'Moonak', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, 1, 0, 'Morinda', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, 1, 0, 'Mubarakpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, 1, 0, 'Mudal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, 1, 0, 'Mudki', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, 1, 0, 'Mukerian', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, 1, 0, 'Muktsar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, 1, 0, 'Mullanpur Dakha', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, 1, 0, 'Mullanpur Garib Dass', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, 1, 0, 'Nabha', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, 1, 0, 'Nakodar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, 1, 0, 'Nangal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, 1, 0, 'Nangal Karkhan adjacent to Master Mehnga Singh Col', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, 1, 0, 'Nangli', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, 1, 0, 'Narot Mehra', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, 1, 0, 'Naushera Khurd', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, 1, 0, 'Nawanshahr', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, 1, 0, 'Naya Gaon', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, 1, 0, 'Nehon', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, 1, 0, 'Nilpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, 1, 0, 'Nurmahal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, 1, 0, 'Oustee Colony Block-A', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, 1, 0, 'Oustee Colony Block-B', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, 1, 0, 'Outgrowth of Gandhi Nagar on Pitho Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, 1, 0, 'Partap Singhwala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, 1, 0, 'Parti Vihar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, 1, 0, 'Pathankot', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, 1, 0, 'Patiala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, 1, 0, 'Patran', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, 1, 0, 'Patti', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, 1, 0, 'Payal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, 1, 0, 'Phagwara', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, 1, 0, 'Phagwara Sharki', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, 1, 0, 'Phillaur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, 1, 0, 'Prem Nagar on Ajnala Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, 1, 0, 'Punjabi University', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, 1, 0, 'Qadian', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(210, 1, 0, 'Raggar Colony, Gali No. 1,2 & 3, Ram Tirath Road', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, 1, 0, 'Rahon', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, 1, 0, 'Raikot', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, 1, 0, 'Rail', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, 1, 0, 'Raipur Rasulpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(215, 1, 0, 'Raja Sansi', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(216, 1, 0, 'Rajpura', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, 1, 0, 'Rakri', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, 1, 0, 'Raman', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, 1, 0, 'Ramdas', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, 1, 0, 'Rampura Phul', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, 1, 0, 'Ranjit Nagar Extension – I', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, 1, 0, 'Ranjit Nagar Extension – II', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, 1, 0, 'Ranjit Vihar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, 1, 0, 'Rayya', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, 1, 0, 'Rupnagar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, 1, 0, 'Rurki Kasba', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, 1, 0, 'Sahnewal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, 1, 0, 'Saloh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, 1, 0, 'Samana', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, 1, 0, 'Samrala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, 1, 0, 'Sanaur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, 1, 0, 'Sangat', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, 1, 0, 'Sangrur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, 1, 0, 'Sansarpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, 1, 0, 'Sant Nagar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, 1, 0, 'Sarai Khas', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, 1, 0, 'Sardulgarh', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(238, 1, 0, 'Sarna', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(239, 1, 0, 'S.A.S. Nagar (Mohali, Ajitgarh)', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, 1, 0, 'Satyewala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, 1, 0, 'Sector – 66', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, 1, 0, 'Sector – 67', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, 1, 0, 'Sector – 68', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, 1, 0, 'Sector – 69', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, 1, 0, 'Shahkot', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, 1, 0, 'Sham ChaurasI', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, 1, 0, 'Shikar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, 1, 0, 'Silver Estate', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, 1, 0, 'Sirhind Fatehgarh Sahib', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, 1, 0, 'Sohana', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, 1, 0, 'Sri Hargobindpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, 1, 0, 'Sufipind', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, 1, 0, 'Sujanpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, 1, 0, 'Sultanpur', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, 1, 0, 'Sunam', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, 1, 0, 'Talwandi Bhai', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, 1, 0, 'Talwandi Sabo', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, 1, 0, 'Talwara', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, 1, 0, 'Tapa', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, 1, 0, 'Tarn Taran', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, 1, 0, 'Thandewala', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, 1, 0, 'Tharial', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, 1, 0, 'ludhiana', 1, '2015-05-06 11:49:47', '2015-05-06 11:49:47'),
(264, 1, 0, 'ludhiana', 1, '2015-05-06 11:50:35', '2015-05-06 11:50:35'),
(265, 1, 3, 'ludhian', 1, '2015-05-06 11:51:51', '2015-05-06 11:51:51'),
(266, 1, 1, 'Modal City', 0, '2015-05-07 06:28:32', '2015-05-07 06:28:32'),
(267, 1, 1, 'modal add ', 1, '2015-05-07 07:07:18', '2015-05-07 07:07:18'),
(268, 0, 19, 'aassamcity', 1, '2015-05-27 12:27:10', '2015-05-27 12:27:10');

-- --------------------------------------------------------

--
-- Table structure for table `coapplicant_proofs`
--

CREATE TABLE IF NOT EXISTS `coapplicant_proofs` (
`id` int(11) NOT NULL,
  `co_applicant_id` int(11) NOT NULL,
  `owner_other_ownership_proof` varchar(255) NOT NULL,
  `owner_property_paper` varchar(255) NOT NULL,
  `owner_sevrage_bill` varchar(255) NOT NULL,
  `owner_electricity_bill` varchar(255) NOT NULL,
  `sign_other_proof` varchar(255) NOT NULL,
  `sign_passport_proof` varchar(255) NOT NULL,
  `sign_passport__number` varchar(255) NOT NULL,
  `sign_pan_card_proof` varchar(255) NOT NULL,
  `sign_pan_card_number` varchar(255) NOT NULL,
  `bank_bank_account_number` varchar(255) NOT NULL,
  `bank_bank_name` varchar(255) NOT NULL,
  `income_other_proof` varchar(255) NOT NULL,
  `income_fard` varchar(255) NOT NULL,
  `income_bank_2_proof` varchar(255) NOT NULL,
  `income_bank_1_proof` varchar(255) NOT NULL,
  `income_bank_account_number` varchar(255) NOT NULL,
  `income_bank_name` varchar(255) NOT NULL,
  `income_computation_2_proof` varchar(255) NOT NULL,
  `income_computaion_1_proof` varchar(255) NOT NULL,
  `income_computation_number` varchar(255) NOT NULL,
  `income_itr_2_proof` varchar(255) NOT NULL,
  `income_itr_1_proof` varchar(255) NOT NULL,
  `income_itr_number` varchar(255) NOT NULL,
  `resi_voter_card_proof` varchar(255) NOT NULL,
  `resi_voter_car_number` varchar(255) NOT NULL,
  `resi_passport_proof` varchar(255) NOT NULL,
  `resi_passport__number` varchar(255) NOT NULL,
  `resi_driving_license_proof` varchar(255) NOT NULL,
  `resi_driving_license__number` varchar(255) NOT NULL,
  `resi_rashan_id_proof` varchar(255) NOT NULL,
  `other_proof` varchar(255) NOT NULL,
  `proof_bank_proof` varchar(255) NOT NULL,
  `proof_bank_account_number` varchar(255) NOT NULL,
  `proof_arm_license_proof` varchar(255) NOT NULL,
  `proof_arm_license_number` varchar(255) NOT NULL,
  `proof_passport_proof` varchar(255) NOT NULL,
  `prrof_passport_number` varchar(255) NOT NULL,
  `proof_driving_license_proof` varchar(255) NOT NULL,
  `proof_driving_license_number` varchar(255) NOT NULL,
  `proof_adhar_card_proof` varchar(255) NOT NULL,
  `proof_adhar_card_number` varchar(255) NOT NULL,
  `proof_voter_card_proof` varchar(255) NOT NULL,
  `proof_voter_card_number` varchar(255) NOT NULL,
  `proof_pan_card_proof` varchar(255) NOT NULL,
  `proof_pan_card_number` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coapplicant_proofs`
--

INSERT INTO `coapplicant_proofs` (`id`, `co_applicant_id`, `owner_other_ownership_proof`, `owner_property_paper`, `owner_sevrage_bill`, `owner_electricity_bill`, `sign_other_proof`, `sign_passport_proof`, `sign_passport__number`, `sign_pan_card_proof`, `sign_pan_card_number`, `bank_bank_account_number`, `bank_bank_name`, `income_other_proof`, `income_fard`, `income_bank_2_proof`, `income_bank_1_proof`, `income_bank_account_number`, `income_bank_name`, `income_computation_2_proof`, `income_computaion_1_proof`, `income_computation_number`, `income_itr_2_proof`, `income_itr_1_proof`, `income_itr_number`, `resi_voter_card_proof`, `resi_voter_car_number`, `resi_passport_proof`, `resi_passport__number`, `resi_driving_license_proof`, `resi_driving_license__number`, `resi_rashan_id_proof`, `other_proof`, `proof_bank_proof`, `proof_bank_account_number`, `proof_arm_license_proof`, `proof_arm_license_number`, `proof_passport_proof`, `prrof_passport_number`, `proof_driving_license_proof`, `proof_driving_license_number`, `proof_adhar_card_proof`, `proof_adhar_card_number`, `proof_voter_card_proof`, `proof_voter_card_number`, `proof_pan_card_proof`, `proof_pan_card_number`, `created_at`, `updated_at`) VALUES
(3, 3, '', '', '', '', '', '', '', '07kRQJDdBUdescimages.jpeg', '123', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7xaNI7SYLNdescimages.jpeg', '123', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7xaNI7SYLNdescimages.jpeg', '123', '07kRQJDdBUdescimages.jpeg', '123', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, '', '', '', '', '', '', '', 'vfh1QhwLl8canberra_hero_image.jpg', '55511', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'WxM4Tz7D3Ximages.jpeg', '551545', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'WxM4Tz7D3Ximages.jpeg', '551545', 'vfh1QhwLl8canberra_hero_image.jpg', '55511', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, '', '', '', '', '', '', '', 'f8q20YvRhDFunny-Quotes-11.jpg', '12345', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'f8q20YvRhDFunny-Quotes-11.jpg', '12345', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 6, '', '', '', '', '', '', '', 'M4zClbh8diFunny-Quotes-11.jpg', '123', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'M4zClbh8diFunny-Quotes-11.jpg', '123', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 7, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 8, '', '', '', '', '', '', '', '79WYXbVq5S10470929_985610308115808_1636870564461191951_n.jpg', '23212', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'VuTOfIu5Faindex.jpeg', '231323', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'VuTOfIu5Faindex.jpeg', '231323', '79WYXbVq5S10470929_985610308115808_1636870564461191951_n.jpg', '23212', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 9, '', '', '', '', '', '', '', 'ITYJIMKNjfindex.jpeg', '23213', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 't8VANdS9uWholi-color-throw-berlin.jpg', '2313', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 't8VANdS9uWholi-color-throw-berlin.jpg', '2313', 'ITYJIMKNjfindex.jpeg', '23213', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 10, '', '', '', '', '', '', '', '9OAI65Ymiccompare.jpg', '514545', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ouNbDNf0ztholi-color-throw-berlin.jpg', '86464', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ouNbDNf0ztholi-color-throw-berlin.jpg', '86464', '9OAI65Ymiccompare.jpg', '514545', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 16, '', '', '', '', '', '', '', '0uCgPj5Og3images.jpeg', '12345', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0Ft6VRdorhFunny-Quotes-11.jpg', '12345', '', '', '', '', '', '', '', '', 'mSksDzSDf7Funny-Quotes-11.jpg', '125', '', '', '', '', '', '', '0Ft6VRdorhFunny-Quotes-11.jpg', '12345', '0uCgPj5Og3images.jpeg', '12345', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 17, '', '', '', '', '', '', '', 'gkdklf9CIxholi-color-throw-berlin.jpg', '123456', '123456', '', '', '', '', 'FcWxWQoN4fholi-color-throw-berlin.jpg', '123456', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FcWxWQoN4fholi-color-throw-berlin.jpg', '123456', '', '', '', '', '', '', '', '', '', '', 'gkdklf9CIxholi-color-throw-berlin.jpg', '123456', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `co_applicants`
--

CREATE TABLE IF NOT EXISTS `co_applicants` (
`id` int(11) NOT NULL,
  `whatsapp_number` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `decline` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) NOT NULL,
  `constitution` varchar(255) NOT NULL,
  `company_category` varchar(255) NOT NULL,
  `guardian` varchar(255) DEFAULT NULL,
  `guardian_first_name` varchar(255) NOT NULL,
  `guardian_middle_name` varchar(255) NOT NULL,
  `guardian_last_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `meeting_medium` varchar(255) NOT NULL,
  `enquiry_type` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `lead_type_id` int(11) NOT NULL,
  `branch_id` varchar(255) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `emi_amount` varchar(255) NOT NULL,
  `vehicle_cost` varchar(255) NOT NULL,
  `executive_phone` varchar(255) NOT NULL,
  `executive_email` varchar(255) NOT NULL,
  `executive_name` varchar(255) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `loan_amount` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `permanent_office_fax` varchar(255) NOT NULL,
  `permanent_office_pincode` varchar(255) NOT NULL,
  `permanent_office_mobile` varchar(255) NOT NULL,
  `permanent_office_alternate_phone` varchar(255) NOT NULL,
  `permanent_office_phone` varchar(255) NOT NULL,
  `permanent_office_email` varchar(255) NOT NULL,
  `permanent_office_state_id` varchar(255) NOT NULL,
  `permanent_office_district_id` varchar(255) NOT NULL,
  `permanent_office_city_id` int(11) NOT NULL,
  `permanent_office_street` varchar(255) NOT NULL,
  `permanent_office_name` varchar(255) NOT NULL,
  `current_office_fax` varchar(255) NOT NULL,
  `current_office_pincode` varchar(255) NOT NULL,
  `current_office_mobile` varchar(255) NOT NULL,
  `current_office_alternate_phone` varchar(255) NOT NULL,
  `current_office_phone` varchar(255) NOT NULL,
  `current_office_email` varchar(255) NOT NULL,
  `current_office_state_id` varchar(255) NOT NULL,
  `current_office_district_id` varchar(255) NOT NULL,
  `current_office_city_id` int(11) NOT NULL,
  `current_office_street` varchar(255) NOT NULL,
  `current_office_name` varchar(255) NOT NULL,
  `permanent_resi_phone` varchar(255) NOT NULL,
  `permanent_resi_pincode` varchar(255) NOT NULL,
  `permanent_resi_state_id` varchar(255) NOT NULL,
  `permanent_resi_district_id` varchar(255) NOT NULL,
  `pemanent_resi_city_id` int(11) NOT NULL,
  `permanent_resi_street` varchar(255) NOT NULL,
  `current_resi_phone` varchar(255) NOT NULL,
  `current_resi_pincode` varchar(255) NOT NULL,
  `current_resi_state_id` varchar(255) NOT NULL,
  `current_resi_district_id` varchar(255) NOT NULL,
  `current_resi_city_id` int(11) NOT NULL,
  `current_resi_street` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `co_applicants`
--

INSERT INTO `co_applicants` (`id`, `whatsapp_number`, `lead_id`, `decline`, `first_name`, `middle_name`, `last_name`, `occupation`, `type`, `company_name`, `constitution`, `company_category`, `guardian`, `guardian_first_name`, `guardian_middle_name`, `guardian_last_name`, `phone`, `email_id`, `customer_id`, `meeting_medium`, `enquiry_type`, `product_id`, `lead_type_id`, `branch_id`, `signature`, `profile_picture`, `emi_amount`, `vehicle_cost`, `executive_phone`, `executive_email`, `executive_name`, `dealer_id`, `vehicle_id`, `loan_amount`, `account_number`, `bank_id`, `permanent_office_fax`, `permanent_office_pincode`, `permanent_office_mobile`, `permanent_office_alternate_phone`, `permanent_office_phone`, `permanent_office_email`, `permanent_office_state_id`, `permanent_office_district_id`, `permanent_office_city_id`, `permanent_office_street`, `permanent_office_name`, `current_office_fax`, `current_office_pincode`, `current_office_mobile`, `current_office_alternate_phone`, `current_office_phone`, `current_office_email`, `current_office_state_id`, `current_office_district_id`, `current_office_city_id`, `current_office_street`, `current_office_name`, `permanent_resi_phone`, `permanent_resi_pincode`, `permanent_resi_state_id`, `permanent_resi_district_id`, `pemanent_resi_city_id`, `permanent_resi_street`, `current_resi_phone`, `current_resi_pincode`, `current_resi_state_id`, `current_resi_district_id`, `current_resi_city_id`, `current_resi_street`, `dob`, `created_at`, `updated_at`) VALUES
(3, 0, 4, 0, 'shubh', '', '', '', 'person', '', '', '', 'S/O', 'sdfef', '', '', '95698560071', 'smart_coolboy777@yahoo.co.uk', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', 'punjab', 'ludhiana', 156, '123 ', '', '', 'punjab', 'ludhiana', 156, '1234', '2015-08-29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 0, 4, 0, 'shubhhhhhhhhhhhhhhhhhhhhh', '', '', '', 'person', '', '', '', 'S/O', 'sdfef', '', '', '95698560071', 'smart_coolboy777@yahoo.co.uk', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '2015-08-29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 0, 4, 0, 'aaaaaaa', '', '', '', 'person', '', '', '', NULL, '', '', '', '12544554', 'aman@gmail.com', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 0, 5, 0, 'efdgrfg', '', '', '', 'person', '', '', '', NULL, '', '', '', '111111111111', 'smart_coolboy777@yahoo.co.uk', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '2015-10-30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 0, 7, 0, '4rtyry', '', 'yty', '', 'person', '', '', '', NULL, '', '', '', '14531452', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 0, 2, 0, 'qqqr', '', '', '', 'person', '', '', '', NULL, '', '', '', '24234', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '45354fhg', '', '', '', '', 0, '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 0, 2, 0, 'ggg', '', '', '', 'person', '', '', '', NULL, '', '', '', '45435435', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '345435gfgg', '', '', '', '', 0, '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 0, 6, 0, 'sfrt', '', 'erfegrtg', '', 'person', '', '', '', 'S/O', 'rgtrgt', '', 'rtgrgt', '454486', 'smart_coolboy777@yahoo.co.uk', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '2016-02-05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 0, 8, 0, 'abc', '', 'xyz', '', 'person', '', '', '', 'S/O', 'abc', '', 'xyz', '12345', 'aman@gmail.com', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '2015-10-05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 123456, 8, 0, '', '', '', NULL, 'firm', 'abcxyz', 'PVT Ltd. Co.', 'CAT DOC HOSPITAL', '', '', '', '', '123456', 'smart_coolboy777@yahoo.co.uk', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `daily_tasks`
--

CREATE TABLE IF NOT EXISTS `daily_tasks` (
`id` int(11) NOT NULL,
  `client` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `assign_on` varchar(255) NOT NULL,
  `completion_on` datetime NOT NULL,
  `assign_to` int(255) NOT NULL DEFAULT '0',
  `task` text NOT NULL,
  `assign_by` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `status_delete` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_tasks`
--

INSERT INTO `daily_tasks` (`id`, `client`, `phone`, `assign_on`, `completion_on`, `assign_to`, `task`, `assign_by`, `status`, `status_delete`, `created_at`, `updated_at`) VALUES
(1, 'testioipipyutyu1', 444444444, '2015-06-18 05:00:19 pm', '2015-06-20 05:42:44', 2, 'testing....ouiouippuytyty111', '1', 0, 1, '2015-06-13 09:24:35', '2015-07-16 08:53:33'),
(3, 'test', 2147483647, '2015-06-13 05:34:51 pm', '2015-06-15 05:34:54', 2, 'testing...', '1', 0, 1, '2015-06-13 12:05:19', '2015-07-17 04:27:18'),
(4, 'kamal', 0, '2015-06-15 12:43:26 pm', '2015-06-15 06:00:30', 6, 'testing....', '1', 0, 1, '2015-06-15 07:14:47', '2015-07-17 04:27:18'),
(5, 'Bhag singh sidhu', 2147483647, '2015-06-17 12:00:56 pm', '0000-00-00 00:00:00', 1, 'noc for Bhag Singh Sidhu', '1', 0, 1, '2015-06-17 06:23:12', '2015-06-17 06:23:12'),
(6, 'fdfsdfdf', 42323424, '2015-07-23 03:00:03 pm', '2015-07-23 03:00:06', 1, 'eeeeeeeeeeeee', '1', 0, 1, '2015-07-23 09:30:15', '2015-07-23 09:30:15'),
(7, 'yyyyyyyyyy', 0, '2015-07-23 03:12:37 pm', '2015-07-23 03:12:39', 1, 'hhhhhhhhhhhh', '1', 0, 1, '2015-07-23 09:42:53', '2015-07-23 09:42:53');

-- --------------------------------------------------------

--
-- Table structure for table `dealers`
--

CREATE TABLE IF NOT EXISTS `dealers` (
`id` int(11) NOT NULL,
  `dealer` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gm_name` varchar(255) NOT NULL,
  `gm_phone` varchar(255) NOT NULL,
  `gm_email` varchar(255) NOT NULL,
  `rc_name` varchar(255) NOT NULL,
  `rc_phone` varchar(255) NOT NULL,
  `rc_email` varchar(255) NOT NULL,
  `ac_name` varchar(255) NOT NULL,
  `ac_phone` varchar(255) NOT NULL,
  `ac_email` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealers`
--

INSERT INTO `dealers` (`id`, `dealer`, `email`, `gm_name`, `gm_phone`, `gm_email`, `rc_name`, `rc_phone`, `rc_email`, `ac_name`, `ac_phone`, `ac_email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'dealer', 'dear@gmail.com', 'gm name', '45454', 'gm@gmail.com', 'rc name', '45545', 'rc@gmail.com', 'account name', '45454', 'acc@gmail.comssssssssssss', 1, '2015-05-08 11:23:24', '2015-05-08 11:23:24'),
(2, 'dealer', 'dear@gmail.com', 'gm name', '45454', 'gm@gmail.com', 'rc name', '45545', 'rc@gmail.com', 'account name', '45454', 'acc@gmail.com', 0, '2015-05-08 11:23:24', '2015-05-08 11:23:24'),
(3, 'xyzzzzzzzzzzzz', '', 'gm xyzzzzzzzzz', '1234', 'gn@gmail.com', 'rc xyz', '3424', 'dsfsdf@gmail.com', 'acc xyz', '24234', 'sa@gmail.com', 1, '2015-05-09 05:02:10', '2015-05-09 05:02:10'),
(4, 'vidhata honda', '', 'vipan puri', '9888811189', 'sales.gm@vidhatahonda.com', 'Akash', '9417521709', 'rc.registration@vidhatahonda.com', 'abc', '9417521709', 'accounts@vidhatahonda.com', 1, '2015-06-19 11:36:01', '2015-06-19 11:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `dealer_branches`
--

CREATE TABLE IF NOT EXISTS `dealer_branches` (
`id` int(11) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `bm_name` varchar(255) NOT NULL,
  `bm_phone` varchar(255) NOT NULL,
  `bm_email` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer_branches`
--

INSERT INTO `dealer_branches` (`id`, `dealer_id`, `branch_id`, `bm_name`, `bm_phone`, `bm_email`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'bn namesssssssssss', '2653555555555555555', 'bm@gmail.com', 0, '2015-05-09 07:34:22', '2015-05-09 07:34:22'),
(2, 1, 9, 'bm 1', '35215', 'bm1@gmail.com', 1, '2015-05-09 07:35:53', '2015-05-09 07:35:53');

-- --------------------------------------------------------

--
-- Table structure for table `declines`
--

CREATE TABLE IF NOT EXISTS `declines` (
`id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `decline_by` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `declines`
--

INSERT INTO `declines` (`id`, `lead_id`, `decline_by`, `reason`, `comment`, `created_at`, `updated_at`) VALUES
(1, 7, 23, 'Customer Not Interested', 'hjkhkhjk', '2015-09-28 04:41:29', '2015-09-28 04:41:29');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE IF NOT EXISTS `districts` (
`id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `district` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `state_id`, `district`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'amritsar', 0, '2015-05-05 12:16:26', '2015-05-05 12:16:26'),
(2, 1, 'ludhiana', 0, '2015-05-05 12:16:41', '2015-05-05 12:16:41'),
(3, 1, 'patoials', 0, '2015-05-06 07:07:01', '2015-05-06 07:07:01'),
(4, 1, 'jallandhar', 0, '2015-05-06 07:09:09', '2015-05-06 07:09:09'),
(5, 1, 'fdgdfg', 0, '2015-05-06 07:11:26', '2015-05-06 07:11:26'),
(6, 1, 'malerkotla', 1, '2015-05-06 07:14:10', '2015-05-06 07:14:10'),
(7, 1, 'sadsdf', 0, '2015-05-06 07:15:27', '2015-05-06 07:15:27'),
(8, 1, 'ferozpur', 1, '2015-05-06 07:16:57', '2015-05-06 07:16:57'),
(9, 1, 'phagwara', 0, '2015-05-06 07:18:58', '2015-05-06 07:18:58'),
(10, 1, 'asdsad', 1, '2015-05-06 07:19:06', '2015-05-06 07:19:06'),
(11, 1, 'adasd', 1, '2015-05-06 07:19:15', '2015-05-06 07:19:15'),
(12, 1, 'asfsfd', 0, '2015-05-06 07:19:55', '2015-05-06 07:19:55'),
(13, 1, 'fgdfg', 0, '2015-05-06 07:48:07', '2015-05-06 07:48:07'),
(14, 1, 'undefined', 1, '2015-05-06 09:19:58', '2015-05-06 09:19:58'),
(15, 1, 'malerkotlammmmm', 1, '2015-05-06 10:05:38', '2015-05-06 10:05:38'),
(16, 1, 'maler', 1, '2015-05-06 10:10:38', '2015-05-06 10:10:38'),
(17, 1, 'asdsadddddddddddd', 1, '2015-05-06 10:10:55', '2015-05-06 10:10:55'),
(18, 1, 'test', 1, '2015-05-06 10:11:30', '2015-05-06 10:11:30'),
(19, 2, 'assam 1', 1, '2015-05-27 12:26:52', '2015-05-27 12:26:52'),
(20, 1, 'hlkhjl', 1, '2015-06-08 10:20:21', '2015-06-08 10:20:21');

-- --------------------------------------------------------

--
-- Table structure for table `finacial_details`
--

CREATE TABLE IF NOT EXISTS `finacial_details` (
`id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `update` varchar(255) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `advance_emi` varchar(255) NOT NULL,
  `delivery` date NOT NULL,
  `disbursement` date NOT NULL,
  `first_installment` date NOT NULL,
  `tenure` varchar(255) NOT NULL,
  `last_installment` date NOT NULL,
  `upload_1` varchar(255) NOT NULL,
  `upload_2` varchar(255) NOT NULL,
  `upload_3` varchar(255) NOT NULL,
  `upload_4` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `finacial_details`
--

INSERT INTO `finacial_details` (`id`, `lead_id`, `update`, `bank_id`, `advance_emi`, `delivery`, `disbursement`, `first_installment`, `tenure`, `last_installment`, `upload_1`, `upload_2`, `upload_3`, `upload_4`, `created_at`, `updated_at`) VALUES
(1, 1, '', 2, '01', '0000-00-00', '2015-07-18', '2015-08-07', '25 months', '2017-08-07', '', '', '', '', '2015-07-13 07:13:19', '2015-07-13 07:13:19'),
(2, 2, '', 2, '', '0000-00-00', '2015-07-15', '0000-00-00', '', '0000-00-00', '1ZrD4pZuZccompare.jpg', '', '', '', '2015-07-13 09:01:59', '2015-07-13 09:01:59'),
(3, 3, '', 2, '02', '0000-00-00', '2024-08-15', '2015-07-01', '24', '2017-01-05', 'ftLv7Mn3AjFunny-Quotes-11.jpg', 'Q5K6ZXt67I10470929_985610308115808_1636870564461191951_n.jpg', '', '', '2015-07-14 05:13:11', '2015-07-14 05:13:11'),
(4, 4, '', 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '', '', '', '2015-07-14 07:38:16', '2015-07-14 07:38:16'),
(5, 5, '', 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '', '', '', '2015-07-14 07:46:29', '2015-07-14 07:46:29'),
(6, 6, '', 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '', '', '', '2015-07-29 04:58:25', '2015-07-29 04:58:25'),
(7, 7, '', 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '', '', '', '2015-07-31 05:45:04', '2015-07-31 05:45:04'),
(8, 8, '', 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '', '', '', '2015-07-31 10:13:53', '2015-07-31 10:13:53'),
(9, 9, '', 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '', '', '', '2015-08-07 10:29:06', '2015-08-07 10:29:06');

-- --------------------------------------------------------

--
-- Table structure for table `fi_initiations`
--

CREATE TABLE IF NOT EXISTS `fi_initiations` (
`id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0',
  `decline` int(11) NOT NULL DEFAULT '1',
  `mail` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fi_initiations`
--

INSERT INTO `fi_initiations` (`id`, `lead_id`, `bank_id`, `branch_id`, `status`, `decline`, `mail`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 11, 'los', 1, 'sent', '2015-07-13 10:21:34', '2015-07-13 11:09:47'),
(2, 2, 3, 11, 'los', 1, 'sent', '2015-07-13 12:25:37', '2015-07-13 11:32:47'),
(3, 3, 3, 11, 'los', 1, 'sent', '2015-07-14 06:28:30', '2015-07-14 05:30:20'),
(4, 4, 3, 11, '0', 1, 'send', '2015-07-30 06:34:57', '2015-07-30 06:36:01'),
(5, 9, 2, 7, '0', 1, 'send', '2015-08-07 11:30:07', '2015-08-07 11:30:07');

-- --------------------------------------------------------

--
-- Table structure for table `insurance`
--

CREATE TABLE IF NOT EXISTS `insurance` (
`id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_phone` int(11) NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  `company_vehicle` varchar(255) NOT NULL,
  `insurance_amount` int(11) NOT NULL,
  `residential_street` varchar(255) NOT NULL,
  `residential_city` int(11) NOT NULL,
  `residential_distt` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `issue_date` date NOT NULL,
  `manufacturing_year` int(11) NOT NULL,
  `nominee_name` varchar(255) NOT NULL,
  `nominee_relation` varchar(255) NOT NULL,
  `nominee_dob` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance`
--

INSERT INTO `insurance` (`id`, `customer_name`, `customer_phone`, `from`, `to`, `company_vehicle`, `insurance_amount`, `residential_street`, `residential_city`, `residential_distt`, `dob`, `issue_date`, `manufacturing_year`, `nominee_name`, `nominee_relation`, `nominee_dob`, `created_at`, `updated_at`) VALUES
(9, 'fgf', 454, '2015-05-26', '2015-07-26', 'fgf', 54, 'fgdfg', 0, '0', '2015-05-26', '2015-06-26', 45, 'fg', 'fgf', '2015-05-19', '2015-05-28 06:44:06', '2015-05-28 06:44:06'),
(10, 'fgf', 454, '2015-05-26', '2015-07-26', 'fgf', 54, 'fgdfg', 0, '0', '2015-05-26', '2015-06-26', 45, 'fg', 'fgf', '2015-05-19', '2015-05-28 06:44:06', '2015-05-28 06:44:06'),
(11, 'fgf', 454, '2015-05-26', '2015-07-26', 'fgf', 54, 'fgdfg', 0, '0', '2015-05-26', '2015-06-26', 45, 'fg', 'fgf', '2015-05-19', '2015-05-28 06:44:06', '2015-05-28 06:44:06'),
(13, 'customer', 9876, '2015-06-16', '2015-06-17', 'herohonda', 50000, '90', 156, 'ludhiana', '2015-06-15', '2015-06-16', 2013, 'customerr', 'jklkl', '2015-06-16', '2015-06-15 14:04:27', '2015-06-15 14:04:27'),
(14, 'rt', 466, '2015-10-06', '2015-10-06', 'rtgert', 5444, 'rter', 12, 'malerkotla', '2015-09-06', '2015-08-18', 2015, 'thtrh', 'tyty', '2015-09-06', '2015-08-06 02:31:45', '2015-08-06 02:31:45');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_branches`
--

CREATE TABLE IF NOT EXISTS `insurance_branches` (
`id` int(11) NOT NULL,
  `insurance_id` int(11) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance_branches`
--

INSERT INTO `insurance_branches` (`id`, `insurance_id`, `branch`, `name`, `phone`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'branch 1', 'branch1 name', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 'branch 2', 'branch2 name', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 'ggggggggg', 'gggggggggg@gmail.com', '54366666666666666666', 1, '2015-05-30 11:29:04', '2015-05-30 11:29:04'),
(4, 4, 'mohali1', 'mohalicompany1@gmail.com', '12344411', 1, '2015-06-15 07:11:42', '2015-06-15 07:11:42');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_companies`
--

CREATE TABLE IF NOT EXISTS `insurance_companies` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `alternate_phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `alternate_email` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance_companies`
--

INSERT INTO `insurance_companies` (`id`, `name`, `phone`, `alternate_phone`, `email`, `alternate_email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'insurance1', '1234', '', 'insur1@gmail.com', '', 1, '2015-05-23 00:00:00', '0000-00-00 00:00:00'),
(2, 'insurance2', '12345', '', 'insur2@gmail.com', '', 1, '2015-05-23 00:00:00', '0000-00-00 00:00:00'),
(3, 'test', '13123333333333333', '', 'xyztest@gmail.com', '', 1, '2015-05-30 11:28:22', '2015-05-30 11:28:22'),
(4, 'company1', '1234561', '9077881', 'com1p@gmail.com', '', 1, '2015-06-15 07:08:36', '2015-06-15 07:08:36');

-- --------------------------------------------------------

--
-- Table structure for table `is_superusers`
--

CREATE TABLE IF NOT EXISTS `is_superusers` (
`id` int(11) NOT NULL,
  `is_superuser` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_superusers`
--

INSERT INTO `is_superusers` (`id`, `is_superuser`, `description`, `created_at`, `updated_at`) VALUES
(2, 'Agent', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Commission Agent', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Team Leader', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Branch Manager', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'General Manager', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Admin', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Super Admin', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE IF NOT EXISTS `leads` (
`id` int(11) NOT NULL,
  `fi_initiation` int(11) DEFAULT NULL,
  `update` varchar(255) NOT NULL DEFAULT 'yes',
  `whatsapp_number` text NOT NULL,
  `tenure` varchar(255) NOT NULL,
  `approve_lead` int(11) NOT NULL,
  `lead_first_name` varchar(255) NOT NULL,
  `lead_middle_name` varchar(255) NOT NULL,
  `lead_last_name` varchar(255) NOT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) NOT NULL,
  `constitution` varchar(255) NOT NULL,
  `company_category` varchar(255) NOT NULL,
  `guardian` varchar(255) DEFAULT NULL,
  `guardian_first_name` varchar(255) NOT NULL,
  `guardian_middle_name` varchar(255) NOT NULL,
  `guardian_last_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `meeting_medium` varchar(255) NOT NULL,
  `enquiry_type` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `lead_type_id` int(11) NOT NULL,
  `branch_id` varchar(255) NOT NULL,
  `lead_from` int(11) NOT NULL,
  `lead_to` int(11) NOT NULL,
  `forward_to` varchar(255) NOT NULL DEFAULT '0',
  `decline_lead` int(11) NOT NULL,
  `existing_customer` int(11) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `emi_amount` varchar(255) NOT NULL,
  `vehicle_cost` varchar(255) NOT NULL,
  `executive_phone` varchar(255) NOT NULL,
  `executive_email` varchar(255) NOT NULL,
  `executive_name` varchar(255) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `loan_amount` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `permanent_office_fax` varchar(255) NOT NULL,
  `permanent_office_pincode` varchar(255) NOT NULL,
  `permanent_office_mobile` varchar(255) NOT NULL,
  `permanent_office_alternate_phone` varchar(255) NOT NULL,
  `permanent_office_phone` varchar(255) NOT NULL,
  `permanent_office_email` varchar(255) NOT NULL,
  `permanent_office_state_id` varchar(255) NOT NULL,
  `permanent_office_district_id` varchar(255) NOT NULL,
  `permanent_office_city_id` int(11) NOT NULL,
  `permanent_office_street` varchar(255) NOT NULL,
  `permanent_office_name` varchar(255) NOT NULL,
  `current_office_fax` varchar(255) NOT NULL,
  `current_office_pincode` varchar(255) NOT NULL,
  `current_office_mobile` varchar(255) NOT NULL,
  `current_office_alternate_phone` varchar(255) NOT NULL,
  `current_office_phone` varchar(255) NOT NULL,
  `current_office_email` varchar(255) NOT NULL,
  `current_office_state_id` varchar(255) NOT NULL,
  `current_office_district_id` varchar(255) NOT NULL,
  `current_office_city_id` int(11) NOT NULL,
  `current_office_street` varchar(255) NOT NULL,
  `current_office_name` varchar(255) NOT NULL,
  `permanent_resi_phone` varchar(255) NOT NULL,
  `permanent_resi_pincode` varchar(255) NOT NULL,
  `permanent_resi_state_id` varchar(255) NOT NULL,
  `permanent_resi_district_id` varchar(255) NOT NULL,
  `pemanent_resi_city_id` int(11) NOT NULL,
  `permanent_resi_street` varchar(255) NOT NULL,
  `current_resi_phone` varchar(255) NOT NULL,
  `current_resi_pincode` varchar(255) NOT NULL,
  `current_resi_state_id` varchar(255) NOT NULL,
  `current_resi_district_id` varchar(255) NOT NULL,
  `current_resi_city_id` int(11) NOT NULL,
  `current_resi_street` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `fi_initiation`, `update`, `whatsapp_number`, `tenure`, `approve_lead`, `lead_first_name`, `lead_middle_name`, `lead_last_name`, `occupation`, `type`, `company_name`, `constitution`, `company_category`, `guardian`, `guardian_first_name`, `guardian_middle_name`, `guardian_last_name`, `phone`, `email_id`, `customer_id`, `meeting_medium`, `enquiry_type`, `product_id`, `lead_type_id`, `branch_id`, `lead_from`, `lead_to`, `forward_to`, `decline_lead`, `existing_customer`, `signature`, `profile_picture`, `emi_amount`, `vehicle_cost`, `executive_phone`, `executive_email`, `executive_name`, `dealer_id`, `vehicle_id`, `loan_amount`, `account_number`, `bank_id`, `permanent_office_fax`, `permanent_office_pincode`, `permanent_office_mobile`, `permanent_office_alternate_phone`, `permanent_office_phone`, `permanent_office_email`, `permanent_office_state_id`, `permanent_office_district_id`, `permanent_office_city_id`, `permanent_office_street`, `permanent_office_name`, `current_office_fax`, `current_office_pincode`, `current_office_mobile`, `current_office_alternate_phone`, `current_office_phone`, `current_office_email`, `current_office_state_id`, `current_office_district_id`, `current_office_city_id`, `current_office_street`, `current_office_name`, `permanent_resi_phone`, `permanent_resi_pincode`, `permanent_resi_state_id`, `permanent_resi_district_id`, `pemanent_resi_city_id`, `permanent_resi_street`, `current_resi_phone`, `current_resi_pincode`, `current_resi_state_id`, `current_resi_district_id`, `current_resi_city_id`, `current_resi_street`, `dob`, `password`, `created_at`, `updated_at`) VALUES
(1, 3, '', '', '25 months', 1, 'Kamal ', '', 'maan', '', 'person', '', '', '', NULL, '', '', '', '95698560071', '', '009G1513748', 'Face To Face', 'Cold', 9, 2, '', 1, 2, '0', 0, 0, 'RR0HLibHyGholi-color-throw-berlin.jpg', 'ZtSygQN1fDdescimages.jpeg', '1234', '2500000', '11231323', 'executive@gmail.com', 'execuutive', 4, 2, '2300000', '', 3, '1233213', '3123123', '13131', '3123', '2331', 'permannet@gmail.coim', 'punjab', 'fdgdfg', 1, 'street123', 'ITian', '23123', '3213', '312313', '1233123', '323123', 'permannet@gmail.coim', 'punjab', 'fdgdfg', 1, 'street123', 'ITian', '1234', '123', 'punjab', 'fdgdfg', 1, 'permanent Residence', '123455', '123', 'punjab', 'fdgdfg', 1, 'current Residence', '1990-05-13', '95698560071', '2015-07-13 07:13:18', '2015-07-13 11:10:48'),
(2, 3, '', '', '', 1, 'Aman ', '', 'Kaur', NULL, 'firm', 'Company', 'Public Ltd. Co.', 'CAT DEN WITH CLINIC', '', '', '', '', '823947284', '', '009G1513495', '', '', 9, 2, '', 2, 3, '0', 0, 0, '', '', '', '', '', '', '', 0, 2, '100000', '', 1, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '823947284', '2015-07-13 09:01:59', '2015-07-13 11:33:28'),
(3, 3, '', '', '', 1, 'Tets Finance', '', 'Last name', '', 'person', '', '', '', NULL, '', '', '', '12313', '', '009G1514217', 'Email', '', 9, 2, '', 2, 3, '0', 0, 0, '', '', '', '', '', '', '', 0, 2, '45409000', '', 2, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '12313', '2015-07-14 05:13:10', '2015-07-14 06:57:36'),
(4, 2, '', '', '', 1, 'dddd', '', 'dddd', '', 'person', '', '', '', NULL, '', '', '', '4545445', '', '005G1514202', 'Face To Face', 'Warm', 5, 2, '', 2, 2, '0', 0, 0, '', '', '', '', '', '', '', 0, 2, '', '', 4, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '2015-08-28', '4545445', '2015-07-14 07:38:16', '2015-07-30 06:34:57'),
(5, 1, '', '', '', 1, 'ffffff', '', 'ffffff', NULL, 'firm', 'abc', '', '', '', '', '', '', '3435', '', '009G1514216', 'Face To Face', 'Cold', 9, 0, '', 2, 1, '1', 0, 0, '', '', '', '', '', '', '', 0, 2, '', '', 8, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '3435', '2015-07-14 07:46:29', '2015-09-28 04:22:27'),
(6, 1, 'yes', '', '', 1, 'abcd', '', '', NULL, 'person', '', '', '', NULL, '', '', '', '95698560071', 'smart_coolboy777@yahoo.co.uk', '008G1529614', 'Face To Face', 'Warm', 8, 2, '', 2, 1, '0', 0, 0, '', '', '', '', '', '', '', 0, 0, '', '', 9, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '', '2015-07-29 04:58:24', '2015-07-29 04:58:24'),
(7, NULL, 'yes', '', '', 0, 'vsfdg', '', '', NULL, 'person', '', '', '', NULL, '', '', '', '5256465', 'smart_coolboy777@yahoo.co.uk', '005G1531160', '', '', 5, 2, '', 6, 1, '0', 1, 0, '', '', '', '', '', '', '', 0, 0, '', '', 10, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '', '2015-07-31 05:45:03', '2015-09-28 04:41:29'),
(8, NULL, 'yes', '', '', 0, 'tytryj', '', '', NULL, 'person', '', '', '', 'S/O', 'yuyruytiut', '', '', '3521541254', '', '008G1531936', '', '', 8, 2, '', 6, 1, '0', 0, 0, '', '', '', '', '', '', '', 0, 0, '', '', 11, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '', '2015-07-31 10:13:53', '2015-07-31 10:13:53'),
(9, 2, 'yes', '', '', 1, 'GHH', 'GHGJ', 'HJHGJK', NULL, 'person', '', '', '', NULL, '', '', '', '1133', 'smart_coolboy777@yahoo.co.uk', '009H15007390', 'Email', 'Hot', 9, 2, '', 2, 1, '0', 0, 0, '', '', '', '', '', '', '', 0, 0, '', '', 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '0000-00-00', '', '2015-08-07 10:29:06', '2015-08-07 11:30:06');

-- --------------------------------------------------------

--
-- Table structure for table `lead_types`
--

CREATE TABLE IF NOT EXISTS `lead_types` (
`id` int(11) NOT NULL,
  `lead_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_types`
--

INSERT INTO `lead_types` (`id`, `lead_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Website', 0, '2015-04-16 12:04:16', '2015-04-16 12:04:16'),
(2, 'Walk In ', 1, '2015-04-16 12:08:22', '2015-04-16 12:08:22'),
(3, 'Campaign', 1, '2015-04-16 12:09:29', '2015-04-16 12:09:29');

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE IF NOT EXISTS `meetings` (
`id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `meeting_topic` varchar(255) NOT NULL,
  `meeting_date` varchar(255) NOT NULL,
  `meeting_medium` varchar(255) NOT NULL,
  `meeting_discussion` text NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `lead_agent_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meetings`
--

INSERT INTO `meetings` (`id`, `lead_id`, `meeting_topic`, `meeting_date`, `meeting_medium`, `meeting_discussion`, `remarks`, `lead_agent_id`, `created_at`, `updated_at`) VALUES
(1, 2, 'test', '2015-07-18 02:42:11 pm', 'Email', 'test previous 1', '', 5, '2015-07-14 09:12:22', '2015-07-16 12:21:17'),
(2, 1, 'thisss', '2015-07-18 02:43:12 pm', 'Face To Face', 'test previous 2', '', 4, '2015-07-14 09:13:21', '2015-08-06 12:00:28'),
(3, 4, 'hhhhhh', '2015-07-18 02:45:24 pm', 'Face To Face', 'discuyssion 1', '', 5, '2015-07-14 09:15:33', '2015-07-27 05:56:41'),
(7, 1, 'test', '2015-07-24 12:25:04 pm', 'Phone', '', '', 4, '2015-07-22 06:55:14', '2015-08-06 12:00:28'),
(8, 2, 'test', '2015-07-23 12:25:30 pm', 'Face To Face', '', '', 5, '2015-07-22 06:55:42', '2015-09-08 07:50:31'),
(9, 3, 'test', '2015-07-23 12:26:07 pm', 'Email', '', '', 5, '2015-07-22 06:56:19', '2015-09-08 07:50:31'),
(10, 4, 'new topic', '2015-07-27 11:23:33 am', 'Face To Face', '', '', 5, '2015-07-27 05:53:53', '2015-09-08 07:50:24'),
(13, 5, 'fffff topic', '2015-07-27 11:25:08 am', 'Face To Face', '', '', 5, '2015-07-27 05:55:31', '2015-09-08 07:50:24'),
(14, 4, 'gggggggggggg', '2015-07-28 11:43:15 am', 'Face To Face', '', '', 4, '2015-07-27 06:13:26', '2015-08-06 12:00:28'),
(15, 4, 'meeting 2', '2015-07-19 02:45:24 pm', 'Face To Face', 'discuyssion 2', '', 5, '2015-07-14 09:15:33', '2015-07-27 05:56:41');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
`module_id` int(255) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `module_table` varchar(255) NOT NULL,
  `module_description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `module_name`, `module_table`, `module_description`, `created_at`) VALUES
(1, 'USER', 'User', '', '2015-10-13 06:12:30'),
(2, 'LEAD', 'Lead', '', '2015-10-13 06:12:33'),
(3, 'DECLINE LEADS', 'Decline', '', '2015-10-13 06:13:58'),
(4, 'COMPONENTS OF LEADS', '', '', '2015-08-29 10:35:49'),
(5, 'INSURANCE', '', '', '2015-08-29 10:35:55'),
(6, 'FI STATUS', 'FiInitiation', '', '2015-10-13 06:14:23'),
(7, 'UNDER WRITING', '', '', '2015-08-29 10:36:10'),
(8, 'UNDER WRITING APPROVED', '', '', '2015-08-29 10:36:24'),
(9, 'UNDER APPROVED DECLINE', '', '', '2015-08-29 10:36:46'),
(10, 'TEMPORARY CUSTOMER', '', '', '2015-08-29 10:37:05'),
(11, 'TEMPORARY DECLINE CUSTOMER', '', '', '2015-08-29 10:37:22'),
(12, 'PERMANENT CUSTOMER', '', '', '2015-08-29 10:37:35'),
(13, 'TODAY SCHEDULE', '', '', '2015-08-29 10:37:55'),
(14, 'DAILY TASK', '', '', '2015-08-29 10:38:04'),
(15, 'BRANCH', '', '', '2015-08-29 10:38:17'),
(16, 'SEARCH', '', '', '2015-09-02 07:04:27');

-- --------------------------------------------------------

--
-- Table structure for table `module_roles`
--

CREATE TABLE IF NOT EXISTS `module_roles` (
`module_role_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `permissions` int(11) NOT NULL DEFAULT '0',
  `sub_permissions` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module_roles`
--

INSERT INTO `module_roles` (`module_role_id`, `role_id`, `module_id`, `permissions`, `sub_permissions`, `created_at`, `updated_at`) VALUES
(1, 8, 1, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 8, 2, 2, 'lead_coapplicant,lead_decline,lead_fiinitiation,lead_approve,lead_forward,lead_assign,lead_profile,lead_add,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 8, 3, 1, 'declinelead_delete,declinelead_recycle,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 8, 4, 1, 'component_bank,component_leadtype,component_state,component_vehicle,component_product,component_dealer,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 8, 5, 1, 'insurance_insurancecompany,insurance_insurance,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 8, 6, 1, 'fistatus_downloadcustomize,fistatus_reject,fistatus_accept,fistatus_applyagain,fistatus_sendmaildirectly,fistatus_filerejected,fistatus_uploadsendfi,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 8, 7, 1, 'underwriting_declineunderwriting,underwriting_losnumber,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 8, 8, 1, 'underwritingapproved_profile,underwritingapproved_decline,underwritingapproved_status,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 8, 9, 1, 'underapproveddecline_recycle,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 8, 10, 1, 'temporarycustomer_profile,temporarycustomer_coapplicant,temporarycustomer_update,temporarycustomer_decline,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 8, 11, 1, 'temporarydeclinecustomer_recycle,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 8, 12, 1, 'permanentcustomer_rc,permanentcustomer_profile,permanentcustomer_coapplicant,permanentcustomer_update,permanentcustomer_calculation,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 8, 13, 1, 'todayschedule_assignlead,todayschedule_print,todayschedule_forwardmeeting,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 8, 14, 1, 'dailytask_forwardmeeting,dailytask_assignlead,dailytask_enabledisable,dailytask_edit,dailytask_status,dailytask_print,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 8, 15, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 8, 16, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 7, 1, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 7, 2, 0, 'lead_addlead_forwardlead_approve', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 7, 3, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 7, 4, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 7, 5, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 7, 6, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 7, 7, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 7, 8, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 7, 9, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 7, 10, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 7, 11, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 7, 12, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 7, 13, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 7, 14, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 7, 15, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 7, 16, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 6, 1, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 6, 2, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 6, 3, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 6, 4, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 6, 5, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 6, 6, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 6, 7, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 6, 8, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 6, 9, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 6, 10, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 6, 11, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 6, 12, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 6, 13, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 6, 14, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 6, 15, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 6, 16, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 5, 1, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 5, 2, 1, ',lead_add,,lead_profile, ,lead_assign, ,lead_fiinitiation,,lead_decline,,lead_forward,,lead_approve,,,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 5, 3, 2, 'declinelead_delete', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 5, 4, 1, 'component_leadtype,component_dealer,,component_bank,component_state,component_product,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 5, 5, 0, 'insurance_insurance,insurance_insurancecompany', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 5, 6, 1, 'fistatus_downloadcustomize,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 5, 7, 2, 'underwriting_declinelead', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 5, 8, 1, 'underwritingapproved_decline,underwritingapproved_status,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 5, 9, 0, 'underapproveddecline_los', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 5, 10, 1, 'temporarycustomer_decline,temporarycustomer_update,temporarycustomer_coapplicant,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 5, 11, 1, 'temporarydeclinecustomer_los,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 5, 12, 1, 'permanentcustomer_rc,permanentcustomer_coapplicant,permanentcustomer_calculation,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 5, 13, 1, 'todayschedule_assignlead,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 5, 14, 1, 'dailytask_print,dailytask_assignlead,dailytask_forwardmeeting,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 5, 15, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 5, 16, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 4, 1, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 4, 2, 1, ',lead_add,,lead_profile, ,lead_assign, ,lead_fiinitiation,,lead_decline,,lead_forward,,lead_approve,,,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 4, 3, 2, 'declinelead_delete', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 4, 4, 1, 'component_leadtype,component_dealer,,component_bank,component_state,component_product,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 4, 5, 0, 'insurance_insurance,insurance_insurancecompany', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 4, 6, 1, 'fistatus_downloadcustomize,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 4, 7, 2, 'underwriting_declinelead', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 4, 8, 1, 'underwritingapproved_decline,underwritingapproved_status,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 4, 9, 0, 'underapproveddecline_los', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 4, 10, 1, 'temporarycustomer_decline,temporarycustomer_update,temporarycustomer_coapplicant,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 4, 11, 1, 'temporarydeclinecustomer_los,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 4, 12, 1, 'permanentcustomer_rc,permanentcustomer_coapplicant,permanentcustomer_calculation,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 4, 13, 1, 'todayschedule_assignlead,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 4, 14, 1, 'dailytask_print,dailytask_assignlead,dailytask_forwardmeeting,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 4, 15, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 4, 16, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 3, 1, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 3, 2, 1, ',lead_add,,lead_profile, ,lead_assign, ,lead_fiinitiation,,lead_decline,,lead_forward,,lead_approve,,,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 3, 3, 2, 'declinelead_delete', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 3, 4, 1, 'component_leadtype,component_dealer,,component_bank,component_state,component_product,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 3, 5, 0, 'insurance_insurance,insurance_insurancecompany', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 3, 6, 1, 'fistatus_downloadcustomize,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 3, 7, 2, 'underwriting_declinelead', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 3, 8, 1, 'underwritingapproved_decline,underwritingapproved_status,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 3, 9, 0, 'underapproveddecline_los', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 3, 10, 1, 'temporarycustomer_decline,temporarycustomer_update,temporarycustomer_coapplicant,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 3, 11, 1, 'temporarydeclinecustomer_los,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 3, 12, 1, 'permanentcustomer_rc,permanentcustomer_coapplicant,permanentcustomer_calculation,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 3, 13, 1, 'todayschedule_assignlead,,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 3, 14, 1, 'dailytask_print,dailytask_assignlead,dailytask_forwardmeeting,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 3, 15, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 3, 16, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 2, 1, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 2, 2, 2, 'lead_coapplicant,lead_decline,lead_fiinitiation,lead_approve,lead_forward,lead_assign,lead_profile,lead_add,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 2, 3, 1, 'declinelead_delete,declinelead_recycle,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 2, 4, 1, 'component_bank,component_leadtype,component_state,component_vehicle,component_product,component_dealer,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 2, 5, 1, 'insurance_insurancecompany,insurance_insurance,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 2, 6, 1, 'fistatus_downloadcustomize,fistatus_reject,fistatus_accept,fistatus_applyagain,fistatus_sendmaildirectly,fistatus_filerejected,fistatus_uploadsendfi,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 2, 7, 1, 'underwriting_declineunderwriting,underwriting_losnumber,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 2, 8, 1, 'underwritingapproved_profile,underwritingapproved_decline,underwritingapproved_status,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 2, 9, 1, 'underapproveddecline_recycle,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 2, 10, 1, 'temporarycustomer_profile,temporarycustomer_coapplicant,temporarycustomer_update,temporarycustomer_decline,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 2, 11, 1, 'temporarydeclinecustomer_recycle,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 2, 12, 1, 'permanentcustomer_rc,permanentcustomer_profile,permanentcustomer_coapplicant,permanentcustomer_update,permanentcustomer_calculation,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 2, 13, 1, 'todayschedule_assignlead,todayschedule_print,todayschedule_forwardmeeting,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 2, 14, 1, 'dailytask_forwardmeeting,dailytask_assignlead,dailytask_enabledisable,dailytask_edit,dailytask_status,dailytask_print,', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 2, 15, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 2, 16, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
`id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `description`) VALUES
(1, 'add'),
(2, 'edit'),
(3, 'view'),
(4, 'delete'),
(5, 'status'),
(6, 'approve'),
(7, 'fiinitiation');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`id` int(11) NOT NULL,
  `product` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product`, `status`, `created_at`, `updated_at`) VALUES
(4, 'new test product', 0, '2015-04-16 10:02:57', '2015-04-16 10:02:57'),
(5, 'Used Car Purchase Refinaced Loan', 1, '2015-04-16 10:06:26', '2015-04-16 10:06:26'),
(6, 'teste product', 0, '2015-04-21 06:28:59', '2015-04-21 06:28:59'),
(8, 'dfdfgdfg', 1, '2015-04-23 11:12:16', '2015-04-23 11:12:16'),
(9, 'Auto loan bharat loan', 1, '2015-06-17 09:43:51', '2015-06-17 09:43:51'),
(10, 'Used car refinance', 1, '2015-06-27 08:18:15', '2015-06-27 08:18:15'),
(11, 'vcvcv', 1, '2015-09-08 04:48:55', '2015-09-08 04:48:55');

-- --------------------------------------------------------

--
-- Table structure for table `proofs`
--

CREATE TABLE IF NOT EXISTS `proofs` (
`id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `owner_other_ownership_proof` varchar(255) NOT NULL,
  `owner_property_paper` varchar(255) NOT NULL,
  `owner_sevrage_bill` varchar(255) NOT NULL,
  `owner_electricity_bill` varchar(255) NOT NULL,
  `sign_other_proof` varchar(255) NOT NULL,
  `sign_passport_proof` varchar(255) NOT NULL,
  `sign_passport__number` varchar(255) NOT NULL,
  `sign_pan_card_proof` varchar(255) NOT NULL,
  `sign_pan_card_number` varchar(255) NOT NULL,
  `bank_bank_account_number` varchar(255) NOT NULL,
  `bank_bank_name` varchar(255) NOT NULL,
  `income_other_proof` varchar(255) NOT NULL,
  `income_fard` varchar(255) NOT NULL,
  `income_bank_2_proof` varchar(255) NOT NULL,
  `income_bank_1_proof` varchar(255) NOT NULL,
  `income_bank_account_number` varchar(255) NOT NULL,
  `income_bank_name` varchar(255) NOT NULL,
  `income_computation_2_proof` varchar(255) NOT NULL,
  `income_computaion_1_proof` varchar(255) NOT NULL,
  `income_computation_number` varchar(255) NOT NULL,
  `income_itr_2_proof` varchar(255) NOT NULL,
  `income_itr_1_proof` varchar(255) NOT NULL,
  `income_itr_number` varchar(255) NOT NULL,
  `income_balancesheet_proof` varchar(255) NOT NULL,
  `income_pl_proof` varchar(255) NOT NULL,
  `income_form_3cb` varchar(255) NOT NULL,
  `income_form_3cd` varchar(255) NOT NULL,
  `resi_voter_card_proof` varchar(255) NOT NULL,
  `resi_voter_car_number` varchar(255) NOT NULL,
  `resi_passport_proof` varchar(255) NOT NULL,
  `resi_passport__number` varchar(255) NOT NULL,
  `resi_driving_license_proof` varchar(255) NOT NULL,
  `resi_driving_license__number` varchar(255) NOT NULL,
  `resi_rashan_id_proof` varchar(255) NOT NULL,
  `other_proof` varchar(255) NOT NULL,
  `proof_bank_proof` varchar(255) NOT NULL,
  `proof_bank_account_number` varchar(255) NOT NULL,
  `proof_arm_license_proof` varchar(255) NOT NULL,
  `proof_arm_license_number` varchar(255) NOT NULL,
  `proof_passport_proof` varchar(255) NOT NULL,
  `prrof_passport_number` varchar(255) NOT NULL,
  `proof_driving_license_proof` varchar(255) NOT NULL,
  `proof_driving_license_number` varchar(255) NOT NULL,
  `proof_adhar_card_proof` varchar(255) NOT NULL,
  `proof_adhar_card_number` varchar(255) NOT NULL,
  `proof_voter_card_proof` varchar(255) NOT NULL,
  `proof_voter_card_number` varchar(255) NOT NULL,
  `proof_pan_card_proof` varchar(255) NOT NULL,
  `proof_pan_card_number` varchar(255) NOT NULL,
  `address_te_bill` varchar(255) NOT NULL,
  `bankstatement_firm_bill` varchar(255) NOT NULL,
  `other_address_proof1` varchar(255) NOT NULL,
  `other_address_proof2` varchar(255) NOT NULL,
  `establishment_pancard_firm` varchar(255) NOT NULL,
  `establishment_registration_cerificate` varchar(255) NOT NULL,
  `establishment_service_tax` varchar(255) NOT NULL,
  `establishment_other_proofname1` varchar(255) NOT NULL,
  `establishment_other_proof1` varchar(255) NOT NULL,
  `establishment_other_proofname2` varchar(255) NOT NULL,
  `establishment_other_proof2` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proofs`
--

INSERT INTO `proofs` (`id`, `lead_id`, `owner_other_ownership_proof`, `owner_property_paper`, `owner_sevrage_bill`, `owner_electricity_bill`, `sign_other_proof`, `sign_passport_proof`, `sign_passport__number`, `sign_pan_card_proof`, `sign_pan_card_number`, `bank_bank_account_number`, `bank_bank_name`, `income_other_proof`, `income_fard`, `income_bank_2_proof`, `income_bank_1_proof`, `income_bank_account_number`, `income_bank_name`, `income_computation_2_proof`, `income_computaion_1_proof`, `income_computation_number`, `income_itr_2_proof`, `income_itr_1_proof`, `income_itr_number`, `income_balancesheet_proof`, `income_pl_proof`, `income_form_3cb`, `income_form_3cd`, `resi_voter_card_proof`, `resi_voter_car_number`, `resi_passport_proof`, `resi_passport__number`, `resi_driving_license_proof`, `resi_driving_license__number`, `resi_rashan_id_proof`, `other_proof`, `proof_bank_proof`, `proof_bank_account_number`, `proof_arm_license_proof`, `proof_arm_license_number`, `proof_passport_proof`, `prrof_passport_number`, `proof_driving_license_proof`, `proof_driving_license_number`, `proof_adhar_card_proof`, `proof_adhar_card_number`, `proof_voter_card_proof`, `proof_voter_card_number`, `proof_pan_card_proof`, `proof_pan_card_number`, `address_te_bill`, `bankstatement_firm_bill`, `other_address_proof1`, `other_address_proof2`, `establishment_pancard_firm`, `establishment_registration_cerificate`, `establishment_service_tax`, `establishment_other_proofname1`, `establishment_other_proof1`, `establishment_other_proofname2`, `establishment_other_proof2`, `created_at`, `updated_at`) VALUES
(1, 1, '', '', '', '', '', '', '', 'uJ7il98a0v11403333_1014973431868537_3751998900916127048_n.jpg', 'pan123', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2LssdHrhtk11403333_1014973431868537_3751998900916127048_n.jpg', 'voter12341', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2LssdHrhtk11403333_1014973431868537_3751998900916127048_n.jpg', 'voter12341', 'uJ7il98a0v11403333_1014973431868537_3751998900916127048_n.jpg', 'pan123', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '5UZZTHy84odescimages.jpeg', 'wqe324fwr43rdf', '', '', '', '', '', '', '', '', '5UZZTHy84odescimages.jpeg', 'wqe324fwr43rdf', 'QsLzSLi7nSFunny-Quotes-11.jpg', 'adhar123123', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 'skwWG3lx9Oimages.jpeg', '', '', '', 'skwWG3lx9Oimages.jpeg', '', '', '', '', '', '', 'skwWG3lx9Oimages.jpeg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'yL4HzBPzDWholi-color-throw-berlin.jpg', 'skwWG3lx9Oimages.jpeg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, '', '', '', '', '', '', '', 'w2w2HtOoHuFunny-Quotes-11.jpg', '555', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AZBDcJ8tSdFunny-Quotes-11.jpg', '55555555555', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AZBDcJ8tSdFunny-Quotes-11.jpg', '55555555555', 'w2w2HtOoHuFunny-Quotes-11.jpg', '555', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TdP2k3XK1pJellyfish.jpg', '', '', 'iZztcuLMpIDesert.jpg', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `references`
--

CREATE TABLE IF NOT EXISTS `references` (
`id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `update` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address_street` varchar(255) NOT NULL,
  `city_id` int(255) NOT NULL,
  `district_id` varchar(255) NOT NULL,
  `state_id` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `alternate_mobile` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `std_code` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `references`
--

INSERT INTO `references` (`id`, `lead_id`, `update`, `name`, `address_street`, `city_id`, `district_id`, `state_id`, `mobile`, `phone`, `alternate_mobile`, `pincode`, `std_code`, `email`, `relation`, `created_at`, `updated_at`) VALUES
(22, 2, '', 'rrrrr', '', 0, '', '', '', '', '', '', '', '', 'rrrr', '2015-07-13 09:01:59', '2015-07-13 09:01:59'),
(23, 3, '', 'reference', '', 1, 'fdgdfg', 'punjab', '', '', '', '', '', '', 'relation', '2015-07-14 05:13:11', '2015-07-14 05:13:11'),
(24, 4, '', 'hnjn', '', 0, '', '', '', '', '', '', '', '', 'njhnn', '2015-07-14 07:38:16', '2015-07-14 07:38:16'),
(25, 5, '', '', '', 0, '', '', '', '', '', '', '', '', '', '2015-07-14 07:46:29', '2015-07-14 07:46:29'),
(26, 6, '', '', '', 0, '', '', '', '', '', '', '', '', '', '2015-07-29 04:58:24', '2015-07-29 04:58:24'),
(27, 7, '', '', '', 0, '', '', '', '', '', '', '', '', '', '2015-07-31 05:45:04', '2015-07-31 05:45:04'),
(28, 8, '', '', '', 0, '', '', '', '', '', '', '', '', '', '2015-07-31 10:13:53', '2015-07-31 10:13:53'),
(29, 9, '', '', '', 0, '', '', '', '', '', '', '', '', '', '2015-08-07 10:29:06', '2015-08-07 10:29:06'),
(30, 9, '', '', '', 0, '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE IF NOT EXISTS `role_permission` (
`id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`id`, `role_id`, `permission_id`) VALUES
(2, 4, 2),
(4, 4, 4),
(5, 2, 2),
(6, 2, 3),
(7, 2, 4),
(8, 4, 5),
(9, 4, 3),
(10, 2, 1),
(11, 2, 6),
(13, 3, 7),
(14, 4, 6),
(15, 4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `search_filter`
--

CREATE TABLE IF NOT EXISTS `search_filter` (
`id` int(255) NOT NULL,
  `lead_id` int(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
`id` int(11) NOT NULL,
  `state` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `status`, `created_at`, `updated_at`) VALUES
(1, 'punjab', 1, '2015-05-22 00:00:00', '2015-05-22 00:00:00'),
(2, 'assamsssss', 1, '2015-05-27 12:26:16', '2015-05-27 12:26:16'),
(3, 'jlkjl', 1, '2015-06-05 09:40:25', '2015-06-05 09:40:25');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_customers`
--

CREATE TABLE IF NOT EXISTS `temporary_customers` (
`id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `decline` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temporary_customers`
--

INSERT INTO `temporary_customers` (`id`, `lead_id`, `decline`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2015-07-13 10:21:34', '2015-07-13 11:10:39'),
(2, 2, 1, '2015-07-13 12:25:37', '2015-07-13 12:25:37'),
(3, 3, 1, '2015-07-14 06:28:30', '2015-07-14 06:28:30'),
(4, 4, 1, '2015-07-30 06:34:57', '2015-09-28 04:58:02'),
(5, 9, 1, '2015-08-07 11:30:07', '2015-08-07 11:30:07');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_leads`
--

CREATE TABLE IF NOT EXISTS `temporary_leads` (
`id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `applied_loan_amount` varchar(255) NOT NULL,
  `approve_loan_amount` varchar(255) NOT NULL,
  `los_number` varchar(255) NOT NULL,
  `special_condition` varchar(255) NOT NULL,
  `decline` int(11) NOT NULL DEFAULT '1',
  `finally_save` int(11) NOT NULL DEFAULT '0',
  `rc` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temporary_leads`
--

INSERT INTO `temporary_leads` (`id`, `lead_id`, `applied_loan_amount`, `approve_loan_amount`, `los_number`, `special_condition`, `decline`, `finally_save`, `rc`, `created_at`, `updated_at`) VALUES
(1, 1, '2300000', '230000', 'Gd4365fhg', 'test', 1, 1, '', '0000-00-00 00:00:00', '2015-08-01 07:36:53'),
(2, 2, '100000', '10000', '24rf5r43f34', 'test condition', 1, 1, '', '0000-00-00 00:00:00', '2015-08-01 07:36:51'),
(3, 3, '45409000', '45464600', 'd67g8dgy78gwse7', 'this  is test', 1, 1, '', '0000-00-00 00:00:00', '2015-08-28 05:54:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_superuser` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL,
  `is_verified` int(11) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `phone1` varchar(255) NOT NULL,
  `permanent_address` varchar(255) NOT NULL,
  `temporary_address` varchar(255) NOT NULL,
  `additional_pic` varchar(255) NOT NULL,
  `agreement` varchar(255) NOT NULL,
  `id_proof1` varchar(255) NOT NULL,
  `id_proof2` varchar(255) NOT NULL,
  `id_proof3` varchar(255) NOT NULL,
  `id_proof4` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `deleted_by` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(255) NOT NULL,
  `remember_token` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `phone`, `password`, `is_superuser`, `branch_id`, `active`, `is_verified`, `profile_pic`, `parent_id`, `phone1`, `permanent_address`, `temporary_address`, `additional_pic`, `agreement`, `id_proof1`, `id_proof2`, `id_proof3`, `id_proof4`, `created_by`, `deleted_by`, `created_at`, `updated_at`, `updated_by`, `remember_token`) VALUES
(1, 'carloan@admin.com', 'manav', '', '123123124', '$2y$10$HxKvyrKJ8Z9VaFNKEYaWGO.SwFkAIu7rVn2bKtX0wlDQze8yWOpne', 8, 0, 1, 0, 'e46SEWJCJuimgo.jpg', 0, '', '', '', '', '', '', '', '', '', '0', '', '0000-00-00 00:00:00', '2015-06-02 09:31:49', '', 'W2ZgZRqNxPfdiqPmVcC9uiXRpKVZPAwhucXm0S8dF7W5acJG0fqDGnaw1XwV'),
(2, 'agent2@gmail.com', 'agent2', 'loan', '(098) 654-3875', '$2y$10$wO5kH8t3x42oBi2wzp5duuLZmOolokoQ8Nvv3.2ck5bq4LpwZByVS', 2, 83, 1, 0, '', 25, '', '', '', '', '', '', '', '', '', '8', '', '0000-00-00 00:00:00', '2015-09-29 06:51:04', '', '1y3KtLeffIWJU5ZQwPlNWEpShSY05cw00fJmtHFIRlrSwpPtkomTm1RdYIWl'),
(3, 'agent1@gmail.com', 'agent1', 'loan', '(097) 777-2343', '$2y$10$wO5kH8t3x42oBi2wzp5duuLZmOolokoQ8Nvv3.2ck5bq4LpwZByVS', 3, 83, 1, 0, '', 25, '', '', '', '', '', '', '', '', '', '8', '', '2015-04-17 14:27:32', '2015-09-29 07:27:48', '', 'JNv10DrV8YonNfmXYAQo2BvNuRbiPiKFIRR8PT8qx7UBdohT8xFuCCPPlcUG'),
(4, 'admin1@gmail.com', 'admin1', 'loan', '', '$2y$10$wO5kH8t3x42oBi2wzp5duuLZmOolokoQ8Nvv3.2ck5bq4LpwZByVS', 2, 31, 1, 0, '', 25, '', '', '', '', '', '', '', '', '', '8', '', '2015-04-17 14:27:32', '2015-04-13 06:42:05', '', 'JNv10DrV8YonNfmXYAQo2BvNuRbiPiKFIRR8PT8qx7UBdohT8xFuCCPPlcUG'),
(5, 'admin2@gmail.com', 'admin2', 'loan', '', '$2y$10$wO5kH8t3x42oBi2wzp5duuLZmOolokoQ8Nvv3.2ck5bq4LpwZByVS', 2, 2, 1, 0, '', 25, '', '', '', '', '', '', '', '', '', '8', '', '2015-04-17 14:27:32', '2015-04-13 06:42:05', '', 'JNv10DrV8YonNfmXYAQo2BvNuRbiPiKFIRR8PT8qx7UBdohT8xFuCCPPlcUG'),
(6, 'gopalkuamr', 'Gopal Kumar', 'Kumar', '124343534', '$2y$10$PSP2o6fQHEcGobTK/bWEaOkbNXW5ijd3cQiXHnHsYJFSCPefWkbDi', 2, 1, 1, 0, '', 25, '', '', '', '', '', '', '', '', '', '8', '', '2015-05-30 11:59:29', '2015-05-30 12:03:59', '', ''),
(7, 'kamal00923', 'kamaldeep', 'kaur', '(012) 345-6789', '$2y$10$KiniDiEMGLi4racbaGgYUOJ/ZvwLAtLj4u3NZBdHcZzoKGHutgwZa', 4, 31, 1, 0, '', 23, '', '', '', '', '', '', '', '', '', '8', '', '2015-06-15 05:39:08', '2015-06-15 05:43:58', '', ''),
(8, 'test1234', 'test', '123', '1234567', '$2y$10$wBXEi2r9XyfTkI1hGgwuf.G1KhDIYo/jkGq/12fmvMn9TpKOoZney', 3, 31, 1, 0, '', 25, '', '', '', '', '', '', '', '', '', '8', '', '2015-06-15 09:36:25', '2015-09-26 11:12:14', '', ''),
(9, 'Navneet sharma', 'Navneet', 'Sharma', '9417521709', '$2y$10$Rmf2IL1AeH76Mi5OKbN5fuBdXJw1aAbKIgvar4dXhCy8MQZiC6UPS', 2, 2, 1, 0, '', 25, '', '', '', '', '', '', '', '', '', '8', '', '2015-06-17 05:43:43', '2015-06-17 05:43:43', '', ''),
(22, 'gm@gmail.com', 'gm', 'carloan', '0987654321', '$2y$10$0v7KrbgrhgeEiDAdtZy0auanDd5KZf9IVf/j2hTQJJB0pZExus6tW', 6, 0, 1, 0, 'afSh7zrZ6rChrysanthemum.jpg', 0, '1234567890', 'Ludhiana', 'Ludhiana', 'vNUijpexk2Desert.jpg', 'vwq6D5uLaUHydrangeas.jpg', 'GGGxBPsW9YJellyfish.jpg', 'PbKLglOavwKoala.jpg', 'RRbjn43QrcLighthouse.jpg', 'traE0orb5GPenguins.jpg', '8', '', '2015-09-26 09:23:04', '2015-09-26 09:23:04', '', 'm3dVe2p8GkmN9EKZ51vCifq49VsTfy3OG7U1JEirJZg0UyexXkT0Vq5Mc3rr'),
(23, 'bm@gmail.com', 'bm-gm', 'carloan', '(098) 765-4321', '$2y$10$R8ee011diAEOCBWT.ascjuwuYo9pVy06A5thZ1MW8t2f797s.3o..', 5, 83, 1, 0, 'SGM3meIZteChrysanthemum.jpg', 0, '1234567890', 'Ludhiana', 'Ludhiana', 'tRvXVjSvgnDesert.jpg', 'UOmlYTLxnATulips.jpg', '0LtmbGEli8Chrysanthemum.jpg', '4OmacTXXHkKoala.jpg', 'SssnY9iNWJTulips.jpg', 'PGezrPG1vkLighthouse.jpg', '8', '', '2015-09-26 10:22:22', '2015-09-28 03:19:29', '', '6BCLDQ7aBui4NJloys0f1GF95zrbisCbQx8rSHq8P6P10cwmNzjXndieWONz'),
(24, 'admin@gmail.com', 'admin', 'carloan', '1098765432', '$2y$10$FKOpQq1PX81bLV/8gIWQoOYI/mohSYMK1JuEL9AcuIcaH6HgFNlEK', 7, 0, 0, 0, 'IFLLEUO7CuDesert.jpg', 0, '1234567890', 'Ludhiana', 'Ludhiana', 'PKp2sp7Yi7Jellyfish.jpg', 'VoXs4O5RqdKoala.jpg', '1v3Bwje3A7Hydrangeas.jpg', 'qxFQBFETkoChrysanthemum.jpg', '', '', '8', '', '2015-09-28 03:34:26', '2015-09-28 03:34:26', '', 'YBOgvXTWZ9ZKAvYc0iqZwYAb8t3mKKmjfbZEmm4pjrwRwjSFdpJ3PhNbbAvA'),
(25, 'tl@gmail.com', 'tl', 'carloan', '1234567890', '$2y$10$qCG45LOygSmvdsWj3Acq5.pXW8a4wREZBUFRbnsmHKSL6odbdfham', 4, 83, 1, 0, 'DO2NBYxopMChrysanthemum.jpg', 23, '0987654321', 'Ludhiana', 'Ludhiana', 'ziVOjJ8y6zJellyfish.jpg', 'zbdmlKdYZkKoala.jpg', 'xwM7k5SLwjJellyfish.jpg', 'NfgdJt3q9ZDesert.jpg', '', '', '8', '', '2015-09-28 03:39:45', '2015-09-28 03:39:45', '', 'h6SQPKQWAlGw9XR3bD6YdJR9a4CzmWFUNkGps3TTglfBj7DjNNHy7nyHCGib'),
(26, 'agent@gmail.com', 'agent', 'carloan', '1234567890', '$2y$10$/EuDUmt70vUyZ95kF32nFuGcYx6eajSzSQvRp2T2G6lJRWIEcz8LO', 2, 83, 1, 0, 'USy7Dim3pfChrysanthemum.jpg', 25, '0987654321', 'Ludhiana', 'Ludhiana', 'NGdvFIgknVDesert.jpg', 'jabt6bbOSvKoala.jpg', '6I8CbImfb6Jellyfish.jpg', 'Fm6OoFzBaxChrysanthemum.jpg', 'dn4z069MWgJellyfish.jpg', '', '8', '', '2015-09-28 03:47:16', '2015-09-29 07:58:08', '', '7lCwPWBhaMiqYnMQj34CpBY4XImDDBEcvjGhNiPLYWZR7ps5NlquvV0S7012'),
(27, 'ca@gmail.com', 'ca', 'carloan', '0987654321', '$2y$10$4H6iOQdPYuJWG8.yCgl.GOJ/.v4uNoEy8N6KB56SGobQdF6t90cJC', 3, 83, 1, 0, 'ooz8Z24AvWJellyfish.jpg', 25, '1234567890', 'Ludhiana', 'Ludhiana', '', 'oZB64FQjyQKoala.jpg', 'dnS5hleKMhTulips.jpg', 'PgFpjQ8e99Desert.jpg', 'VrzQibXVpqJellyfish.jpg', '', '8', '', '2015-09-28 03:55:20', '2015-09-28 03:55:20', '', '0g740IQx1OHser9XAzJMr1rjyHXtbh4fY2sHHvX21ZAmkAkDVNqyt5UiPzwU'),
(28, 'catest@gmail.com', 'catest', 'carloan', '1243545656756', '$2y$10$RWIEW7LfLwcYj0Mdkl3BCecrG5E0IjVDHeRjH0CA/UERJh/VsP6R6', 3, 83, 1, 1, 'E0OtyNwk9LChrysanthemum.jpg', 25, '909087654', 'Ludhiana', 'Ludhiana', '', 'fIFV3yUtEaHydrangeas.jpg', 'pmfSxb1FWVKoala.jpg', 'rMuyKq93soPenguins.jpg', '', '', '27', '', '2015-09-29 08:15:58', '2015-09-29 08:15:58', '', ''),
(29, 'agenttest@gmail.com', 'testagent', 'carloan', '(098) 765-4321', '$2y$10$74MeB79BicHzLCRW.j8toO4TtNET55QsRTl2qbyGaAbBR08c1P1/S', 2, 83, 0, 0, 'dErsEjaCL2Chrysanthemum.jpg', 25, '1234567889', 'Ludhiana', 'Ludhiana', 'qdV0c122ONDesert.jpg', 'JhKm9W3fsAHydrangeas.jpg', '1IEitBcMoAJellyfish.jpg', 'g3KXwpVH2OKoala.jpg', '', '', '26', '', '2015-09-30 03:48:34', '2015-09-30 03:49:35', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE IF NOT EXISTS `vehicles` (
`id` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `make` varchar(255) NOT NULL,
  `variant` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `segment` varchar(255) NOT NULL,
  `cc` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `model`, `make`, `variant`, `category`, `segment`, `cc`, `status`, `created_at`, `updated_at`) VALUES
(1, 'model', 'make', 'variant', 'Tier-1', 'segment', 'cc', 0, '2015-05-07 09:35:57', '2015-05-07 09:35:57'),
(2, 'CITY', 'HONDA', 'VX MT', 'Tier-1', 'test segment', 'test vehicle cc', 1, '2015-05-14 11:11:59', '2015-05-14 11:11:59'),
(3, 'verna', 'hyundai', 'sx', 'Tier-1', 'A3', '1500', 1, '2015-06-17 09:42:58', '2015-06-17 09:42:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_details`
--
ALTER TABLE `asset_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_branches`
--
ALTER TABLE `bank_branches`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coapplicant_proofs`
--
ALTER TABLE `coapplicant_proofs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `co_applicants`
--
ALTER TABLE `co_applicants`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_tasks`
--
ALTER TABLE `daily_tasks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dealers`
--
ALTER TABLE `dealers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dealer_branches`
--
ALTER TABLE `dealer_branches`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `declines`
--
ALTER TABLE `declines`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `finacial_details`
--
ALTER TABLE `finacial_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fi_initiations`
--
ALTER TABLE `fi_initiations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance`
--
ALTER TABLE `insurance`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_branches`
--
ALTER TABLE `insurance_branches`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_superusers`
--
ALTER TABLE `is_superusers`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_types`
--
ALTER TABLE `lead_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
 ADD PRIMARY KEY (`module_id`), ADD UNIQUE KEY `module_id` (`module_id`);

--
-- Indexes for table `module_roles`
--
ALTER TABLE `module_roles`
 ADD PRIMARY KEY (`module_role_id`), ADD KEY `fk_module_roles_roles1_idx` (`role_id`), ADD KEY `fk_module_roles_modules1_idx` (`module_id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proofs`
--
ALTER TABLE `proofs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `references`
--
ALTER TABLE `references`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `search_filter`
--
ALTER TABLE `search_filter`
 ADD PRIMARY KEY (`id`) COMMENT 'id';

--
-- Indexes for table `states`
--
ALTER TABLE `states`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporary_customers`
--
ALTER TABLE `temporary_customers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporary_leads`
--
ALTER TABLE `temporary_leads`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_details`
--
ALTER TABLE `asset_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `bank_branches`
--
ALTER TABLE `bank_branches`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=269;
--
-- AUTO_INCREMENT for table `coapplicant_proofs`
--
ALTER TABLE `coapplicant_proofs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `co_applicants`
--
ALTER TABLE `co_applicants`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `daily_tasks`
--
ALTER TABLE `daily_tasks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dealers`
--
ALTER TABLE `dealers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `dealer_branches`
--
ALTER TABLE `dealer_branches`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `declines`
--
ALTER TABLE `declines`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `finacial_details`
--
ALTER TABLE `finacial_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `fi_initiations`
--
ALTER TABLE `fi_initiations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `insurance`
--
ALTER TABLE `insurance`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `insurance_branches`
--
ALTER TABLE `insurance_branches`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `is_superusers`
--
ALTER TABLE `is_superusers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `lead_types`
--
ALTER TABLE `lead_types`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
MODIFY `module_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `module_roles`
--
ALTER TABLE `module_roles`
MODIFY `module_role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `proofs`
--
ALTER TABLE `proofs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `references`
--
ALTER TABLE `references`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `search_filter`
--
ALTER TABLE `search_filter`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `temporary_customers`
--
ALTER TABLE `temporary_customers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `temporary_leads`
--
ALTER TABLE `temporary_leads`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
